
# app/control.py
# 
import flask
from flask.helpers import make_response
import app
import os
from re import search
from typing import Awaitable

from flask import Flask, Blueprint, url_for
from flask import request, redirect, render_template, session
from flask import jsonify
from flask.wrappers import Response
from flask_wtf import form
from sqlalchemy.sql import text,func
from sqlalchemy.types import String
from sqlalchemy import or_ , and_
from sqlalchemy import Date, cast
from sqlalchemy.sql.sqltypes import DateTime , Integer
from sqlalchemy.sql.expression import asc, desc, false, insert, true
import sys
import os
from sqlalchemy.sql.expression import false, insert, true

from sqlalchemy.sql.functions import count, random
sys.path.append('.')

from modelpool.models import DeviceDb, DeviceDefectDb, GrowthDataDb,HouseDb, NoticeHistoryDb, NoticeUserHistoryDb, QuestionDb ,TechDb, db, Fcuser, Todo, EmployeesDb, NoticeDb, TestDb, FarmDb
from modelpool.models import AlarmHistoryDb,AlarmSettingSensor ,AlarmSettingGrowth ,AlarmSettingPest,AlarmSettingDisorder,DeviceOperationDb
from modelpool.models import SensorDataDb , ImageBasicDataDb , AiStatusDb , DisorderDataDb ,PestDataDb,LaonFarmDb
from .formspool.forms import FarmBasicForm, RegisterForm, LoginForm , MainFrame
from .formspool.forms import NoticeForm 

from api_v1 import api as api_v1
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import func,Column

from datetime import datetime , timedelta,date
import json
import random
import string
import ast
import pickle
import pandas as pd


main_control = Blueprint('main_control',__name__)
per_page_count = 10
home_url=""


## SSL검증용
@main_control.route('/.well-known/pki-validation/gsdv.txt', methods=['GET'])
def pki_validation():
    print("pki_validation !!!")  
    
    file_basedir = os.path.dirname(__file__)
    directory = "./static/pki-validation/gsdv.txt"
    directory_full_path= os.path.join(file_basedir, directory) 

    response = make_response(open(directory_full_path).read())
    response.headers["Content-type"] = "text/plain"
    return response

'''
@main_control.route('/.well-known/<s1>/<s2>',methods=['GET'])
def well_known(s1,s2):
    print("well_known !!!")  
    return flask.send_file("/pki-validation/gsdv.txt")
'''
## 센서 데이터 인서트(파일&DB)
@main_control.route('/get_test',methods=['GET'])
def get_test():
    print("get_test !!!")  
    print("get test ok !!! ")

    
            
    return jsonify({"result":"ok"})


## 센서 데이터 인서트(파일&DB)
@main_control.route('/none_get_test')
def none_get_test():
    print("none_get_test !!!")  
    print("gnone_get_test ok !!! ")
            
    return jsonify({"result":"ok"})


@main_control.route('/upload_image', methods=['GET','POST'])
def upload_image():
    print("do upload_image()")

    return render_template('/test/upload_image.html')


@main_control.route('/upload_image_to_db', methods=['GET','POST'])
def upload_image_to_db():
    print("do upload_image_to_db()")
    
    ###################################################################
    ##file = request.files['file']
    file = request.files.get('file')
    file_full_path_name=""
    if file and file.filename != '':
        file_basedir = os.path.dirname(__file__)
        directory = "./static/test/"
        directory_full_path= os.path.join(file_basedir, directory) 
        print("directory_full_path  : "  + str(directory_full_path))  
        if not os.path.exists(directory_full_path):
            os.makedirs(directory_full_path)
        file_full_path_name = directory_full_path + file.filename
        file.save(file_full_path_name)
        # CVS Column Names
        col_names = ['device_id','farm_id','greenHouseID', 'targetDate', 'sequenceID' , 'roundID','image_path']
        # Use Pandas to parse the CSV file
        csvData = pd.read_csv(file_full_path_name,names=col_names, header=None)
        # Loop through the Rows
        for i,row in csvData.iterrows():
                if(i > 0 ) :
                    print(i,row['device_id'],row['farm_id'],row['greenHouseID'],row['targetDate'],row['sequenceID'],row['roundID'],row['image_path'],)
                    device_id = row['device_id']
                    farm_id = row['farm_id']
                    greenHouseID = row['greenHouseID']
                    targetDate = row['targetDate']
                    sequenceID = row['sequenceID']
                    roundID = row['roundID']
                    image_file_name = row['image_path']
                    
                    image_path = "/" + str(farm_id) + "/" + str(greenHouseID) + "/" + str(targetDate)+ "/" + str(sequenceID)
                    file_full_path = image_path  + "/" + image_file_name
                    print("file_full_path : " + file_full_path)
                    db.session.add(ImageBasicDataDb(
                            device_id = str(device_id),
                            farmID = str(farm_id),
                            greenHouseID = str(greenHouseID),
                            targetDate = str(targetDate),
                            sequenceID = str(sequenceID),
                            roundID = str(roundID),
                            image_path = str(file_full_path)
                        ))
                    device_id = db.Column(db.String(64))
                    db.session.commit()


    return render_template('/test/upload_image.html')





###################################################################
## login
###################################################################

@main_control.route('/privaeinfo', methods=['GET','POST'])
def privaeinfo():
    print("do privaeinfo()")

    return render_template('/useragreement/priviateinfo.html')


@main_control.route('/serviceagreement', methods=['GET','POST'])
def serviceagreement():
    print("do serviceagreement()")

    return render_template('/useragreement/serviceagreement.html')

@main_control.route('/login', methods=['GET','POST'])
def login():
    print("do login()")

    form = LoginForm()
    print(form.data.get('userid'))

    if form.validate_on_submit():
        session['userid'] = form.data.get('userid')
        print("session['userid'] = form.data.get('userid')")
        ##return redirect('/main_frame?contents=main')
        return redirect('/')

    print("no session['userid'] = form.data.get('userid')")
    return render_template('/login/authentication-login1.html', form=form)


@main_control.route('/logout', methods=['GET','POST'])
def logout():
    print("do logout()")

    session.pop('userid', None)
    return redirect('/')

@main_control.route('/regist', methods=['GET'])
def regist():
    print("do regist()")
    form = RegisterForm()
    return render_template('/login/authentication-register1.html', form=form)


@main_control.route('/regist_do', methods=['GET', 'POST'])
def regist_do():
    print("regist_do()")

    form = RegisterForm()

    if form.validate_on_submit():

        print("form.validate_on_submit()")
        fcuser = Fcuser()
        fcuser.userid = form.data.get('userid')
        fcuser.username = form.data.get('username')
        fcuser.password = form.data.get('password')
        fcuser.bod = form.data.get('bod')
        fcuser.email = form.data.get('email')
        fcuser.phone_number = form.data.get('phone_number')
        fcuser.role = str("0")

        db.session.add(fcuser)
        db.session.commit()

        print("sucess insert new user!")
        return redirect('/login')

    print(form.errors)
     
    print("not form.validate_on_submit()")
    return render_template('/login/authentication-register1.html', form=form)



@main_control.route('/find_id', methods=['GET','POST'])
def find_id():
    print("do find_id()")

    return render_template('/login/find_id.html')


@main_control.route('/get_user_id',methods=['GET','POST'])
def get_user_id():
    print("get_user_id")


    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')

    if request.method == 'POST' :

        username = request.form.get('username')
        print("user_name : "  + str(username))

        try:
            temp_user = Fcuser.query.filter(Fcuser.username ==username).first()
            print(temp_user);
            if not ( temp_user is None) :
                print("user is exgisit !")
                print("temp_user.userid : " + str(temp_user.userid))
                return jsonify({"result":temp_user.userid})
            else:
                print("user is not exgisit !")
                return jsonify({"result":"no"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204



##@main_control.route('/check_user/<userid>',methods=['GET','PUT','DELETE'])
##def check_user(userid):
##위 사용처럼 사용할 경우엔 보내는 ajax에서 반드시  url 스트링으로 파라매터를 구성해야함. "/check_user/실제유저아이디"
@main_control.route('/check_user',methods=['GET','PUT','DELETE'])
def check_user():
    print("check_user")

    print("reqeust querystring:" + str(request.query_string))

    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')

    if request.method == 'GET' :
        print("reqeust userid:" + request.args.get('userid'))

        userid = request.args.get('userid')

        try:
            temp_user = Fcuser.query.filter(Fcuser.userid ==userid).first()
            print(temp_user);
            if not ( temp_user is None) :
                if(temp_user.userid == userid):
                    print("user is exgisit !")
                    return jsonify({"result":"yes"})
                    ##return jsonify(user.serialize)
                else:
                    print("user is not exgisit1 !")
                    return jsonify({"result":"no"})
            else:
                print("user is not exgisit2 !")
                return jsonify({"result":"no"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204



###################################################################
### main
###################################################################

@main_control.route('/main', methods=['GET'])
def main():
    print("do main()")
    url_from = request.url 
    print("request.url  : " + str(request.url))

    '''
    if(url_from.find("3000") > -1 ):
        url_to = "mobile_control.mobile_home"
        return redirect(url_for(url_to) )

        ##url_to ="/mobile_home"
        ##return redirect(url_to )
    '''
        
    userid = session.get('userid', None)
    print(userid)

    temp_now = datetime.now()
    today_date_time = temp_now.strftime("%Y-%m-%d %H:%M:%S")
    print("today_date_time : " + str(today_date_time))
    
    today_date_time_h_m= temp_now.strftime("%Y-%m-%d %H:%M")
    print("today_date_time_h_m : " + str(today_date_time_h_m))
    
    today_date= temp_now.strftime("%Y-%m-%d")
    print("today_date : " + str(today_date))

    three_days_ago = datetime.today() - timedelta(days = 3)
    three_days_ago = three_days_ago.strftime("%Y-%m-%d %H:%M:%S")
    print("three_days_ago : " + str(three_days_ago))
   
    
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    db_session = Session()
    
    ################################################################################
    ## 1.  alarm
    ################################################################################
    #house_full_count= db.session.query(func.count(HouseDb.house_id)).scalar()
    house_full_count = db.session.query(func.count(AlarmHistoryDb.id)).\
                            filter(and_( AlarmHistoryDb.create_at >= today_date_time  )).scalar()
                            
    print("house_full_count : " + str(house_full_count))


    house_check_count= db.session.query(func.count(AlarmHistoryDb.id)).\
                            filter(and_( AlarmHistoryDb.create_at >= today_date_time , AlarmHistoryDb.check_yn == "확인" )).scalar()
    print("house_check_count : " + str(house_check_count))


    house_no_check_count= db.session.query(func.count(AlarmHistoryDb.id)).\
                            filter(and_( AlarmHistoryDb.create_at >= today_date_time , AlarmHistoryDb.check_yn == "미확인" )).scalar()
    print("house_no_check_count : " + str(house_no_check_count))

    house_3days_no_check_count= db.session.query(func.count(AlarmHistoryDb.id)).\
                            filter(and_( AlarmHistoryDb.create_at >= three_days_ago , AlarmHistoryDb.check_yn == "미확인" )).scalar()
    print("house_3days_no_check_count : " + str(house_3days_no_check_count))


    ################################################################################
    ## 2. device defect
    ################################################################################
    device_defect_cctv= db.session.query(func.count(DeviceDefectDb.id)).\
                            filter(and_(  DeviceDefectDb.proc_yn != "처리", DeviceDefectDb.category == "CCTV" )).scalar()
    print("device_defect_cctv : " + str(device_defect_cctv))

    device_defect_ena= db.session.query(func.count(DeviceDefectDb.id)).\
                            filter(and_(  DeviceDefectDb.proc_yn != "처리", DeviceDefectDb.category == "에나" )).scalar()
    print("device_defect_ena : " + str(device_defect_ena))

    device_defect_sensor= db.session.query(func.count(DeviceDefectDb.id)).\
                            filter(and_(  DeviceDefectDb.proc_yn != "처리", DeviceDefectDb.category == "센서" )).scalar()
    print("device_defect_sensor : " +str(device_defect_sensor))

    device_defect_control= db.session.query(func.count(DeviceDefectDb.id)).\
                            filter(and_(  DeviceDefectDb.proc_yn != "처리", DeviceDefectDb.category == "환경제어" )).scalar()
    print("device_defect_control : " + str(device_defect_control))


    ################################################################################
    ## 3. question
    ################################################################################
    question_all= db.session.query(func.count(QuestionDb.id)).\
                            filter(and_( QuestionDb.response_yn == "처리")).scalar()
    print("question_all : " + str(question_all))

    question_no_response= db.session.query(func.count(QuestionDb.id)).\
                            filter(and_(QuestionDb.response_yn == "미처리")).scalar()
    print("question_no_response : " + str(question_no_response))

    question_online= db.session.query(func.count(QuestionDb.id)).\
                            filter(and_(  QuestionDb.from_type == "online" )).scalar()
    print("question_online : " + str(question_online))

    question_offline= db.session.query(func.count(QuestionDb.id)).\
                            filter(and_(  QuestionDb.from_type == "offline" )).scalar()
    print("question_offline : " + str(question_offline))


    ################################################################################
    ## 4. 장비구매현황
    ################################################################################
    device_stock= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.status == "재고")).scalar()
    print("device_stock : " + str(device_stock))

    device_monthly= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.status == "월정액")).scalar()
    print("device_monthly : " + str(device_monthly))

    device_rent= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_(  DeviceDb.status == "대여" )).scalar()
    print("device_rent : " + str(device_rent))

    device_drop= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.status == "패기" )).scalar()
    print("device_drop : " + str(device_drop))
    
    
    device_purchase= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.status == "구매")).scalar()
    print("device_purchase : " + str(device_purchase))


    ################################################################################
    ## 4. 회원관리
    ################################################################################
    user_all= db.session.query(func.count(Fcuser.userid)).scalar()
    print("user_all : " + str(user_all))

    user_new= db.session.query(func.count(Fcuser.userid)).\
                            filter(and_( Fcuser.created_at >=  today_date_time )).scalar()
    print("user_new : " + str(user_new))



    ################################################################################
    ## 5. 장비설정변경
    ################################################################################
   
    device_operation= db.session.query(func.count(DeviceOperationDb.id)).\
                            filter(and_( DeviceOperationDb.checked == 'CHECKED' )).scalar()
    print("device_operation : " + str(device_operation))


    ################################################################################
    ## 6. 알람설정
    ################################################################################

    sensors= db.session.query(AlarmSettingSensor).all()
    sensor_cnt = 0
    for sensor in sensors:
        if(sensor.indoor_temp_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1
        if(sensor.indoor_humidity_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1
        if(sensor.indoor_co2_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1
        if(sensor.indoor_sunny_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1
        if(sensor.basic_temp_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1
        if(sensor.basic_humidity_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1
        if(sensor.basic_ec_alarm_issu_type != "0"):
            sensor_cnt = sensor_cnt + 1

    growths= db.session.query(AlarmSettingGrowth).all()
    growth_cnt = 0
    for growth in growths:
        if(growth.leaf_cnt_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.in_leaf_cnt_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.head_cnt_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.ilack_cnt_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.flower_cnt_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.fruit_cnt_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.fruit_size_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.in_leaf_length_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1
        if(growth.in_leaf_size_alarm_issu_type != "0"):
            growth_cnt = growth_cnt + 1



    pests= db.session.query(AlarmSettingPest).all()
    pest_cnt = 0
    for pest in pests:
        if(pest.ice_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1
        if(pest.aphid_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1
        if(pest.ash_mold_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1
        if(pest.white_mold_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1
        if(pest.plague_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1
        if(pest.anthrax_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1
        if(pest.etc_alarm_issu_type != "0"):
            pest_cnt = pest_cnt + 1

    disorders= db.session.query(AlarmSettingDisorder).all()
    disorder_cnt = 0
    for disorder in disorders:
        if(disorder.nitrogen_alarm_issu_type != "0"):
            disorder_cnt = disorder_cnt + 1
        if(disorder.phosphorus_alarm_issu_type != "0"):
            disorder_cnt = disorder_cnt + 1
        if(disorder.patassium_alarm_issu_type != "0"):
            disorder_cnt = disorder_cnt + 1
        if(disorder.magnetsium_alarm_issu_type != "0"):
            disorder_cnt = disorder_cnt + 1
        if(disorder.calcium_alarm_issu_type != "0"):
            disorder_cnt = disorder_cnt + 1
        if(disorder.etc_alarm_issu_type != "0"):
            disorder_cnt = disorder_cnt + 1

    alarm_setting_total_cnt = sensor_cnt + growth_cnt + pest_cnt +disorder_cnt
    print("alarm_setting_total_cnt : "  + str(alarm_setting_total_cnt))


    ################################################################################
    ## 7.  재고내역 / 판매내역
    ################################################################################

    ## 전체
    device_sensor_all= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("센서") )).scalar()
    print("device_sensor_all : " + str(device_sensor_all))

    device_ena_all= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("에나") )).scalar()
    print("device_ena_all : " + str(device_ena_all))

    device_cctv_all= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("CCTV") )).scalar()
    print("device_cctv_all : " + str(device_cctv_all))

    device_control_all= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("환경제어") )).scalar()
    print("device_control_all : " + str(device_control_all))

    device_etc_all= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("기타") )).scalar()
    print("device_etc_all : " + str(device_etc_all))

    ## 재고
    device_sensor_stock= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("센서") , DeviceDb.status == "재고"  )).scalar()
    print("device_sensor_stock2 : " + str(device_sensor_stock))

    device_ena_stock= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("에나") , DeviceDb.status == "재고" )).scalar()
    print("device_ena_stock : " + str(device_ena_stock))

    device_cctv_stock= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("CCTV") , DeviceDb.status == "재고"  )).scalar()
    print("device_cctv_stock : " + str(device_cctv_stock))

    device_control_stock= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("환경제어") , DeviceDb.status == "재고"  )).scalar()
    print("device_control_stock : " + str(device_control_stock))

    device_etc_stock= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("기타") , DeviceDb.status == "재고"  )).scalar()
    print("device_etc_stock : " + str(device_etc_stock))



    ## 월정액
    device_sensor_monthly= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("센서") , DeviceDb.status == "월정액"  )).scalar()
    print("device_sensor_monthly : " + str(device_sensor_monthly))

    device_ena_monthly= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("에나") , DeviceDb.status == "월정액" )).scalar()
    print("device_ena_monthly : " + str(device_ena_monthly))

    device_cctv_monthly= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("CCTV") ,DeviceDb.type.contains("월정액")  )).scalar()
    print("device_cctv_monthly2 : " + str(device_cctv_monthly))

    device_control_monthly= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("환경제어") , DeviceDb.status == "월정액"  )).scalar()
    print("device_control_monthly : " + str(device_control_monthly))

    device_etc_monthly= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("기타") , DeviceDb.status == "월정액"  )).scalar()
    print("device_etc_monthly : " + str(device_etc_monthly))
    
    
    ## 구매
    device_sensor_purchase= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("센서") , DeviceDb.status == "구매"  )).scalar()
    print("device_sensor_purchase : " + str(device_sensor_purchase))

    device_ena_purchase= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("에나") , DeviceDb.status == "구매" )).scalar()
    print("device_ena_purchase : " + str(device_ena_purchase))

    device_cctv_purchase= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("CCTV") ,DeviceDb.type.contains("구매")  )).scalar()
    print("device_cctv_purchase2 : " + str(device_cctv_purchase))

    device_control_purchase= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("환경제어") , DeviceDb.status == "구매"  )).scalar()
    print("device_control_purchase : " + str(device_control_purchase))

    device_etc_purchase= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("기타") , DeviceDb.status == "구매"  )).scalar()
    print("device_etc_purchase : " + str(device_etc_purchase))



    ## 대여
    device_sensor_rent= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("센서") , DeviceDb.status == "대여"  )).scalar()
    print("device_sensor_rent : " + str(device_sensor_rent))

    device_ena_rent= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("에나") , DeviceDb.status == "대여" )).scalar()
    print("device_ena_rent : " + str(device_ena_rent))

    device_cctv_rent= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("CCTV") , DeviceDb.status == "대여"  )).scalar()
    print("device_cctv_rent : " + str(device_cctv_rent))

    device_control_rent= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("환경제어") , DeviceDb.status == "대여"  )).scalar()
    print("device_control_rent : " + str(device_control_rent))

    device_etc_rent= db.session.query(func.count(DeviceDb.device_id)).\
                            filter(and_( DeviceDb.type.contains("기타") , DeviceDb.status == "대여"  )).scalar()
    print("device_etc_rent : " + str(device_etc_rent))
    
     ## human ai
    image_today_date_temp= temp_now.strftime("%Y%m%d")
    print("image_today_date_temp : " + str(image_today_date_temp))
    
    ##
    image_full_cnt=0
    image_full_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                                                                        scalar()
    print("image_full_cnt : " + str(image_full_cnt))
    
    image_full_today_cnt=0
    image_full_today_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.targetDate==image_today_date_temp)).scalar()
    print("image_full_today_cnt : " + str(image_full_today_cnt))
    ##
    image_decide_cnt=0
    image_decide_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.check_defect_manul =="true" )).\
                            scalar()
    print("image_decide_cnt : " + str(image_decide_cnt))
    
    image_decide_today_cnt=0
    image_decide_today_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.targetDate==image_today_date_temp,ImageBasicDataDb.check_defect_manul =="true" )).\
                            scalar()
    print("image_decide_today_cnt : " + str(image_decide_today_cnt))
    ##
    image_class_cnt=0
    image_class_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.plague_type !="" )).\
                            scalar()
    print("image_class_cnt : " + str(image_class_cnt))
    
    image_class_today_cnt=0
    image_class_today_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.targetDate==image_today_date_temp,ImageBasicDataDb.plague_type !="" )).\
                            scalar()
    print("image_class_today_cnt : " + str(image_class_today_cnt))
    ##
    image_progress_cnt=0
    image_progress_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.plague_level !="" )).\
                            scalar()
    print("image_progress_cnt : " + str(image_progress_cnt))
    
    image_progress_today_cnt=0
    image_progress_today_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.targetDate==image_today_date_temp,ImageBasicDataDb.plague_level !="" )).\
                            scalar()
    print("image_progress_today_cnt : " + str(image_progress_today_cnt))
    
    ##
    image_etc_cnt=0
    image_etc_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.plague_level !="판별불가")).\
                            scalar()
    print("image_etc_cnt : " + str(image_etc_cnt))
    
    image_etc_today_cnt=0
    image_etc_today_cnt= db.session.query(func.count(ImageBasicDataDb.id)).\
                            filter(and_(ImageBasicDataDb.targetDate==image_today_date_temp,ImageBasicDataDb.plague_level !="판별불가" )).\
                            scalar()
    print("image_etc_today_cnt : " + str(image_etc_today_cnt))
    

    return render_template('/main/main.html', userid=userid,today_date=today_date,\
                                            house_full_count = house_full_count,\
                                            house_check_count = house_check_count,\
                                            house_no_check_count = house_no_check_count ,\
                                            house_3days_no_check_count = house_3days_no_check_count ,\
                                            device_defect_cctv = device_defect_cctv ,\
                                            device_defect_ena = device_defect_ena ,\
                                            device_defect_sensor = device_defect_sensor ,\
                                            device_defect_control = device_defect_control ,\
                                            question_all = question_all ,\
                                            question_no_response = question_no_response,\
                                            question_online = question_online ,\
                                            question_offline = question_offline ,\
                                            device_stock = device_stock ,\
                                            device_monthly = device_monthly ,\
                                            device_purchase = device_purchase ,\
                                            device_rent = device_rent ,\
                                            device_drop = device_drop ,\
                                            user_all = user_all ,\
                                            user_new = user_new ,\
                                            device_operation = device_operation ,\
                                            alarm_setting_total_cnt = alarm_setting_total_cnt ,\
                                            device_sensor_all = device_sensor_all,\
                                            device_ena_all = device_ena_all ,\
                                            device_cctv_all = device_cctv_all ,\
                                            device_control_all = device_control_all ,\
                                            device_etc_all = device_etc_all ,\
                                            device_sensor_stock = device_sensor_stock ,\
                                            device_ena_stock = device_ena_stock ,\
                                            device_cctv_stock = device_cctv_stock ,\
                                            device_control_stock = device_control_stock ,\
                                            device_etc_stock = device_etc_stock ,\
                                                
                                            device_sensor_monthly = device_sensor_monthly ,\
                                            device_ena_monthly = device_ena_monthly ,\
                                            device_cctv_monthly = device_cctv_monthly ,\
                                            device_control_monthly = device_control_monthly ,\
                                            device_etc_monthly = device_etc_monthly ,\
                                                
                                                
                                            device_sensor_purchase = device_sensor_purchase ,\
                                            device_ena_purchase = device_ena_purchase ,\
                                            device_cctv_purchase = device_cctv_purchase ,\
                                            device_control_purchase = device_control_purchase ,\
                                            device_etc_purchase = device_etc_purchase ,\
                                                
                                            device_sensor_rent = device_sensor_rent ,\
                                            device_ena_rent = device_ena_rent  ,\
                                            device_cctv_rent = device_cctv_rent ,\
                                            device_control_rent = device_control_rent ,\
                                            device_etc_rent = device_etc_rent ,\
                                                
                                            image_full_cnt = str(image_full_cnt) ,\
                                            image_full_today_cnt = str(image_full_today_cnt) ,\
                                                
                                            image_decide_cnt = str(image_decide_cnt) ,\
                                            image_decide_today_cnt = str(image_decide_today_cnt) ,\
                                                
                                            image_class_cnt = str(image_class_cnt) ,\
                                            image_class_today_cnt = str(image_class_today_cnt) ,\
                                                
                                            image_progress_cnt = str(image_progress_cnt) ,\
                                            image_progress_today_cnt = str(image_progress_today_cnt) ,\
                                                
                                            image_etc_cnt = str(image_etc_cnt) ,\
                                            image_etc_today_cnt = str(image_etc_today_cnt) \
                                                
                                                )



@main_control.route('/main_get_alarm_count', methods=['GET','POST'])
def main_get_alarm_count():
    print("do main_get_alarm_count()")
    url_from = request.url 
    print("request.url  : " + str(request.url))
    
    try:
        search_type = request.form['search_type']
        print(search_type)
        userid = session.get('userid', None)
        print(userid)
        
        temp_now = datetime.now()
        today_date_time = temp_now.strftime("%Y-%m-%d %H:%M:%S")
        print("today_date_time : " + str(today_date_time))
        today_date= temp_now.strftime("%Y-%m-%d")
        print("today_date : " + str(today_date))

        three_days_ago = datetime.today() - timedelta(days = 3)
        three_days_ago = three_days_ago.strftime("%Y-%m-%d %H:%M:%S")
        print("three_days_ago : " + str(three_days_ago))
    
        if(search_type=="full"):
            alarm_list= db.session.query(AlarmHistoryDb)\
                                                .filter(and_( AlarmHistoryDb.create_at >= today_date_time  )) \
                                                .all()         
        elif(search_type=="confirm"):
            alarm_list= db.session.query(AlarmHistoryDb)\
                                                .filter(and_( AlarmHistoryDb.create_at >= today_date_time , AlarmHistoryDb.check_yn == "확인" ))\
                                                .all()
        elif(search_type=="not_confirm"):
            alarm_list= db.session.query(AlarmHistoryDb)\
                                                .filter(and_( AlarmHistoryDb.create_at >= today_date_time , AlarmHistoryDb.check_yn == "미확인" ))\
                                                .all()
        elif(search_type=="treedays_not_confirm"):
            alarm_list= db.session.query(AlarmHistoryDb)\
                                                .filter(and_( AlarmHistoryDb.create_at >= three_days_ago , AlarmHistoryDb.check_yn == "미확인" ))\
                                                .all()
        full_count = 0
        sensor_count=0
        sanyuk_count=0
        byung_count=0
        sangri_count=0
        
        indoor_temp_min=0
        indoor_temp_max=0
        indoor_humidity_min=0
        indoor_humidity_max=0
        indoor_co2_min=0
        indoor_co2_max=0
        indoor_sunny_min=0
        indoor_sunny_max=0
        basic_temp_min=0
        basic_temp_max=0
        basic_humidity_min=0
        basic_humidity_max=0
        basic_ec_min=0
        basic_ec_max=0
        
        leaf_cnt_min=0
        leaf_cnt_max=0
        in_leaf_cnt_min=0
        in_leaf_cnt_max=0
        head_cnt_min=0
        head_cnt_max=0
        ilack_cnt_min=0
        ilack_cnt_max=0
        flower_cnt_min=0
        flower_cnt_max=0
        fruit_cnt_min=0
        fruit_cnt_max=0
        fruit_size_min=0
        fruit_size_max=0
        in_leaf_length_min=0
        in_leaf_length_max=0
        in_leaf_size_min=0
        in_leaf_size_max=0
        
        ice_min=0
        ice_max=0
        aphid_min=0
        aphid_max=0
        ash_mold_min=0
        ash_mold_max=0
        white_mold_min=0
        white_mold_max=0
        white_mold_alarm_issu_type=0
        withered_disease_min=0
        plague_min=0
        anthrax_min=0
        etc_min=0
    

        nitrogen_min=0
        phosphorus_min=0
        patassium_min=0
        magnetsium_min=0
        calcium_min=0
        etc_min=0
        
        print("alarm_list : " + str(alarm_list))
        print("alarm_list length : " + str(len(alarm_list)))
        
        if(alarm_list is not None):
            for alarm_item in alarm_list:
                full_count=full_count+1
               
                if(alarm_item.category=="센서"):
                    sensor_count = sensor_count + 1
                if(alarm_item.category=="생육"):
                    sanyuk_count = sanyuk_count + 1
                if(alarm_item.category=="병혜충"):
                    byung_count = byung_count + 1
                if(alarm_item.category=="생리장애"):
                    sangri_count = sangri_count + 1
            

                ##센서
                if(alarm_item.alarm_contents=="실내온도미만"):
                    indoor_temp_min=indoor_temp_min+1
                if(alarm_item.alarm_contents=="실내온도초과"):
                    indoor_temp_max=indoor_temp_max+1
                if(alarm_item.alarm_contents=="상대습도미만"):
                    indoor_humidity_min=indoor_humidity_min+1
                if(alarm_item.alarm_contents=="상대습도초과"):
                    indoor_humidity_max=indoor_humidity_max+1
                if(alarm_item.alarm_contents=="CO2농도미만"):
                    indoor_co2_min=indoor_co2_min+1
                if(alarm_item.alarm_contents=="CO2농도초과"):
                    indoor_co2_max=indoor_co2_max+1
                if(alarm_item.alarm_contents=="내부광량미만"):
                    indoor_sunny_min=indoor_sunny_min+1
                if(alarm_item.alarm_contents=="내부광량초과"):
                    indoor_sunny_max=indoor_sunny_max+1
                if(alarm_item.alarm_contents=="근원부온도미만"):
                    basic_temp_min=basic_temp_min+1
                if(alarm_item.alarm_contents=="근원부온도초과"):
                    basic_temp_max=basic_temp_max+1
                if(alarm_item.alarm_contents=="근원부EC미만"):
                    basic_humidity_min=basic_humidity_min+1
                if(alarm_item.alarm_contents=="근원부EC초과"):
                    basic_humidity_max=basic_humidity_max+1
                if(alarm_item.alarm_contents=="근원부습도미만"):
                    basic_ec_min=basic_ec_min+1
                if(alarm_item.alarm_contents=="근원부습도초과"):
                    basic_ec_max=basic_ec_max+1
                    
                ##생육
                if(alarm_item.alarm_contents=="잎개수미만"):
                    leaf_cnt_min=leaf_cnt_min+1
                if(alarm_item.alarm_contents=="잎개수초과"):
                    leaf_cnt_max=leaf_cnt_max+1
                if(alarm_item.alarm_contents=="속잎개수미만"):
                    in_leaf_cnt_min=in_leaf_cnt_min+1
                if(alarm_item.alarm_contents=="속잎개수초과"):
                    in_leaf_cnt_max=in_leaf_cnt_max+1
                if(alarm_item.alarm_contents=="뇌두개수미만"):
                    head_cnt_min=head_cnt_min+1
                if(alarm_item.alarm_contents=="뇌두개수초과"):
                    head_cnt_max=head_cnt_max+1
                if(alarm_item.alarm_contents=="일액개수미만"):
                    ilack_cnt_min=ilack_cnt_min+1
                if(alarm_item.alarm_contents=="일액개수초과"):
                    ilack_cnt_max=ilack_cnt_max+1
                if(alarm_item.alarm_contents=="꽃개수미만"):
                    flower_cnt_min=flower_cnt_min+1
                if(alarm_item.alarm_contents=="꽃개수초과"):
                    flower_cnt_max=flower_cnt_max+1
                if(alarm_item.alarm_contents=="열매개수미만"):
                    fruit_cnt_min=fruit_cnt_min+1
                if(alarm_item.alarm_contents=="열매개수초과"):
                    fruit_cnt_max=fruit_cnt_max+1
                if(alarm_item.alarm_contents=="속잎길이미만"):
                    fruit_size_min=fruit_size_min+1
                if(alarm_item.alarm_contents=="속잎길이초과"):
                    fruit_size_max=fruit_size_max+1
                if(alarm_item.alarm_contents=="잎면적미만"):
                    in_leaf_length_min=in_leaf_length_min+1
                if(alarm_item.alarm_contents=="잎면적초과"):
                    in_leaf_length_max=in_leaf_length_max+1
                '''  
                if(alarm_item.alarm_contents=="잎개수초과"):
                    in_leaf_size_min=in_leaf_size_min+1
                if(alarm_item.alarm_contents=="잎개수초과"):
                    in_leaf_size_max=in_leaf_size_max+1
                '''
                
                ##병해충
                if(alarm_item.alarm_contents=="응애미만"):
                    ice_min=ice_min+1
                if(alarm_item.alarm_contents=="응애초과"):
                    ice_max=ice_max+1
                if(alarm_item.alarm_contents=="진딧물미만"):
                    aphid_min=aphid_min+1
                if(alarm_item.alarm_contents=="진딧물초과"):
                    aphid_max=aphid_max+1

                if(alarm_item.alarm_contents=="쟂빛곰팡이미만"):
                    white_mold_min=white_mold_min+1
                if(alarm_item.alarm_contents=="쟂빛곰팡이초과"):
                    white_mold_max=white_mold_max+1
                if(alarm_item.alarm_contents=="시들읆병 이상 단계 진입"):
                    withered_disease_min=withered_disease_min+1
                if(alarm_item.alarm_contents=="역병 이상 단계 진입"):
                    plague_min=plague_min+1
                    
                ##생리장애    
                if(alarm_item.alarm_contents=="질소"):
                    nitrogen_min=nitrogen_min+1
                if(alarm_item.alarm_contents=="인초"):
                    phosphorus_min=phosphorus_min+1
                if(alarm_item.alarm_contents=="칼륨"):
                    patassium_min=patassium_min+1
                if(alarm_item.alarm_contents=="마그네슘"):
                    magnetsium_min=magnetsium_min+1
                if(alarm_item.alarm_contents=="칼슘"):
                    calcium_min=calcium_min+1
                if(alarm_item.alarm_contents=="기타"):
                    etc_min=etc_min+1

        return_json={}  
        return_json.update({"full_count":full_count})
        return_json.update({"sensor_count":sensor_count})
        return_json.update({"sanyuk_count":sanyuk_count})
        return_json.update({"byung_count":byung_count})
        return_json.update({"sangri_count":sangri_count})
        return_json.update({"indoor_temp_min":indoor_temp_min})
        return_json.update({"indoor_temp_max":indoor_temp_max})
        return_json.update({"indoor_humidity_min":indoor_humidity_min})
        return_json.update({"indoor_humidity_max":indoor_humidity_max})
        return_json.update({"indoor_co2_min":indoor_co2_min})
        return_json.update({"indoor_co2_max":indoor_co2_max})
        return_json.update({"indoor_sunny_min":indoor_sunny_min})
        return_json.update({"indoor_sunny_max":indoor_sunny_max})
        return_json.update({"basic_temp_min":basic_temp_min})
        return_json.update({"basic_temp_max":basic_temp_max})
        return_json.update({"basic_humidity_min":basic_humidity_min})
        return_json.update({"basic_humidity_max":basic_humidity_max})
        return_json.update({"basic_ec_min":basic_ec_min})
        return_json.update({"basic_ec_max":basic_ec_max})  
        
        return_json.update({"leaf_cnt_min":leaf_cnt_min})  
        return_json.update({"leaf_cnt_max":leaf_cnt_max})  
        return_json.update({"in_leaf_cnt_min":in_leaf_cnt_min})  
        return_json.update({"in_leaf_cnt_max":in_leaf_cnt_max})  
        return_json.update({"head_cnt_min":head_cnt_min})  
        return_json.update({"head_cnt_max":head_cnt_max})  
        return_json.update({"ilack_cnt_min":ilack_cnt_min})  
        return_json.update({"ilack_cnt_max":ilack_cnt_max})  
        return_json.update({"flower_cnt_min":flower_cnt_min})  
        return_json.update({"flower_cnt_max":flower_cnt_max})  
        return_json.update({"fruit_cnt_min":fruit_cnt_min})  
        return_json.update({"fruit_cnt_max":fruit_cnt_max})  
        return_json.update({"fruit_size_min":fruit_size_min})  
        return_json.update({"fruit_size_max":fruit_size_max})  
        return_json.update({"in_leaf_length_min":in_leaf_length_min})  
        return_json.update({"in_leaf_length_max":in_leaf_length_max})  
        return_json.update({"in_leaf_size_min":in_leaf_size_min})  
        return_json.update({"in_leaf_size_max":in_leaf_size_max})  
        
        return_json.update({"ice_min":ice_min})  
        return_json.update({"ice_max":ice_max})  
        return_json.update({"aphid_min":aphid_min})  
        return_json.update({"aphid_max":aphid_max})  
        return_json.update({"white_mold_min":white_mold_min})  
        return_json.update({"white_mold_max":white_mold_max})  
        return_json.update({"withered_disease_min":withered_disease_min})  
        return_json.update({"plague_min":plague_min})  
        

        return_json.update({"nitrogen_min":nitrogen_min})  
        return_json.update({"phosphorus_min":phosphorus_min})  
        return_json.update({"patassium_min":patassium_min})  
        return_json.update({"magnetsium_min":magnetsium_min})  
        return_json.update({"calcium_min":calcium_min})  
        return_json.update({"etc_min":etc_min})  
        return jsonify({"result":return_json})
    
    except Exception as ex:
            print("except")
            print(ex)
            return jsonify({"result":"no"})
###################################################################
## farm
###################################################################
@main_control.route('/farm_list/<int:page_num>')
def farm_list(page_num):
    print("do farm_list() !!!")

    if request.method == 'GET' :
        try:
           
            farms = FarmDb.query.join(FarmDb, FarmDb.user_id==Fcuser.userid).\
                    add_columns(FarmDb.farm_id, FarmDb.user_id,FarmDb.farm_name,FarmDb.farm_crop,FarmDb.farm_kind,\
                                FarmDb.farm_address, func.ifnull(FarmDb.house_cnt,'0').label("house_cnt") ,\
                                func.ifnull(FarmDb.cctv_cnt,'0').label("cctv_cnt"),func.ifnull(FarmDb.sensor_cnt,'0').label("sensor_cnt"),\
                                    func.ifnull(FarmDb.ana_cnt,'0').label("ana_cnt"),func.ifnull(FarmDb.control_cnt,'0').label("control_cnt"),\
                                    FarmDb.user_id , FarmDb.user_name , \
                                    Fcuser.userid , FarmDb.user_tel).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)

            house_cnt_list = []
            sensor_cnt_list = []
            cctv_cnt_list = []
            ena_cnt_list = []
            control_cnt_list = []

            for farm in farms.items:

                print("farm_id : " + str(farm.farm_id))

                house_cnt = db.session.query(func.count(HouseDb.house_id)).filter(and_(HouseDb.farm_id == farm.farm_id)).scalar()
                print("house_cnt : " + str(house_cnt))
                house_cnt_list.append(house_cnt)

                sensor_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='센서')).scalar()
                print("sensor_cnt : " + str(sensor_cnt))
                sensor_cnt_list.append(sensor_cnt)

                cctv_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='CCTV')).scalar()
                print("cctv_cnt : " + str(cctv_cnt))
                cctv_cnt_list.append(cctv_cnt)

                ena_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='에나')).scalar()
                print("ena_cnt : " + str(ena_cnt))
                ena_cnt_list.append(ena_cnt)

                control_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='환경제어')).scalar()
                print("control_cnt : " + str(control_cnt))
                control_cnt_list.append(control_cnt)

            
            print(farms.items)

            print("aaa")

            url ="main_control.farm_list";
            
            return render_template('/farm/farm_list.html', farms=farms,house_cnt_list = house_cnt_list , house_cnt_list_cnt = len(house_cnt_list),\
                sensor_cnt_list = sensor_cnt_list,cctv_cnt_list = cctv_cnt_list,ena_cnt_list = ena_cnt_list,control_cnt_list = control_cnt_list,\
                url=url,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

'''
@main_control.route('/farm_list_search/<int:page_num>',methods=['GET','POST'])
def farm_list_search(page_num):

    print("farm_list_search()")

    if request.method == 'GET' :
        try:
            print("reqeust querystring:" + request.args.get("search_farm_keyword",""))
            search_text= str(request.args.get('search_farm_keyword', default = '', type = str) )

            print("search_text:" + search_text)

            farms = FarmDb.query.join(FarmDb, FarmDb.user_id==Fcuser.userid).\
                add_columns(FarmDb.farm_id, FarmDb.user_id,FarmDb.farm_crop,FarmDb.farm_kind,\
                            FarmDb.farm_address, func.ifnull(FarmDb.house_cnt,'0').label("house_cnt") ,\
                            func.ifnull(FarmDb.cctv_cnt,'0').label("cctv_cnt"),func.ifnull(FarmDb.sensor_cnt,'0').label("sensor_cnt"),\
                                func.ifnull(FarmDb.ana_cnt,'0').label("ana_cnt"),func.ifnull(FarmDb.control_cnt,'0').label("control_cnt"),\
                                FarmDb.user_id , FarmDb.user_name , \
                                Fcuser.userid , FarmDb.user_tel).\
                                    filter(or_(FarmDb.farm_crop.contains(search_text),FarmDb.farm_kind.contains(search_text)) ).\
                                    paginate(per_page=per_page_count, page=page_num, error_out=True)


            url = 'main_control.farm_list_search'
            return render_template('/farm/farm_list.html', farms=farms, url=url, search_farm_keyword=search_text,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204
'''

@main_control.route('/farm_list_search/<int:page_num>',methods=['GET','POST'])       
def farm_list_search(page_num):
    print("do farm_list() !!!")

    
    if request.method == 'GET' :
        try:
            print("reqeust querystring:" + request.args.get("search_farm_keyword",""))
            search_text= str(request.args.get('search_farm_keyword', default = '', type = str) )
            print("search_text:" + search_text)

            farms = FarmDb.query.join(FarmDb, FarmDb.user_id==Fcuser.userid).\
                    add_columns(FarmDb.farm_id, FarmDb.user_id,FarmDb.farm_name,FarmDb.farm_crop,FarmDb.farm_kind,\
                                FarmDb.farm_address, func.ifnull(FarmDb.house_cnt,'0').label("house_cnt") ,\
                                func.ifnull(FarmDb.cctv_cnt,'0').label("cctv_cnt"),func.ifnull(FarmDb.sensor_cnt,'0').label("sensor_cnt"),\
                                    func.ifnull(FarmDb.ana_cnt,'0').label("ana_cnt"),func.ifnull(FarmDb.control_cnt,'0').label("control_cnt"),\
                                    FarmDb.user_id , FarmDb.user_name , \
                                    Fcuser.userid , FarmDb.user_tel).\
                                    filter(or_(FarmDb.farm_id.contains(search_text),\
                                        FarmDb.user_id.contains(search_text),\
                                        FarmDb.farm_name.contains(search_text),\
                                        FarmDb.user_tel.contains(search_text),\
                                        FarmDb.farm_crop.contains(search_text),\
                                        FarmDb.farm_kind.contains(search_text)) ).\
                                    paginate(per_page=per_page_count, page=page_num, error_out=True)

            house_cnt_list = []
            sensor_cnt_list = []
            cctv_cnt_list = []
            ena_cnt_list = []
            control_cnt_list = []

            for farm in farms.items:

                print("farm_id : " + str(farm.farm_id))

                house_cnt = db.session.query(func.count(HouseDb.house_id)).filter(and_(HouseDb.farm_id == farm.farm_id)).scalar()
                print("house_cnt : " + str(house_cnt))
                house_cnt_list.append(house_cnt)

                sensor_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='센서')).scalar()
                print("sensor_cnt : " + str(sensor_cnt))
                sensor_cnt_list.append(sensor_cnt)

                cctv_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='CCTV')).scalar()
                print("cctv_cnt : " + str(cctv_cnt))
                cctv_cnt_list.append(cctv_cnt)

                ena_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='에나')).scalar()
                print("ena_cnt : " + str(ena_cnt))
                ena_cnt_list.append(ena_cnt)

                control_cnt = db.session.query(func.count(DeviceDb.device_id)).filter(and_(DeviceDb.farm_id == farm.farm_id ,DeviceDb.type=='환경제어')).scalar()
                print("control_cnt : " + str(control_cnt))
                control_cnt_list.append(control_cnt)

            
            print(farms.items)

            print("aaa")

            url ="main_control.farm_list";
            
            return render_template('/farm/farm_list.html', search_farm_keyword=search_text ,farms=farms,house_cnt_list = house_cnt_list , house_cnt_list_cnt = len(house_cnt_list),\
                sensor_cnt_list = sensor_cnt_list,cctv_cnt_list = cctv_cnt_list,ena_cnt_list = ena_cnt_list,control_cnt_list = control_cnt_list,\
                url=url,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204


@main_control.route('/farm_detail')
def farm_detail():
    print("do farm_detail() !!!")
    farm_id= request.args.get('farm_id', default = '', type = str) 
    page_num= request.args.get('page_num', default = '', type = str) 
    print("farm_id :"  + farm_id)

    return render_template('/farm/farm_detail.html',farm_id=farm_id,page_num=page_num)
   

@main_control.route('/get_farm_list')
def get_farm_list():
    print("do get_farm_list() !!!")
    

    farms= db.session.query(FarmDb).all()

    print(farms)
 
    print("====================================================================")
    
    result_json ={};
    farm_list_temp = []
    
    for farm in farms:
        ##print("farm_ : {}, farm_ : {} , farm : {}".format(farm_.FarmDb , farm_.HouseDb , farm_.DeviceDb)) 
        
        farm_json ={};
        farm_json.update({"farm_id" : farm.farm_id})
        farm_json.update({"farm_name" : farm.farm_name})

        farm_list_temp.append(farm_json)
        print(farm_list_temp);

    print("Final farm_list_temp : "  + str(farm_list_temp))    
    print("====================================================================")

    return jsonify({"result":farm_list_temp})
    

@main_control.route('/loading_farm_alarm_setting')
def loading_farm_alarm_setting():
    print("do loading_farm_alarm_setting() !!!")
    farmid= request.args.get('farmid', default = '', type = str) 
    print("farmid :"  + farmid)

    farms= db.session.query(FarmDb).filter(FarmDb.farm_id == farmid).all()
    print(farms)
 
    print("====================================================================")

    farm_json ={};
    
    for farm in farms:
        ##print("farm_ : {}, farm_ : {} , farm : {}".format(farm_.FarmDb , farm_.HouseDb , farm_.DeviceDb)) 
        farm_json.update({"farm_id" : farm.farm_id})
        farm_json.update({"alarm_setting" : farm.alarm_setting})

    print("Final farm_json : "  + str(farm_json))    
    print("====================================================================")

    return jsonify({"result":farm_json})

@main_control.route('/update_farm_alarm_setting')
def update_farm_alarm_setting():
    print("do update_farm_alarm_setting() !!!")
    farmid= request.args.get('farmid', default = '', type = str) 
    print("farmid :"  + farmid)
    alarm_setting= request.args.get('alarm_setting', default = '', type = str) 
    print("alarm_setting :"  + alarm_setting)

    upateQery = db.session.query(FarmDb)
    upateQery = upateQery.filter(FarmDb.farm_id==farmid)
    upateDeviceRecord = upateQery.one()
    upateDeviceRecord.alarm_setting = str(alarm_setting)
    db.session.commit()
    
    print("====================================================================")


    return jsonify({"result":"ok"})


@main_control.route('/farm_detail_farm_list')
def farm_detail_farm_list():
    print("do farm_detail_farm_list() !!!")

    farm_id= request.args.get('farm_id', default = '', type = str) 

    print("farm_id :"  + farm_id)
    
    '''
    farms = FarmDb.query.join(HouseDb,FarmDb.farm_id==HouseDb.farm_id).filter(FarmDb.farm_id==farm_id).\
                    add_columns(FarmDb.farm_id ,FarmDb.farm_name, HouseDb.house_id,HouseDb.house_name)
    '''
    ##fcuser = Fcuser.query.filter_by(userid=userid).first()


    farms= db.session.query(FarmDb, HouseDb).filter(FarmDb.farm_id == farm_id).\
                    join(FarmDb , FarmDb.farm_id == HouseDb.farm_id).all()

    print(farms)
 
    print("====================================================================")
    
    result_json ={};
    farm_json ={};
    temp_farm_id = ""
    farm_house_list_temp = []
    
    for farm in farms:
        ##print("farm_ : {}, farm_ : {} , farm : {}".format(farm_.FarmDb , farm_.HouseDb , farm_.DeviceDb)) 
        if(temp_farm_id != farm.FarmDb.farm_id):
          farm_json.update({"farm_id" : farm.FarmDb.farm_id})
          farm_json.update({"farm_name" : farm.FarmDb.farm_name})
        
        house_json ={};
        house_json.update({"house_id" : farm.HouseDb.house_id})
        print("house_id : "  + str(farm.HouseDb.house_id))
        house_json.update({"house_name" : farm.HouseDb.house_name})
        print("house_name : "  + str(farm.HouseDb.house_name))

        farm_house_list_temp.append(house_json)
        print(farm_house_list_temp);

        temp_farm_id = farm.FarmDb.farm_id

    farm_json.update({"houses" : farm_house_list_temp})

    print("Final farm_detail_farm_list : "  + str(farm_json))    
    print("====================================================================")

    return jsonify({"result":farm_json})
    

@main_control.route('/farm_detail_device_list')
def farm_detail_device_list():
    print("do farm_detail_device_list() !!!")

    house_id= request.args.get('house_id', default = '', type = str) 

    print("house_id :"  + house_id)
    
    '''
    farms = FarmDb.query.join(HouseDb,FarmDb.farm_id==HouseDb.farm_id).filter(FarmDb.farm_id==farm_id).\
                    add_columns(FarmDb.farm_id ,FarmDb.farm_name, HouseDb.house_id,HouseDb.house_name)
    '''
    ##fcuser = Fcuser.query.filter_by(userid=userid).first()

    
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    devices= db.session.query(HouseDb, DeviceDb).filter(house_id == DeviceDb.house_id).\
                    join(HouseDb , HouseDb.house_id == DeviceDb.house_id).all()
  
    print(devices)
 
    print("====================================================================")
    
  
    house_json ={};
    temp_house_id = ""
    farm_device_list_temp = []  
    cnt =0;
    for device in devices:
        cnt = cnt +1;
        if(temp_house_id != device.HouseDb.house_id):
          house_json.update({"house_id" : device.HouseDb.house_id})
          house_json.update({"house_name" : device.HouseDb.house_name})
        
        ### device 정보 생성
        device_json ={};
        device_json.update({"device_id" : device.DeviceDb.device_id})
        device_json.update({"type" : device.DeviceDb.type})
        device_json.update({"status" : device.DeviceDb.status})

        
        farm_device_list_temp.append(device_json)
        ## device 정보 생성 종료

        temp_house_id = device.HouseDb.house_id
    
    if devices is None:
        house_json.update({"house_id" : house_id})
    else:
        house_json.update({"house_id" : house_id})
        house_json.update({"devices" : farm_device_list_temp})
    

    print("Final farm_detail_device_list : "  + str(house_json))    
    print("====================================================================")
 
    #return jsonify({"result":"no"})
    return jsonify({"result":house_json})
    
    

@main_control.route('/farm_device_add_to_farm')
def farm_device_add_to_farm():
    print("do farm_device_add_to_farm() !!!")

    farm_id= request.args.get('farm_id', default = '', type = str) 
    house_id= request.args.get('house_id', default = '', type = str) 
    device_id= request.args.get('device_id', default = '', type = str) 

    print("farm_id :"  + farm_id)
    print("house_id :"  + house_id)
    print("device_id :"  + device_id)
    
    
    try:
        '''
        farms = FarmDb.query.join(HouseDb,FarmDb.farm_id==HouseDb.farm_id).filter(FarmDb.farm_id==farm_id).\
                        add_columns(FarmDb.farm_id ,FarmDb.farm_name, HouseDb.house_id,HouseDb.house_name)
        '''
        ##fcuser = Fcuser.query.filter_by(userid=userid).first()

        
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.farm_id = farm_id
        upateDeviceRecord.house_id = house_id
        db.session.commit()

        '''
        upateDevice = DeviceDb(device_id = 1,
                            toner_color = 'blue',
                            toner_hex = '#0F85FF')

        newToner2 = Toner(toner_id = 2,
                            toner_color = 'red',
                            toner_hex = '#F01731')

        dbsession.add_all([newToner1, newToner2])   
        '''
        
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
        
    
@main_control.route('/farm_device_to_add')
def farm_device_to_add():
    print("do farm_device_to_add() !!!")

    farm_id= request.args.get('farm_id', default = '', type = str) 

    print("farm_id :"  + farm_id)
    
    '''
    farms = FarmDb.query.join(HouseDb,FarmDb.farm_id==HouseDb.farm_id).filter(FarmDb.farm_id==farm_id).\
                    add_columns(FarmDb.farm_id ,FarmDb.farm_name, HouseDb.house_id,HouseDb.house_name)
    '''
    ##fcuser = Fcuser.query.filter_by(userid=userid).first()

    
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    ## house id가 미지정된 장치 목록을 조회한다.
 
    devices= db.session.query(FarmDb, DeviceDb).filter(or_(DeviceDb.house_id=="", DeviceDb.house_id.is_(None))).\
                    join(FarmDb , FarmDb.farm_id == DeviceDb.farm_id).all()

    
    '''
    SELECt  farm.farm_id , device.device_id
    from  farm , device
    where farm.farm_id = device.farm_id and ( device.house_id is null or device.house_id=="")
    '''
  

    print(devices)
 
    print("====================================================================")
    
  
    device_root_json ={};
    temp_house_id = ""
    device_list_temp = []

    for device in devices:
        ### device 정보 생성
        device_json ={};
        device_json.update({"device_id" : device.DeviceDb.device_id})
        device_json.update({"type" : device.DeviceDb.type})
        device_json.update({"status" : device.DeviceDb.status})

  
        device_list_temp.append(device_json)
        ## device 정보 생성 종료

    device_root_json.update({"devices" : device_list_temp})



    print("Final : "  + str(device_root_json))    
    print("====================================================================")
 
    #return jsonify({"result":"no"})
    return jsonify({"result":device_root_json})
    


@main_control.route('/farm_detail_backup')
def farm_detail_backup():
    print("do farm_detail() !!!")

    farm_id= request.args.get('farm_id', default = '', type = str) 

    print("farm_id :"  + farm_id)
    
    '''
    farms = FarmDb.query.join(HouseDb,FarmDb.farm_id==HouseDb.farm_id).filter(FarmDb.farm_id==farm_id).\
                    add_columns(FarmDb.farm_id ,FarmDb.farm_name, HouseDb.house_id,HouseDb.house_name)
    '''
    ##fcuser = Fcuser.query.filter_by(userid=userid).first()

    
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    '''
    for a, b in db.session.query(FarmDb, HouseDb).filter(FarmDb.farm_id == HouseDb.farm_id).all():
       print ("FarmDb.farm_id : {} , HouseDb.house_id{}".format(a.farm_id,b.house_id))
    

    farms= db.session.query(FarmDb, HouseDb).filter(FarmDb.farm_id == HouseDb.farm_id).all()
    print(farms)


    print("======================================")

    for farm in farms:
        print(farm.FarmDb.farm_id)

    ##for c, i in db.session.query(FarmDb, Fcuser).filter(FarmDb.owner_id == Fcuser.userid).all():

    '''
    farms= db.session.query(FarmDb, HouseDb, DeviceDb).filter(FarmDb.farm_id == HouseDb.farm_id).\
                    join(HouseDb , HouseDb.house_id == DeviceDb.house_id).all()
    print(farms)


    print("======================================")

    farm_list_root_temp=[]  
    for farm_ in farms:
        ##print("farm_ : {}, farm_ : {} , farm : {}".format(farm_.FarmDb , farm_.HouseDb , farm_.DeviceDb)) 

        farm_list_temp = []
        farm_list_temp.append(farm_.FarmDb.farm_id)
        farm_list_temp.append(farm_.FarmDb.farm_name)

        farm_house_list_temp=[]

        farm_house_list_temp.append(farm_.HouseDb.house_id)
        farm_house_list_temp.append(farm_.HouseDb.house_name)
        farm_list_temp.append(farm_house_list_temp)


        farm_device_list_temp=[]
        farm_device_list_temp.append(farm_.DeviceDb.device_id)
        farm_device_list_temp.append(farm_.DeviceDb.type)
        farm_device_list_temp.append(farm_.DeviceDb.status)
        farm_house_list_temp.append(farm_device_list_temp)


        farm_list_root_temp.append(farm_list_temp)

    print("Final : "  + str(farm_list_root_temp))    
    print("======================")

    ''''
    ## full list print
    for a in farm_list_root_temp :
        cnt = 0
        for b in a:
           print ("{},".format(b),end='')
           cnt=cnt+1
           if(cnt==3):   
               cnt2=0
               print('\n')
               for c in b:
                    print ("{},".format(c),end='')
                    cnt2=cnt2+1
                    if(cnt2==3):

                        print('\n')
                        for d in c:
                            print ("{},".format(d),end='')
                        cnt2=0
                        print('\n')
               cnt=0
    '''
    html_ = ""
    for a in farm_list_root_temp :
        cnt = 0
        html_ = html_ + "<tr>"
        for b in a:

           if(cnt < 2):
                html_ = html_ + "<td>"
                html_ = html_+format(b)
                html_ = html_ + "</td>"

           cnt=cnt+1
           if(cnt==3):   
               cnt2=0
               for c in b:
                    if(cnt2 < 2):
                        html_ = html_ + "<td>"
                        html_ = html_+format(c)
                        html_ = html_ + "</td>"
                    cnt2=cnt2+1
                    if(cnt2==3):

                        html_ = html_ + "<td>"   

                        html_ = html_ + "<table>"                     
                        html_ = html_ + "<tr>"  

                        for d in c:
                                             

                            html_ = html_ + "<td>"
                            html_ = html_+format(d)
                            html_ = html_ + "</td>"

                        cnt2=0
                                                   
                        html_ = html_ + "</tr>"
                        html_ = html_ + "</table>" 

                        html_ = html_ + "</td>"
               cnt=0
            
        html_ = html_ + "</tr>"
                
    print(html_)


    return render_template('/farm/farm_detail.html', farm_list_root_temp=farm_list_root_temp, html_=html_)
    
## 농장 기본 정보
@main_control.route('/farm_basic')
def farm_basic():
    print("do farm_basic() !!!")
    farm_id= request.args.get('farm_id', default = '', type = str) 
    page_num= request.args.get('page_num', default = '', type = str) 
    print("farm_id  : " , farm_id)
    print("page num  : " , page_num)


    ###############################
    ###############################

    return render_template('/farm/farm_basic.html', farm_id=farm_id,page_num=page_num)


@main_control.route('/farm_basic_loading_data')
def farm_basic_loading_data():
    print("do farm_basic_loading_data() !!!")
    farm_id= request.args.get('farm_id', default = '', type = str) 
    print(farm_id)

    ###############################

    #farms= db.session.query(FarmDb).filter(FarmDb.farm_id == farm_id).all()
    
    farms= db.session.query(FarmDb).filter(FarmDb.farm_id == farm_id).all()
    print(farms)


    
    farm_json ={}
    for farm in farms:    
        farm_json.update({"user_id" : farm.user_id})

        farm_json.update({"user_name" : farm.user_name})
        farm_json.update({"user_bod" : farm.user_bod})
        farm_json.update({"user_email" : farm.user_email})
        farm_json.update({"user_mobile" : farm.user_mobile})
        farm_json.update({"user_tel" : farm.user_tel})

        farm_json.update({"farm_name" : farm.farm_name})
        farm_json.update({"farm_crop" : farm.farm_crop})
        farm_json.update({"farm_kind" : farm.farm_kind})
        farm_json.update({"farm_address" : farm.farm_address})

        farm_json.update({"farm_id" : farm.farm_id})


        farm_json.update({"sensor_display_type" : farm.sensor_display_type})
        farm_json.update({"sanyuk_display_type" : farm.sanyuk_display_type})
        farm_json.update({"pest_display_type" : farm.pest_display_type})
        farm_json.update({"sanrijang_display_type" : farm.sanrijang_display_type})
        
    
    house_list=[]
    houses= db.session.query(HouseDb).filter(HouseDb.farm_id == farm_id).all()
    for houses in houses:    
        house_json={}
        house_json.update({"house_id" : houses.house_id})
        house_json.update({"house_name" : houses.house_name})
        house_json.update({"farm_id" : houses.farm_id})
        house_list.append(house_json)
    

    print("house_list : " + str(house_list))
    farm_json.update({"house_list" : house_list})


    print("farm_json : " + str(farm_json))



    ###############################
    print(farm_json)
    return jsonify({"result":farm_json})



@main_control.route('/farm_new')
def farm_new():
    print("do farm_new() !!!")

    return render_template('/farm/farm_new.html')


@main_control.route('/farm_new_insert_to_db',methods=['POST'])
def farm_new_insert_to_db():
    print("do farm_new_insert_to_db() !!!")

    user_id = request.form.get('user_id')
    user_name = request.form.get('user_name')
    user_bod = request.form.get('user_bod')
    user_email = request.form.get('user_email')
    user_mobile = request.form.get('user_mobile')
    user_tel = request.form.get('user_tel')
    farm_name = request.form.get('farm_name')
    farm_crop = request.form.get('farm_crop')
    farm_kind = request.form.get('farm_kind')

    farm_address = request.form.get('farm_address')


    house_list = request.form.get('house_list')
    print("house_list : "  +  str( house_list))

    try:
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        ##############################################################################
        ## 1.farm insert
        ##############################################################################
        db.session.add(FarmDb(
                    user_id = str(user_id),
                    user_name = str(user_name),
                    user_bod = str(user_bod),
                    user_email = str(user_email),
                    user_mobile = str(user_mobile),
                    user_tel = str(user_tel),

                    farm_name = str(farm_name),
                    farm_crop = str(farm_crop),
                    farm_kind = str(farm_kind),
                    farm_address = str(farm_address)
                ))

        db.session.commit()

        farms= db.session.query(FarmDb).order_by(FarmDb.created_at.desc()).limit(1).all()
        for farm in farms:    
            farm_id = farm.farm_id
            print("farm_id : " + str(farm_id))
        db.session.commit()

        ##############################################################################
        ## 2.house insert
        ##############################################################################
        house_name_for_list = house_list.split(':')
        print("house_name_for_list : " + str(house_name_for_list))

        for  house_name in house_name_for_list:
                print("house_name : " + str(house_name))
                print("(house_name.isspace()) : " + str((house_name.isspace())))
                if( not(house_name.isspace()) ):
                    print("insert house_name : " + str(house_name))
                    db.session.add(HouseDb(house_name=str(house_name),farm_id=str(farm_id)))
                    print("insert House DB")
        
        db.session.commit()

        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

##농장정보수정 /하우스정보와 같이
@main_control.route('/farm_house_basic_udpate',methods=['GET','POST'])
def farm_house_basic_udpate():
    print("do farm_house_basic_udpateㄴ() !!!")
    ##farm_id= request.args.get('farm_id', default = '', type = str) 
    ## request.form.get('name') 
    ## request.form['name']
    ## request.form.getlist('name') # 키가 여러번 전송되고 값의 리스트를 원하면 사용
    page_num = request.form.get("page_num")
    print("page_num : " + str(page_num));
    farm_id = request.form.get('farm_id')
    print("farm_id : " + str(farm_id));

    user_id = request.form.get('user_id')
    user_name = request.form.get('user_name')
    user_bod = request.form.get('user_bod')
    user_email = request.form.get('user_email')
    user_mobile = request.form.get('user_mobile')
    user_tel = request.form.get('user_tel')
    farm_name = request.form.get('farm_name')
    farm_crop = request.form.get('farm_crop')
    farm_kind = request.form.get('farm_kind')
    farm_address = request.form.get('farm_address')

    sensor_display_type = request.form.get('sensor_display_type')
    sanyuk_display_type = request.form.get('sanyuk_display_type')
    pest_display_type = request.form.get('pest_display_type')
    sanrijang_display_type = request.form.get('sanrijang_display_type')

    try :
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()
        #####################################################################
        ## 1. farm upadate
        upateQery = db.session.query(FarmDb)
        upateQery = upateQery.filter(FarmDb.farm_id==farm_id)
        upateRecord = upateQery.one()
        upateRecord.user_id = user_id
        upateRecord.user_name = user_name
        upateRecord.user_bod = user_bod
        upateRecord.user_email = user_email
        upateRecord.user_mobile = user_mobile
        upateRecord.user_tel = user_tel
        upateRecord.farm_name = farm_name
        upateRecord.farm_crop = farm_crop
        upateRecord.farm_kind = farm_kind
        upateRecord.farm_address = farm_address
        upateRecord.sensor_display_type = sensor_display_type
        upateRecord.sanyuk_display_type = sanyuk_display_type
        upateRecord.pest_display_type = pest_display_type
        upateRecord.sanrijang_display_type = sanrijang_display_type
        print("sanyuk_display_type : " + str(sanyuk_display_type))
        
        db.session.commit()
        #####################################################################
        ## 2. house upadate

        house_list_name = request.form.get('house_list_name')
        print("house_list_name : "  +  str( house_list_name))
        house_list_id = request.form.get('house_list_id')
        print("house_list_id : "  +  str( house_list_id))

        house_id_for_list = house_list_id.split(':')
        print("house_id_for_list : " + str(house_id_for_list))


        house_name_for_list = house_list_name.split(':')
        print("house_name_for_list : " + str(house_name_for_list))

        house_id_list_to_delete=[]

        for  house_id_to_delete  in house_id_for_list :
             if(str(house_id_to_delete) != "-1" ):
                house_id_list_to_delete.append(house_id_to_delete)
        
        print("house_id_list_to_delete : " + str(house_id_list_to_delete))
        
        deteteQuery = db.session.query(HouseDb).filter(and_(HouseDb.house_id.not_in(house_id_list_to_delete), HouseDb.farm_id==farm_id))
        deteteQuery.delete(synchronize_session=False)
        db.session.commit()



        for  house_id , house_name in zip(house_id_for_list , house_name_for_list):
                print("house_id : " + str(house_id))
                print("house_name : " + str(house_name))

                if( not(house_id.isspace()) and str(house_id) != "-1" ):

                    print("db.session.query(func.count(HouseDb.id)).filter(and_( HouseDb.house_id == house_id)).scalar()")
                    house_id_check_count= db.session.query(func.count(HouseDb.house_id)).filter(and_( HouseDb.house_id == house_id)).scalar()
                    print("house_id_check_count : " + str(house_id_check_count))

                    if(house_id_check_count > 0 ):
                        upateQery = db.session.query(HouseDb)
                        upateQery = upateQery.filter(HouseDb.house_id==house_id)
                        upateRecord = upateQery.one()
                        upateRecord.house_name = house_name
                        db.session.commit()

                elif(str(house_id) == "-1"):
                   
                    print("2 house_name : " + str(house_name))
                    print("2 (house_name.isspace()) : " + str((house_name.isspace())))
                    if( not(house_name.isspace()) ):
                        print("insert house_name : " + str(house_name))
                        db.session.add(HouseDb(house_name=str(house_name),farm_id=str(farm_id)))
                        db.session.commit()
                        print("insert House DB")
                print("house udpated !!!! ")

        

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
    

    return jsonify({"result":"ok"})
   

##농장정보 수정
@main_control.route('/farm_basic_udpate',methods=['GET','POST'])
def farm_basic_update():
    print("do farm_basic() !!!")
    ##farm_id= request.args.get('farm_id', default = '', type = str) 
    ## request.form.get('name') 
    ## request.form['name']
    ## request.form.getlist('name') # 키가 여러번 전송되고 값의 리스트를 원하면 사용
    page_num = request.form.get("page_num")
    print("page_num : " + str(page_num));
    farm_id = request.form.get('input_farm_id')
    print("farm_id : " + str(farm_id));

    user_id = request.form.get('user_id')
    user_name = request.form.get('user_name')
    user_bod = request.form.get('user_bod')
    user_email = request.form.get('user_email')
    user_mobile = request.form.get('user_mobile')
    user_tel = request.form.get('user_tel')
    farm_name = request.form.get('farm_name')
    farm_crop = request.form.get('farm_crop')
    farm_kind = request.form.get('farm_kind')
    farm_address = request.form.get('farm_address')

    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    upateQery = db.session.query(FarmDb)
    upateQery = upateQery.filter(FarmDb.farm_id==farm_id)
    upateRecord = upateQery.one()
    upateRecord.user_id = user_id
    upateRecord.user_name = user_name
    upateRecord.user_bod = user_bod
    upateRecord.user_email = user_email
    upateRecord.user_mobile = user_mobile
    upateRecord.user_tel = user_tel
    upateRecord.farm_name = farm_name
    upateRecord.farm_crop = farm_crop
    upateRecord.farm_kind = farm_kind
    upateRecord.farm_address = farm_address


    db.session.commit()
    

    url = "main_control.farm_list"

    print("oh my got !!!")
    return redirect(url_for(url, page_num=page_num) )

    ##return render_template(url, farm_id=farm_id,page_num=page_num)
    ##return render_template('/farm/farm_list.html', farm_id=farm_id,page_num=page_num)
    #return render_template('/farm/farm_list.html', farms=farms,url=url,page_num=page_num)



## 하우스 정보 로딩
@main_control.route('/house_loading_data')
def house_loading_data():
    print("do house_loading_data() !!!")

    farm_id= request.args.get('farm_id', default = '', type = str) 
    print("farm_id :"  + farm_id)
    print("====================================================")
    session['select_farm_id'] = str(farm_id)
    print("session['select_farm_id'] : "  + str(session['select_farm_id']))
    
    print("====================================================")

    sensor_type = ""
    sensor_type = request.args.get('sensor_type', default = '', type = str) 
    print("sensor_type :"  + sensor_type)
    
    houses= db.session.query(HouseDb).filter(HouseDb.farm_id == farm_id).order_by(HouseDb.house_name.asc()).all()  
    print(houses)
    print("====================================================================")
    
    result_json ={};
    house_root_json ={};
    temp_house_id = ""
    house_list_temp = []
    temp_b = false
    for house in houses:
        temp_b = false
        house_json ={};
        if sensor_type != "" :
            devices= db.session.query(DeviceDb).filter(DeviceDb.house_id == house.house_id).all()  
            print(devices)
            print("====================================================================")
            for device in devices:
                print("str(device.type) : "  + str(device.type))   
                if str(device.type) == sensor_type :
                    temp_b = true;
                    house_json.update({"house_id" :house.house_id})  ## 한개의 테이블만 로딩시에는 FarmDb.farm.farm_id 가 아닌 farm.farm.id로 명시
                    house_json.update({"house_name" : house.house_name})
        else :
            temp_b = true;
            house_json.update({"house_id" :house.house_id})  ## 한개의 테이블만 로딩시에는 FarmDb.farm.farm_id 가 아닌 farm.farm.id로 명시
            house_json.update({"house_name" : house.house_name})
        
        if temp_b == true:
            house_list_temp.append(house_json)


    house_root_json.update({"house" : house_list_temp})
    

    print("Final house_root_json : "  + str(house_root_json))    
    print("====================================================================")


    return jsonify({"result": house_root_json})

    



## 농장 기본정보2
@main_control.route('/farm_loading_data_by_user')
def farm_loading_data_by_user():
    print("do farm_loading_data_by_user() !!!")

    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
        
    print("userid : " + str(userid))


    farms= db.session.query(FarmDb).filter(FarmDb.user_id==userid).all()
    db.session.commit();

    print(farms)
 
    print("====================================================================")
    
    result_json ={};
    farm_root_json ={};
    temp_farm_id = ""
    farm_list_temp = []
    
    for farm in farms:
       
        farm_json ={};
        farm_json.update({"farm_id" :farm.farm_id})  ## 한개의 테이블만 로딩시에는 FarmDb.farm.farm_id 가 아닌 farm.farm.id로 명시
        farm_json.update({"farm_name" : farm.farm_name})

        farm_list_temp.append(farm_json)


    farm_root_json.update({"farms" : farm_list_temp})
    

    print("Final farm_root_json : "  + str(farm_root_json))    
    print("====================================================================")

    return jsonify({"result": farm_root_json})


## 농장 기본정보2
@main_control.route('/farm_loading_data')
def farm_loading_data():
    print("do farm_loading_data() !!!")

    
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    farms= db.session.query(FarmDb).all()

    print(farms)
 
    print("====================================================================")
    
    result_json ={};
    farm_root_json ={};
    temp_farm_id = ""
    farm_list_temp = []
    
    for farm in farms:
       
        farm_json ={};
        farm_json.update({"farm_id" :farm.farm_id})  ## 한개의 테이블만 로딩시에는 FarmDb.farm.farm_id 가 아닌 farm.farm.id로 명시
        farm_json.update({"farm_name" : farm.farm_name})

        farm_list_temp.append(farm_json)


    farm_root_json.update({"farms" : farm_list_temp})
    

    print("Final farm_root_json : "  + str(farm_root_json))    
    print("====================================================================")

    return jsonify({"result": farm_root_json})




## 농장 기본정보2
@main_control.route('/farm_basic2')
def farm_basic2():
    print("do farm_basic2() !!!")

    farm_id= request.args.get('farm_id', default = '', type = str) 

    print("farm_id :"  + farm_id)
    
    '''
    farms = FarmDb.query.join(HouseDb,FarmDb.farm_id==HouseDb.farm_id).filter(FarmDb.farm_id==farm_id).\
                    add_columns(FarmDb.farm_id ,FarmDb.farm_name, HouseDb.house_id,HouseDb.house_name)
    '''
    ##fcuser = Fcuser.query.filter_by(userid=userid).first()

    
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    farms= db.session.query(FarmDb, HouseDb).filter(FarmDb.farm_id == farm_id).\
                    join(FarmDb , FarmDb.farm_id == HouseDb.farm_id).all()

    print(farms)
 
    print("====================================================================")
    
    result_json ={};
    farm_json ={};
    temp_farm_id = ""
    farm_house_list_temp = []
    
    for farm in farms:
        ##print("farm_ : {}, farm_ : {} , farm : {}".format(farm_.FarmDb , farm_.HouseDb , farm_.DeviceDb)) 
        if(temp_farm_id != farm.FarmDb.farm_id):
          farm_json.update({"farm_id" : farm.FarmDb.farm_id})
          farm_json.update({"farm_name" : farm.FarmDb.farm_name})
        
        house_json ={};
        house_json.update({"house_id" : farm.HouseDb.house_id})
        print("house_id : "  + str(farm.HouseDb.house_id))
        house_json.update({"house_name" : farm.HouseDb.house_name})
        print("house_name : "  + str(farm.HouseDb.house_name))

        farm_house_list_temp.append(house_json)
        print(farm_house_list_temp);

        temp_farm_id = farm.FarmDb.farm_id

    farm_json.update({"houses" : farm_house_list_temp})

    print("Final farm_detail_farm_list : "  + str(farm_json))    
    print("====================================================================")

    return jsonify({"result":farm_json})


###################################################################
## qustion
###################################################################


@main_control.route('/question_list_main/<int:page_num>')
def question_list_main(page_num):
    print("do notice_list_history()")

    search_keyword= request.args.get("search_keyword","")
    search_cloumn= request.args.get("search_cloumn","")
    print("reqeust search_keyword:" + search_keyword)
    print("reqeust search_cloumn:" + search_cloumn)


    try :
        if( search_cloumn and  not(search_cloumn.isspace()) ):
            if search_cloumn == "response_yn" :
                print("aaa")
                questions= QuestionDb.query.\
                                    add_columns(QuestionDb.id,\
                                                        func.ifnull(QuestionDb.create_at,'').label("create_at") ,\
                                                        func.ifnull(QuestionDb.from_type,'').label("from_type"),\
                                                        func.ifnull(QuestionDb.type,'').label("type"),\
                                                        func.ifnull(QuestionDb.title,'').label("title"),\
                                                        func.ifnull(func.substr(QuestionDb.content,0,10),'').label("content"),\
                                                        func.ifnull(QuestionDb.content,'').label("content_full"),\
                                                        func.ifnull(QuestionDb.user_id,'').label("user_id"),\
                                                        func.ifnull(QuestionDb.user_phone,'').label("user_phone"),\
                                                        func.ifnull(QuestionDb.user_address,'').label("user_address"),\
                                                        func.ifnull(QuestionDb.response_yn,'').label("response_yn"),\
                                                        func.ifnull(QuestionDb.response_date,'').label("response_date"),\
                                                        func.ifnull(QuestionDb.response_content,'').label("response_content_full"),\
                                                        func.ifnull(func.substr(QuestionDb.response_content,0,10),'').label("response_content")).\
                                                        filter(  QuestionDb.response_yn.like(search_keyword) ).\
                                                        order_by(QuestionDb.create_at.desc()).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
        else :
            #print("here2")
            if(search_keyword is None) :   
                questions= QuestionDb.query.\
                                    add_columns(QuestionDb.id,\
                                                        func.ifnull(QuestionDb.create_at,'').label("create_at") ,\
                                                        func.ifnull(QuestionDb.from_type,'').label("from_type"),\
                                                        func.ifnull(QuestionDb.type,'').label("type"),\
                                                        func.ifnull(QuestionDb.title,'').label("title"),\
                                                        func.ifnull(func.substr(QuestionDb.content,0,10),'').label("content"),\
                                                        func.ifnull(QuestionDb.content,'').label("content_full"),\
                                                        func.ifnull(QuestionDb.user_id,'').label("user_id"),\
                                                        func.ifnull(QuestionDb.user_phone,'').label("user_phone"),\
                                                        func.ifnull(QuestionDb.user_address,'').label("user_address"),\
                                                        func.ifnull(QuestionDb.response_yn,'').label("response_yn"),\
                                                        func.ifnull(QuestionDb.response_date,'').label("response_date"),\
                                                        func.ifnull(QuestionDb.response_content,'').label("response_content_full"),\
                                                        func.ifnull(func.substr(QuestionDb.response_content,0,10),'').label("response_content")).\
                                                        order_by(QuestionDb.create_at.desc()).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
            else:
                questions= QuestionDb.query.\
                                    add_columns(QuestionDb.id,\
                                                        func.ifnull(QuestionDb.create_at,'').label("create_at") ,\
                                                        func.ifnull(QuestionDb.from_type,'').label("from_type"),\
                                                        func.ifnull(QuestionDb.type,'').label("type"),\
                                                        func.ifnull(QuestionDb.title,'').label("title"),\
                                                        func.ifnull(func.substr(QuestionDb.content,0,10),'').label("content"),\
                                                        func.ifnull(QuestionDb.content,'').label("content_full"),\
                                                        func.ifnull(QuestionDb.user_id,'').label("user_id"),\
                                                        func.ifnull(QuestionDb.user_phone,'').label("user_phone"),\
                                                        func.ifnull(QuestionDb.user_address,'').label("user_address"),\
                                                        func.ifnull(QuestionDb.response_yn,'').label("response_yn"),\
                                                        func.ifnull(QuestionDb.response_date,'').label("response_date"),\
                                                        func.ifnull(QuestionDb.response_content,'').label("response_content_full"),\
                                                        func.ifnull(func.substr(QuestionDb.response_content,0,10),'').label("response_content")).\
                                    filter( or_(\
                                                QuestionDb.from_type.contains(search_keyword) , QuestionDb.type.contains(search_keyword) ,
                                                QuestionDb.content.contains(search_keyword) , QuestionDb.user_id.contains(search_keyword) ,
                                                QuestionDb.user_address.contains(search_keyword) , QuestionDb.response_yn.contains(search_keyword) ,
                                                QuestionDb.response_date.contains(search_keyword) , QuestionDb.user_phone.contains(search_keyword) ,
                                                QuestionDb.response_content.contains(search_keyword)  ,QuestionDb.title.contains(search_keyword) 
                                                )).\
                                                order_by(QuestionDb.create_at.desc()).\
                                                paginate(per_page=per_page_count, page=page_num, error_out=True)

        #print(questions)
        
      
    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

    url ="main_control.question_list_main";
    return render_template('/notice/question_list_main.html', questions=questions , url=url,search_keyword=search_keyword ,search_cloumn=search_cloumn )

@main_control.route('/question_save_data',methods=['GET','POST'])
def question_save_data():
    print("do question_save_data()")

    if request.method == 'POST': 
        try:
            
            question_id = request.form['question_id']
            input_schedule_response = request.form['input_schedule_response']
            response_content = request.form['response_content']
            
            print("question_id : " + question_id );
            print("input_schedule_response : " + input_schedule_response );
            print("response_content : " + response_content );

            print("====================================================================")
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            upateQery = db.session.query(QuestionDb)
            upateQery = upateQery.filter(QuestionDb.id==question_id)
            upateDeviceRecord = upateQery.one()
            upateDeviceRecord.response_date = input_schedule_response
            upateDeviceRecord.response_content = response_content
            upateDeviceRecord.response_yn ="처리"

            db.session.commit()

            print("update success ! ")
            
            print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204


###################################################################
## notice
###################################################################



def get_yn_check_alarm(user_id):
    print("do get_yn_check_alarm()")
    print("user_id : "  + str(user_id))
    user_id = user_id
    if( user_id is None):
        return ""

    try :
       
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        alarm_history= db.session.query(AlarmHistoryDb).filter(AlarmHistoryDb.farm_owner_id == user_id).all()
        db.session.commit()

        print(alarm_history)
        alarm_check_yn ="확인"

        for alarm_history_ in alarm_history:
            alarm_check_yn= alarm_history_.check_yn
            #print(alarm_check_yn)
            if(alarm_check_yn =="미확인"):
               alarm_check_yn = "미확인"
               break

    except Exception as ex:
        print(ex)
        return "no"

    
    return alarm_check_yn


def get_yn_acc_check_alarm(user_id):
    print("do get_yn_acc_check_alarm()")
    print("user_id : "  + str(user_id))
    user_id = user_id
    if( user_id is None):
        return ""

    try :
       
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        alarm_history= db.session.query(AlarmHistoryDb).filter(AlarmHistoryDb.farm_owner_id == user_id).all()
        db.session.commit()

        print(alarm_history)
        alarm_check_yn_cnt = 0 

        for alarm_history_ in alarm_history:
            alarm_check_yn_cnt= int(alarm_check_yn_cnt) + int(alarm_history_.alarm_none_check_cnt)
          

    except Exception as ex:
        print(ex)
        return 0

    
    return alarm_check_yn_cnt




@main_control.route('/notice_save_data_to_send',methods=['GET','POST'])
def notice_save_data_to_send():
    print("do notice_save_data_to_send()")

    if request.method == 'POST': 
        try:
            
            receive_person_list = request.form['userlist']
            receive_person_list_display = request.form['userlist_display']
            title = request.form['title']
            content = request.form['content']
            urgent_yn = request.form['urgent_type']
            date_to_send = request.form['date_to_send']

            print("date_to_send : " + date_to_send );


            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            sessiondb = Session()
        
            print("========================Insert===============================")
            temp_now = datetime.now()
            ##notice_id = temp_now.strftime("%Y-%m-%d %H:%M:%S")
            ##print(notice_id)

            if( urgent_yn == "긴급" ) :
                date_to_send = datetime.now() + timedelta(minutes=10)
                date_to_send = date_to_send.strftime("%Y-%m-%d %H:%M:%S")

            db.session.add(NoticeHistoryDb(
                        ##id = str(notice_id),
                        urgent_yn = str(urgent_yn),
                        title = str(title),
                        content = str(content),
                        date_to_send = str(date_to_send),
                        receive_person_list_display = str(receive_person_list_display),
                        receive_person_list = str(receive_person_list),
                        send_yn = str("0"),
                    ))
            db.session.commit()
            notice_histoy =  db.session.query(NoticeHistoryDb).filter(and_(NoticeHistoryDb.title==title, NoticeHistoryDb.content==content)).first()
            print("notice_histoy.id : "  + str(notice_histoy.id))
            notice_history_id =  notice_histoy.id

            user_temp_list = receive_person_list.split(':')
            print(user_temp_list)

            for user_ in user_temp_list:
                print("user_ : " + str(user_))
                db.session.add(NoticeUserHistoryDb(
                            notice_id = notice_history_id,
                            user_id = str(user_),
                            check_yn = "미확인"
                        ))
            db.session.commit()
        

            print("Insert sucess !")
            print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204




@main_control.route('/notice_list_main/<int:page_num>')
def notice_list_main(page_num):
    print("do notice_list_main()")

    print("reqeust querystring:" + request.args.get("search_keyword",""))
    search_keyword= request.args.get("search_keyword","")

    print("reqeust select_notice_corp:" + request.args.get("select_notice_corp",""))
    select_notice_corp=""
    select_notice_corp= request.args.get("select_notice_corp","")

    print("reqeust select_notice_kind:" + request.args.get("select_notice_kind",""))
    select_notice_kind=""
    select_notice_kind= request.args.get("select_notice_kind","")

    try :
        if(search_keyword is None) :
                
            notices= HouseDb.query.outerjoin(FarmDb,FarmDb.farm_id==HouseDb.farm_id).\
                                add_columns(HouseDb.house_id,\
                                                    func.ifnull(HouseDb.house_name,'').label("house_name") ,\
                                                    func.ifnull(FarmDb.farm_id,'').label("farm_id"),\
                                                    func.ifnull(FarmDb.farm_name,'').label("farm_name"),\
                                                    func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                                    func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                                    func.ifnull(FarmDb.user_bod,'').label("user_bod"),\
                                                    func.ifnull(FarmDb.user_tel,'').label("user_tel"),\
                                                    func.ifnull(FarmDb.farm_address,'').label("farm_address"),\
                                                    func.ifnull(FarmDb.farm_crop,'').label("farm_crop"),\
                                                    func.ifnull(FarmDb.farm_kind,'').label("farm_kind"),\
                                                    func.ifnull(FarmDb.user_mobile,'').label("user_mobile"),\
                                                
                                                    )\
                                                        .order_by(FarmDb.user_id.desc())\
                                                        .order_by(FarmDb.user_name.desc())\
                                                        .paginate(per_page=per_page_count, page=page_num, error_out=True)
        else:
                
            notices= HouseDb.query.outerjoin(FarmDb,FarmDb.farm_id==HouseDb.farm_id).\
                                add_columns(HouseDb.house_id,\
                                                    func.ifnull(HouseDb.house_name,'').label("house_name") ,\
                                                    func.ifnull(FarmDb.farm_id,'').label("farm_id"),\
                                                    func.ifnull(FarmDb.farm_name,'').label("farm_name"),\
                                                    func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                                    func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                                    func.ifnull(FarmDb.user_bod,'').label("user_bod"),\
                                                    func.ifnull(FarmDb.user_tel,'').label("user_tel"),\
                                                    func.ifnull(FarmDb.farm_address,'').label("farm_address"),\
                                                    func.ifnull(FarmDb.farm_crop,'').label("farm_crop"),\
                                                    func.ifnull(FarmDb.farm_kind,'').label("farm_kind"),\
                                                    func.ifnull(FarmDb.user_mobile,'').label("user_mobile"),\
                                                
                                                    ).\
                                filter( or_(\
                                            HouseDb.house_name.contains(search_keyword) , FarmDb.user_name.contains(search_keyword) ,
                                            FarmDb.user_tel.contains(search_keyword) , FarmDb.user_bod.contains(search_keyword) ,
                                            FarmDb.farm_address.contains(search_keyword) ,
                                            FarmDb.farm_crop.contains(search_keyword) , FarmDb.farm_kind.contains(search_keyword) 
                                            ))\
                                                        .order_by(FarmDb.user_id.desc())\
                                                        .order_by(FarmDb.user_name.desc())\
                                                        .paginate(per_page=per_page_count, page=page_num, error_out=True)


        #print(notices)
        alarm_list_check = []
        temp_str = ""
        for notice in notices.items:
            temp_str =   get_yn_check_alarm(notice.user_id)
            alarm_list_check.append(temp_str)
        #print(type(alarm_list_check))


        alarm_list_acc_nocheck = []
        temp_int = 0
        for notice in notices.items:
            temp_int =   get_yn_acc_check_alarm(notice.user_id)
            alarm_list_acc_nocheck.append(temp_int)
        #print(str(alarm_list_acc_nocheck))

        
            

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

    url ="main_control.notice_list_main";
    return render_template('/notice/notice_list_main.html', notices=notices,alarm_list_check_cnt= len(alarm_list_check),\
                                                alarm_list_check = alarm_list_check ,\
                                                     alarm_list_acc_nocheck= alarm_list_acc_nocheck,\
                                                url=url,search_keyword=search_keyword , \
                                                    select_notice_corp = str(select_notice_corp) ,select_notice_kind = str(select_notice_kind) )


def get_notice_no_check_cnt(notice_id):
    print("do get_notice_no_check_cnt()")
    print("notice_id : "  + str(notice_id))
    notice_id = notice_id
    if( notice_id is None):
        return ""

    try :
       
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        notice_no_check_cnt= db.session.query(NoticeUserHistoryDb).\
                                    filter(\
                                        and_( NoticeUserHistoryDb.notice_id == notice_id,
                                              NoticeUserHistoryDb.check_yn == "미확인" )
                                          ).all()
        db.session.commit()

        #print(str(notice_no_check_cnt))

        no_check_cnt = "0";
        no_check_cnt= str(len(notice_no_check_cnt))
        #print(str(no_check_cnt))
    
    
    except Exception as ex:
        print(ex)
        return "0"

    
    return no_check_cnt



@main_control.route('/notice_list_history/<int:page_num>')
def notice_list_history(page_num):
    print("do notice_list_history()")

    search_keyword= request.args.get("search_keyword","")
    search_cloumn= request.args.get("search_cloumn","")
    print("reqeust search_keyword:" + search_keyword)
    print("reqeust search_cloumn:" + search_cloumn)

    try :
        if( search_cloumn and not not(search_cloumn.isspace()) ):
            if search_cloumn == "urgent_yn" :
                notices= NoticeHistoryDb.query.\
                                    add_columns(NoticeHistoryDb.id,\
                                                        func.ifnull(NoticeHistoryDb.date_to_send,'').label("date_to_send") ,\
                                                        func.ifnull(NoticeHistoryDb.urgent_yn,'').label("urgent_yn"),\
                                                        func.ifnull(NoticeHistoryDb.title,'').label("title"),\
                                                        func.ifnull(NoticeHistoryDb.content,'').label("content"),\
                                                        func.ifnull(NoticeHistoryDb.receive_person_list_display,'').label("receive_person_list_display")).\
                                                        filter(  NoticeHistoryDb.urgent_yn.contains(search_keyword) ).\
                                                        order_by(NoticeHistoryDb.create_at.desc()).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
        else :
            #print("here2")
            if(search_keyword is None) :         
                notices= NoticeHistoryDb.query.\
                                    add_columns(NoticeHistoryDb.id,\
                                                        func.ifnull(NoticeHistoryDb.date_to_send,'').label("date_to_send") ,\
                                                        func.ifnull(NoticeHistoryDb.urgent_yn,'').label("urgent_yn"),\
                                                        func.ifnull(NoticeHistoryDb.title,'').label("title"),\
                                                        func.ifnull(NoticeHistoryDb.content,'').label("content"),\
                                                        func.ifnull(NoticeHistoryDb.receive_person_list_display,'').label("receive_person_list_display"),\
                                                    
                                                        ).\
                                                        order_by(NoticeHistoryDb.create_at.desc()).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
            else:
                notices=  NoticeHistoryDb.query.\
                                    add_columns(NoticeHistoryDb.id,\
                                                        func.ifnull(NoticeHistoryDb.date_to_send,'').label("date_to_send") ,\
                                                        func.ifnull(NoticeHistoryDb.urgent_yn,'').label("urgent_yn"),\
                                                        func.ifnull(NoticeHistoryDb.title,'').label("title"),\
                                                        func.ifnull(NoticeHistoryDb.content,'').label("content"),\
                                                        func.ifnull(NoticeHistoryDb.receive_person_list_display,'').label("receive_person_list_display"),\
                                                        ).\
                                    filter( or_(\
                                                NoticeHistoryDb.date_to_send.contains(search_keyword) , NoticeHistoryDb.urgent_yn.contains(search_keyword) ,
                                                NoticeHistoryDb.title.contains(search_keyword) , NoticeHistoryDb.content.contains(search_keyword) ,
                                                NoticeHistoryDb.receive_person_list.contains(search_keyword)  
                                                )).\
                                                order_by(NoticeHistoryDb.create_at.desc()).\
                                                paginate(per_page=per_page_count, page=page_num, error_out=True)

        #print(notices)
        notice_user_list_check = []
        temp_str = ""
        for notice in notices.items:
            temp_str =   get_notice_no_check_cnt(notice.id)
            notice_user_list_check.append(temp_str)
            
        
        
        notice_user_nocheck_list = []
        temp_user_str = ""
        first_name = ""
        for notice in notices.items:
            temp_user_str = ""
            notice_historys= db.session.query(NoticeUserHistoryDb ).\
                                filter(and_(  NoticeUserHistoryDb.check_yn == "미확인" , NoticeUserHistoryDb.notice_id == notice.id)).all()
            print("notice_historys : " + str(notice_historys))
            
            temp_cnt = 0
            for notice_history in notice_historys:
                if temp_user_str != "" :
                    if str(notice_history.user_id) != "":
                        if temp_cnt == 0 :
                            first_name = str(notice_history.user_id)
                        temp_cnt = temp_cnt + 1
                        temp_user_str = temp_user_str  + "," + str(notice_history.user_id)
                else :
                    
                    if str(notice_history.user_id) != "":
                        if temp_cnt == 0 :
                            first_name = str(notice_history.user_id)
                            
                        temp_cnt = temp_cnt + 1
                        temp_user_str = str(notice_history.user_id)
            
            
            if temp_cnt > 1 :
               lst = temp_user_str.split(",")
               print("lst[0] : " + str(lst[0]))
               temp_user_str = str(lst[0]) + "외" + str(temp_cnt-1) +"명"
               
            print("temp_user_str : " + str(temp_user_str))
            notice_user_nocheck_list.append(temp_user_str)

        print("notice_user_nocheck_list : " + str(notice_user_nocheck_list))
        
        
        
        #print(type(notice_user_list_check))
        #print(notice_user_list_check)

      
    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

    url ="main_control.notice_list_history";
    return render_template('/notice/notice_list_history.html', notices=notices,notice_user_list_check_cnt= len(notice_user_list_check) , notice_user_list_check=notice_user_list_check,notice_user_nocheck_list=notice_user_nocheck_list, url=url,search_keyword=search_keyword ,search_cloumn=search_cloumn )



@main_control.route('/notice_send_search/<int:page_num>',methods=['GET','POST'])
def notice_send_search(page_num):

    print("notice_send_search()")
    if request.method == 'GET' :
        try:
            print("reqeust querystring:" + request.args.get("search_notice_keyword",""))
            search_text= str(request.args.get('search_notice_keyword', default = '', type = str) )

            print("search_text:" + search_text)
            
            notices = NoticeDb.query.filter(or_(NoticeDb.crop.contains(search_text),NoticeDb.kind.contains(search_text)) ).paginate(per_page=per_page_count, page=page_num, error_out=True)
            #oldcoin = MyTable.query.filter_by(name = 'USD').filter_by(price> 0).all()

            url = 'main_control.notice_send_search'
            return render_template('/notice/notice_send_self.html', notices=notices, url=url, search_notice_keyword=search_text)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204


@main_control.route('/notice_send/<int:page_num>')
def notice_send(page_num):
    print("do notice_send()")
    print("reqeust querystring:" + request.args.get("search_notice_keyword",""))
    search_notice_keyword= request.args.get('search_notice_keyword', default = '', type = str) 

    url ="main_control.notice_send";

    notices= NoticeDb.query.paginate(per_page=per_page_count, page=page_num, error_out=True)
    return render_template('/notice/notice_send_self.html', notices=notices,url=url)


@main_control.route('/employees/<int:page_num>')
def employees(page_num):
   
    '''
    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(150))
    position = db.Column(db.String(150))
    office = db.Column(db.String(150))
    age = db.Column(db.Integer)
    startdate = db.Column(db.String(150))
    salary = db.Column(db.String(150))
    '''
    employees = EmployeesDb.query.paginate(per_page=per_page_count, page=page_num, error_out=True)


    for employee in employees.items:
        print(employee.fullname)

    return render_template('/notice/employee.html', employees=employees)


###################################################################
## farm monitoring
###################################################################

@main_control.route('/farm_monitoring')
def farm_monitoring():

    return render_template('/farm/farm_monitoring.html')


@main_control.route('/farm_monitoring_growth')
def farm_monitoring_growth():
    
    return render_template('/farm/farm_monitoring_growth.html')

@main_control.route('/farm_monitoring_pest')
def farm_monitoring_pest():

    return render_template('/farm/farm_monitoring_pest.html')

@main_control.route('/farm_monitoring_disorder')
def farm_monitoring_disorder():

    return render_template('/farm/farm_monitoring_disorder.html')




###################################################################
## account manager
###################################################################

@main_control.route('/user_list/<int:page_num>')
def user_list(page_num):
    print("do user_list !!!")

    if request.method == 'GET' :
        try:
           

            users = Fcuser.query.filter( Fcuser.role =='0' ).paginate(per_page=per_page_count, page=page_num, error_out=True)
            print(users)

            url ="main_control.user_list";
            
            return render_template('/users/user_list.html', users=users,url=url,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204


@main_control.route('/user_update_pwd')
def user_update_pwd():
    print("do user_update_pwd() !!!")

    userid= request.args.get('userid', default = '', type = str) 
    password= request.args.get('password', default = '', type = str) 

    print("userid :"  + userid)
    print("password :"  + password)

    try:
        upateQery = db.session.query(Fcuser)
        upateQery = upateQery.filter(Fcuser.userid==userid)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.password = password
        db.session.commit()

        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
        
    
    
@main_control.route('/user_list_new/<int:page_num>')
def user_list_new(page_num):
    print("do user_list_new !!!")

    if request.method == 'GET' :
        try:
            temp_now = datetime.now()
            today_date_time = temp_now.strftime("%Y-%m-%d %H:%M:%S")
            print("today_date_time : " + str(today_date_time))
            today_date= temp_now.strftime("%Y-%m-%d")
            print("today_date : " + str(today_date))

            users = Fcuser.query.filter( and_(Fcuser.role =='0', Fcuser.created_at >= today_date_time) ).paginate(per_page=per_page_count, page=page_num, error_out=True)
            print(users)

            url ="main_control.user_list";
            
            return render_template('/users/user_list.html', users=users,url=url,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204



@main_control.route('/user_list_search/<int:page_num>',methods=['GET','POST'])
def user_list_search(page_num):

    print("user_list_search()")

    if request.method == 'GET' :
        try:
            print("reqeust querystring:" + request.args.get("search_user_keyword",""))
            search_text= str(request.args.get('search_user_keyword', default = '', type = str) )

            print("search_text:" + search_text)
            '''
            users = Fcuser.query.filter(or_(Fcuser.userid.like(search_text),Fcuser.username.like(search_text)) ).\
                                    paginate(per_page=per_page_count, page=page_num, error_out=True)
            '''
            users = Fcuser.query.filter( Fcuser.userid.contains(search_text) | Fcuser.username.contains(search_text) | Fcuser.role != "1" | Fcuser.role != "2" ).\
                                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)


            print(users)

            url = 'main_control.user_list_search'
            return render_template('/users/user_list.html', users=users, url=url, search_farm_keyword=search_text,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

################################################################

@main_control.route('/user_admin_list/<int:page_num>')
def user_admin_list(page_num):
    print("do user_admin_list !!!")

    if request.method == 'GET' :
        try:
           

            users = Fcuser.query.filter( or_(Fcuser.role == '1'  ,Fcuser.role == '2') ).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
            print(users)

            url ="main_control.user_admin_list";
            
            return render_template('/users/user_admin_list_v2.html', users=users,url=url,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204
   

@main_control.route('/user_get_data')
def user_get_data():
  
    print("do user_get_data !!!")

    user_id= request.args.get('user_id', default = '', type = str) 
    print("user_id :"  + user_id)

    try:
        

        users= db.session.query(Fcuser).filter(Fcuser.userid == user_id).all()

        print(users)

        print("====================================================================")

        result_json ={};

        for user in users:

            user_json ={};
            user_json.update({"userid" : user.userid})
            user_json.update({"username" : user.username})
            user_json.update({"bod" : user.bod})
            user_json.update({"email" : user.email})
            user_json.update({"phone_number" : user.phone_number})
            user_json.update({"role" : user.role})
            user_json.update({"created_at" : user.created_at})
            
        if user is None:
            result_json.update({"result" : "no"})
        else: 
            result_json.update({"result" : user_json})

        #print("Final result_json : "  + str(result_json))    
        print("====================================================================")

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
   

@main_control.route('/user_add_admin_role')
def user_add_admin_role():
  
    print("do user_add_admin_role !!!")

    user_id= request.args.get('user_id', default = '', type = str) 
    user_role= request.args.get('role', default = '', type = str) 
    print("user_id :"  + user_id)
    print("user_role :"  + user_role)

    try:
        

        print("====================================================================")
        upateQery = db.session.query(Fcuser)
        upateQery = upateQery.filter(Fcuser.userid==user_id)
        upateRecord = upateQery.one()
        upateRecord.role = user_role
        db.session.commit()
        print("====================================================================")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
   


@main_control.route('/user_insert_admin_role')
def user_insert_admin_role():
  
    print("do user_insert_admin_role !!!")

    user_id= request.args.get('user_id', default = '', type = str) 
    user_name= request.args.get('user_name', default = '', type = str) 
    password= request.args.get('password', default = '', type = str) 
    user_phone= request.args.get('user_phone', default = '', type = str) 
    user_role= request.args.get('role', default = '', type = str) 
    print("user_id :"  + user_id)
    print("user_name :"  + user_name)
    print("password :"  + password)
    print("user_phone :"  + user_phone)
    print("user_role :"  + user_role)

    try:
        print("====================================================================")
        db.session.add(Fcuser(userid=user_id,username=user_name,password=password,\
                                        phone_number=user_phone,role=user_role))
        db.session.commit()
        print("====================================================================")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
   

###################################################################
## device
###################################################################


@main_control.route('/file_write')
def file_write():
    print("do file_write() !!!")

    
    try:
        # file open
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/table_data_temp.txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
       
        print(file_full_path)

        with open(file_full_path,'w+') as file_p: 
            ##read_ = file_p.readline()
            file_p.write("\n Nice bro~ wool :)")
        file_p.close()
       
        
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


@main_control.route('/file_read')
def file_read():
    print("do file_read() !!!")

    
    try:
        # file open
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/table_data.txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
       
        print(file_full_path)

        with open(file_full_path,'r') as file_p: 
            ##read_ = file_p.readline()
            read_ = file_p.read()
            print("++++++++++++++++++++")
            print(read_)
        file_p.close()
       
        
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


## 센서 데이터 읽기 ( 장비 아이디 기준 )
## http://127.0.0.1:5000/get_sensor_data_by_device_id?device_id=0000000002&start_date=2021.09.20 01:00&end_date=2021.09.24 24:59

@main_control.route('/get_sensor_data_by_device_id_from_db')
def get_sensor_data_by_device_id_from_db():
    print("do get_sensor_data_by_device_id_from_db() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        ## 4. make data to send to web
        sensor_list =[];
        sensor_json ={};
        sensor_fild_data_1_json ={};
        sensor_fild_data_2_json ={};
        sensor_fild_data_4_json ={};
        sensor_fild_data_6_json ={};
        sensor_fild_data_8_json ={};
        sensor_fild_data_10_json ={};
        sensor_fild_data_12_json ={};
        sensor_fild_data_14_json ={};
        sensor_fild_data_16_json ={};
        sensor_fild_data_18_json ={};
        sensor_fild_data_20_json ={};
        field_1_list = [] ## date
        field_2_list = [] ## time
        field_date_list = []
        field_4_list = [] ## tp value
        field_6_list = [] ## rh value
        field_8_list = [] ## co2 value
        field_10_list = [] ## ppf value
        field_12_list = [] ## uv value
        field_14_list = [] ## uv_space value
        field_16_list = [] ## st value
        field_18_list = [] ## ec_space value
        field_20_list = [] ## sm_space value

        ##search_end_date_db = datetime.strptime(str(search_end_date),'%Y-%m-%d %H:%M:%S')
        start_date_ = datetime.strptime(str(start_date), '%Y-%m-%d %H:%M')
        print("start_date_:" + str(start_date_))
        end_date_ = datetime.strptime(str(end_date), '%Y-%m-%d %H:%M')
        print("end_date_:" + str(end_date_))


        senser_data_list= db.session.query(SensorDataDb)\
                         .filter( 
                                and_( SensorDataDb.device_id == device_id , \
                                      cast((SensorDataDb.field1 + " " + SensorDataDb.field2),DateTime) >= start_date_  \
                                    , cast((SensorDataDb.field1 + " " + SensorDataDb.field2),DateTime) <= end_date_ 
                                    )
                                ).all()
        db.session.close()
        print("senser_data_list : " + str(senser_data_list))
 
        if( not senser_data_list is None):
            for sensor in senser_data_list :

                temp_final_date_time = sensor.field1 + " " + sensor.field2;
                #print("temp_final_date_time : " + str(temp_final_date_time))
                ##time_obj = datetime.strptime(temp_final_date_time, "%Y-%m-%d %H:%M")
                ##print("time_obj : " + str(time_obj))
                
                field_date_list.append(str(temp_final_date_time))  ## date
                field_4_list.append(sensor.field4)  ## tp value
                field_6_list.append(sensor.field6)  ## tp value
                field_8_list.append(sensor.field8)  ## tp value
                field_10_list.append(sensor.field10)  ## tp value
                field_12_list.append(sensor.field12)  ## tp value
                field_14_list.append(sensor.field14)  ## tp value
                field_16_list.append(sensor.field16)  ## tp value
                field_18_list.append(sensor.field18)  ## tp value
                field_20_list.append(sensor.field20)  ## tp value


        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 
        sensor_json.update({"date" : field_date_list}) 
        sensor_json.update({"field_4_list" : field_4_list})
        sensor_json.update({"field_6_list" : field_6_list})
        sensor_json.update({"field_8_list" : field_8_list})
        sensor_json.update({"field_10_list" : field_10_list})
        sensor_json.update({"field_12_list" : field_12_list})
        sensor_json.update({"field_14_list" : field_14_list})
        sensor_json.update({"field_16_list" : field_16_list})
        sensor_json.update({"field_18_list" : field_18_list})
        sensor_json.update({"field_20_list" : field_20_list})


        result_json ={};
        result_json.update({"result" : sensor_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 



@main_control.route('/get_sensor_data_by_device_id')
def get_sensor_data_by_device_id():
    print("do get_sensor_data_by_device_id() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/sensor_data_" + device_id + ".txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
        print(file_full_path)

        ## 4. make data to send to web
        sensor_list =[];
        sensor_json ={};
        sensor_fild_data_1_json ={};
        sensor_fild_data_2_json ={};
        sensor_fild_data_4_json ={};
        sensor_fild_data_6_json ={};
        sensor_fild_data_8_json ={};
        sensor_fild_data_10_json ={};
        sensor_fild_data_12_json ={};
        sensor_fild_data_14_json ={};
        sensor_fild_data_16_json ={};
        sensor_fild_data_18_json ={};
        sensor_fild_data_20_json ={};
        field_1_list = [] ## date
        field_2_list = [] ## time
        field_date_list = []
        field_4_list = [] ## tp value
        field_6_list = [] ## rh value
        field_8_list = [] ## co2 value
        field_10_list = [] ## ppf value
        field_12_list = [] ## uv value
        field_14_list = [] ## uv_space value
        field_16_list = [] ## st value
        field_18_list = [] ## ec_space value
        field_20_list = [] ## sm_space value

        read_line_cnt = 0
        with  open(file_full_path,'r') as file_p: 
            ##read_contents = file_p.readline()
            read_contents = file_p.readline().splitlines()  ## list object

            while read_contents:
                #print(len(str(read_contents)))

                if read_contents is None :
                    break;

                #read_line_cnt = read_line_cnt + 1
                #if(read_line_cnt > 3):
                #    break;

                ##read_contents = file_p.readline().split()
                #print("================================")
                #print(read_contents)
                #print("here1")
                #print(type(read_contents))
                
                ##read_contents = json.dumps(read_contents, ensure_ascii=False)
                #print("here2")
                #print(type(read_contents))
                #read_contents_json_obj = json.dumps(read_contents, ensure_ascii=False)
                for sensor_dic in read_contents :
                    #print("here3")
                    #print(type(sensor_dic))
                    sensor_dic = ast.literal_eval(sensor_dic)
                    #print(sensor_dic)
                    #print(type(sensor_dic))
                    ##sensor_dic = ast.literal_eva(sensor_dic)
                    ##sensor_dic = json.load(sensor_dic)
        
            
                    temp_date = sensor_dic["field1"]
                    temp_time = sensor_dic["field2"]

                    #print(temp_date)
                    #print(temp_time)

                    temp_final_date_time = temp_date + " " + temp_time;

                    #print(temp_final_date_time)
                    time_obj = datetime.strptime(temp_final_date_time, "%Y-%m-%d %H:%M")
                    temp_start_date_obj= datetime.strptime(start_date, "%Y-%m-%d %H:%M")
                    temp_end_date_obj= datetime.strptime(end_date, "%Y-%m-%d %H:%M")
                    ##print(time_obj)
                    ##print(temp_start_date_obj)
                    ##print(temp_end_date_obj)

                    if time_obj > temp_start_date_obj and time_obj < temp_end_date_obj :
                        field_date_list.append(str(time_obj))  ## date
                        ##print(field_date_list)
                        field_4_list.append(sensor_dic["field4"])  ## tp value
                        field_6_list.append(sensor_dic["field6"])  ## tp value
                        field_8_list.append(sensor_dic["field8"])  ## tp value
                        field_10_list.append(sensor_dic["field10"])  ## tp value
                        field_12_list.append(sensor_dic["field12"])  ## tp value
                        field_14_list.append(sensor_dic["field14"])  ## tp value
                        field_16_list.append(sensor_dic["field16"])  ## tp value
                        field_18_list.append(sensor_dic["field18"])  ## tp value
                        field_20_list.append(sensor_dic["field20"])  ## tp value
                        ##print(field_4_list)

                    read_contents = file_p.readline().splitlines()  ## list object

                
        file_p.close()
        

        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 
        sensor_json.update({"date" : field_date_list}) 
        sensor_json.update({"field_4_list" : field_4_list})
        sensor_json.update({"field_6_list" : field_6_list})
        sensor_json.update({"field_8_list" : field_8_list})
        sensor_json.update({"field_10_list" : field_10_list})
        sensor_json.update({"field_12_list" : field_12_list})
        sensor_json.update({"field_14_list" : field_14_list})
        sensor_json.update({"field_16_list" : field_16_list})
        sensor_json.update({"field_18_list" : field_18_list})
        sensor_json.update({"field_20_list" : field_20_list})


        result_json ={};
        result_json.update({"result" : sensor_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 

def getImageFullListByDevice(device_id,start_date, end_date):
    print("do getImageFullListByDevice() !!!")
    
    print("device_id :"  + device_id)
    start_date_ = int(start_date)
    print("start_date_:" + str(start_date_))
    end_date_ = int(end_date)
    print("end_date_:" + str(end_date_))

    images= db.session.query(ImageBasicDataDb).\
                            filter( and_(~ImageBasicDataDb.image_path.contains('cam1'), ImageBasicDataDb.check_defect_manul == "true" , ImageBasicDataDb.device_id == device_id , \
                                      cast((ImageBasicDataDb.targetDate),Integer) >= start_date_   \
                                    , cast((ImageBasicDataDb.targetDate),Integer) <= end_date_ 
                                
                                )).all()
    
    print(images)
    print("====================================================================")
    
    result_json ={};
    image_list_temp = []
    
    for image in images:
        temp_json ={};
        
        temp_json.update({"id" : image.id})    
        temp_json.update({"created_at" : str(image.created_at)})
        temp_json.update({"device_id" : image.device_id})
        temp_json.update({"farmID" : image.farmID})
        temp_json.update({"greenHouseID" : image.greenHouseID})
        temp_json.update({"targetDate" : image.targetDate})
        temp_json.update({"sequenceID" : image.sequenceID})
        temp_json.update({"roundID" : image.roundID})
        temp_json.update({"image_path" : image.image_path})
        temp_json.update({"check_defect_manul" : image.check_defect_manul})
        temp_json.update({"check_defect_auto" : image.check_defect_auto})
        temp_json.update({"plague_type" : image.plague_type})
        temp_json.update({"plague_level" : image.plague_level})
        image_list_temp.append(temp_json)
        
    result_json.update({"images" : image_list_temp})
    #print("Final images result_json : "  + str(result_json))    
    print("====================================================================")

    return result_json


def getImageFullListByDevice_only_issue(device_id,start_date, end_date):
    print("do getImageFullListByDevice_only_issue() !!!")
    
    print("device_id :"  + device_id)
    start_date_ = int(start_date)
    print("start_date_:" + str(start_date_))
    end_date_ = int(end_date)
    print("end_date_:" + str(end_date_))

    images= db.session.query(ImageBasicDataDb).\
                            filter( and_(~ImageBasicDataDb.image_path.contains('cam1'), ImageBasicDataDb.plague_level != "" ,\
                                     ImageBasicDataDb.check_defect_manul == "true" , ImageBasicDataDb.device_id == device_id , \
                                      cast((ImageBasicDataDb.targetDate),Integer) >= start_date_   \
                                    , cast((ImageBasicDataDb.targetDate),Integer) <= end_date_ 
                                
                                )).all()
    
    print(images)
    print("====================================================================")
    
    result_json ={};
    image_list_temp = []
    
    for image in images:
        temp_json ={};
        
        temp_json.update({"id" : image.id})    
        temp_json.update({"created_at" : str(image.created_at)})
        temp_json.update({"device_id" : image.device_id})
        temp_json.update({"farmID" : image.farmID})
        temp_json.update({"greenHouseID" : image.greenHouseID})
        temp_json.update({"targetDate" : image.targetDate})
        temp_json.update({"sequenceID" : image.sequenceID})
        temp_json.update({"roundID" : image.roundID})
        temp_json.update({"image_path" : image.image_path})
        temp_json.update({"check_defect_manul" : image.check_defect_manul})
        temp_json.update({"check_defect_auto" : image.check_defect_auto})
        temp_json.update({"plague_type" : image.plague_type})
        temp_json.update({"plague_level" : image.plague_level})
        image_list_temp.append(temp_json)
        
    result_json.update({"images" : image_list_temp})
    #print("Final images result_json : "  + str(result_json))    
    print("====================================================================")

    return result_json

@main_control.route('/get_growth_data_by_device_id_from_db')
def get_growth_data_by_device_id_from_db():
    print("do get_growth_data_by_device_id_from_db() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        ## 4. make data to send to web
   
        growth_json ={};

        field_date_list = []

        flowercount= []
        fruitcount= []
        fruitmaturity= []
        fruitSize=[]
        avgleafarea= []
        totalleafcount= []
        leafcolor= []
        aplicalleafcount= []
        avgaplicalleaflength= []
        plantheadcount= []
        avgplantheadsize= []
        guttation= []
        avgguttationsize= []


        '''
        start_date_ = datetime.strptime(str(start_date)+":00", '%Y-%m-%d %H:%M:%S')
        print("start_date_:" + str(start_date_))
        end_date_ = datetime.strptime(str(end_date)+":00", '%Y-%m-%d %H:%M:%S')
        print("end_date_:" + str(end_date_))
        '''
        ##start_date_ = datetime.strptime(str(start_date), '%Y%m%d')
        ##print("start_date_:" + str(start_date_))
        ##end_date_ = datetime.strptime(str(end_date), '%Y%m%d')
        ##print("end_date_:" + str(end_date_))


        start_date_ = int(start_date)
        print("start_date_:" + str(start_date_))
        end_date_ = int(end_date)
        print("end_date_:" + str(end_date_))


        ## 2021-08-11 00:41:10
        ##cast((GrowthDataDb.date + " 00:00:00"),DateTime) >= end_date_ 
        '''
        growth_data_list= db.session.query(GrowthDataDb)\
                         .filter( 
                                and_( GrowthDataDb.device_id == device_id , \
                                      cast(("20210814" + " 00:00:00"),DateTime) >= start_date_   \
                                    , cast(("20210814" + " 00:00:00"),DateTime) <= end_date_ 
                                    )
                                ).all()
        '''
        
        growth_data_list= db.session.query(GrowthDataDb)\
                         .filter( 
                                and_( GrowthDataDb.device_id == device_id , \
                                      cast((GrowthDataDb.date),Integer) >= start_date_   \
                                    , cast((GrowthDataDb.date),Integer) <= end_date_ 
                                    )
                                ).all()
        '''
        growth_data_list= db.session.query(GrowthDataDb)\
                         .filter( 
                                and_( GrowthDataDb.device_id == device_id 
                                    )
                                ).all()
        '''
        db.session.close()
        print("growth_data_list : " + str(growth_data_list))

        if( not growth_data_list is None):
            for growth in growth_data_list :
                
                field_date_list.append(str(growth.date))  
 
                totalleafcount.append(growth.totalleafcount)
                aplicalleafcount.append(growth.aplicalleafcount)
                plantheadcount.append(growth.plantheadcount)
                guttation.append(growth.guttation)
                flowercount.append(growth.flowercount)
                fruitcount.append(growth.fruitcount)
                fruitSize.append(growth.fruitSize)
                
                avgaplicalleaflength.append(growth.avgaplicalleaflength)
                avgleafarea.append(growth.avgleafarea)
                avgplantheadsize.append(growth.avgplantheadsize)
                avgguttationsize.append(growth.avgguttationsize)

        growth_json.update({"device_id" : device_id}) 
        growth_json.update({"set_location" : set_location}) 
        growth_json.update({"date" : field_date_list}) 

        growth_json.update({"totalleafcount" : totalleafcount})
        growth_json.update({"aplicalleafcount" : aplicalleafcount})
        growth_json.update({"plantheadcount" : plantheadcount})
        growth_json.update({"guttation" : guttation})
        growth_json.update({"flowercount" : flowercount})
        growth_json.update({"fruitcount" : fruitcount})
        growth_json.update({"fruitSize" : fruitSize})
        growth_json.update({"avgaplicalleaflength" : avgaplicalleaflength})
        growth_json.update({"avgleafarea" : avgleafarea})
        growth_json.update({"avgplantheadsize" : avgplantheadsize})
        growth_json.update({"avgguttationsize" : avgguttationsize})
        
        image_json = getImageFullListByDevice(device_id,start_date ,end_date)
        #print("image_json : "  + str(image_json))   
        growth_json.update(image_json)
        
        result_json ={};
        result_json.update({"result" : growth_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


@main_control.route('/get_growth_data_by_device_id')
def get_growth_data_by_device_id():
    print("do get_growth_data_by_device_id() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/growth_data_" + device_id + ".txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
        print(file_full_path)

        ## 4. make data to send to web
        sensor_json ={};
        field_date_list = []
        field_4_list = [] ## tp value
        field_6_list = [] ## rh value
        field_8_list = [] ## co2 value
        field_10_list = [] ## ppf value
        field_12_list = [] ## uv value
        field_14_list = [] ## uv_space value
        field_16_list = [] ## st value
        field_18_list = [] ## ec_space value
        field_20_list = [] ## sm_space value
        field_22_list = [] ## sm_space value
        field_24_list = [] ## sm_space value

        read_line_cnt = 0
        with  open(file_full_path,'r') as file_p: 
            ##read_contents = file_p.readline()
            read_contents = file_p.readline().splitlines()  ## list object

            while read_contents:
                #print(len(str(read_contents)))

                if read_contents is None :
                    break;

                #read_line_cnt = read_line_cnt + 1
                #if(read_line_cnt > 3):
                #    break;

                ##read_contents = file_p.readline().split()
                #print("================================")
                #print(read_contents)
                #print("here1")
                #print(type(read_contents))
                
                ##read_contents = json.dumps(read_contents, ensure_ascii=False)
                #print("here2")
                #print(type(read_contents))
                #read_contents_json_obj = json.dumps(read_contents, ensure_ascii=False)
                for sensor_dic in read_contents :
                    #print("here3")
                    #print(type(sensor_dic))
                    sensor_dic = ast.literal_eval(sensor_dic)
                    #print(sensor_dic)
                    #print(type(sensor_dic))
                    ##sensor_dic = ast.literal_eva(sensor_dic)
                    ##sensor_dic = json.load(sensor_dic)
        
            
                    temp_date = sensor_dic["field1"]
                    temp_time = sensor_dic["field2"]

                    #print(temp_date)
                    #print(temp_time)

                    temp_final_date_time = temp_date + " " + temp_time;

                    #print(temp_final_date_time)
                    time_obj = datetime.strptime(temp_final_date_time, "%Y-%m-%d %H:%M")
                    temp_start_date_obj= datetime.strptime(start_date, "%Y-%m-%d %H:%M")
                    temp_end_date_obj= datetime.strptime(end_date, "%Y-%m-%d %H:%M")
                    ##print(time_obj)
                    ##print(temp_start_date_obj)
                    ##print(temp_end_date_obj)

                    if time_obj > temp_start_date_obj and time_obj < temp_end_date_obj :
                        field_date_list.append(str(time_obj))  ## date
                        ##print(field_date_list)
                        field_4_list.append(sensor_dic["field4"])  ## tp value
                        field_6_list.append(sensor_dic["field6"])  ## tp value
                        field_8_list.append(sensor_dic["field8"])  ## tp value
                        field_10_list.append(sensor_dic["field10"])  ## tp value
                        field_12_list.append(sensor_dic["field12"])  ## tp value
                        field_14_list.append(sensor_dic["field14"])  ## tp value
                        field_16_list.append(sensor_dic["field16"])  ## tp value
                        field_18_list.append(sensor_dic["field18"])  ## tp value
                        field_20_list.append(sensor_dic["field20"])  ## tp value
                        field_22_list.append(sensor_dic["field22"])  ## tp value
                        field_24_list.append(sensor_dic["field24"])  ## tp value
                        ##print(field_4_list)

                    read_contents = file_p.readline().splitlines()  ## list object

                
        file_p.close()
        

        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 
        sensor_json.update({"date" : field_date_list}) 
        sensor_json.update({"field_4_list" : field_4_list})
        sensor_json.update({"field_6_list" : field_6_list})
        sensor_json.update({"field_8_list" : field_8_list})
        sensor_json.update({"field_10_list" : field_10_list})
        sensor_json.update({"field_12_list" : field_12_list})
        sensor_json.update({"field_14_list" : field_14_list})
        sensor_json.update({"field_16_list" : field_16_list})
        sensor_json.update({"field_18_list" : field_18_list})
        sensor_json.update({"field_20_list" : field_20_list})
        sensor_json.update({"field_22_list" : field_22_list})
        sensor_json.update({"field_24_list" : field_24_list})


        result_json ={};
        result_json.update({"result" : sensor_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 



@main_control.route('/get_pest_data_by_device_id_from_db')
def get_pest_data_by_device_id_from_db():
    print("do get_pest_data_by_device_id_from_db() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        ## 4. make data to send to web
   
        pest_json ={};

        field_date_list = []

        mite= []
        tick= []
        graymoldrot= []
        sphaerothecahumuli= []
        fusariumoxysporum= []
        phytophtoranicotianae= []

        '''
        start_date_ = datetime.strptime(str(start_date)+":00", '%Y-%m-%d %H:%M:%S')
        print("start_date_:" + str(start_date_))
        end_date_ = datetime.strptime(str(end_date)+":00", '%Y-%m-%d %H:%M:%S')
        print("end_date_:" + str(end_date_))
        '''
        ##start_date_ = datetime.strptime(str(start_date), '%Y%m%d')
        ##print("start_date_:" + str(start_date_))
        ##end_date_ = datetime.strptime(str(end_date), '%Y%m%d')
        ##print("end_date_:" + str(end_date_))


        start_date_ = int(start_date)
        print("start_date_:" + str(start_date_))
        end_date_ = int(end_date)
        print("end_date_:" + str(end_date_))


        ## 2021-08-11 00:41:10
        ##cast((GrowthDataDb.date + " 00:00:00"),DateTime) >= end_date_ 
       
        
        pest_data_list= db.session.query(PestDataDb)\
                         .filter( 
                                and_( PestDataDb.device_id == device_id , \
                                      cast((PestDataDb.date),Integer) >= start_date_   \
                                    , cast((PestDataDb.date),Integer) <= end_date_ 
                                    )
                                ).all()

        db.session.close()
        print("pest_data_list : " + str(pest_data_list))

        if( not pest_data_list is None):
            for pest in pest_data_list :
                
                field_date_list.append(str(pest.timestamp))  
                #print("str(pest.mite) : " + str(str(pest.mite)))
                mite.append(str(pest.mite))  ## tp value
                
                #print(" mite.append : " + str(str(mite)))
                
                tick.append(str(pest.tick) )  ## tp value
                graymoldrot.append(str(pest.graymoldrot) )  ## tp value
                sphaerothecahumuli.append(str(pest.sphaerothecahumuli) )  ## tp value
                fusariumoxysporum.append(str(pest.fusariumoxysporum) )  ## tp value
                phytophtoranicotianae.append(str(pest.phytophtoranicotianae) )  ## tp value
                '''
                mite.append(pest.mite)
                tick.append(pest.tick)
                graymoldrot.append(pest.graymoldrot)
                sphaerothecahumuli.append(pest.sphaerothecahumuli)
                fusariumoxysporum.append(pest.fusariumoxysporum)
                phytophtoranicotianae.append(pest.phytophtoranicotianae)
                '''

        pest_json.update({"device_id" : device_id}) 
        pest_json.update({"set_location" : set_location}) 
        pest_json.update({"date" : field_date_list}) 

        pest_json.update({"mite" : mite})
        pest_json.update({"tick" : tick})
        pest_json.update({"graymoldrot" : graymoldrot})
        pest_json.update({"sphaerothecahumuli" : sphaerothecahumuli})
        pest_json.update({"fusariumoxysporum" : fusariumoxysporum})
        pest_json.update({"phytophtoranicotianae" : phytophtoranicotianae})
        
        image_json = getImageFullListByDevice(device_id,start_date ,end_date)
        #print("image_json : "  + str(image_json))   
        pest_json.update(image_json)

        result_json ={};
        result_json.update({"result" : pest_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


@main_control.route('/get_pest_data_by_device_id')
def get_pest_data_by_device_id():
    print("do get_pest_data_by_device_id() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/pest_data_" + device_id + ".txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
        print(file_full_path)

        ## 4. make data to send to web
        sensor_json ={};
        field_date_list = []
        field_4_list = [] ## tp value
        field_6_list = [] ## rh value
        field_8_list = [] ## co2 value
        field_10_list = [] ## ppf value

        read_line_cnt = 0
        with  open(file_full_path,'r') as file_p: 
            ##read_contents = file_p.readline()
            read_contents = file_p.readline().splitlines()  ## list object

            while read_contents:
                #print(len(str(read_contents)))

                if read_contents is None :
                    break;

                #read_line_cnt = read_line_cnt + 1
                #if(read_line_cnt > 3):
                #    break;

                ##read_contents = file_p.readline().split()
                #print("================================")
                #print(read_contents)
                #print("here1")
                #print(type(read_contents))
                
                ##read_contents = json.dumps(read_contents, ensure_ascii=False)
                #print("here2")
                #print(type(read_contents))
                #read_contents_json_obj = json.dumps(read_contents, ensure_ascii=False)
                for sensor_dic in read_contents :
                    #print("here3")
                    #print(type(sensor_dic))
                    sensor_dic = ast.literal_eval(sensor_dic)
                    #print(sensor_dic)
                    #print(type(sensor_dic))
                    ##sensor_dic = ast.literal_eva(sensor_dic)
                    ##sensor_dic = json.load(sensor_dic)
        
            
                    temp_date = sensor_dic["field1"]
                    temp_time = sensor_dic["field2"]

                    #print(temp_date)
                    #print(temp_time)

                    temp_final_date_time = temp_date + " " + temp_time;

                    #print(temp_final_date_time)
                    time_obj = datetime.strptime(temp_final_date_time, "%Y-%m-%d %H:%M")
                    temp_start_date_obj= datetime.strptime(start_date, "%Y-%m-%d %H:%M")
                    temp_end_date_obj= datetime.strptime(end_date, "%Y-%m-%d %H:%M")
                    ##print(time_obj)
                    ##print(temp_start_date_obj)
                    ##print(temp_end_date_obj)

                    if time_obj > temp_start_date_obj and time_obj < temp_end_date_obj :
                        field_date_list.append(str(time_obj))  ## date
                        ##print(field_date_list)
                        field_4_list.append(sensor_dic["field4"])  ## tp value
                        field_6_list.append(sensor_dic["field6"])  ## tp value
                        field_8_list.append(sensor_dic["field8"])  ## tp value
                        field_10_list.append(sensor_dic["field10"])  ## tp value
                        ##print(field_4_list)

                    read_contents = file_p.readline().splitlines()  ## list object

                
        file_p.close()
        

        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 
        sensor_json.update({"date" : field_date_list}) 
        sensor_json.update({"field_4_list" : field_4_list})
        sensor_json.update({"field_6_list" : field_6_list})
        sensor_json.update({"field_8_list" : field_8_list})
        sensor_json.update({"field_10_list" : field_10_list})


        result_json ={};
        result_json.update({"result" : sensor_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 



@main_control.route('/get_disorder_data_by_device_id_from_db')
def get_disorder_data_by_device_id_from_db():
    print("do get_disorder_data_by_device_id_from_db() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        ## 4. make data to send to web
        sensor_json ={};
        field_date_list = []
        nitrogen = [] ## tp value
        phosphorus = [] ## rh value
        potassium = [] ## co2 value
        magnesium = [] ## ppf value
        calcium = [] ## uv value
        etc = [] ## uv_space value

        '''
        print("start_date:" + str(start_date))
        start_date_ = datetime.strptime(str(start_date)+":00", '%Y-%m-%d %H:%M:%S')
        print("start_date_:" + str(start_date_))
        end_date_ = datetime.strptime(str(end_date)+":00", '%Y-%m-%d %H:%M:%S')
        print("end_date_:" + str(end_date_))
        '''
        
        start_date_ = start_date[0:10]
        print("start_date_:" + str(start_date_))
        start_date_ = start_date_.replace('-','')
        print("start_date_:" + str(start_date_))
        start_date_ = int(start_date_)
        print("start_date_:" + str(start_date_))
        
        end_date_ = end_date[0:10]
        print("end_date_:" + str(end_date_))
        end_date_ = end_date_.replace('-','')
        print("end_date_:" + str(end_date_))
        end_date_ = int(end_date_)
        print("end_date_:" + str(end_date_))
        

        ## 2021-08-11 00:41:10
        senser_data_list= db.session.query(DisorderDataDb)\
                         .filter( 
                                and_( DisorderDataDb.device_id == device_id , \
                                      cast((DisorderDataDb.date),Integer) >= start_date_  \
                                    , cast((DisorderDataDb.date),Integer) <= end_date_ 
                                    )
                                ).all()
        db.session.close()
        print("wsenser_data_list : " + str(senser_data_list))

        if( not senser_data_list is None):
            for sensor in senser_data_list :
                
                field_date_list.append(str(sensor.date))  ## date

                nitrogen.append(sensor.nitrogen)  ## tp value
                phosphorus.append(sensor.phosphorus)  ## tp value
                potassium.append(sensor.potassium)  ## tp value
                magnesium.append(sensor.magnesium)  ## tp value
                calcium.append(sensor.calcium)  ## tp value
                etc.append(sensor.etc)  ## tp value


        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 
        sensor_json.update({"date" : field_date_list}) 
        sensor_json.update({"nitrogen" : nitrogen})
        sensor_json.update({"phosphorus" : phosphorus})
        sensor_json.update({"potassium" : potassium})
        sensor_json.update({"magnesium" : magnesium})
        sensor_json.update({"calcium" : calcium})
        sensor_json.update({"etc" : etc})
        #2010-01-01 12:00
        #search_date_converte = start_date[0:10].replace("-", "")
        #print("search_date_converte : "  + search_date_converte);
        #end_date_converte = end_date[0:10].replace("-", "")
        #image_json = getImageFullListByDevice(device_id,str(start_date_) ,str(end_date_))
        image_json = getImageFullListByDevice_only_issue(device_id,str(start_date_) ,str(end_date_))
        
        #print("image_json : "  + str(image_json))   
        sensor_json.update(image_json)


        result_json ={};
        result_json.update({"result" : sensor_json })
        print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 
    

## 생리장재정보를 이미지 데이터에서 얻어온다.
@main_control.route('/get_disorder_data_by_device_id_from_imagedata')
def get_disorder_data_by_device_id_from_imagedata():
    print("do get_disorder_data_by_device_id_from_imagedata() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        ## 4. make data to send to web
        sensor_json ={};
        field_date_list = []

        plague_level=[]

        fusariumoxysporum = [] 
        phytophtoranicotianae = [] 
        anthrax = [] 
        nitrogen = [] 

        phosphorus= [] 
        potassium= [] 
        magnesium= [] 
        calcium= [] 
        not_decide= [] 
        no_issue= [] 

        start_date_int = int(str(start_date))
        print("start_date_:" + str(start_date_int))

        end_date_int = int(str(end_date))
        print("end_date_:" + str(end_date_int))

        ## 2021-08-11 00:41:10
        '''
        senser_data_list= db.session.query(ImageBasicDataDb)\
                         .filter( 
                                and_( 
                                    ImageBasicDataDb.device_id == device_id, \
                                      cast((ImageBasicDataDb.targetDate +  " 00:00:00"),DateTime) >= start_date_  \
                                    , cast((ImageBasicDataDb.targetDate +  " 23:59:59"),DateTime) <= end_date_ 

                                    )
                                ).all()
        '''
        senser_data_list= db.session.query(ImageBasicDataDb)\
                         .filter( 
                                and_( 
                                    ImageBasicDataDb.device_id == device_id
                                    )
                                ).all()
        db.session.close()
        #print("senser_data_list : " + str(senser_data_list))

        if( not senser_data_list is None):

            for  sensor in senser_data_list :
                temp_date_int = int(str(sensor.targetDate))

                if( temp_date_int >= start_date_int  and temp_date_int <= end_date_int ):

                  
                    field_date_list.append(str(sensor.targetDate))
                    
                    if( not( sensor.plague_level is None ) and \
                            not (sensor.plague_level.isspace()) and \
                                    sensor.plague_level == "" ):
                        plague_level.append(str(sensor.plague_level))
                    
                    if( '시들음병' in str(sensor.plague_type) ):
                        fusariumoxysporum.append(sensor.plague_level)
                    else:
                        fusariumoxysporum.append("0")

                    if( '역병' in  str(sensor.plague_type)  ):
                        phytophtoranicotianae.append(sensor.plague_level)
                    else:
                        phytophtoranicotianae.append("0")

                    if( '탄저병' in str(sensor.plague_type)):
                        anthrax.append(sensor.plague_level)
                    else:
                        anthrax.append("0")


                    if('질소' in str(sensor.plague_type) ):
                        nitrogen.append(sensor.plague_level)
                    else:
                        nitrogen.append("0")

                    if( '인' in str(sensor.plague_type) ):
                        phosphorus.append(sensor.plague_level)
                    else:
                        phosphorus.append("0")

                    if( '칼륨' in str(sensor.plague_type)  ):
                        potassium.append(sensor.plague_level)
                    else:
                        potassium.append("0")

                    if( '마그네슘' in str(sensor.plague_type)):
                        magnesium.append(sensor.plague_level)
                    else:
                        magnesium.append("0")

                    if( '칼슘' in str(sensor.plague_type) ):
                        calcium.append(sensor.plague_level)
                    else:
                        calcium.append("0")

                    if( '판별불가' in str(sensor.plague_type) ):
                        not_decide.append(sensor.plague_level)
                    else:
                        not_decide.append("0")


                    if( '이상없음' in str(sensor.plague_type) ):
                        no_issue.append(sensor.plague_level)
                    else:
                        no_issue.append("0")

        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 

        sensor_json.update({"date" : field_date_list}) 
        
        sensor_json.update({"plague_level" : plague_level}) 

        sensor_json.update({"fusariumoxysporum" : fusariumoxysporum}) 
        sensor_json.update({"phytophtoranicotianae" : phytophtoranicotianae}) 
        sensor_json.update({"anthrax" : anthrax}) 
        sensor_json.update({"nitrogen" : nitrogen}) 

        sensor_json.update({"phosphorus" : phosphorus}) 
        sensor_json.update({"potassium" : potassium}) 
        sensor_json.update({"magnesium" : magnesium}) 
        sensor_json.update({"calcium" : calcium}) 
        sensor_json.update({"not_decide" : not_decide}) 
        sensor_json.update({"no_issue" : no_issue}) 

        result_json ={};
        result_json.update({"result" : sensor_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


@main_control.route('/set_disorder_data')
def set_disorder_data():
    print("do set_disorder_data() !!!")

    created_at = request.args.get('created_at', default = '', type = str)
    send_alarm = request.args.get('send_alarm', default = '', type = str)
    device_id = request.args.get('device_id', default = '', type = str)
    nitrogen = request.args.get('nitrogen', default = '', type = str)
    phosphorus = request.args.get('phosphorus', default = '', type = str)
    potassium = request.args.get('potassium', default = '', type = str)
    magnesium = request.args.get('magnesium', default = '', type = str)
    calcium = request.args.get('calcium', default = '', type = str)
    etc = request.args.get('etc', default = '', type = str)

    try:
        print("====================================================================")
        db.session.add(DisorderDataDb(created_at=created_at,send_alarm=send_alarm,device_id=device_id,\
                                        nitrogen=nitrogen,phosphorus=phosphorus,\
                                        potassium=potassium,magnesium=magnesium,\
                                        calcium=calcium,\
                                        etc=etc))
        db.session.commit()
        print("====================================================================")

        ## history update
        update_device_history(device_id,"재고")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})



@main_control.route('/get_disorder_data_by_device_id')
def get_disorder_data_by_device_id():
    print("do get_disorder_data_by_device_id() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    start_date= request.args.get('start_date', default = '', type = str) 
    end_date= request.args.get('end_date', default = '', type = str) 

    print("device_id :"  + device_id)
    print("start_date :"  + start_date)
    print("end_date :"  + end_date)
   
    ## 3. get file contents
    try:
       
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/disorder_data_" + device_id + ".txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
        print(file_full_path)

        ## 4. make data to send to web
        sensor_json ={};
        field_date_list = []
        field_4_list = [] ## tp value
        field_6_list = [] ## rh value
        field_8_list = [] ## co2 value
        field_10_list = [] ## ppf value
        field_12_list = [] ## ppf value
        field_14_list = [] ## ppf value

        read_line_cnt = 0
        with  open(file_full_path,'r') as file_p: 
            ##read_contents = file_p.readline()
            read_contents = file_p.readline().splitlines()  ## list object

            while read_contents:
                #print(len(str(read_contents)))

                if read_contents is None :
                    break;

                #read_line_cnt = read_line_cnt + 1
                #if(read_line_cnt > 3):
                #    break;

                ##read_contents = file_p.readline().split()
                #print("================================")
                #print(read_contents)
                #print("here1")
                #print(type(read_contents))
                
                ##read_contents = json.dumps(read_contents, ensure_ascii=False)
                #print("here2")
                #print(type(read_contents))
                #read_contents_json_obj = json.dumps(read_contents, ensure_ascii=False)
                for sensor_dic in read_contents :
                    #print("here3")
                    #print(type(sensor_dic))
                    sensor_dic = ast.literal_eval(sensor_dic)
                    #print(sensor_dic)
                    #print(type(sensor_dic))
                    ##sensor_dic = ast.literal_eva(sensor_dic)
                    ##sensor_dic = json.load(sensor_dic)
        
            
                    temp_date = sensor_dic["field1"]
                    temp_time = sensor_dic["field2"]

                    #print(temp_date)
                    #print(temp_time)

                    temp_final_date_time = temp_date + " " + temp_time;

                    #print(temp_final_date_time)
                    time_obj = datetime.strptime(temp_final_date_time, "%Y-%m-%d %H:%M")
                    temp_start_date_obj= datetime.strptime(start_date, "%Y-%m-%d %H:%M")
                    temp_end_date_obj= datetime.strptime(end_date, "%Y-%m-%d %H:%M")
                    ##print(time_obj)
                    ##print(temp_start_date_obj)
                    ##print(temp_end_date_obj)

                    if time_obj > temp_start_date_obj and time_obj < temp_end_date_obj :
                        field_date_list.append(str(time_obj))  ## date
                        ##print(field_date_list)
                        field_4_list.append(sensor_dic["field4"])  ## tp value
                        field_6_list.append(sensor_dic["field6"])  ## tp value
                        field_8_list.append(sensor_dic["field8"])  ## tp value
                        field_10_list.append(sensor_dic["field10"])  ## tp value
                        field_12_list.append(sensor_dic["field12"])  ## tp value
                        field_14_list.append(sensor_dic["field14"])  ## tp value
                        ##print(field_4_list)

                    read_contents = file_p.readline().splitlines()  ## list object

                
        file_p.close()
        

        sensor_json.update({"device_id" : device_id}) 
        sensor_json.update({"set_location" : set_location}) 
        sensor_json.update({"date" : field_date_list}) 
        sensor_json.update({"field_4_list" : field_4_list})
        sensor_json.update({"field_6_list" : field_6_list})
        sensor_json.update({"field_8_list" : field_8_list})
        sensor_json.update({"field_10_list" : field_10_list})
        sensor_json.update({"field_12_list" : field_12_list})
        sensor_json.update({"field_14_list" : field_14_list})


        result_json ={};
        result_json.update({"result" : sensor_json })
        #print("Final result_json : "  + str(result_json))    

        return jsonify(result_json)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


## 센서 데이터 인서트(파일&DB)
@main_control.route('/update_sensor',methods=['GET'])
def update_sensor():
    print("update_sensor !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    print("device_id.isspace() : " + str(device_id.isspace()));

    if(device_id =='' or device_id is None or device_id.isspace()):
         return jsonify({"result":"device_id is null!"}) 

    print("device_id :"  + device_id)

    field1= request.args.get('field1', default = '', type = str) 
    if(field1 =='' or field1 is None or field1.isspace()):
        print("result : field1 is null!")
        return jsonify({"result":"field1 is null!"}) 

    field2= request.args.get('field2', default = '', type = str)
    if(field2 =='' or field2 is None or field2.isspace()):
        print("result : field2 is null!")
        return jsonify({"result":"field2 is null!"}) 

    field3= request.args.get('field3', default = '', type = str) 
    if(field3 =='' or field3 is None or field3.isspace()):
        print("result : field3 is null!")
        return jsonify({"result":"field3 is null!"}) 

    field4= request.args.get('field4', default = '', type = str) 
    if(field4 =='' or field4 is None or field4.isspace()):
        print("result : field4 is null!")
        return jsonify({"result":"field4 is null!"}) 

    field5= request.args.get('field5', default = '', type = str) 
    if(field5 =='' or field5 is None or field5.isspace()):
        print("result : field5 is null!")
        return jsonify({"result":"field5 is null!"}) 

    field6= request.args.get('field6', default = '', type = str) 
    if(field6 =='' or field6 is None or field6.isspace()):
        print("result : field6 is null!")
        return jsonify({"result":"field6 is null!"}) 


    field7= request.args.get('field7', default = '', type = str) 
    if(field7 =='' or field7 is None or field7.isspace()):
        print("result : field7 is null!")
        return jsonify({"result":"field7 is null!"}) 

    field8= request.args.get('field8', default = '', type = str) 
    if(field8 =='' or field8 is None or field8.isspace()):
        print("result : field8 is null!")
        return jsonify({"result":"field8 is null!"}) 

    field9= request.args.get('field9', default = '', type = str) 
    if(field9 =='' or field9 is None or field9.isspace()):
        print("result : field9 is null!")
        return jsonify({"result":"field9 is null!"})

    field10= request.args.get('field10', default = '', type = str) 
    if(field10 =='' or field10 is None or field10.isspace()):
        print("result : field10 is null!")
        return jsonify({"result":"field10 is null!"})


    field11= request.args.get('field11', default = '', type = str) 
    if(field11 =='' or field11 is None or field11.isspace()):
        print("result : field11 is null!")
        return jsonify({"result":"field11 is null!"})

    field12= request.args.get('field12', default = '', type = str) 
    if(field12 =='' or field12 is None or field12.isspace()):
        print("result : field12 is null!")
        return jsonify({"result":"field12 is null!"})

    field13= request.args.get('field13', default = '', type = str) 
    if(field13 =='' or field13 is None or field13.isspace()):
        print("result : field13 is null!")
        return jsonify({"result":"field13 is null!"})

    field14= request.args.get('field14', default = '', type = str) 
    if(field14 =='' or field14 is None or field14.isspace()):
        print("result : field14 is null!")
        return jsonify({"result":"field14 is null!"})

    field15= request.args.get('field15', default = '', type = str)  
    if(field15 =='' or field15 is None or field15.isspace()):
        print("result : field115 is null!")
        return jsonify({"result":"field15 is null!"})

    field16= request.args.get('field16', default = '', type = str) 
    if(field16 =='' or field16 is None or field16.isspace()):
        print("result : field116 is null!")
        return jsonify({"result":"field16 is null!"})

    field17= request.args.get('field17', default = '', type = str) 
    if(field17 =='' or field17 is None or field17.isspace()):
        print("result : field117 is null!")
        return jsonify({"result":"field17 is null!"})

    field18= request.args.get('field18', default = '', type = str) 
    if(field18 =='' or field18 is None or field18.isspace()):
        print("result : field118 is null!")
        return jsonify({"result":"field18 is null!"})

    field19= request.args.get('field19', default = '', type = str) 
    if(field19 =='' or field19 is None or field19.isspace()):
        print("result : field119 is null!")
        return jsonify({"result":"field19 is null!"})

    field20= request.args.get('field20', default = '', type = str)
    if(field20 =='' or field20 is None or field20.isspace()):
        print("result : field120 is null!")
        return jsonify({"result":"field20 is null!"}) 


    sensor_data_list = []
    sensor_data_dic ={}
    
    sensor_data_dic.update({'device_id' : device_id}) 
    sensor_data_dic.update({'field1' : field1}) 
    sensor_data_dic.update({'field2' : field2}) 
    sensor_data_dic.update({'field3' : "TP"}) 
    sensor_data_dic.update({'field4' : field4}) 
    sensor_data_dic.update({'field5' : "RH"}) 
    sensor_data_dic.update({'field6' : field6}) 
    sensor_data_dic.update({'field7' : "CO2"}) 
    sensor_data_dic.update({'field8' : field8}) 
    sensor_data_dic.update({'field9' : "PPF"}) 
    sensor_data_dic.update({'field10' : field10}) 
    sensor_data_dic.update({'field11' : "PSP"}) 
    sensor_data_dic.update({'field12' : field12}) 
    sensor_data_dic.update({'field13' : "UV"}) 
    sensor_data_dic.update({'field14' : field14}) 
    sensor_data_dic.update({'field15' : "ST"}) 
    sensor_data_dic.update({'field16' : field16}) 
    sensor_data_dic.update({'field17' : "EC"}) 
    sensor_data_dic.update({'field18' : field18}) 
    sensor_data_dic.update({'field19' : "SM"}) 
    sensor_data_dic.update({'field20' : field20}) 
    ##print(sensor_data_dic)
    sensor_data_list.append(sensor_data_dic)

    print(sensor_data_list)

    try:
        ##############################################################
        ## 1. save file
        ##############################################################
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/sensor_data_" + device_id + ".txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
       
        print(file_full_path)
        ##print(type(sensor_data_list))
        
        with open(file_full_path,'a+') as file_p: 
            
            for sensor_data in sensor_data_list:
                    ##file_p.write(json.dumps(sensor_date, ensure_ascii=False))
                    ##print(sensor_data)
                    ##한줄씩 쓰기
                    ##file_p.write(json.dumps(sensor_data, ensure_ascii=False)+"\n")

                    print(type(sensor_data));  ## dic type
                    ## string type  --> 줄바꿈을 삽입하려면 스트링타입으로 저장하여야하고, 스트링타입으로 저장하면 쌍따옴표가 홑따옴표를 변경됨 , 읽을때 쌍따옴표로 변경하여 딕션너리로 변환
                    file_p.write(str(sensor_data)+"\n") ##print(sensor_data);
                    ##file_p.write('\n'.join(sensor_data))
                   
                    ##file_p.write("AAAAAAA\n")
            
        file_p.close()

        with open(file_full_path,'r') as file_p: 
            read_contents = file_p.read()
            print("================================")
            ##print(read_contents)
        file_p.close()
        
        print("file save !! ")
        ##############################################################
        ## 2. save file path to db -- ui will load this file
        ##############################################################
        # db file
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.data_file_path_prefix = file_full_path
        #upateDeviceRecord.sensor_data = sensor_data_list
        #upateDeviceRecord.sensor_data = "[{'device_id':'000000001',field1':'2021.09.20','field2':'12:13','field3:'10' }]"
        #upateDeviceRecord.sensor_data = json.dumps(sensor_data_list, ensure_ascii=False) ## python object --> json string with unicode
        db.session.commit()

        print("db save for file path !! ")
        ##############################################################
        ## 3. save law data to db -- we can use this just to check log
        ##############################################################
        db.session.add(SensorDataDb(\
            device_id=device_id,field1=field1,field2=field2,field3=field3,field4=field4,field5=field5,\
            field6=field6,field7=field7,field8=field8,field9=field9,field10=field10,\
            field11=field11,field12=field12,field13=field13,field14=field14,field15=field15,\
            field16=field16,field17=field17,field18=field18,field19=field19,field20=field20\
                ))
        db.session.commit()
        print("db save for low data!! ")
             
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


## 센서 데이터 인서트(파일&DB)
@main_control.route('/insert_sensor_data_to_device_and_file')
def insert_sensor_data_to_device_and_file():
    print("do insert_sensor_data_to_device_and_file() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    print("device_id :"  + device_id)

    sensor_data_list = []
    sensor_data_dic ={}
    

    temp_date = "2021-07-12"
    temp_str_minit = ""

    for hour_ in range(1,24):

        if hour_ < 10:
                temp_str_hour_="0" + str(hour_)
        else:
                temp_str_hour_=str(hour_)

        for minit_ in range(1,59,20):
            sensor_data_dic = {}
            sensor_data_dic.update({'device_id' : device_id}) 
            sensor_data_dic.update({'field1' : temp_date}) 
            if minit_ < 10:
                temp_str_minit="0" + str(minit_)
            else:
                temp_str_minit=str(minit_)

                
            sensor_data_dic.update({'field2' : temp_str_hour_+":"+temp_str_minit}) 
            sensor_data_dic.update({'field3' : "TP"}) 
            sensor_data_dic.update({'field4' : str(random.randint(32,40))}) 
            sensor_data_dic.update({'field5' : "RH"}) 
            sensor_data_dic.update({'field6' : str(random.randint(50,70))}) 
            sensor_data_dic.update({'field7' : "CO2"}) 
            sensor_data_dic.update({'field8' : str(random.randint(80,120))}) 
            sensor_data_dic.update({'field9' : "PPF"}) 
            sensor_data_dic.update({'field10' : str(random.randint(10,30))}) 
            sensor_data_dic.update({'field11' : "PSP"}) 
            sensor_data_dic.update({'field12' : str(random.randint(50,70))}) 
            sensor_data_dic.update({'field13' : "UV"}) 
            sensor_data_dic.update({'field14' : str(random.randint(50,70))}) 
            sensor_data_dic.update({'field15' : "ST"}) 
            sensor_data_dic.update({'field16' : str(random.randint(20,40))}) 
            sensor_data_dic.update({'field17' : "EC"}) 
            sensor_data_dic.update({'field18' : str(random.randint(1,3))}) 
            sensor_data_dic.update({'field19' : "SM"}) 
            sensor_data_dic.update({'field20' : str(random.randint(20,50))}) 
            ##print(sensor_data_dic)
            sensor_data_list.append(sensor_data_dic)
        

    ##print(sensor_data_list)
    print(type(sensor_data_list))

    try:
       
        file_basedir = os.path.dirname(__file__)
        file_name = "./static/data/sensor_data_" + device_id + ".txt"

        ##file_full_path= file_name
        file_full_path= os.path.join(file_basedir, file_name)
       
        print(file_full_path)
        ##print(type(sensor_data_list))
        
        with open(file_full_path,'a+') as file_p: 
            
            for sensor_data in sensor_data_list:
                    ##file_p.write(json.dumps(sensor_date, ensure_ascii=False))
                    ##print(sensor_data)
                    ##한줄씩 쓰기
                    ##file_p.write(json.dumps(sensor_data, ensure_ascii=False)+"\n")

                    print(type(sensor_data));  ## dic type
                    ## string type  --> 줄바꿈을 삽입하려면 스트링타입으로 저장하여야하고, 스트링타입으로 저장하면 쌍따옴표가 홑따옴표를 변경됨 , 읽을때 쌍따옴표로 변경하여 딕션너리로 변환
                    file_p.write(str(sensor_data)+"\n") ##print(sensor_data);
                    ##file_p.write('\n'.join(sensor_data))
                   
                    ##file_p.write("AAAAAAA\n")
            
        file_p.close()

        with open(file_full_path,'r') as file_p: 
            read_contents = file_p.read()
            print("================================")
            ##print(read_contents)
        file_p.close()
        

        # db file
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.data_file_path_prefix = file_full_path
        #upateDeviceRecord.sensor_data = sensor_data_list
        #upateDeviceRecord.sensor_data = "[{'device_id':'000000001',field1':'2021.09.20','field2':'12:13','field3:'10' }]"
        #upateDeviceRecord.sensor_data = json.dumps(sensor_data_list, ensure_ascii=False) ## python object --> json string with unicode
        db.session.commit()
      
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


## 센서 데이터 인서트
@main_control.route('/insert_sensor_data_to_device')
def insert_sensor_data_to_device():
    print("do insert_sensor_data_to_device() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    print("device_id :"  + device_id)

    sensor_data_list = []
    sensor_data_dic ={}
    

    temp_date = "2021.09.20"
    temp_str_minit = ""

    for hour_ in range(1,24):

        if hour_ < 10:
                temp_str_hour_="0" + str(hour_)
        else:
                temp_str_hour_=str(hour_)

        for minit_ in range(1,59):
            sensor_data_dic = {}
            sensor_data_dic.update({"device_id" : device_id}) 
            sensor_data_dic.update({"field1" : temp_date}) 
       
            if minit_ < 10:
                temp_str_minit="0" + str(minit_)
            else:
                temp_str_minit=str(minit_)

                
            sensor_data_dic.update({"field2" : temp_str_hour_+"."+temp_str_minit}) 
            sensor_data_dic.update({"field3" : str(random.randint(1,60))}) 
            print(sensor_data_dic)
            sensor_data_list.append(sensor_data_dic)
        
    
    print(sensor_data_list)
    

    
    try:
        
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        #upateDeviceRecord.sensor_data = sensor_data_list
        upateDeviceRecord.sensor_data = "[{'device_id':'000000001',field1':'2021.09.20','field2':'12:13','field3:'10' }]"
        #upateDeviceRecord.sensor_data = json.dumps(sensor_data_list, ensure_ascii=False) ## python object --> json string with unicode
        db.session.commit()

       
        
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"}) 


## 장치 정보 전달 ( 하우스 아이디 기준 )
@main_control.route('/get_device_data')
def get_device_data():
    print("do get_device_data() !!!")

    house_id= request.args.get('house_id', default = '', type = str) 

    print("house_id :"  + house_id)
    
    devices= db.session.query(DeviceDb).filter(DeviceDb.house_id == house_id).all()
    db.session.commit()

    print(devices)
 
    print("====================================================================")
    
    result_json ={};
    device_json ={};
    device_list_temp = []
    
    for device in devices:

        device_json.update({"device_id" : device.device_id})
        device_json.update({"created_at" : device.created_at})

        device_json.update({"type" : device.type}) 
        device_json.update({"sale_date" : device.sale_date}) 
        device_json.update({"contents" : device.contents})
        device_json.update({"status" : device.status}) 
        device_json.update({"remark" : device.remark}) 
        device_json.update({"usage_history_list" : device.usage_history_list}) 
        device_json.update({"farm_id" : device.farm_id}) 
        device_json.update({"farm_name" : device.farm_name}) 

        device_json.update({"house_id" : device.house_id})
        device_json.update({"house_name" : device.house_name}) 
        device_json.update({"mac_address" : device.mac_address}) 
        device_json.update({"set_location" : device.set_location}) 

        device_json.update({"data_file_path_prefix" : device.data_file_path_prefix}) 

        device_list_temp.append(device_json)

        device_json = {} ## reset


    result_json.update({"devices" : device_list_temp})

    #print("Final result_json : "  + str(result_json))    
    print("====================================================================")

    return jsonify({"result":result_json})


## 장치 정보 전달 ( 하우스 아이디 기준 )
@main_control.route('/get_device_data_only_ena')
def get_device_data_only_ena():
    print("do get_device_data_only_ena() !!!")

    house_id= request.args.get('house_id', default = '', type = str) 
    print("house_id :"  + house_id)
    
    devices= db.session.query(DeviceDb).filter(and_(DeviceDb.house_id == house_id,DeviceDb.type.contains("에나"))).all()
    db.session.commit()

    print(devices)
    print("====================================================================")
    
    result_json ={};
    device_json ={};
    device_list_temp = []
    
    for device in devices:
   
        device_json.update({"device_id" : device.device_id})
        device_json.update({"created_at" : device.created_at})

        device_json.update({"type" : device.type}) 
        device_json.update({"sale_date" : device.sale_date}) 
        device_json.update({"contents" : device.contents})
        device_json.update({"status" : device.status}) 
        device_json.update({"remark" : device.remark}) 
        device_json.update({"usage_history_list" : device.usage_history_list}) 
        device_json.update({"farm_id" : device.farm_id}) 
        device_json.update({"farm_name" : device.farm_name}) 

        device_json.update({"house_id" : device.house_id})
        device_json.update({"house_name" : device.house_name}) 
        device_json.update({"mac_address" : device.mac_address}) 
        device_json.update({"set_location" : device.set_location}) 

        device_json.update({"data_file_path_prefix" : device.data_file_path_prefix}) 

        device_list_temp.append(device_json)

        device_json = {} ## reset


    result_json.update({"devices" : device_list_temp})

    #print("Final result_json : "  + str(result_json))    
    print("====================================================================")

    return jsonify({"result":result_json})

@main_control.route('/get_device_by_farmid',methods=['GET','POST'])
def get_device_by_farmid():
    print("do get_device_by_farmid()!!!")


    farmid = str(request.args.get('farmid', default = '', type = str) )
    print("farmid : " + farmid)

    try :
        
        devices= db.session.query(DeviceDb).filter(DeviceDb.farm_id == farmid ).all()
        print("devices : " + str(devices))

        devices_list = []
        for device in devices:
            devices_list.append(device.device_id)
        
        print("devices_list : " + str(devices_list))

        return jsonify({"result":devices_list})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})



@main_control.route('/device_list_main/<int:page_num>')
def device_list_main(page_num):
    print("do device_list_main()!!!")


    search_cloumn = str(request.args.get('search_cloumn', default = '', type = str) )
    search_keyword= str(request.args.get('search_keyword', default = '', type = str) )

    print("search_cloumn:" + search_cloumn)
    print("search_keyword:" + search_keyword)

    print(len(search_cloumn))
    ##

    if request.method == 'GET' :
        try:
            if (search_cloumn=='type'):
                     
                if len(search_cloumn) != 0 : 
                
                    ##filter_query = "DeviceDb.status.contains('구매')"

                    devices = DeviceDb.query.outerjoin(FarmDb, FarmDb.farm_id == DeviceDb.farm_id).\
                                add_columns(DeviceDb.device_id,\
                                            func.ifnull(DeviceDb.type,'').label("type") ,\
                                            func.ifnull(DeviceDb.mac_address,'').label("mac_address"),\
                                            func.ifnull(DeviceDb.created_at,'').label("created_at"),\
                                            func.ifnull(DeviceDb.status,'').label("status"),\
                                            func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                            func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                            func.ifnull(DeviceDb.farm_id,'').label("farm_id"),\
                                            func.ifnull(DeviceDb.farm_name,'').label("farm_name"),\
                                            func.ifnull(DeviceDb.sale_date,'').label("sale_date"),\
                                            func.ifnull(DeviceDb.set_location,'').label("set_location"),\
                                            func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                            func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                            func.ifnull(FarmDb.user_mobile,'').label("user_mobile")).\
                                        filter(DeviceDb.type.contains(search_keyword)).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                

                else:
                    devices = DeviceDb.query.outerjoin(DeviceDb, DeviceDb.farm_id==FarmDb.farm_id).\
                                add_columns(DeviceDb.device_id,\
                                              func.ifnull(DeviceDb.type,'').label("type") ,\
                                            func.ifnull(DeviceDb.mac_address,'').label("mac_address"),\
                                            func.ifnull(DeviceDb.created_at,'').label("created_at"),\
                                            func.ifnull(DeviceDb.status,'').label("status"),\
                                            func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                            func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                            func.ifnull(DeviceDb.farm_id,'').label("farm_id"),\
                                            func.ifnull(DeviceDb.farm_name,'').label("farm_name"),\
                                            func.ifnull(DeviceDb.sale_date,'').label("sale_date"),\
                                            func.ifnull(DeviceDb.set_location,'').label("set_location"),\
                                            func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                            func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                            func.ifnull(FarmDb.user_mobile,'').label("user_mobile")).\
                                                paginate(per_page=per_page_count, page=page_num, error_out=True)

            elif (search_cloumn=='status'):
                if len(search_cloumn) != 0 : 
                
                    ##filter_query = "DeviceDb.status.contains('구매')"

                    devices = DeviceDb.query.outerjoin(FarmDb, FarmDb.farm_id == DeviceDb.farm_id).\
                                add_columns(DeviceDb.device_id,\
                                            func.ifnull(DeviceDb.type,'').label("type") ,\
                                            func.ifnull(DeviceDb.mac_address,'').label("mac_address"),\
                                            func.ifnull(DeviceDb.created_at,'').label("created_at"),\
                                            func.ifnull(DeviceDb.status,'').label("status"),\
                                            func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                            func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                            func.ifnull(DeviceDb.farm_id,'').label("farm_id"),\
                                            func.ifnull(DeviceDb.farm_name,'').label("farm_name"),\
                                            func.ifnull(DeviceDb.sale_date,'').label("sale_date"),\
                                            func.ifnull(DeviceDb.set_location,'').label("set_location"),\
                                            func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                            func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                            func.ifnull(FarmDb.user_mobile,'').label("user_mobile")).\
                                        filter(DeviceDb.status.contains(search_keyword)).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                

                else:
                    devices = DeviceDb.query.outerjoin(DeviceDb, DeviceDb.farm_id==FarmDb.farm_id).\
                                add_columns(DeviceDb.device_id,\
                                            func.ifnull(DeviceDb.type,'').label("type") ,\
                                            func.ifnull(DeviceDb.mac_address,'').label("mac_address"),\
                                            func.ifnull(DeviceDb.created_at,'').label("created_at"),\
                                            func.ifnull(DeviceDb.status,'').label("status"),\
                                            func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                            func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                            func.ifnull(DeviceDb.farm_id,'').label("farm_id"),\
                                            func.ifnull(DeviceDb.farm_name,'').label("farm_name"),\
                                            func.ifnull(DeviceDb.sale_date,'').label("sale_date"),\
                                            func.ifnull(DeviceDb.set_location,'').label("set_location"),\
                                            func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                            func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                            func.ifnull(FarmDb.user_mobile,'').label("user_mobile")).\
                                            paginate(per_page=per_page_count, page=page_num, error_out=True)
            else:

                    devices = DeviceDb.query.outerjoin(FarmDb, DeviceDb.farm_id==FarmDb.farm_id).\
                                add_columns(DeviceDb.device_id,\
                                            func.ifnull(DeviceDb.type,'').label("type") ,\
                                            func.ifnull(DeviceDb.mac_address,'').label("mac_address"),\
                                            func.ifnull(DeviceDb.created_at,'').label("created_at"),\
                                            func.ifnull(DeviceDb.status,'').label("status"),\
                                            func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                            func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                            func.ifnull(DeviceDb.farm_id,'').label("farm_id"),\
                                            func.ifnull(DeviceDb.farm_name,'').label("farm_name"),\
                                            func.ifnull(DeviceDb.sale_date,'').label("sale_date"),\
                                            func.ifnull(DeviceDb.set_location,'').label("set_location"),\
                                            func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                            func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                            func.ifnull(FarmDb.user_mobile,'').label("user_mobile")).\
                                            paginate(per_page=per_page_count, page=page_num, error_out=True)

           
            
            print(devices.items)

            url ="main_control.device_list_main";
            
            return render_template('/devices/device_list_main.html', devices=devices,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

'''
@main_control.route('/device_list_main_orderby/<int:page_num>',methods=['GET','POST'])
def device_list_main_orderby(page_num):

    print("device_list_main_orderby()")

    if request.method == 'GET' :
        try:

            search_text= str(request.args.get('search_keyword', default = '', type = str) )

            search_cloumn = str(request.args.get('search_cloumn', default = '', type = str) )
            search_keyword= str(request.args.get('search_keyword', default = '', type = str) )



            devices = DeviceDb.query.join(DeviceDb, DeviceDb.farm_id==FarmDb.farm_id).\
                        add_columns(DeviceDb.device_id,DeviceDb.type,DeviceDb.mac_address,DeviceDb.created_at,\
                                    DeviceDb.created_at,DeviceDb.status,DeviceDb.farm_id,DeviceDb.house_id,DeviceDb.house_name,\
                                    DeviceDb.farm_id,FarmDb.user_id,FarmDb.user_name,FarmDb.user_mobile).\
                                    filter(DeviceDb.type.like(search_text) ).\
                                      paginate(per_page=per_page_count, page=page_num, error_out=True)

            print(devices.items)
            url = 'main_control.device_list_main_orderby'
            return render_template('/devices/device_list_main.html', devices=devices,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204
'''

@main_control.route('/device_list_main_search/<int:page_num>',methods=['GET','POST'])
def device_list_main_search(page_num):

    print("device_list_main_search()")

    if request.method == 'GET' :
        try:
            print("reqeust querystring:" + request.args.get("search_keyword",""))
            search_text= str(request.args.get('search_keyword', default = '', type = str) )

            search_cloumn = str(request.args.get('search_cloumn', default = '', type = str) )
            search_keyword= str(request.args.get('search_keyword', default = '', type = str) )



            devices = DeviceDb.query.join(DeviceDb, DeviceDb.farm_id==FarmDb.farm_id).\
                                add_columns(DeviceDb.device_id,\
                                            func.ifnull(DeviceDb.type,'').label("type") ,\
                                            func.ifnull(DeviceDb.mac_address,'').label("mac_address"),\
                                            func.ifnull(DeviceDb.created_at,'').label("created_at"),\
                                            func.ifnull(DeviceDb.status,'').label("status"),\
                                            func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                            func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                            func.ifnull(DeviceDb.farm_id,'').label("farm_id"),\
                                            func.ifnull(DeviceDb.farm_name,'').label("farm_name"),\
                                            func.ifnull(DeviceDb.sale_date,'').label("sale_date"),\
                                            func.ifnull(DeviceDb.set_location,'').label("set_location"),\
                                            func.ifnull(FarmDb.user_id,'').label("user_id"),\
                                            func.ifnull(FarmDb.user_name,'').label("user_name"),\
                                            func.ifnull(FarmDb.user_mobile,'').label("user_mobile")).\
                                    filter(or_(DeviceDb.device_id.contains(search_text))).\
                                      paginate(per_page=per_page_count, page=page_num, error_out=True)


            url = 'main_control.device_list_main_search'
            return render_template('/devices/device_list_main.html', devices=devices,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204




@main_control.route('/device_delete')
def device_delete():
  
    print("do device_uninstall !!!")
    device_id= request.args.get('device_id', default = '', type = str) 

    try:
        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.status = "패기"

        db.session.commit()

        ## history update
        update_device_history(device_id,"패기")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

@main_control.route('/device_uninstall')
def device_uninstall():
  
    print("do device_uninstall !!!")
    device_id= request.args.get('device_id', default = '', type = str) 

    try:

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.status = "재고"
        upateDeviceRecord.farm_id=""
        upateDeviceRecord.house_id=""


        db.session.commit()

        ## history update
        update_device_history(device_id,"재고")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/device_in_stock')
def device_in_stock():
  
    print("do device_in_stock !!!")

    device_type= request.args.get('device_type', default = '', type = str) 
    device_id= request.args.get('device_id', default = '', type = str) 
    device_created_at= request.args.get('device_created_at', default = '', type = str) 
    device_remark= request.args.get('device_remark', default = '', type = str) 

    print(device_created_at);

    try:
        

        print("====================================================================")
        db.session.add(DeviceDb(type=device_type,device_id=device_id,created_at=device_created_at,remark=device_remark,status='재고'))
        db.session.commit()
        print("====================================================================")


        ## history update
        update_device_history(device_id,"재고")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/device_in_sail')
def device_in_sail():
  
    print("do device_in_sail !!!")

    device_id= request.args.get('device_id', default = '', type = str) 
    farm_id= request.args.get('farm_id', default = '', type = str) 
    sale_date= request.args.get('sale_date', default = '', type = str) 
    device_status= request.args.get('device_status', default = '', type = str) 
    device_set_location= request.args.get('device_set_location', default = '', type = str) 

    print("{}-{}-{}-{}-{}".format(device_id,farm_id,sale_date,device_status,device_set_location))


    ############################################################################
    ## validation
    if(farm_id=="" or farm_id=="undefined"):
       farm_id = ""
    if(sale_date=="" or sale_date=="undefined"):
       sale_date = ""
    if(device_status=="" or device_status=="undefined"):
       device_status = ""
    if(device_set_location=="" or device_set_location=="undefined"):
       device_set_location = ""
    ############################################################################

    try:
      
        

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.farm_id = farm_id

        temp_farm_name = get_form_name(farm_id)
        upateDeviceRecord.farm_name = temp_farm_name

        upateDeviceRecord.sale_date = sale_date
        upateDeviceRecord.status = device_status
        upateDeviceRecord.set_location = device_set_location
        db.session.commit()


        ## history update
        update_device_history(device_id,device_status+ "/" + temp_farm_name)

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/device_edit_sail')
def device_edit_sail():
  
    print("do device_edit_sail !!!")
    device_id= request.args.get('device_id', default = '', type = str) 
    mac_address= request.args.get('mac_address', default = '', type = str) 
    type= request.args.get('type', default = '', type = str) 
    status= request.args.get('status', default = '', type = str) 
    set_location= request.args.get('set_location', default = '', type = str) 
    farm_id= request.args.get('farm_id', default = '', type = str) 
    sale_date= request.args.get('sale_date', default = '', type = str) 

    print("{}-{}-{}-{}-{}-{}".format(device_id,mac_address,type,status,farm_id,sale_date))

    ############################################################################
    ## validation
    if(mac_address=="" or mac_address=="undefined"):
       mac_address = ""
    if(type=="" or type=="undefined"):
       type = ""
    if(status=="" or status=="undefined"):
       status = ""
    if(set_location=="" or set_location=="undefined"):
       set_location = ""
    if(farm_id=="" or farm_id=="undefined"):
       farm_id = ""
    if(sale_date=="" or sale_date=="undefined"):
       sale_date = ""
    ############################################################################


    try:
      
        

        upateQery = db.session.query(DeviceDb)
        upateQery = upateQery.filter(DeviceDb.device_id==device_id)
        upateDeviceRecord = upateQery.one()
        ##upateDeviceRecord.device_id = device_id
        upateDeviceRecord.mac_address = mac_address
        upateDeviceRecord.type = type
        upateDeviceRecord.status = status
        upateDeviceRecord.set_location = set_location
        upateDeviceRecord.farm_id = farm_id
        upateDeviceRecord.sale_date = sale_date

        temp_farm_name = get_form_name(farm_id)
        upateDeviceRecord.farm_name = temp_farm_name


        db.session.commit()

        ## history update
        update_device_history(device_id,status+ "/" + temp_farm_name)

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


def get_form_name(farm_id):
    
    devices = FarmDb.query.add_columns(FarmDb.farm_name).\
                                    filter(or_(FarmDb.farm_id.contains(farm_id)))
    
    for farm in devices:
        re_farm_name = farm.farm_name
    
    print("farm name  : {}".format(re_farm_name))
    return re_farm_name

@main_control.route('/get_device_usage_history')
def web_get_update_device_history_from_db():


    device_id= request.args.get('device_id', default = '', type = str) 
    print("web_get_update_device_history_from_db  : {}".format(str(device_id)))
    
    devices = DeviceDb.query.add_columns(DeviceDb.usage_history_list).\
                                    filter(or_(DeviceDb.device_id.contains(device_id)))

    print(type(devices))
    print(devices.count())
    
    for devcice in devices:
            if(devcice is None):
                return jsonify({"result":"No"})
            else:
                usage_history_list_ = devcice.usage_history_list
                print("aaa: " + usage_history_list_)
                
                if(usage_history_list_ == ""):
                    print("usage_history_list_ == ''")
                    return jsonify({"result":"No"})
                else:
                    json_usage_history_list_ = json.loads(usage_history_list_)
                    print("from usage_history_list_  : {}".format(str(json_usage_history_list_)))
                    return jsonify({"result":json_usage_history_list_})
        
    
    return jsonify({"result":"No"})


def get_update_device_history_from_db(device_id):
    
    devices = DeviceDb.query.add_columns(DeviceDb.usage_history_list).\
                                    filter(or_(DeviceDb.device_id.contains(device_id)))
    
    for history in devices:
        usage_history_list_ = history.usage_history_list

    if(usage_history_list_ is None):
        usage_history_list_ = "";
    
    print("from usage_history_list_  : {}".format(str(usage_history_list_)))
    return usage_history_list_


def update_device_history(device_id,event):

    history_root ={};
    history_list = []
    new_history_content ={};
    

    ## 이전 정보 로딩
    tempstr= get_update_device_history_from_db(device_id)

    print(type(tempstr))

    if  not(tempstr == ""):
        print("here00");
        tempstr=json.loads(tempstr)  ## json string to python object
        print("here0");
        print(type(tempstr));
        print("here1");
        print("tempstr" + str(tempstr));
        if not(tempstr is None):
            print("here2");
            for item in tempstr:
                print(type(item))
                ##item_s=json.loads(item)

                new_history_content.update({"date" : item['date']} )
                new_history_content.update({"event" : item['event']})
                history_list.append(new_history_content)
                new_history_content = {}
        else:
            print("here3");

    ##print("history_list1 : " + str(history_list))

    ## 신규 정보 로딩
    date_ = datetime.today().strftime('%Y.%m.%d %H:%M:%S')
    new_history_content.update({"date" : date_})
    new_history_content.update({"event" : event})
    history_list.append(new_history_content)
    ##print("history_list2 : " + str(history_list))
 

    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    upateQery = db.session.query(DeviceDb)
    upateQery = upateQery.filter(DeviceDb.device_id==device_id)
    upateDeviceRecord = upateQery.one()
    ##upateDeviceRecord.device_id = device_id
    upateDeviceRecord.usage_history_list = json.dumps(history_list, ensure_ascii=False) ## python object --> json string with unicode
       

    db.session.commit()


## device defect

@main_control.route('/device_defect_list/<int:page_num>')
def device_defect_list(page_num):
    print("do device_defect_list()")

    search_keyword= request.args.get("search_keyword","")
    search_cloumn= request.args.get("search_cloumn","")
    print("reqeust search_keyword:" + search_keyword)
    print("reqeust search_cloumn:" + search_cloumn)

    try :
        if( search_cloumn and  not(search_cloumn.isspace()) ):
            if search_cloumn == "category" :
                defects= DeviceDefectDb.query.\
                                    add_columns(DeviceDefectDb.id,\
                                                        func.ifnull(DeviceDefectDb.created_at,'').label("created_at") ,\
                                                        func.ifnull(DeviceDefectDb.category,'').label("category"),\
                                                        func.ifnull(DeviceDefectDb.device_id,'').label("device_id"),\
                                                        func.ifnull(DeviceDefectDb.error_code,'').label("error_code"),\
                                                        func.ifnull(DeviceDefectDb.detail_content,'').label("detail_content"),\
                                                        func.ifnull(DeviceDefectDb.user_id,'').label("user_id"),\
                                                        func.ifnull(DeviceDefectDb.user_name,'').label("user_name"),\
                                                        func.ifnull(DeviceDefectDb.user_tel,'').label("user_tel"),\
                                                        func.ifnull(DeviceDefectDb.proc_yn,'').label("proc_yn"),\
                                                        func.ifnull(DeviceDefectDb.proc_date,'').label("proc_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_yn,'').label("fix_yn"),\
                                                        func.ifnull(DeviceDefectDb.fix_accept_date,'').label("fix_accept_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_fish_date,'').label("fix_fish_date"),\


                                                        func.ifnull(DeviceDefectDb.fix_content,'').label("fix_content_all"),\
                                                        func.ifnull(func.substr(DeviceDefectDb.fix_content,0,10),'').label("fix_content")).\
                                                        filter(  DeviceDefectDb.category.like(search_keyword) ).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
        
            elif search_cloumn == "proc_yn" :
                defects= DeviceDefectDb.query.\
                                    add_columns(DeviceDefectDb.id,\
                                                        func.ifnull(DeviceDefectDb.created_at,'').label("created_at") ,\
                                                        func.ifnull(DeviceDefectDb.category,'').label("category"),\
                                                        func.ifnull(DeviceDefectDb.device_id,'').label("device_id"),\
                                                        func.ifnull(DeviceDefectDb.error_code,'').label("error_code"),\
                                                        func.ifnull(DeviceDefectDb.detail_content,'').label("detail_content"),\
                                                        func.ifnull(DeviceDefectDb.user_id,'').label("user_id"),\
                                                        func.ifnull(DeviceDefectDb.user_name,'').label("user_name"),\
                                                        func.ifnull(DeviceDefectDb.house_id,'').label("house_id"),\
                                                        func.ifnull(DeviceDefectDb.house_name,'').label("house_name"),\
                                                        func.ifnull(DeviceDefectDb.user_tel,'').label("user_tel"),\
                                                        func.ifnull(DeviceDefectDb.proc_yn,'').label("proc_yn"),\
                                                        func.ifnull(DeviceDefectDb.proc_date,'').label("proc_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_yn,'').label("fix_yn"),\
                                                        func.ifnull(DeviceDefectDb.fix_accept_date,'').label("fix_accept_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_fish_date,'').label("fix_fish_date"),\


                                                        func.ifnull(DeviceDefectDb.fix_content,'').label("fix_content_all"),\
                                                        func.ifnull(func.substr(DeviceDefectDb.fix_content,0,10),'').label("fix_content")).\
                                                        filter(  DeviceDefectDb.proc_yn.like(search_keyword) ).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)

        
            elif search_cloumn == "fix_yn" :
                defects= DeviceDefectDb.query.\
                                    add_columns(DeviceDefectDb.id,\
                                                        func.ifnull(DeviceDefectDb.created_at,'').label("created_at") ,\
                                                        func.ifnull(DeviceDefectDb.category,'').label("category"),\
                                                        func.ifnull(DeviceDefectDb.device_id,'').label("device_id"),\
                                                        func.ifnull(DeviceDefectDb.error_code,'').label("error_code"),\
                                                        func.ifnull(DeviceDefectDb.detail_content,'').label("detail_content"),\
                                                        func.ifnull(DeviceDefectDb.user_id,'').label("user_id"),\
                                                        func.ifnull(DeviceDefectDb.user_name,'').label("user_name"),\
                                                        func.ifnull(DeviceDefectDb.house_id,'').label("house_id"),\
                                                        func.ifnull(DeviceDefectDb.house_name,'').label("house_name"),\
                                                        func.ifnull(DeviceDefectDb.user_tel,'').label("user_tel"),\
                                                        func.ifnull(DeviceDefectDb.proc_yn,'').label("proc_yn"),\
                                                        func.ifnull(DeviceDefectDb.proc_date,'').label("proc_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_yn,'').label("fix_yn"),\
                                                        func.ifnull(DeviceDefectDb.fix_accept_date,'').label("fix_accept_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_fish_date,'').label("fix_fish_date"),\


                                                        func.ifnull(DeviceDefectDb.fix_content,'').label("fix_content_all"),\
                                                        func.ifnull(func.substr(DeviceDefectDb.fix_content,0,10),'').label("fix_content")).\
                                                        filter(  DeviceDefectDb.fix_yn.like(search_keyword) ).\
                                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
            elif search_cloumn == "all" :
                defects= DeviceDefectDb.query.\
                                add_columns(DeviceDefectDb.id,\
                                                    func.ifnull(DeviceDefectDb.created_at,'').label("created_at") ,\
                                                    func.ifnull(DeviceDefectDb.category,'').label("category"),\
                                                    func.ifnull(DeviceDefectDb.device_id,'').label("device_id"),\
                                                    func.ifnull(DeviceDefectDb.error_code,'').label("error_code"),\
                                                    func.ifnull(DeviceDefectDb.detail_content,'').label("detail_content"),\
                                                    func.ifnull(DeviceDefectDb.user_id,'').label("user_id"),\
                                                    func.ifnull(DeviceDefectDb.user_name,'').label("user_name"),\
                                                    func.ifnull(DeviceDefectDb.house_id,'').label("house_id"),\
                                                    func.ifnull(DeviceDefectDb.house_name,'').label("house_name"),\
                                                    func.ifnull(DeviceDefectDb.user_tel,'').label("user_tel"),\
                                                    func.ifnull(DeviceDefectDb.proc_yn,'').label("proc_yn"),\
                                                    func.ifnull(DeviceDefectDb.proc_date,'').label("proc_date"),\
                                                    func.ifnull(DeviceDefectDb.fix_yn,'').label("fix_yn"),\
                                                    func.ifnull(DeviceDefectDb.fix_accept_date,'').label("fix_accept_date"),\
                                                    func.ifnull(DeviceDefectDb.fix_fish_date,'').label("fix_fish_date"),\


                                                    func.ifnull(DeviceDefectDb.fix_content,'').label("fix_content_all"),\
                                                    func.ifnull(func.substr(DeviceDefectDb.fix_content,0,10),'').label("fix_content")).\
                                                    filter( \
                                                                or_(DeviceDefectDb.created_at.contains(search_keyword), 
                                                                    DeviceDefectDb.category.contains(search_keyword),
                                                                    DeviceDefectDb.device_id.contains(search_keyword),
                                                                    DeviceDefectDb.error_code.contains(search_keyword),
                                                                    DeviceDefectDb.detail_content.contains(search_keyword),
                                                                    DeviceDefectDb.user_id.contains(search_keyword),
                                                                    DeviceDefectDb.user_name.contains(search_keyword),
                                                                    DeviceDefectDb.house_name.contains(search_keyword),
                                                                    DeviceDefectDb.house_id.contains(search_keyword),
                                                                    DeviceDefectDb.house_name.contains(search_keyword),
                                                                    DeviceDefectDb.house_name.contains(search_keyword),
                                                                    DeviceDefectDb.house_name.contains(search_keyword),
                                                                    DeviceDefectDb.user_tel.contains(search_keyword),
                                                                    DeviceDefectDb.proc_yn.contains(search_keyword),
                                                                    DeviceDefectDb.proc_date.contains(search_keyword),
                                                                    DeviceDefectDb.fix_yn.contains(search_keyword),
                                                                    DeviceDefectDb.fix_accept_date.contains(search_keyword),
                                                                    DeviceDefectDb.fix_fish_date.contains(search_keyword)
                                                            
                                                            )).\
                                                    paginate(per_page=per_page_count, page=page_num, error_out=True)



        else:
                print("here all")
                defects= DeviceDefectDb.query.\
                                    add_columns(DeviceDefectDb.id,\
                                                        func.ifnull(DeviceDefectDb.created_at,'').label("created_at") ,\
                                                        func.ifnull(DeviceDefectDb.category,'').label("category"),\
                                                        func.ifnull(DeviceDefectDb.device_id,'').label("device_id"),\
                                                        func.ifnull(DeviceDefectDb.error_code,'').label("error_code"),\
                                                        func.ifnull(DeviceDefectDb.detail_content,'').label("detail_content"),\
                                                        func.ifnull(DeviceDefectDb.user_id,'').label("user_id"),\
                                                        func.ifnull(DeviceDefectDb.user_name,'').label("user_name"),\
                                                        func.ifnull(DeviceDefectDb.house_id,'').label("house_id"),\
                                                        func.ifnull(DeviceDefectDb.house_name,'').label("house_name"),\
                                                        func.ifnull(DeviceDefectDb.user_tel,'').label("user_tel"),\
                                                        func.ifnull(DeviceDefectDb.proc_yn,'').label("proc_yn"),\
                                                        func.ifnull(DeviceDefectDb.proc_date,'').label("proc_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_yn,'').label("fix_yn"),\
                                                        func.ifnull(DeviceDefectDb.fix_accept_date,'').label("fix_accept_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_fish_date,'').label("fix_fish_date"),\
                                                        func.ifnull(DeviceDefectDb.fix_content,'').label("fix_content_all"),\
                                                        func.ifnull(func.substr(DeviceDefectDb.fix_content,0,10),'').label("fix_content")).\
                               paginate(per_page=per_page_count, page=page_num, error_out=True)

        print(len(defects.items))

        for defect in defects.items:
            print(defect.created_at)
        
      
    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

    url ="main_control.device_defect_list";
    return render_template('/devices/device_defect_list.html', defects=defects , url=url,search_keyword=search_keyword ,search_cloumn=search_cloumn )

@main_control.route('/device_fix_save_data',methods=['GET','POST'])
def device_fix_save_data():
    print("do device_fix_save_data()")

    if request.method == 'POST': 
        try:
            
            id =request.form['id'];
            proc_date =request.form['proc_date'];
            fix_accept_date =request.form['fix_accept_date'];
            fix_fish_date =request.form['fix_fish_date'];
            fix_content =request.form['fix_content'];
            
            print("id : " + id );
            print("proc_date : " + proc_date );
            print("fix_accept_date : " + fix_accept_date );
            print("fix_fish_date : " + fix_fish_date );
            print("fix_content : " + fix_content );

            print("====================================================================")
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            upateQery = db.session.query(DeviceDefectDb)
            upateQery = upateQery.filter(DeviceDefectDb.id==id)
            upateDeviceRecord = upateQery.one()
            upateDeviceRecord.proc_date = proc_date


            if( proc_date and  not(proc_date.isspace()) ):
                upateDeviceRecord.proc_date = proc_date
                upateDeviceRecord.pro_yn ="처리"

            if( fix_accept_date and  not(fix_accept_date.isspace()) ):
                upateDeviceRecord.fix_accept_date = fix_accept_date
                upateDeviceRecord.fix_yn ="수리접수"

            
            if( fix_fish_date and  not(fix_fish_date.isspace()) ):
                upateDeviceRecord.fix_fish_date = fix_fish_date
                upateDeviceRecord.fix_yn ="수리완료"

            upateDeviceRecord.fix_content = fix_content

            db.session.commit()

            print("update success ! ")
            
            print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204

## device operation

@main_control.route('/device_operation_list')
def device_operation_list():
    print("do device_operation_list()")


    try:

        return render_template('/devices/device_operation_list.html')


    except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})



@main_control.route('/device_operation_loading')
def device_operation_loading():
    print("do device_operation_loading()")

    try:

        operation_cnt = db.session.query(func.count(DeviceOperationDb.id)).scalar()
        print("operation_cnt : " + str(operation_cnt))
        if operation_cnt < 1 :
            for type_cnt in range(1,5,1):
                for temp_h in range( 1 , 24 , 1) :
                    if(temp_h < 10):
                        time_ = "0" + str(temp_h) + ":00"
                    else:
                        time_ = str(temp_h) + ":00"

                    print(time_)
                    if(type_cnt == 1):
                        db.session.add( DeviceOperationDb(type = str("CCTV"),time = str(time_)) )
                    if(type_cnt == 2):
                        db.session.add( DeviceOperationDb(type = str("SENSOR"),time = str(time_)) )
                    if(type_cnt == 3):
                        db.session.add( DeviceOperationDb(type = str("ENA"),time = str(time_)) )
                    if(type_cnt == 4):
                        db.session.add( DeviceOperationDb(type = str("CONTROL"),time = str(time_)) )

            db.session.commit()

            device_operation_list= db.session.query(DeviceOperationDb).all()
            db.session.commit()

        else:
            device_operation_list= db.session.query(DeviceOperationDb).all()
            db.session.commit()

        ##print(device_operation_list);
        device_operation_json = {}
        temp_list= []

        for device_operation in device_operation_list:
            temp_json = {}
            temp_json.update({"type":device_operation.type})
            temp_json.update({"time":device_operation.time})
            temp_json.update({"checked":device_operation.checked})
            temp_list.append(temp_json)
        
        #print(temp_list)
        return jsonify({"result":temp_list})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

@main_control.route('/device_save_operation',methods=['GET','POST'])
def device_save_operation():
    print("do device_save_operation()")

    if request.method == 'POST': 
        try:
            

            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()


            print("========================Update===============================")
            
            
            if(request.data):
                jdata = request.get_json()
                for j in jdata['operation_setting']:
    
                    print( "udpate : "  + str(j['time']) ) 
                    print("updpate : {} , {} ,{}".format(str(j['type']),str(j['time']) ,str(j['checked'])))
                    
                    upateQery = db.session.query(DeviceOperationDb)
                    upateQery = upateQery.filter(and_(DeviceOperationDb.type ==  j['type'], DeviceOperationDb.time ==  j['time']))
                    upateDeviceRecord = upateQery.one()
                    upateDeviceRecord.checked = j['checked']

                    print( "udpate : "  + str(j['time']) ) 
                    

            else:
               
                print( "nothig !") 
                return jsonify({"result":"no"})

            
            db.session.commit()
            print("Updae sucess !")
            print("====================================================================")
            return jsonify({"result":"ok"})

            

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204


@main_control.route('/device_analy_list/<int:page_num>')
def device_analy_list(page_num):
    print("do device_analy_list()")
    
    farm_id = str(request.args.get('farm_id', default = '', type = str) )
    print("farm_id:" + farm_id)
    farm_name = str(request.args.get('farm_name', default = '', type = str) )
    print("farm_name:" + farm_name)

    house_id = str(request.args.get('house_id', default = '', type = str) )
    print("house_id:" + house_id)
    house_name = str(request.args.get('house_name', default = '', type = str) )
    print("house_name:" + house_name)


    search_keyword= request.args.get("search_keyword","")
    print("reqeust search_keyword:" + search_keyword)

    search_start_date = str(request.args.get('search_start_date', default = '', type = str) )
    print("search_start_date:" + search_start_date)

    search_end_date = str(request.args.get('search_end_date', default = '', type = str) )
    print("search_end_date:" + search_end_date)

    ##todays_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)


    try :

        if( not ( search_start_date  == "")  and not ( search_end_date  == "")  ):
            search_start_date = search_start_date + " 00:00:00"
            search_start_date_db = datetime.strptime(search_start_date,'%Y-%m-%d %H:%M:%S')
            print("search_start_date_db:" + str(search_start_date_db))
    
            search_end_date = search_end_date + " 00:00:00"
            search_end_date_db = datetime.strptime(str(search_end_date),'%Y-%m-%d %H:%M:%S')
            print("search_end_date_db:" + str(search_end_date_db))

       
            aistatues = DeviceDb.query.join(DeviceDb, DeviceDb.device_id==AiStatusDb.device_id).\
                                add_columns(AiStatusDb.id,\
                                                func.ifnull(AiStatusDb.created_at,'').label("created_at") ,\
                                                func.ifnull(AiStatusDb.device_id,'').label("device_id"),\
                                                func.ifnull(AiStatusDb.phase,'').label("phase"),\
                                                func.ifnull(AiStatusDb.status,'').label("status"),\
                                                func.ifnull(AiStatusDb.image_path,'').label("image_path")).\
                                                filter( \
                                                            or_(DeviceDb.house_id.contains(house_id)),
                                                            and_(AiStatusDb.created_at > search_start_date_db , AiStatusDb.created_at < search_end_date_db )
                                                    ).\
                                                paginate(per_page=per_page_count, page=page_num, error_out=True)
        else:
            aistatues = DeviceDb.query.join(DeviceDb, DeviceDb.device_id==AiStatusDb.device_id).\
                                add_columns(AiStatusDb.id,\
                                                func.ifnull(AiStatusDb.created_at,'').label("created_at") ,\
                                                func.ifnull(AiStatusDb.device_id,'').label("device_id"),\
                                                func.ifnull(AiStatusDb.phase,'').label("phase"),\
                                                func.ifnull(AiStatusDb.status,'').label("status"),\
                                                func.ifnull(AiStatusDb.image_path,'').label("image_path")).\
                                                filter( \
                                                            or_(DeviceDb.house_id.contains(house_id))
                                                      ).\
                                                paginate(per_page=per_page_count, page=page_num, error_out=True)


      
    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

    url ="main_control.device_analy_list";
    return render_template('/devices/device_analy_list.html',  url=url,aistatues=aistatues ,farm_id=farm_id,farm_name=farm_name,house_id=house_id,house_name=house_name, search_start_date=search_start_date ,search_end_date=search_end_date )


      

   

###################################################################
## Alarm Setting
###################################################################

##############################################################
## alarm setting disorder
@main_control.route('/alarm_setting_disorder')
def alarm_setting_disorder():
    print("do alarm_setting_disorder()!!!")

    try:
 
        return render_template('/alarm/alarm_setting_disorder.html')\

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
    

@main_control.route('/alarm_setting_disorder_individual')
def alarm_setting_disorder_individual():
    print("alarm_setting_disorder_individual")


    farmid = request.args.get('farmid') 
    print("farmid : " + str(farmid))
    
    selected_device_id = request.args.get('selected_device_id') 
    print("selected_device_id : " + str(selected_device_id))
    
    
    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
             return render_template('/alarm/alarm_setting_disorder_individual.html',farmid = farmid,selected_device_id=selected_device_id)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})



@main_control.route('/alarm_loading_disorder_data')
def alarm_loading_disorder_data():
    print("do alarm_loading_disorder_data()!!!")

    alarm_type = str(request.args.get('alarm_type', default = '', type = str) )
    print("alarm_type : " + alarm_type)
    
    if(alarm_type is None or  alarm_type == "undefined" ):
        alarm_type = "기본값"
    else :
       alarm_type = alarm_type

    try:
        
        alarm_setting_info= db.session.query(AlarmSettingDisorder).filter(AlarmSettingDisorder.alarm_type==alarm_type).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:


            alarm_json.update({"alarm_id":alarm_setting.id})
            alarm_json.update({"alarm_type":alarm_setting.alarm_type})

            alarm_json.update({"nitrogen_min":alarm_setting.nitrogen_min})
            alarm_json.update({"nitrogen_alarm_issu_type":alarm_setting.nitrogen_alarm_issu_type})

            alarm_json.update({"phosphorus_min":alarm_setting.phosphorus_min})
            alarm_json.update({"phosphorus_alarm_issu_type":alarm_setting.phosphorus_alarm_issu_type}) 

            alarm_json.update({"patassium_min":alarm_setting.patassium_min}) 
            alarm_json.update({"patassium_alarm_issu_type":alarm_setting.patassium_alarm_issu_type}) 



            alarm_json.update({"magnetsium_min":alarm_setting.magnetsium_min}) 
            alarm_json.update({"magnetsium_alarm_issu_type":alarm_setting.magnetsium_alarm_issu_type}) 

            alarm_json.update({"calcium_min":alarm_setting.calcium_min}) 
            alarm_json.update({"calcium_alarm_issu_type":alarm_setting.calcium_alarm_issu_type}) 

            alarm_json.update({"etc_min":alarm_setting.etc_min})
            alarm_json.update({"etc_alarm_issu_type":alarm_setting.etc_alarm_issu_type}) 

        print(alarm_json)
        root_jon.update({"result":alarm_json})
        return jsonify(root_jon)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/alarm_save_setting_disorder',methods=['GET','POST'])
def alarm_save_setting_disorder():
    print("do alarm_save_setting_disorder()!!!")

    if request.method == 'POST': 
        try:
            
            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))


            alarm_type = request.form['alarm_type']
            print("alarm_type  : "  + str(alarm_type))

            nitrogen_min = request.form['nitrogen_min']
            #nitrogen_min = request.form.get('nitrogen_min')
            print("nitrogen_min : "  + str(nitrogen_min))
            nitrogen_alarm_issu_type = request.form['nitrogen_alarm_issu_type']
            print(nitrogen_alarm_issu_type)

            phosphorus_min = request.form['phosphorus_min']
            print(phosphorus_min)
            phosphorus_alarm_issu_type = request.form['phosphorus_alarm_issu_type']
            print(phosphorus_alarm_issu_type)

            patassium_min = request.form['patassium_min']
            patassium_alarm_issu_type = request.form['patassium_alarm_issu_type']

            magnetsium_min = request.form['magnetsium_min']
            magnetsium_alarm_issu_type = request.form['magnetsium_alarm_issu_type']


            calcium_min = request.form['calcium_min']
            calcium_alarm_issu_type = request.form['calcium_alarm_issu_type']
            print("calcium_alarm_issu_type:"  + str(calcium_alarm_issu_type))

            etc_min = request.form['etc_min']
            etc_alarm_issu_type = request.form['etc_alarm_issu_type']
            print("etc_min:"  + str(etc_min))
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            if ( bool(db.session.query(AlarmSettingDisorder).\
                    filter(AlarmSettingDisorder.alarm_type==str(alarm_type)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingDisorder(

                            
                            alarm_type = str(alarm_type),

                            nitrogen_min = str(nitrogen_min),
                            nitrogen_alarm_issu_type = str(nitrogen_alarm_issu_type),

                            phosphorus_min = str(phosphorus_min),
                            phosphorus_alarm_issu_type = str(phosphorus_alarm_issu_type),

                            patassium_min = str(patassium_min),
                            patassium_alarm_issu_type = str(patassium_alarm_issu_type),
                            
                            magnetsium_min = str(magnetsium_min),
                            magnetsium_alarm_issu_type = str(magnetsium_alarm_issu_type),

                            calcium_min = str(calcium_min),
                            calcium_alarm_issu_type = str(calcium_alarm_issu_type),

                            etc_min = str(etc_min),
                            etc_alarm_issu_type = str(etc_alarm_issu_type)

                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingDisorder)
                upateQery = upateQery.filter( AlarmSettingDisorder.alarm_type==str(alarm_type) )
                upateAlarm = upateQery.one()

                upateAlarm.alarm_type = str(alarm_type)

                upateAlarm.nitrogen_min = str(nitrogen_min)
                upateAlarm.nitrogen_alarm_issu_type = str(nitrogen_alarm_issu_type)
                print("nitrogen_alarm_issu_type : " + nitrogen_alarm_issu_type)

                upateAlarm.phosphorus_min = str(phosphorus_min)
                upateAlarm.phosphorus_alarm_issu_type = str(phosphorus_alarm_issu_type)

                upateAlarm.patassium_min = str(patassium_min)
                upateAlarm.patassium_alarm_issu_type = str(patassium_alarm_issu_type)

                upateAlarm.magnetsium_min = str(magnetsium_min)
                upateAlarm.magnetsium_alarm_issu_type = str(magnetsium_alarm_issu_type)

                upateAlarm.calcium_min = str(calcium_min)
                upateAlarm.calcium_alarm_issu_type = str(calcium_alarm_issu_type)

                upateAlarm.etc_min = str(etc_min)
                upateAlarm.etc_alarm_issu_type = str(etc_alarm_issu_type)

                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204






##############################################################
## alarm setting pest

@main_control.route('/alarm_setting_pest')
def alarm_setting_pest():
    print("do alarm_setting_pest()!!!")

    try:
    
        return render_template('/alarm/alarm_setting_pest.html')


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})



@main_control.route('/alarm_setting_pest_individual')
def alarm_setting_pest_individual():
    print("alarm_setting_pest_individual")


    farmid = request.args.get('farmid') 
    print("farmid : " + str(farmid))
    
    selected_device_id = request.args.get('selected_device_id') 
    print("selected_device_id : " + str(selected_device_id))
    
    
    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
             return render_template('/alarm/alarm_setting_pest_individual.html',farmid = farmid,selected_device_id=selected_device_id)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})


@main_control.route('/alarm_loading_pest_data')
def alarm_loading_pest_data():
    print("do alarm_loading_pest_data()!!!")

    alarm_type = str(request.args.get('alarm_type', default = '', type = str) )
    print("alarm_type : " + alarm_type)
    
    if(alarm_type is None or  alarm_type == "undefined" ):
        alarm_type = "기본값"
    else :
       alarm_type = alarm_type

    try:
        
        alarm_setting_info= db.session.query(AlarmSettingPest).filter(AlarmSettingPest.alarm_type==alarm_type).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:


            alarm_json.update({"alarm_id":alarm_setting.id})
            alarm_json.update({"alarm_type":alarm_setting.alarm_type})

            alarm_json.update({"ice_min":alarm_setting.ice_min})
            alarm_json.update({"ice_max":alarm_setting.ice_max})
            alarm_json.update({"ice_alarm_issu_type":alarm_setting.ice_alarm_issu_type})

            alarm_json.update({"aphid_min":alarm_setting.aphid_min})
            alarm_json.update({"aphid_max":alarm_setting.aphid_max}) 
            alarm_json.update({"aphid_alarm_issu_type":alarm_setting.aphid_alarm_issu_type}) 

            alarm_json.update({"ash_mold_min":alarm_setting.ash_mold_min}) 
            alarm_json.update({"ash_mold_max":alarm_setting.ash_mold_max})
            alarm_json.update({"ash_mold_alarm_issu_type":alarm_setting.ash_mold_alarm_issu_type}) 



            alarm_json.update({"white_mold_min":alarm_setting.white_mold_min}) 
            alarm_json.update({"white_mold_max":alarm_setting.white_mold_max})
            alarm_json.update({"white_mold_alarm_issu_type":alarm_setting.white_mold_alarm_issu_type}) 

            alarm_json.update({"withered_disease_min":alarm_setting.withered_disease_min}) 
            alarm_json.update({"withered_disease_alarm_issu_type":alarm_setting.withered_disease_alarm_issu_type}) 

            alarm_json.update({"plague_min":alarm_setting.plague_min})
            alarm_json.update({"plague_alarm_issu_type":alarm_setting.plague_alarm_issu_type}) 

            alarm_json.update({"anthrax_min":alarm_setting.anthrax_min}) 
            alarm_json.update({"anthrax_alarm_issu_type":alarm_setting.anthrax_alarm_issu_type}) 

            alarm_json.update({"etc_min":alarm_setting.etc_min}) 
            alarm_json.update({"etc_alarm_issu_type":alarm_setting.etc_alarm_issu_type}) 



        print(alarm_json)
        root_jon.update({"result":alarm_json})
        return jsonify(root_jon)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/alarm_save_setting_pest',methods=['GET','POST'])
def alarm_save_setting_pest():
    print("do alarm_save_setting_pest()!!!")

    if request.method == 'POST': 
        try:
            
            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))


            alarm_type = request.form['alarm_type']
            print("alarm_type  : "  + str(alarm_type))

            ice_min = request.form['ice_min']
            #ice_min = request.form.get('ice_min')
            print("ice_min : "  + str(ice_min))
            ice_max = request.form['ice_max']
            print(ice_max)
            ice_alarm_issu_type = request.form['ice_alarm_issu_type']
            print(ice_alarm_issu_type)

            aphid_min = request.form['aphid_min']
            print(aphid_min)
            aphid_max = request.form['aphid_max']
            print(aphid_max)
            aphid_alarm_issu_type = request.form['aphid_alarm_issu_type']
            print(aphid_alarm_issu_type)

            ash_mold_min = request.form['ash_mold_min']
            ash_mold_max = request.form['ash_mold_max']
            ash_mold_alarm_issu_type = request.form['ash_mold_alarm_issu_type']



            white_mold_min = request.form['white_mold_min']
            white_mold_max = request.form['white_mold_max']
            white_mold_alarm_issu_type = request.form['white_mold_alarm_issu_type']



            withered_disease_min = request.form['withered_disease_min']
            withered_disease_alarm_issu_type = request.form['withered_disease_alarm_issu_type']
            print("withered_disease_alarm_issu_type:"  + str(withered_disease_alarm_issu_type))

            plague_min = request.form['plague_min']
            plague_alarm_issu_type = request.form['plague_alarm_issu_type']
            print("plague_min:"  + str(plague_min))

            anthrax_min = request.form['anthrax_min']
            anthrax_alarm_issu_type = request.form['anthrax_alarm_issu_type']

            print("anthrax_alarm_issu_type:"  + str(anthrax_alarm_issu_type))

            etc_min = request.form['etc_min']
            print("etc_min:"  + str(etc_min))
            etc_alarm_issu_type = request.form['etc_alarm_issu_type']
            print("etc_alarm_issu_type:"  + str(etc_alarm_issu_type))
           
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            if ( bool(db.session.query(AlarmSettingPest).\
                    filter(AlarmSettingPest.alarm_type==str(alarm_type)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingPest(

                            
                            alarm_type = str(alarm_type),

                            ice_min = str(ice_min),
                            ice_max = str(ice_max),
                            ice_alarm_issu_type = str(ice_alarm_issu_type),

                            aphid_min = str(aphid_min),
                            aphid_max = str(aphid_max),
                            aphid_alarm_issu_type = str(aphid_alarm_issu_type),

                            ash_mold_min = str(ash_mold_min),
                            ash_mold_max = str(ash_mold_max),
                            ash_mold_alarm_issu_type = str(ash_mold_alarm_issu_type),
                            
                            white_mold_min = str(white_mold_min),
                            white_mold_max = str(white_mold_max),
                            white_mold_alarm_issu_type = str(white_mold_alarm_issu_type),

                            withered_disease_min = str(withered_disease_min),
                            withered_disease_alarm_issu_type = str(withered_disease_alarm_issu_type),

                            plague_min = str(plague_min),
                            plague_alarm_issu_type = str(plague_alarm_issu_type),

                            anthrax_min = str(anthrax_min),
                            anthrax_alarm_issu_type = str(anthrax_alarm_issu_type),

                            etc_min = str(etc_min),
                            etc_alarm_issu_type = str(etc_alarm_issu_type),

                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingPest)
                upateQery = upateQery.filter( AlarmSettingPest.alarm_type==str(alarm_type) )
                upateAlarm = upateQery.one()

                upateAlarm.alarm_type = str(alarm_type)

                upateAlarm.ice_min = str(ice_min)
                upateAlarm.ice_max = str(ice_max)
                upateAlarm.ice_alarm_issu_type = str(ice_alarm_issu_type)
                print("ice_alarm_issu_type : " + ice_alarm_issu_type)

                upateAlarm.aphid_min = str(aphid_min)
                upateAlarm.aphid_max = str(aphid_max)
                upateAlarm.aphid_alarm_issu_type = str(aphid_alarm_issu_type)

                upateAlarm.ash_mold_min = str(ash_mold_min)
                upateAlarm.ash_mold_max = str(ash_mold_max)
                upateAlarm.ash_mold_alarm_issu_type = str(ash_mold_alarm_issu_type)

                upateAlarm.white_mold_min = str(white_mold_min)
                upateAlarm.white_mold_max = str(white_mold_max)
                upateAlarm.white_mold_alarm_issu_type = str(white_mold_alarm_issu_type)

                upateAlarm.withered_disease_min = str(withered_disease_min)
                upateAlarm.withered_disease_alarm_issu_type = str(withered_disease_alarm_issu_type)

                upateAlarm.plague_min = str(plague_min)
                upateAlarm.plague_alarm_issu_type = str(plague_alarm_issu_type)

                upateAlarm.anthrax_min = str(anthrax_min)
                upateAlarm.anthrax_alarm_issu_type = str(anthrax_alarm_issu_type)

                upateAlarm.etc_min = str(etc_min)
                upateAlarm.etc_alarm_issu_type = str(etc_alarm_issu_type)


                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204



##############################################################
## alarm_setting_growth

@main_control.route('/alarm_setting_growth')
def alarm_setting_growth():
    print("do alarm_setting_growth()!!!")

    try:
    
        return render_template('/alarm/alarm_setting_growth.html')


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/alarm_setting_growth_individual')
def alarm_setting_growth_individual():
    print("alarm_setting_growth_individual")


    farmid = request.args.get('farmid') 
    print("farmid : " + str(farmid))
    
    selected_device_id = request.args.get('selected_device_id') 
    print("selected_device_id : " + str(selected_device_id))
    
    
    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
             return render_template('/alarm/alarm_setting_growth_individual.html',farmid = farmid,selected_device_id=selected_device_id)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})



@main_control.route('/alarm_loading_growth_data')
def alarm_loading_growth_data():
    print("do alarm_loading_growth_data()!!!")

    alarm_type = str(request.args.get('alarm_type', default = '', type = str) )
    print("alarm_type : " + alarm_type)
    
    if(alarm_type is None or  alarm_type == "undefined" ):
        alarm_type = "기본값"
    else :
       alarm_type = alarm_type

    try:
        
        alarm_setting_info= db.session.query(AlarmSettingGrowth).filter(AlarmSettingGrowth.alarm_type==alarm_type).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:


            alarm_json.update({"alarm_id":alarm_setting.id})
            alarm_json.update({"alarm_type":alarm_setting.alarm_type})

            alarm_json.update({"leaf_cnt_min":alarm_setting.leaf_cnt_min})
            alarm_json.update({"leaf_cnt_max":alarm_setting.leaf_cnt_max})
            alarm_json.update({"leaf_cnt_alarm_issu_type":alarm_setting.leaf_cnt_alarm_issu_type})

            alarm_json.update({"in_leaf_cnt_min":alarm_setting.in_leaf_cnt_min})
            alarm_json.update({"in_leaf_cnt_max":alarm_setting.in_leaf_cnt_max}) 
            alarm_json.update({"in_leaf_cnt_alarm_issu_type":alarm_setting.in_leaf_cnt_alarm_issu_type}) 

            alarm_json.update({"head_cnt_min":alarm_setting.head_cnt_min}) 
            alarm_json.update({"head_cnt_max":alarm_setting.head_cnt_max})
            alarm_json.update({"head_cnt_alarm_issu_type":alarm_setting.head_cnt_alarm_issu_type}) 

            alarm_json.update({"ilack_cnt_min":alarm_setting.ilack_cnt_min}) 
            alarm_json.update({"ilack_cnt_max":alarm_setting.ilack_cnt_max}) 
            alarm_json.update({"ilack_cnt_alarm_issu_type":alarm_setting.ilack_cnt_alarm_issu_type}) 

            alarm_json.update({"flower_cnt_min":alarm_setting.flower_cnt_min})
            alarm_json.update({"flower_cnt_max":alarm_setting.flower_cnt_max})
            alarm_json.update({"flower_cnt_alarm_issu_type":alarm_setting.flower_cnt_alarm_issu_type}) 

            alarm_json.update({"fruit_cnt_min":alarm_setting.fruit_cnt_min}) 
            alarm_json.update({"fruit_cnt_max":alarm_setting.fruit_cnt_max}) 
            alarm_json.update({"fruit_cnt_alarm_issu_type":alarm_setting.fruit_cnt_alarm_issu_type}) 

            alarm_json.update({"fruit_size_min":alarm_setting.fruit_size_min}) 
            alarm_json.update({"fruit_size_max":alarm_setting.fruit_size_max}) 
            alarm_json.update({"fruit_size_alarm_issu_type":alarm_setting.fruit_size_alarm_issu_type}) 

            alarm_json.update({"in_leaf_length_min":alarm_setting.in_leaf_length_min}) 
            alarm_json.update({"in_leaf_length_max":alarm_setting.in_leaf_length_max}) 
            alarm_json.update({"in_leaf_length_alarm_issu_type":alarm_setting.in_leaf_length_alarm_issu_type}) 

            alarm_json.update({"in_leaf_size_min":alarm_setting.in_leaf_size_min}) 
            alarm_json.update({"in_leaf_size_max":alarm_setting.in_leaf_size_max}) 
            alarm_json.update({"in_leaf_size_alarm_issu_type":alarm_setting.in_leaf_size_alarm_issu_type}) 



        print(alarm_json)
        root_jon.update({"result":alarm_json})
        return jsonify(root_jon)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/alarm_save_setting_growth',methods=['GET','POST'])
def alarm_save_setting_growth():
    print("do alarm_save_setting_growth()!!!")

    if request.method == 'POST': 
        try:
            
            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))


            alarm_type = request.form['alarm_type']
            print("alarm_type  : "  + str(alarm_type))

            leaf_cnt_min = request.form['leaf_cnt_min']
            #leaf_cnt_min = request.form.get('leaf_cnt_min')
            print(leaf_cnt_min)
            leaf_cnt_max = request.form['leaf_cnt_max']
            print(leaf_cnt_max)
            leaf_cnt_alarm_issu_type = request.form['leaf_cnt_alarm_issu_type']
            print(leaf_cnt_alarm_issu_type)

            in_leaf_cnt_min = request.form['in_leaf_cnt_min']
            print(in_leaf_cnt_min)
            in_leaf_cnt_max = request.form['in_leaf_cnt_max']
            print(in_leaf_cnt_max)
            in_leaf_cnt_alarm_issu_type = request.form['in_leaf_cnt_alarm_issu_type']
            print(in_leaf_cnt_alarm_issu_type)

            head_cnt_min = request.form['head_cnt_min']
            head_cnt_max = request.form['head_cnt_max']
            head_cnt_alarm_issu_type = request.form['head_cnt_alarm_issu_type']

            ilack_cnt_min = request.form['ilack_cnt_min']
            ilack_cnt_max = request.form['ilack_cnt_max']
            ilack_cnt_alarm_issu_type = request.form['ilack_cnt_alarm_issu_type']
            print("ilack_cnt_alarm_issu_type:"  + str(ilack_cnt_alarm_issu_type))

            flower_cnt_min = request.form['flower_cnt_min']
            flower_cnt_max = request.form['flower_cnt_max']
            flower_cnt_alarm_issu_type = request.form['flower_cnt_alarm_issu_type']

            fruit_cnt_min = request.form['fruit_cnt_min']
            fruit_cnt_max = request.form['fruit_cnt_max']
            fruit_cnt_alarm_issu_type = request.form['fruit_cnt_alarm_issu_type']

            fruit_size_min = request.form['fruit_size_min']
            fruit_size_max = request.form['fruit_size_max']
            fruit_size_alarm_issu_type = request.form['fruit_size_alarm_issu_type']

            in_leaf_length_min = request.form['in_leaf_length_min']
            in_leaf_length_max = request.form['in_leaf_length_max']
            in_leaf_length_alarm_issu_type = request.form['in_leaf_length_alarm_issu_type']


            in_leaf_size_min = request.form['in_leaf_size_min']
            in_leaf_size_max = request.form['in_leaf_size_max']
            in_leaf_size_alarm_issu_type = request.form['in_leaf_size_alarm_issu_type']
           
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            if ( bool(db.session.query(AlarmSettingGrowth).\
                    filter(AlarmSettingGrowth.alarm_type==str(alarm_type)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingGrowth(

                            
                            alarm_type = str(alarm_type),

                            leaf_cnt_min = str(leaf_cnt_min),
                            leaf_cnt_max = str(leaf_cnt_max),
                            leaf_cnt_alarm_issu_type = str(leaf_cnt_alarm_issu_type),

                            in_leaf_cnt_min = str(in_leaf_cnt_min),
                            in_leaf_cnt_max = str(in_leaf_cnt_max),
                            in_leaf_cnt_alarm_issu_type = str(in_leaf_cnt_alarm_issu_type),

                            head_cnt_min = str(head_cnt_min),
                            head_cnt_max = str(head_cnt_max),
                            head_cnt_alarm_issu_type = str(head_cnt_alarm_issu_type),

                            ilack_cnt_min = str(ilack_cnt_min),
                            ilack_cnt_max = str(ilack_cnt_max),
                            ilack_cnt_alarm_issu_type = str(ilack_cnt_alarm_issu_type),

                            flower_cnt_min = str(flower_cnt_min),
                            flower_cnt_max = str(flower_cnt_max),
                            flower_cnt_alarm_issu_type = str(flower_cnt_alarm_issu_type),

                            fruit_cnt_min = str(fruit_cnt_min),
                            fruit_cnt_max = str(fruit_cnt_max),
                            fruit_cnt_alarm_issu_type = str(fruit_cnt_alarm_issu_type),

                            fruit_size_min = str(fruit_size_min),
                            fruit_size_max = str(fruit_size_max),
                            fruit_size_alarm_issu_type = str(fruit_size_alarm_issu_type),


                            in_leaf_length_min = str(in_leaf_length_min),
                            in_leaf_length_max = str(in_leaf_length_max),
                            in_leaf_length_alarm_issu_type = str(in_leaf_length_alarm_issu_type),


                            in_leaf_size_min = str(in_leaf_size_min),
                            in_leaf_size_max = str(in_leaf_size_max),
                            in_leaf_size_alarm_issu_type = str(in_leaf_size_alarm_issu_type)
                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingGrowth)
                upateQery = upateQery.filter( AlarmSettingGrowth.alarm_type==str(alarm_type) )
                upateAlarm = upateQery.one()

                upateAlarm.alarm_type = str(alarm_type)

                upateAlarm.leaf_cnt_min = str(leaf_cnt_min)
                upateAlarm.leaf_cnt_max = str(leaf_cnt_max)
                upateAlarm.leaf_cnt_alarm_issu_type = str(leaf_cnt_alarm_issu_type)
                print("leaf_cnt_alarm_issu_type : " + leaf_cnt_alarm_issu_type)

                upateAlarm.in_leaf_cnt_min = str(in_leaf_cnt_min)
                upateAlarm.in_leaf_cnt_max = str(in_leaf_cnt_max)
                upateAlarm.in_leaf_cnt_alarm_issu_type = str(in_leaf_cnt_alarm_issu_type)

                upateAlarm.head_cnt_min = str(head_cnt_min)
                upateAlarm.head_cnt_max = str(head_cnt_max)
                upateAlarm.head_cnt_alarm_issu_type = str(head_cnt_alarm_issu_type)

                upateAlarm.ilack_cnt_min = str(ilack_cnt_min)
                upateAlarm.ilack_cnt_max = str(ilack_cnt_max)
                upateAlarm.ilack_cnt_alarm_issu_type = str(ilack_cnt_alarm_issu_type)

                upateAlarm.flower_cnt_min = str(flower_cnt_min)
                upateAlarm.flower_cnt_max = str(flower_cnt_max)
                upateAlarm.flower_cnt_alarm_issu_type = str(flower_cnt_alarm_issu_type)

                upateAlarm.fruit_cnt_min = str(fruit_cnt_min)
                upateAlarm.fruit_cnt_max = str(fruit_cnt_max)
                upateAlarm.fruit_cnt_alarm_issu_type = str(fruit_cnt_alarm_issu_type)

                upateAlarm.fruit_size_min = str(fruit_size_min)
                upateAlarm.fruit_size_max = str(fruit_size_max)
                upateAlarm.fruit_size_alarm_issu_type = str(fruit_size_alarm_issu_type)


                
                upateAlarm.in_leaf_length_min = str(in_leaf_length_min)
                upateAlarm.in_leaf_length_max = str(in_leaf_length_max)
                upateAlarm.in_leaf_length_alarm_issu_type = str(in_leaf_length_alarm_issu_type)


                
                upateAlarm.in_leaf_size_min = str(in_leaf_size_min)
                upateAlarm.in_leaf_size_max = str(in_leaf_size_max)
                upateAlarm.in_leaf_size_alarm_issu_type = str(in_leaf_size_alarm_issu_type)


                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204



##############################################################

@main_control.route('/alarm_setting_sensor')
def alarm_setting_sensor():
    print("do alarm_setting()!!!")

    try:
    
        return render_template('/alarm/alarm_setting_sensor.html')


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/alarm_setting_sensor_individual')
def alarm_setting_sensor_individual():
    print("alarm_setting_sensor_individual")


    farmid = request.args.get('farmid') 
    print("farmid : " + str(farmid))
    
    selected_device_id = request.args.get('selected_device_id') 
    print("selected_device_id : " + str(selected_device_id))
    
    
    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
             return render_template('/alarm/alarm_setting_sensor_individual.html',farmid = farmid,selected_device_id=selected_device_id)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})


@main_control.route('/alarm_loading_sensor_data')
def alarm_loading_sensor_data():
    print("do alarm_loading_sensor_data()!!!")

    alarm_type = str(request.args.get('alarm_type', default = '', type = str) )
    print("alarm_type : " + alarm_type)
    
    if(alarm_type is None or  alarm_type == "undefined" ):
        alarm_type = "기본값"
    else :
       alarm_type = alarm_type

    try:
        
        alarm_setting_info= db.session.query(AlarmSettingSensor).filter(AlarmSettingSensor.alarm_type==alarm_type).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:


            alarm_json.update({"alarm_id":alarm_setting.id})
            alarm_json.update({"alarm_type":alarm_setting.alarm_type})

            alarm_json.update({"indoor_temp_min":alarm_setting.indoor_temp_min})

            alarm_json.update({"indoor_temp_max":alarm_setting.indoor_temp_max})
            alarm_json.update({"indoor_temp_alarm_issu_type":alarm_setting.indoor_temp_alarm_issu_type})

            alarm_json.update({"indoor_humidity_min":alarm_setting.indoor_humidity_min})
            alarm_json.update({"indoor_humidity_max":alarm_setting.indoor_humidity_max}) 
            alarm_json.update({"indoor_humidity_alarm_issu_type":alarm_setting.indoor_humidity_alarm_issu_type}) 

            alarm_json.update({"indoor_co2_min":alarm_setting.indoor_co2_min}) 
            alarm_json.update({"indoor_co2_max":alarm_setting.indoor_co2_max})
            alarm_json.update({"indoor_co2_alarm_issu_type":alarm_setting.indoor_co2_alarm_issu_type}) 

            alarm_json.update({"indoor_sunny_min":alarm_setting.indoor_sunny_min}) 
            alarm_json.update({"indoor_sunny_max":alarm_setting.indoor_sunny_max}) 
            alarm_json.update({"indoor_sunny_alarm_issu_type":alarm_setting.indoor_sunny_alarm_issu_type}) 

            alarm_json.update({"basic_temp_min":alarm_setting.basic_temp_min})
            alarm_json.update({"basic_temp_max":alarm_setting.basic_temp_max})
            alarm_json.update({"basic_temp_alarm_issu_type":alarm_setting.basic_temp_alarm_issu_type}) 

            alarm_json.update({"basic_humidity_min":alarm_setting.basic_humidity_min}) 
            alarm_json.update({"basic_humidity_max":alarm_setting.basic_humidity_max}) 
            alarm_json.update({"basic_humidity_alarm_issu_type":alarm_setting.basic_humidity_alarm_issu_type}) 

            alarm_json.update({"basic_ec_min":alarm_setting.basic_ec_min}) 
            alarm_json.update({"basic_ec_max":alarm_setting.basic_ec_max}) 
            alarm_json.update({"basic_ec_alarm_issu_type":alarm_setting.basic_ec_alarm_issu_type}) 

        print(alarm_json)
        root_jon.update({"result":alarm_json})
        return jsonify(root_jon)


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/alarm_save_setting_sensor',methods=['GET','POST'])
def alarm_save_setting_sensor():
    print("do alarm_save_setting_sensor()!!!")

    if request.method == 'POST': 
        try:
            
            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))


            alarm_type = request.form['alarm_type']
            print("alarm_type  : "  + str(alarm_type))

            indoor_temp_min = request.form['indoor_temp_min']
            #indoor_temp_min = request.form.get('indoor_temp_min')
            print(indoor_temp_min)
            indoor_temp_max = request.form['indoor_temp_max']
            print(indoor_temp_max)
            indoor_temp_alarm_issu_type = request.form['indoor_temp_alarm_issu_type']
            print(indoor_temp_alarm_issu_type)

            indoor_humidity_min = request.form['indoor_humidity_min']
            print(indoor_humidity_min)
            indoor_humidity_max = request.form['indoor_humidity_max']
            print(indoor_humidity_max)
            indoor_humidity_alarm_issu_type = request.form['indoor_humidity_alarm_issu_type']
            print(indoor_humidity_alarm_issu_type)

            indoor_co2_min = request.form['indoor_co2_min']
            indoor_co2_max = request.form['indoor_co2_max']
            indoor_co2_alarm_issu_type = request.form['indoor_co2_alarm_issu_type']

            indoor_sunny_min = request.form['indoor_sunny_min']
            indoor_sunny_max = request.form['indoor_sunny_max']
            indoor_sunny_alarm_issu_type = request.form['indoor_sunny_alarm_issu_type']

            basic_temp_min = request.form['basic_temp_min']
            basic_temp_max = request.form['basic_temp_max']
            basic_temp_alarm_issu_type = request.form['basic_temp_alarm_issu_type']

            basic_humidity_min = request.form['basic_humidity_min']
            basic_humidity_max = request.form['basic_humidity_max']
            basic_humidity_alarm_issu_type = request.form['basic_humidity_alarm_issu_type']

            basic_ec_min = request.form['basic_ec_min']
            basic_ec_max = request.form['basic_ec_max']
            basic_ec_alarm_issu_type = request.form['basic_ec_alarm_issu_type']
           
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()


        
           

            if ( bool(db.session.query(AlarmSettingSensor).\
                    filter(AlarmSettingSensor.alarm_type==str(alarm_type)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingSensor(

                            
                            alarm_type = str(alarm_type),

                            indoor_temp_min = str(indoor_temp_min),
                            indoor_temp_max = str(indoor_temp_max),
                            indoor_temp_alarm_issu_type = str(indoor_temp_alarm_issu_type),

                            indoor_humidity_min = str(indoor_humidity_min),
                            indoor_humidity_max = str(indoor_humidity_max),
                            indoor_humidity_alarm_issu_type = str(indoor_humidity_alarm_issu_type),

                            indoor_co2_min = str(indoor_co2_min),
                            indoor_co2_max = str(indoor_co2_max),
                            indoor_co2_alarm_issu_type = str(indoor_co2_alarm_issu_type),

                            indoor_sunny_min = str(indoor_sunny_min),
                            indoor_sunny_max = str(indoor_sunny_max),
                            indoor_sunny_alarm_issu_type = str(indoor_sunny_alarm_issu_type),

                            basic_temp_min = str(basic_temp_min),
                            basic_temp_max = str(basic_temp_max),
                            basic_temp_alarm_issu_type = str(basic_temp_alarm_issu_type),

                            basic_humidity_min = str(basic_humidity_min),
                            basic_humidity_max = str(basic_humidity_max),
                            basic_humidity_alarm_issu_type = str(basic_humidity_alarm_issu_type),

                            basic_ec_min = str(basic_ec_min),
                            basic_ec_max = str(basic_ec_max),
                            basic_ec_alarm_issu_type = str(basic_ec_alarm_issu_type)
                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingSensor)
                upateQery = upateQery.filter( AlarmSettingSensor.alarm_type==str(alarm_type) )
                upateAlarm = upateQery.one()

                upateAlarm.alarm_type = str(alarm_type)

                upateAlarm.indoor_temp_min = str(indoor_temp_min)
                upateAlarm.indoor_temp_max = str(indoor_temp_max)
                upateAlarm.indoor_temp_alarm_issu_type = str(indoor_temp_alarm_issu_type)
                print("indoor_temp_alarm_issu_type : " + indoor_temp_alarm_issu_type)

                upateAlarm.indoor_humidity_min = str(indoor_humidity_min)
                upateAlarm.indoor_humidity_max = str(indoor_humidity_max)
                upateAlarm.indoor_humidity_alarm_issu_type = str(indoor_humidity_alarm_issu_type)

                upateAlarm.indoor_co2_min = str(indoor_co2_min)
                upateAlarm.indoor_co2_max = str(indoor_co2_max)
                upateAlarm.indoor_co2_alarm_issu_type = str(indoor_co2_alarm_issu_type)

                upateAlarm.indoor_sunny_min = str(indoor_sunny_min)
                upateAlarm.indoor_sunny_max = str(indoor_sunny_max)
                upateAlarm.indoor_sunny_alarm_issu_type = str(indoor_sunny_alarm_issu_type)

                upateAlarm.basic_temp_min = str(basic_temp_min)
                upateAlarm.basic_temp_max = str(basic_temp_max)
                upateAlarm.basic_temp_alarm_issu_type = str(basic_temp_alarm_issu_type)
                upateAlarm.basic_humidity_min = str(basic_humidity_min)
                upateAlarm.basic_humidity_max = str(basic_humidity_max)
                upateAlarm.basic_humidity_alarm_issu_type = str(basic_humidity_alarm_issu_type)

                upateAlarm.basic_ec_min = str(basic_ec_min)
                upateAlarm.basic_ec_max = str(basic_ec_max)
                upateAlarm.basic_ec_alarm_issu_type = str(basic_ec_alarm_issu_type)

                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204



###################################################################
## AlarmList
###################################################################

@main_control.route('/alarm_list_main/<int:page_num>')
def alarm_list_main(page_num):
    print("do alarm_list_main()!!!")


    search_cloumn = str(request.args.get('search_cloumn', default = '', type = str) )
    search_keyword= str(request.args.get('search_keyword', default = '', type = str) )

    print("search_cloumn:" + search_cloumn)
    print("search_keyword:" + search_keyword)

    ##print(len(search_cloumn))
    ##

    print("query search_cloumn=='3'")
    #DT = Column(DateTime(timezone=True), default=func.now())
    todays_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)

    print("todays_datetime : " + str(todays_datetime))
    print("datetime.today() : " + str(datetime.today()))
    
    
    three_days_ago = datetime.today() - timedelta(days = 3)
    three_days_ago = three_days_ago.strftime("%Y-%m-%d %H:%M:%S")
    print("three_days_ago : " + str(three_days_ago))
    
    print("date.today() : " + str(date.today()))

    if request.method == 'GET' :
        try:

            ################
            if(search_cloumn=='not_confirm'):
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        #func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        func.replace(cast( func.datediff(AlarmHistoryDb.create_at, date.today()), String ), '-', '').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    (AlarmHistoryDb.check_yn =='미확인') )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
            elif(search_cloumn=='confirm'):
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.replace(cast( func.datediff(AlarmHistoryDb.create_at, date.today()), String ), '-', '').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    (AlarmHistoryDb.check_yn =='확인') )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
            elif(search_cloumn=='3'):
                print("aaaaa")
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.replace(cast( func.datediff(AlarmHistoryDb.create_at, date.today()), String ), '-', '').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    and_(AlarmHistoryDb.create_at <= three_days_ago ,AlarmHistoryDb.check_yn =='미확인') )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
            elif(search_cloumn=='today'):
                print("aaa")
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.replace(cast( func.datediff(AlarmHistoryDb.create_at, date.today()), String ), '-', '').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    AlarmHistoryDb.create_at >= todays_datetime )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
                
            else:
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.replace(cast( func.datediff(AlarmHistoryDb.create_at, date.today()), String ), '-', '').label("alarm_none_check_cnt")).\
                                        filter( \
                                            or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                AlarmHistoryDb.type.contains(search_keyword),
                                                AlarmHistoryDb.category.contains(search_keyword),
                                                AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                AlarmHistoryDb.house_name.contains(search_keyword),
                                                AlarmHistoryDb.crop.contains(search_keyword),
                                                AlarmHistoryDb.kind.contains(search_keyword),
                                                AlarmHistoryDb.house_name.contains(search_keyword),
                                                AlarmHistoryDb.check_yn.contains(search_keyword)
                                        )).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'

            ##################
            print(alarms.items)

            url ="main_control.alarm_list_main";
            
            return render_template('/alarm/alarm_list_main.html', alarms=alarms,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204



@main_control.route('/alarm_list_main_today/<int:page_num>')
def alarm_list_main_today(page_num):
    print("do alarm_list_main_today()!!!")
 

    search_cloumn = str(request.args.get('search_cloumn', default = '', type = str) )
    search_keyword= str(request.args.get('search_keyword', default = '', type = str) )

    print("search_cloumn:" + search_cloumn)
    print("search_keyword:" + search_keyword)

    ##print(len(search_cloumn))
    ##

    print("query search_cloumn=='3'")
    #DT = Column(DateTime(timezone=True), default=func.now())
    todays_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)

    print("todays_datetime : " + str(todays_datetime))
    print("datetime.today() : " + str(datetime.today()))
    

    three_days_ago = datetime.today() - timedelta(days = 3)
    three_days_ago = three_days_ago.strftime("%Y-%m-%d %H:%M:%S")
    print("three_days_ago : " + str(three_days_ago))
    

    if request.method == 'GET' :
        try:

            ################
            if(search_cloumn=='not_confirm'):
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    (AlarmHistoryDb.check_yn =='미확인') ,(AlarmHistoryDb.create_at >= todays_datetime ) )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
            elif(search_cloumn=='confirm'):
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    (AlarmHistoryDb.check_yn =='확인'),(AlarmHistoryDb.create_at >= todays_datetime ) )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
            elif(search_cloumn=='3'):
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    and_(AlarmHistoryDb.create_at >= three_days_ago ,AlarmHistoryDb.check_yn =='미확인') )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
            elif(search_cloumn=='today'):
                print("aaa")
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter( \
                                                and_( 
                                                     or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                        AlarmHistoryDb.type.contains(search_keyword),
                                                        AlarmHistoryDb.category.contains(search_keyword),
                                                        AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                        AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.crop.contains(search_keyword),
                                                        AlarmHistoryDb.kind.contains(search_keyword),
                                                        AlarmHistoryDb.house_name.contains(search_keyword),
                                                        AlarmHistoryDb.check_yn.contains(search_keyword))\
                                                        ,
                                                    AlarmHistoryDb.create_at >= todays_datetime )\
                                            ).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'
                
            else:
                alarms = AlarmHistoryDb.query.\
                            add_columns(
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter( \
                                            or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                AlarmHistoryDb.type.contains(search_keyword),
                                                AlarmHistoryDb.category.contains(search_keyword),
                                                AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                AlarmHistoryDb.house_name.contains(search_keyword),
                                                AlarmHistoryDb.crop.contains(search_keyword),
                                                AlarmHistoryDb.kind.contains(search_keyword),
                                                AlarmHistoryDb.house_name.contains(search_keyword),
                                                AlarmHistoryDb.check_yn.contains(search_keyword)
                                        )).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
                                        ##'%Y-%m-%d %H:%M:%S'

            ##################
            print(alarms.items)

            url ="main_control.alarm_list_main";
            
            return render_template('/alarm/alarm_list_main.html', alarms=alarms,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword,page_num=page_num)


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204




###################################################################
## Image Processing
###################################################################

import requests
def image_download():
     
    image_url= "http://127.0.0.1:5000/static/crop_image/sample_image_define.png"
    print("image_url  : "  + str(image_url))  
    r = requests.get(image_url)


    file_basedir = os.path.dirname(__file__)

    directory = "./static/crop_image/test"
    directory_full_path= os.path.join(file_basedir, directory) 
    print("directory_full_path  : "  + str(directory_full_path))  
    if not os.path.exists(directory_full_path):
        os.makedirs(directory_full_path)

    '''
    ## success
    directory_full_path = "./static/crop_image/" + str(farm_name) + "/" + str(house_id)
    print("directory_full_path  : "  + str(directory_full_path))  
    if not os.path.exists(directory_full_path):
        os.makedirs(directory_full_path)
    '''

    file_full_path = directory_full_path + "/" + "sample_image_define.png"     
    print("file_full_path  : "  + str(file_full_path))  
        
    with open(file_full_path,'wb') as f:
        f.write(r.content)

@main_control.route('/image_judgment/<int:page_num>')
def image_judgment(page_num):
    print("do image_judgment()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id:" + device_id)


    device_name = str(request.args.get('device_name', default = '', type = str) )
    print("device_name:" + device_name)

    search_date = str(request.args.get('search_date', default = '', type = str) )
    print("search_date:" + search_date)
   
    search_date_converte = search_date.replace("-", "")
    print("search_date_converte:" + search_date_converte)


    house_id = str(request.args.get('house_id', default = '', type = str) )
    print("house_id:" + house_id)
    house_name = str(request.args.get('house_name', default = '', type = str) )
    print("house_name:" + house_name)


    farm_id = str(request.args.get('farm_id', default = '', type = str) )
    print("farm_id:" + farm_id)
    farm_name = str(request.args.get('farm_name', default = '', type = str) )
    print("farm_name:" + farm_name)


    if request.method == 'GET' :
        try:
            ##devices= db.session.query(DeviceDb).filter(DeviceDb.farm_id==farm_id).all()
            ##db.session.commit()
            ##print(devices);            
            images = ImageBasicDataDb.query.\
                        add_columns(ImageBasicDataDb.id,\
                                    func.ifnull(ImageBasicDataDb.created_at,'').label("created_at") ,\
                                    func.ifnull(ImageBasicDataDb.device_id,'').label("device_id"),\
                                    func.ifnull(ImageBasicDataDb.sequenceID,'').label("sequenceID"),\
                                    func.ifnull(ImageBasicDataDb.roundID,'').label("roundID"),\
                                    func.ifnull(ImageBasicDataDb.image_path,'').label("image_path"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_manul,'').label("check_defect_manul"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_auto,'').label("check_defect_auto"),\
                                    func.ifnull(ImageBasicDataDb.plague_level,'').label("plague_level")).\
                                    filter(and_(~ImageBasicDataDb.image_path.contains('cam1') , ImageBasicDataDb.device_id == device_id, ImageBasicDataDb.targetDate.contains(search_date_converte) )).\
                                paginate(per_page=12, page=page_num, error_out=True)

            print("len(images.items) : " + str(len(images.items)))
            list_cnt = len(images.items);

            for image in images.items:
                print(image.image_path)
            
            url ="main_control.image_judgment";
            return render_template('/imageprocess/image_judgment.html', farm_name=farm_name,farm_id=farm_id,\
                                                house_name=house_name,house_id=house_id,device_id=device_id,device_name=device_name, \
                                                    list_cnt = list_cnt, images=images,url=url,search_date=search_date,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204




@main_control.route('/update_image_data', methods=['GET', 'POST'])
def update_image_data():
    print("update_image_data")

    image_id = str(request.args.get('image_id', default = '', type = str) )
    print("image_id:" + image_id)
    check_defect_manul = str(request.args.get('check_defect_manul', default = '', type = str) )
    print("check_defect_manul:" + image_id)

    try:
        print("====================================================================")

      
        upateQery = db.session.query(ImageBasicDataDb)
        upateQery = upateQery.filter(ImageBasicDataDb.id==image_id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.check_defect_manul = check_defect_manul


        db.session.commit()

        print("update success ! ")

        print("====================================================================")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/image_classfication/<int:page_num>')
def image_classfication(page_num):
    print("do image_classfication()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id:" + device_id)


    device_name = str(request.args.get('device_name', default = '', type = str) )
    print("device_name:" + device_name)

    search_date = str(request.args.get('search_date', default = '', type = str) )
    print("search_date:" + search_date)

    search_date_converte = search_date.replace("-", "")
    print("search_date_converte:" + search_date_converte)


    house_id = str(request.args.get('house_id', default = '', type = str) )
    print("house_id:" + house_id)
    house_name = str(request.args.get('house_name', default = '', type = str) )
    print("house_name:" + house_name)


    farm_id = str(request.args.get('farm_id', default = '', type = str) )
    print("farm_id:" + farm_id)
    farm_name = str(request.args.get('farm_name', default = '', type = str) )
    print("farm_name:" + farm_name)



    todays_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)

    print("todays_datetime : " + str(todays_datetime))
    print("datetime.today() : " + str(datetime.today()))


    three_days_ago = datetime.today() - timedelta(days = 3)
    print("three_days_ago : " + str(three_days_ago))

   
    if request.method == 'GET' :
        try:
            ##devices= db.session.query(DeviceDb).filter(DeviceDb.farm_id==farm_id).all()
            ##db.session.commit()
            ##print(devices);            
            images = ImageBasicDataDb.query.\
                        add_columns(ImageBasicDataDb.id,\
                                    func.ifnull(ImageBasicDataDb.created_at,'').label("created_at") ,\
                                    func.ifnull(ImageBasicDataDb.device_id,'').label("device_id"),\
                                    func.ifnull(ImageBasicDataDb.roundID,'').label("roundID"),\
                                    func.ifnull(ImageBasicDataDb.sequenceID,'').label("sequenceID"),\
                                    func.ifnull(ImageBasicDataDb.image_path,'').label("image_path"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_manul,'').label("check_defect_manul"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_auto,'').label("check_defect_auto"),\
                                    func.ifnull(ImageBasicDataDb.plague_type,'').label("plague_type"),\
                                    func.ifnull(ImageBasicDataDb.plague_level,'').label("plague_level")).\
                                    filter(and_(~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.device_id == device_id, ImageBasicDataDb.targetDate.contains(search_date_converte) , \
                                        ImageBasicDataDb.check_defect_manul == "true"
                                        )).\
                                paginate(per_page=12, page=page_num, error_out=True)

            print("len(images.items) : " + str(len(images.items)))
            list_cnt = len(images.items);

            for image in images.items:
                plague_type_list = image.plague_type
                print(str(plague_type_list))
                for temp in plague_type_list:
                    print(str(temp))
            
            url ="main_control.image_classfication";
            return render_template('/imageprocess/image_classfication.html', farm_name=farm_name,farm_id=farm_id,\
                                                house_name=house_name,house_id=house_id,device_id=device_id,device_name=device_name, \
                                                    list_cnt = list_cnt, images=images,url=url,search_date=search_date,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204



## 이미지 리스트
@main_control.route('/get_plague_info_by_image_id')
def get_plague_info_by_image_id():
    print("do get_plague_info_by_image_id() !!!")

    image_id= request.args.get('image_id', default = '', type = str) 

    print("image_id :"  + image_id)
   
    image_temp= db.session.query(ImageBasicDataDb).filter(ImageBasicDataDb.id == image_id).first()
    print(image_temp)
    print("====================================================================")
    result_json ={};
    if image_temp:
        result_json.update({"image_id" : image_temp.id})
        result_json.update({"image_plague_type" : image_temp.plague_type})
        result_json.update({"image_plague_level" : image_temp.plague_level })
        result_json.update({"image_path" : image_temp.image_path})
        result_json.update({"image_sequence_id" : image_temp.sequenceID})
        result_json.update({"image_round_id" : image_temp.roundID})

        
    
    print("Final result_json : "  + str(result_json))    
    print("====================================================================")

    return jsonify({"result":result_json})



@main_control.route('/image_set_plague_type', methods=['POST'])
def image_set_plague_type():
    print("image_set_plague_type")

    ##{"device_id":"2136-01-01-01","plague_type":["시들음병"]}
    #{"device_id":"2136-01-01-01"}

    if(request.data):
        jdata = request.get_json()
        image_id = jdata['image_id']
        print("image_id:" + image_id)

        plague_type = jdata['plague_type']
        print("plague_type:" + str(plague_type))
        print("====================================================================")

        try:
            upateQery = db.session.query(ImageBasicDataDb)
            upateQery = upateQery.filter(ImageBasicDataDb.id==image_id)
            upateDeviceRecord = upateQery.one()
            ##upateDeviceRecord.plague_type = str(plague_type)
            upateDeviceRecord.plague_type = json.dumps(plague_type, ensure_ascii=False)

            db.session.commit()

            print("update success ! ")
            print("====================================================================")

            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    
    return jsonify({"result":"no"})





@main_control.route('/image_set_step/<int:page_num>')
def image_set_step(page_num):
    print("do image_set_step()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id:" + device_id)

    device_name = str(request.args.get('device_name', default = '', type = str) )
    print("device_name:" + device_name)

    search_date = str(request.args.get('search_date', default = '', type = str) )
    print("search_date:" + search_date)

    search_date_converte = search_date.replace("-", "")
    print("search_date_converte:" + search_date_converte)


    house_id = str(request.args.get('house_id', default = '', type = str) )
    print("house_id:" + house_id)
    house_name = str(request.args.get('house_name', default = '', type = str) )
    print("house_name:" + house_name)

    farm_id = str(request.args.get('farm_id', default = '', type = str) )
    print("farm_id:" + farm_id)
    farm_name = str(request.args.get('farm_name', default = '', type = str) )
    print("farm_name:" + farm_name)

    roundID = str(request.args.get('roundID', default = '', type = str) )
    print("roundID:" + roundID)
    
    plague_type = str(request.args.get('plague_type', default = '', type = str) )
    print("plague_type:" + plague_type)


    todays_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)

    print("todays_datetime : " + str(todays_datetime))
    print("datetime.today() : " + str(datetime.today()))


    three_days_ago = datetime.today() - timedelta(days = 3)
    print("three_days_ago : " + str(three_days_ago))

   
    if request.method == 'GET' :
        try:
            ##devices= db.session.query(DeviceDb).filter(DeviceDb.farm_id==farm_id).all()
            ##db.session.commit()
            ##print(devices);            
            images = ImageBasicDataDb.query.\
                        add_columns(ImageBasicDataDb.id,\
                                    func.ifnull(ImageBasicDataDb.created_at,'').label("created_at") ,\
                                    func.ifnull(ImageBasicDataDb.device_id,'').label("device_id"),\
                                    func.ifnull(ImageBasicDataDb.sequenceID,'').label("sequenceID"),\
                                    func.ifnull(ImageBasicDataDb.roundID,'').label("roundID"),\
                                    func.ifnull(ImageBasicDataDb.image_path,'').label("image_path"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_manul,'').label("check_defect_manul"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_auto,'').label("check_defect_auto"),\
                                    func.ifnull(ImageBasicDataDb.plague_type,'').label("plague_type"),\
                                    func.ifnull(ImageBasicDataDb.plague_level,'').label("plague_level")).\
                                    filter(and_( ~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.device_id == device_id, \
                                        ImageBasicDataDb.check_defect_manul == "true",\
                                        ImageBasicDataDb.plague_type.contains(plague_type),\
                                        ImageBasicDataDb.targetDate.contains(search_date_converte)  )).\
                                paginate(per_page=6, page=page_num, error_out=True)

            print("len(images.items) : " + str(len(images.items)))
            list_cnt = len(images.items);

            for image in images.items:
                plague_type_list = image.plague_type
                print(str(plague_type_list))
                for temp in plague_type_list:
                    print(str(temp))
            
            url ="main_control.image_set_step";
            return render_template('/imageprocess/image_set_step.html', plague_type=plague_type,roundID=roundID,farm_name=farm_name,farm_id=farm_id,\
                                                house_name=house_name,house_id=house_id,device_id=device_id,device_name=device_name, \
                                                    list_cnt = list_cnt, images=images,url=url,search_date=search_date,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204


@main_control.route('/image_set_step_origin/<int:page_num>')
def image_set_step_origin(page_num):
    print("do image_set_step_origin()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id:" + device_id)

    device_name = str(request.args.get('device_name', default = '', type = str) )
    print("device_name:" + device_name)

    search_date = str(request.args.get('search_date', default = '', type = str) )
    print("search_date:" + search_date)

    search_date_converte = search_date.replace("-", "")
    print("search_date_converte:" + search_date_converte)


    house_id = str(request.args.get('house_id', default = '', type = str) )
    print("house_id:" + house_id)
    house_name = str(request.args.get('house_name', default = '', type = str) )
    print("house_name:" + house_name)

    farm_id = str(request.args.get('farm_id', default = '', type = str) )
    print("farm_id:" + farm_id)
    farm_name = str(request.args.get('farm_name', default = '', type = str) )
    print("farm_name:" + farm_name)

    roundID = str(request.args.get('roundID', default = '', type = str) )
    print("roundID:" + roundID)

    todays_datetime = datetime(datetime.today().year, datetime.today().month, datetime.today().day)

    print("todays_datetime : " + str(todays_datetime))
    print("datetime.today() : " + str(datetime.today()))


    three_days_ago = datetime.today() - timedelta(days = 3)
    print("three_days_ago : " + str(three_days_ago))

   
    if request.method == 'GET' :
        try:
            ##devices= db.session.query(DeviceDb).filter(DeviceDb.farm_id==farm_id).all()
            ##db.session.commit()
            ##print(devices);            
            images = ImageBasicDataDb.query.\
                        add_columns(ImageBasicDataDb.id,\
                                    func.ifnull(ImageBasicDataDb.created_at,'').label("created_at") ,\
                                    func.ifnull(ImageBasicDataDb.device_id,'').label("device_id"),\
                                    func.ifnull(ImageBasicDataDb.sequenceID,'').label("sequenceID"),\
                                    func.ifnull(ImageBasicDataDb.roundID,'').label("roundID"),\
                                    func.ifnull(ImageBasicDataDb.image_path,'').label("image_path"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_manul,'').label("check_defect_manul"),\
                                    func.ifnull(ImageBasicDataDb.check_defect_auto,'').label("check_defect_auto"),\
                                    func.ifnull(ImageBasicDataDb.plague_type,'').label("plague_type"),\
                                    func.ifnull(ImageBasicDataDb.plague_level,'').label("plague_level")).\
                                    filter(and_( ~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.device_id == device_id,ImageBasicDataDb.roundID == roundID, \
                                        ImageBasicDataDb.check_defect_manul == "true",\
                                        ImageBasicDataDb.targetDate.contains(search_date_converte)  )).\
                                paginate(per_page=6, page=page_num, error_out=True)

            print("len(images.items) : " + str(len(images.items)))
            list_cnt = len(images.items);

            for image in images.items:
                plague_type_list = image.plague_type
                print(str(plague_type_list))
                for temp in plague_type_list:
                    print(str(temp))
            
            url ="main_control.image_set_step";
            return render_template('/imageprocess/image_set_step.html', roundID=roundID,farm_name=farm_name,farm_id=farm_id,\
                                                house_name=house_name,house_id=house_id,device_id=device_id,device_name=device_name, \
                                                    list_cnt = list_cnt, images=images,url=url,search_date=search_date,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

@main_control.route('/image_update_plague_level', methods=['POST'])
def image_update_plague_level():
    print("image_update_plague_level")

    ##{"device_id":"2136-01-01-01","plague_type":["시들음병"]}
    #{"device_id":"2136-01-01-01"}

    if(request.data):
        jdata = request.get_json()
        image_id = jdata['image_id']
        print("image_id:" + image_id)

        plague_level = jdata['plague_level']
        print("plague_level:" + str(plague_level))
        print("====================================================================")

        
        try:
            upateQery = db.session.query(ImageBasicDataDb)
            upateQery = upateQery.filter(ImageBasicDataDb.id==image_id)
            upateDeviceRecord = upateQery.one()
            ##upateDeviceRecord.plague_level = str(plague_level)
            upateDeviceRecord.plague_level = plague_level

            db.session.commit()

            setDisorderData(image_id)
            print("update success ! ")
            print("====================================================================")

            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    
    return jsonify({"result":"no"})


## 생리장애 데이터
def setDisorderData(image_id):
    print("setDisorderData !!!!")
    print("##############################################################")
    print("#####################  setDisorderData      ##################")
    print("##############################################################")
    
    imagedatalist = db.session.query(ImageBasicDataDb).filter(and_(ImageBasicDataDb.id == image_id)).all()
    db.session.close()

    try:
        if not ( imagedatalist is None ) :
            for image in imagedatalist : 
                
                if image.plague_level is None or not image.plague_level :
                    break;
                #if( str(laonfarm.farm_id) != "knu2021"):
                # continue
                print("id : " + str(image.id))
                print("device_id : " + str(image.device_id))
                print("farm_id : " + str(image.farmID))
                print("house_id : " + str(image.greenHouseID))
                print("roundID : " + str(image.roundID))
                print("sequenceID : " + str(image.sequenceID))
                print("targetDate : " + str(image.targetDate))
                print("targetDate : " + str(image.targetDate))
                print("plague_level : " + str(image.plague_level))
                print("plague_type : " + str(image.plague_type))
            
                disorder_old_count= db.session.query(func.count(DisorderDataDb.device_id)).\
                                                filter(and_( DisorderDataDb.device_id == image.device_id \
                                                                        , DisorderDataDb.farmID == image.farmID \
                                                                        , DisorderDataDb.greenHouseID == image.greenHouseID \
                                                                        , DisorderDataDb.roundID == image.roundID \
                                                                        , DisorderDataDb.sequenceID == image.sequenceID \
                                                                        , DisorderDataDb.date == image.targetDate \
                                                                            
                                                                        )).scalar()
                print("disorder_old_count : " + str(disorder_old_count))
                db.session.close()

                if( disorder_old_count > 0 ) :

                    upateQery = db.session.query(DisorderDataDb)
                    upateQery = upateQery.filter(and_( DisorderDataDb.device_id == image.device_id \
                                                    , DisorderDataDb.farmID == image.farmID \
                                                    , DisorderDataDb.greenHouseID == image.greenHouseID \
                                                    , DisorderDataDb.roundID == image.roundID \
                                                    , DisorderDataDb.sequenceID == image.sequenceID \
                                                    , DisorderDataDb.date == image.targetDate \
                                                        
                                                    ))
                    for upatePestDataRecord in upateQery.all():
                        #######################
                        upatePestDataRecord.farmID = image.farmID
                        upatePestDataRecord.greenHouseID = image.greenHouseID
                        upatePestDataRecord.date = image.targetDate
                        upatePestDataRecord.sequenceID= image.sequenceID
                        upatePestDataRecord.roundID= image.roundID
                    
                        if  '질소' in image.plague_type :
                            upatePestDataRecord.nitrogen= image.plague_level
                        if '인' in image.plague_type :
                            upatePestDataRecord.phosphorus= image.plague_level
                        if '칼륨' in image.plague_type :
                            upatePestDataRecord.potassium= image.plague_level
                        if '마그네슘' in image.plague_type :
                            upatePestDataRecord.magnesium= image.plague_level
                        if '칼슘' in image.plague_type :
                            upatePestDataRecord.calcium= image.plague_level
                        if '기타' in image.plague_type :
                            upatePestDataRecord.etc= image.plague_level
                        #######################
                    db.session.commit()
                    db.session.close()
                    db.session.remove()
                    print ("update DisorderDataDb data from rest api !!  farmid: {} , round id : {} , date : {} ".format(image.farmID,image.roundID,image.targetDate))

                else:
                    nitrogen=""
                    phosphorus=""
                    potassium=""
                    magnesium=""
                    calcium=""
                    etc=""
           
                    if '질소' in image.plague_type :
                        nitrogen= image.plague_level
                    if '인' in image.plague_type :
                        phosphorus= image.plague_level
                    if '칼륨' in image.plague_type :
                        potassium= image.plague_level
                    if '마그네슘' in image.plague_type :
                        magnesium= image.plague_level
                    if '칼슘' in image.plague_type :
                        calcium= image.plague_level
                    if '기타' in image.plague_type :
                        etc= image.plague_level
                        
                    
                    db.session.add(DisorderDataDb(

                                device_id = image.device_id,
                                farmID = image.farmID,
                                greenHouseID = image.greenHouseID,
                                date = image.targetDate,
                                sequenceID= image.sequenceID,
                                roundID= image.roundID,
                                #######################
                                nitrogen= nitrogen,
                                phosphorus= phosphorus,
                                potassium= potassium,
                                magnesium= magnesium,
                                calcium= calcium,
                                etc= etc
                                
                                ))
                    db.session.commit()
                    db.session.close()
                    db.session.remove()
                    print ("add Disorder data from rest api !!  farmid: {} , round id : {} , date : {} ".format(image.farmID,image.roundID,image.targetDate))
    
    except Exception as ex:
        print("error : " + str(ex))




@main_control.route('/image_set_plague_level', methods=['POST'])
def image_set_plague_level():
    print("image_set_plague_level")

    ##{"device_id":"2136-01-01-01","plague_type":["시들음병"]}
    #{"device_id":"2136-01-01-01"}

    if(request.data):
        jdata = request.get_json()
        image_id = jdata['image_id']
        print("image_id:" + image_id)

        plague_level = jdata['plague_level']
        print("plague_level:" + str(plague_level))
        print("====================================================================")

        
        try:
            upateQery = db.session.query(ImageBasicDataDb)
            upateQery = upateQery.filter(ImageBasicDataDb.id==image_id)
            upateDeviceRecord = upateQery.one()
            ##upateDeviceRecord.plague_level = str(plague_level)
            upateDeviceRecord.plague_level = plague_level

            db.session.commit()

            print("update success ! ")
            print("====================================================================")

            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})
    
    return jsonify({"result":"no"})




## 이미지 리스트
@main_control.route('/get_image_detail_info')
def get_image_detail_info():
    print("do get_image_detail_info() !!!")

    image_id= request.args.get('image_id', default = '', type = str) 

    print("image_id :"  + image_id)
   
    image_temp= db.session.query(ImageBasicDataDb).filter(ImageBasicDataDb.id == image_id).first()
    print(image_temp)
    print("====================================================================")
    
    if image_temp:
        imageId= image_temp.id;
        targetDate= image_temp.targetDate;
        image_path= image_temp.image_path;
        plague_type= image_temp.plague_type;
        plague_level= image_temp.plague_level;
    
        greenHouseID = image_temp.greenHouseID;
        sequenceID = image_temp.sequenceID;
        roundID = image_temp.roundID;
        
        
        targetDate_temp = datetime.strptime(str(targetDate),"%Y%m%d")
        print("targetDate_temp : " + str(targetDate_temp))
        
        onedayago = targetDate_temp - timedelta(days = 1)
        onedayago = str(onedayago).replace("-","");
        onedayago = str(onedayago)[0:8]
        print("onedayago : " + str(onedayago))
        
        twodayago = targetDate_temp - timedelta(days = 2)
        twodayago = str(twodayago).replace("-","");
        twodayago = str(twodayago)[0:8]
        print("twodayago : " + str(twodayago))
        
        threedayago = targetDate_temp - timedelta(days = 3)
        threedayago = str(threedayago).replace("-","");
        threedayago = str(threedayago)[0:8]
        print("threedayago : " + str(threedayago))
        
        fourdayago = targetDate_temp - timedelta(days = 4)
        fourdayago = str(fourdayago).replace("-","");
        fourdayago = str(fourdayago)[0:8]
        print("fourdayago : " + str(fourdayago))
        
        print("onedayagoe : " + str(onedayago))
        print("twodayagoe : " + str(twodayago))
        print("threedayagoe : " + str(threedayago))
        print("fourdayagoe : " + str(fourdayago))
        
        images_oneday= db.session.query(ImageBasicDataDb).\
                filter(and_( ~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.targetDate == onedayago, \
                    ImageBasicDataDb.greenHouseID == greenHouseID, \
                        ImageBasicDataDb.sequenceID == sequenceID, \
                            ImageBasicDataDb.roundID == roundID
                    )).\
                    first()
        print(images_oneday)
        
        images_twoday= db.session.query(ImageBasicDataDb).\
                filter(and_( ~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.targetDate == twodayago, \
                    ImageBasicDataDb.greenHouseID == greenHouseID, \
                        ImageBasicDataDb.sequenceID == sequenceID, \
                            ImageBasicDataDb.roundID == roundID
                    )).\
                    first()
        print(images_twoday)
        
        
        images_threeday= db.session.query(ImageBasicDataDb).\
                filter(and_( ~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.targetDate == threedayago, \
                    ImageBasicDataDb.greenHouseID == greenHouseID, \
                        ImageBasicDataDb.sequenceID == sequenceID, \
                            ImageBasicDataDb.roundID == roundID
                    )).\
                    first()
        print(images_threeday)
        
        
        images_fourday= db.session.query(ImageBasicDataDb).\
                filter(and_( ~ImageBasicDataDb.image_path.contains('cam1') ,ImageBasicDataDb.targetDate == fourdayago, \
                    ImageBasicDataDb.greenHouseID == greenHouseID, \
                        ImageBasicDataDb.sequenceID == sequenceID, \
                            ImageBasicDataDb.roundID == roundID
                    )).\
                    first()
        print(images_fourday)

    result_json ={};
    
    image_json ={};
    image_path
    
    image_json.update({"imagePath" : image_path})
    image_json.update({"imageId" : imageId})
    image_json.update({"imagePlague_type_current" : plague_type})
    image_json.update({"imagePlague_level_current" : plague_level})
                        
    
    image_json.update({"imageID_oneday" : images_oneday.id})
    image_json.update({"imagePlague_type_oneday" : images_oneday.plague_type})
    image_json.update({"imagePlague_level_oneday" : images_oneday.plague_level })
    image_json.update({"imageDate_oneday" : "1일차"})
    image_json.update({"image_path_oneday" : images_oneday.image_path})
    image_json.update({"sequenceID_oneday" : images_oneday.sequenceID})
    image_json.update({"roundID_oneday" : images_oneday.roundID})
    
    
    image_json.update({"imageID_twoday" : images_twoday.id})
    image_json.update({"imagePlague_type_twoday" : images_twoday.plague_type})
    image_json.update({"imagePlague_level_twoday" : images_twoday.plague_level })
    image_json.update({"imageDate_twoday" : "2일차"})
    image_json.update({"image_path_twoday" : images_twoday.image_path})
    image_json.update({"sequenceID_twoday" : images_twoday.sequenceID})
    image_json.update({"roundID_twoday" : images_twoday.roundID})
    
    
    image_json.update({"imageID_threeday" : images_threeday.id})
    image_json.update({"imagePlague_type_threeday" : images_threeday.plague_type})
    image_json.update({"imagePlague_level_threeday" : images_threeday.plague_level })
    image_json.update({"imageDate_threeday" : "3일차"})
    image_json.update({"image_path_threeday" : images_threeday.image_path})
    image_json.update({"sequenceID_threeday" : images_threeday.sequenceID})
    image_json.update({"roundID_threeday" : images_threeday.roundID})
    
    
    image_json.update({"imageID_fourday" : images_fourday.id})
    image_json.update({"imagePlague_type_fourday" : images_fourday.plague_type})
    image_json.update({"imagePlague_level_fourday" : images_fourday.plague_level })
    image_json.update({"imageDate_fourday" : "4일차"})
    image_json.update({"image_path_fourday" : images_fourday.image_path})
    image_json.update({"sequenceID_fourday" : images_fourday.sequenceID})
    image_json.update({"roundID_fourday" : images_fourday.roundID})
    

    result_json.update(image_json)

    print("Final result_json : "  + str(image_json))    
    print("====================================================================")

    return jsonify({"result":result_json})


## 카메라 포인트 전달 
@main_control.route('/get_points_data_by_devic_id')
def get_points_data_by_devic_id():
    print("do get_points_data_by_devic_id() !!!")

    device_id= request.args.get('device_id', default = '', type = str) 

    print("device_id :"  + device_id)
   
    images= db.session.query(ImageBasicDataDb).filter(ImageBasicDataDb.device_id == device_id).all()

    print(images)
 
    print("====================================================================")
    
    result_json ={};
    temp_json ={};
    point_list_temp = []
    
    for image in images:
        point_json ={};
        point_json.update({"roundID" : image.roundID})
        
        if point_json not in point_list_temp :
            point_list_temp.append(point_json)


    result_json.update({"points" : point_list_temp})

    #print("Final result_json : "  + str(result_json))    
    print("====================================================================")

    return jsonify({"result":result_json})


###################################################################
## test aws
###################################################################
import time
##https://laonfarm.laonpeople.com:84/api/getSequenceList
##https://laonfarm.laonpeople.com:84/api/getRoundList
##https://laonfarm.laonpeople.com:84/api/getRoundImage
@main_control.route('/getImageDataFromDb',methods=['POST'])
def getImageDataFromDb():
    print("getImageDataFromDb !!!!")
    posted_data = request.get_json(force=True)
    print("posted_data : "  + str(posted_data))

    API_HOST = "https://laonfarm.laonpeople.com:84"

    url_image = API_HOST + "/api/getRoundImage"
    headers = {'Content-Type':'application/json','charset':'UTF-8','Accept':'*/*'}

    try :

        targetDate = posted_data['targetDate']   
        farmID = posted_data['farmID'] 

        laonfarms= db.session.query(LaonFarmDb).all()
        #laonfarms= db.session.query(LaonFarmDb).filter(LaonFarmDb.farm_id=="knu2021").all()
        
        db.session.close()

        if not ( laonfarms is None ) :
                
            for laonfarm in laonfarms :

                if(str(laonfarm.farm_id) != farmID):
                   continue
                print("farm_id : " + str(laonfarm.farm_id))
                print("house_id : " + str(laonfarm.house_id))
                print("sequence_id : " + str(laonfarm.sequence_id))
                print("round_id  : "  + str(laonfarm.round_id))
                
                print("targetDate : " + str(targetDate))

                body_image = {
                "farmID":laonfarm.farm_id,
                "greenHouseID": laonfarm.house_id,
                "targetDate": str(targetDate),
                "sequenceID": str(laonfarm.sequence_id),
                "roundID": str(laonfarm.round_id)
                }
 
                response_image = requests.post(url_image,headers=headers , data=json.dumps(body_image,ensure_ascii=False,indent="\t"), timeout=3)
                time.sleep(2)
                
                if( not ( response_image.text is None ) and not (response_image.text == "notyet") ):
                    image_json_list= json.loads(response_image.text)
                    ##print(type(image_json_list))
                    print("image_json_list : "  + str(image_json_list))

                    for image_dic in image_json_list :   
                        imageName = image_dic['imageName'] 
                        print("imageName  : "  + str(imageName))         
                        image_download(laonfarm.farm_id,laonfarm.house_id, targetDate,laonfarm.sequence_id,laonfarm.round_id,imageName)
        return jsonify({"result":"ok"})
    except Exception as ex:
        print("error : " + str(ex))
        return jsonify({"result":"error"})
    
###################################################################################################
## start : amazon aws s3 control
###################################################################################################
APP_ROOT = os.path.dirname(os.path.abspath(__file__))   # refers to application_top
APP_STATIC = os.path.join(APP_ROOT, 'static')
AWS_ROOT_PATH = "https://smartfarm-image.s3.ap-northeast-2.amazonaws.com/"
#knu2021/knu0001/20211020/2/1_1634688054_cam0_rgb.jpg
ACCESS_KEY_ID = 'AKIATU6R3K3A2HABZ7QQ' #s3 관련 권한을 가진 IAM계정 정보
ACCESS_SECRET_KEY = 'hIKN3DaqFooUSAH6KmA3ihLc47s6tuTvH+gxc7IM'
BUCKET_NAME = 'smartfarm-image'
import boto3

def image_download(farm_id,house_id,date,sequence_num,roundID,imageName):
    print("image_download :  imageName : "  + str(imageName))
    image_url= "http://laonfarm.laonpeople.com/testbed/api/getFile/" + str(farm_id) + "/" + str(house_id) + "/" + str(date) + "/" + str(sequence_num)+ "/" + str(imageName)
    print("image_url  : "  + str(image_url))  
    r = requests.get(image_url)

    file_basedir = os.path.dirname(__file__)

    image_path = "/" + str(farm_id) + "/" + str(house_id) + "/" + str(date)+ "/" + str(sequence_num)
    image_path_noneroot=  str(farm_id) + "/" + str(house_id) + "/" + str(date)+ "/" + str(sequence_num)
    
    directory = "./static/crop_image" + image_path
    directory_full_path= os.path.join(file_basedir, directory) 
    print("directory_full_path  : "  + str(directory_full_path))  
    if not os.path.exists(directory_full_path):
        os.makedirs(directory_full_path)

    '''
    ## success
    directory_full_path = "./static/crop_image/" + str(farm_name) + "/" + str(house_id)
    print("directory_full_path  : "  + str(directory_full_path))  
    if not os.path.exists(directory_full_path):
        os.makedirs(directory_full_path)
    '''


    file_full_path = directory_full_path + "/" + imageName      
    print("file_full_path  : "  + str(file_full_path))  
        
    with open(file_full_path,'wb') as f:
        f.write(r.content)

    image_full_path = str(farm_id) + "/" + str(house_id) + "/" + str(date)+ "/" + str(sequence_num)  + "/" + imageName   
    print("image_full_path befor aws  : "  + str(image_full_path))    
    image_full_path_aws = upload_file_to_aws(file_full_path,image_full_path)
    print("image_full_path_aws uploaded aws  : "  + str(image_full_path_aws))    
    
    if os.path.exists(file_full_path):
        os.remove(file_full_path)

    # db "/" 추가
    image_full_path = "/" + str(farm_id) + "/" + str(house_id) + "/" + str(date)+ "/" + str(sequence_num)  + "/" + imageName   
    insert_image(farm_id,house_id,date,sequence_num,roundID,image_full_path)

def insert_image(farmID,greenHouseID,targetDate,sequenceID,roundID,image_path):

    if(  ( not  image_path is None ) and image_path != ""  ):

        image_count= db.session.query(func.count(ImageBasicDataDb.id)).\
                    filter(and_( ImageBasicDataDb.farmID == farmID , ImageBasicDataDb.greenHouseID == greenHouseID ,\
                                 ImageBasicDataDb.targetDate == targetDate ,ImageBasicDataDb.sequenceID == sequenceID ,\
                                ImageBasicDataDb.roundID == roundID ,ImageBasicDataDb.image_path == image_path \
                             )).scalar()
        print("image_count : " + str(image_count))
        db.session.close()

        if( image_count > 0 ) :
        
            print ("image already is in ImageBasicDataDb !!")

        else:

            db.session.add(ImageBasicDataDb(
                        device_id = greenHouseID,
                        farmID = farmID,
                        greenHouseID = greenHouseID,
                        targetDate = targetDate,
                        sequenceID = sequenceID,
                        roundID = roundID,
                        image_path = image_path
                        ))
            db.session.commit()
            db.session.close()
            db.session.remove()

            print ("update ImageBasicDataDb !!  farmid: {} , round id : {} , date : {} ".format(farmID,roundID,targetDate))




def upload_file_to_aws(from_file_full_path,aws_file_full_path):
    print("upload_file_to_aws from_file_full_path : "  + str(from_file_full_path)) 
    print("upload_file_to_aws aws_file_full_path : "  + str(aws_file_full_path))
    try :
      
        s3 = boto3.client('s3',
                        aws_access_key_id=ACCESS_KEY_ID,
                        aws_secret_access_key= ACCESS_SECRET_KEY
                        )
        s3.upload_file(
                        Bucket = BUCKET_NAME,
                        Filename=from_file_full_path,
                        Key = aws_file_full_path)
        return AWS_ROOT_PATH + aws_file_full_path
    except Exception as ex:     
        print(ex)
        return "fail"
    

def get_file_temp_path(floder_name):
    try:
        directory_full_path = APP_STATIC + floder_name 
        if not os.path.exists(directory_full_path):
            os.makedirs(directory_full_path)

        directory_full_path = directory_full_path + "/"    
        print("directory_full_path  : "  + str(directory_full_path))  
        
        return directory_full_path
    except Exception as ex:     
        print(ex)
        return ""
    

                
###################################################################################################
## end amazon aws s3 control
###################################################################################################


###################################################################
## tech
###################################################################

@main_control.route('/techtip_delete_data', methods=['GET', 'POST'])
def techtip_delete_data():
    print("techtip_delete_data")
    

    tech_id = str(request.args.get('tech_id', default = '', type = str) )

    print("tech_id:" + tech_id)
    
    try:
        
        

        print("====================================================================")
        obj=db.session.query(TechDb).filter(TechDb.id==tech_id).first()
        session.delete(obj)
        db.session.commit()


        print("delete sucess !")
        print("====================================================================")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

@main_control.route('/techtip_insert_data', methods=['GET', 'POST'])
def techtip_insert_data():
    print("techtip_insert_data")
    
    if request.method == 'POST':
        id = request.form['id']
        kind = request.form['kind']
        title = request.form['title']
        content = request.form['content']

    elif request.method == 'GET':
        id = str(request.args.get('id', default = '', type = str) )
        kind = str(request.args.get('kind', default = '', type = str) )
        title = str(request.args.get('title', default = '', type = str) )
        content = str(request.args.get('content', default = '', type = str) )

    print("id:" + id)
    print("kind:" + kind)
    print("title:" + title)
    print("content:" + content)

    print("====================================================================")
    
    try:
        

        db.session.add(TechDb(kind=kind,title=title,content=content))
        db.session.commit()

        print("insert sucess !")
        print("====================================================================")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
        

@main_control.route('/techtip_update_data', methods=['GET', 'POST'])
def techtip_update_data():
    print("techtip_update_data")

    if request.method == 'POST':
        print("request.method == 'POST'")
        id = request.form['id']
        kind = request.form['kind']
        title = request.form['title']
        content = request.form['content']

    elif request.method == 'GET':
        print("request.method == 'Get'")
        id = str(request.args.get('id', default = '', type = str) )
        kind = str(request.args.get('kind', default = '', type = str) )
        title = str(request.args.get('title', default = '', type = str) )
        content = str(request.args.get('content', default = '', type = str) )

    print("id:" + id)
    print("kind:" + kind)
    print("title:" + title)
    print("content:" + content)

    try:
        print("====================================================================")

        

        upateQery = db.session.query(TechDb)
        upateQery = upateQery.filter(TechDb.id==id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.kind = kind
        upateDeviceRecord.title = title
        upateDeviceRecord.content = content

        db.session.commit()

        print("update success ! ")

        print("====================================================================")

        return jsonify({"result":"yes"})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@main_control.route('/techtip_get_data_all', methods=['GET', 'POST'])
def techtip_get_data_all():
    print("techtip_get_data_all")

    print("====================================================================")
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    techlist= db.session.query(TechDb).all()

    ##print(techlist)
    print("====================================================================")

    result_json ={}
    json_tech_list = []

    try:
    
        for tech in techlist:
        
            tech_json ={};
            tech_json.update({"id" : tech.id})
            tech_json.update({"kind" : tech.kind})
            tech_json.update({"title" : tech.title})
            tech_json.update({"content" : tech.content})

            json_tech_list.append(tech_json)

    
        ##print(json_tech_list)

    except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    return jsonify({"result":json_tech_list})



@main_control.route('/techtip_get_data_single', methods=['GET', 'POST'])
def techtip_get_data_single():
    print("techtip_get_data")


    id = str(request.args.get('id', default = '', type = str) )
    print("id:" + id)



    print("====================================================================")
    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    session = Session()

    techlist= db.session.query(TechDb).filter(TechDb.id == id).all()


    print(techlist)
    print("====================================================================")

    tech_list = []

    try:
    
        for tech in techlist:
        
            tech_json ={};
            tech_json.update({"id" : tech.id})
            tech_json.update({"kind" : tech.kind})
            tech_json.update({"title" : tech.title})
            tech_json.update({"content" : tech.content})

            tech_list.append(tech_json)

    except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify({"result":tech_list})


@main_control.route('/techtip_main', methods=['GET', 'POST'])
def techtip_main():
    print("techtip_main")
    return render_template('/techtip/techtip_main.html')

@main_control.route('/techtip_view', methods=['GET', 'POST'])
def techtip_view():
    print("techtip_view")

    id = str(request.args.get('id', default = '', type = str) )
    print("id:" + id)

    print("====================================================================")

    tech_id = ""
    kind = ""
    title = ""
    content = ""

    try :
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        #print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        session = Session()

        techlist= db.session.query(TechDb).filter(TechDb.id == id).all()

        for tech in techlist:
            tech_id =tech.id
            kind = tech.kind
            title = tech.title
            content = tech.content


        print("kind  " + kind)
        print("title  " + title)
        print("====================================================================")
   
    except Exception  as ex :
        print(ex)



    return render_template('/techtip/techtip_view.html',id=tech_id,kind=kind,title=title,content=content)

###################################################################
## test
###################################################################


@main_control.route('/test_table', methods=['GET', 'POST'])
def test_table():
    
    return render_template('/test/table_ajax_test.html')



@main_control.route('/table_datatable_basic', methods=['GET', 'POST'])
def table_datatable_basic():
    
    return render_template('/test/table-datatable-basic.html')

@main_control.route('/test_home', methods=['GET'])
def test_home():
    print("home()")
    userid = session.get('userid', None)
    todos = []
    if userid:
        fcuser = Fcuser.query.filter_by(userid=userid).first()
        todos = Todo.query.filter_by(fcuser_id=fcuser.id)
        return render_template('/home.html', userid=userid, todos=todos)
    else:
  
        form = LoginForm()
        if form.validate_on_submit():
            session['userid'] = form.data.get('userid')

            return redirect('/')

        return render_template('/test/login.html', form=form)


@main_control.route('/login_test', methods=['GET'])
def login_test():
    print("login_test()")
    return render_template('/login/authentication-login1.html')


@main_control.route('/test_js_css', methods=['GET'])
def testpath():
    print("test_js_css()")
    return render_template('/test/test.html')



@main_control.route('/test/login', methods=['GET', 'POST'])
def test_login():
    form = LoginForm()
    if form.validate_on_submit():
        session['userid'] = form.data.get('userid')
        print("session['userid'] = form.data.get('userid')")
        return redirect('/')


    print("no session['userid'] = form.data.get('userid')")
    return render_template('/test/login.html', form=form)

@main_control.route('/test/logout', methods=['GET'])
def test_logout():
    session.pop('userid', None)
    return redirect('/')

@main_control.route('/test/register', methods=['GET', 'POST'])
def test_register():
    form = RegisterForm()
    if form.validate_on_submit():
        fcuser = Fcuser()
        fcuser.userid = form.data.get('userid')
        fcuser.password = form.data.get('password')

        db.session.add(fcuser)
        db.session.commit()

        return redirect('/test/login')

    return render_template('/test/register.html', form=form)
