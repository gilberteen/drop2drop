$(function () {
    "use strict";
	
	//Line Chart
	//labels_ = "4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,4.10,4.11";
	//labels_2 = [4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,4.10,4.11];

	// 리스트 생성
	var lables_ = new Array() ;
	for(var i=1; i<=10; i++){
		// 객체 생성
	
		//var data = new Object() ;
		//data.number = i ;
		//data.name = "4." + i ;
		var date_ = '4.' + i ;
		// 리스트에 생성된 객체 삽입
		lables_.push(date_) ;
	}
	var data_intemp = new Array() ;
	data_intemp = [50,10,70,10,40,50,60,70,10,90,100];

	new Chart(document.getElementById("line-chart"), {
	  type: 'line',
	  data: {
		labels: lables_,
		datasets: [{ 
			data: data_intemp,
			label: "내부온도",
			borderColor: "#3e95cd",
			fill: false
		  }, { 
			data: [100,10,78,30,40,27,60,70,10,19,20],
			label: "내부습도",
			borderColor: "#26a2ef",
			fill: false
		  }, { 
			data: [11,10,20,80,40,21,60,70,10,90,90],
			label: "실내CO2",
			borderColor: "#07b107",
			fill: false
		  }, { 
			data: [90,10,20,30,40,50,20,70,80,90,100],
			label: "내부광량",
			borderColor: "#ff6384",
			fill: false
		  }, { 
			data: [70,10,20,30,35,50,30,70,10,90,87],
			label: "근권부온도",
			borderColor: "#4bcfc0",
			fill: false
		  },{ 
			data: [30,10,20,30,20,50,30,32,10,10,87],
			label: "근권부습도",
			borderColor: "#fbccc0",
			fill: false
		  },{ 
			data: [70,10,21,30,23,50,30,60,10,90,87],
			label: "근권부EC",
			borderColor: "ffbc0c0",
			fill: false
		  }
		]
	  },
	  options: {
		title: {
		  display: true,
		  text: '센서_앞'
		}
	  }
	});

	// line second
}); 