# manage_admin.py 
import os 
from flask_script import Manager 
from app import create_admin_app 
import ssl
from flask_cors import CORS

app = create_admin_app(os.getenv("TEST_APP") or "prod") 
#1.전체 적용시
CORS(app)

#2.해당api만 적용시
#cors = CORS(app, resources={ r"/api_cc/*": {"origin": "*"},})

##app = create_app()
manager = Manager(app) 
@manager.command 
def run(): 

    #app.run(host="0.0.0.0", port=5000, debug=True)
    app.run(host="0.0.0.0", port=443, debug=True,ssl_context=('./app/certi/admin/www_drop2droporder_com.crt', './app/certi/admin/www_drop2droporder_com.key'))
      

if __name__ == "__main__": 
    manager.run()


