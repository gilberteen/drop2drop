
import sys
import os

from wtforms.fields.core import DateTimeField, IntegerField
sys.path.append('..')
from modelpool.models import db, TB_DROP_USER

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, EqualTo
import utill
from flask import flash
from sqlalchemy import or_ , and_


class MainFrame(FlaskForm):
    maint_path = StringField('maint_path', validators=[DataRequired()])


class RegisterForm(FlaskForm):
    userid = StringField('userid', validators=[DataRequired()])
    username = StringField('username', validators=[DataRequired()])
    company_code = StringField('company_code', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    repassword = PasswordField('repassword', validators=[DataRequired()])
    ##bod = StringField('bod', validators=[DataRequired()])
    email = StringField('email')
    phone_number = StringField('phone_number')

class LoginForm(FlaskForm):

    class UserPassword(object):
        def __init__(self, message=None):
            self.message = message
            
        def __call__(self, form, field):
            userid = form['userid'].data
            company_code = form['company_code'].data
            password = field.data

            print("userid :" + userid)
            print("password :" + password)

            fcuser = TB_DROP_USER.query.filter( and_( TB_DROP_USER.user_id==str(userid),TB_DROP_USER.company_code==str(company_code) ) ).first()

            if not ( fcuser is None) :
                if fcuser.password != password: 
                    print("Wrong password")
                    flash("Wrong password")
                    raise ValueError('')
            else:
                 print("Tere is no user in db")
                 flash("Id is not exist")
                 raise ValueError('')
       

    userid = StringField('userid', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired(), UserPassword()])
    company_code = StringField('company_code', validators=[DataRequired()])



class FarmBasicForm(FlaskForm):
   
    user_id = StringField('user_id', validators=[DataRequired()])
    user_name = StringField('user_name', validators=[DataRequired()])
    user_bod = StringField('user_bod', validators=[DataRequired()])
    farm_name = StringField('farm_name', validators=[DataRequired()])
    farm_crop = StringField('farm_crop', validators=[DataRequired()])
    farm_email = StringField('farm_email', validators=[DataRequired()])
    farm_mobile = StringField('farm_mobile', validators=[DataRequired()])
    farm_tel = StringField('farm_tel', validators=[DataRequired()])
    farm_fax = StringField('farm_fax', validators=[DataRequired()])
    farm_address = StringField('farm_address', validators=[DataRequired()])


    @property
    def serialize(self):
        return{
                'user_id' : self.user_id,
                'user_name' : self.user_name,
                'user_bod' : self.user_bod ,
                'farm_name' : self.farm_name ,
                'farm_crop' : self.farm_crop ,
                'farm_email': self.farm_email ,
                'farm_mobile' : self.farm_mobile ,
                'farm_tel': self.farm_tel ,
                'farm_fax': self.farm_fax,
                'farm_address': self.farm_address
            }
    
    def set_user_id(self,user_id_):
        user_id = user_id_



class NoticeForm(FlaskForm):
   
    index = IntegerField('index', validators=[DataRequired()])
    create_at = DateTimeField('create_at', validators=[DataRequired()])
    title = StringField('title', validators=[DataRequired()])
    urgent_yn = StringField('urgent_yn', validators=[DataRequired()])
    catagory = StringField('catagory', validators=[DataRequired()])
    detail_item = StringField('detail_item', validators=[DataRequired()])
    content = StringField('content', validators=[DataRequired()])


    farm_owner = StringField('farm_owner', validators=[DataRequired()])
    house_name = StringField('house_name', validators=[DataRequired()])
    crop = StringField('crop', validators=[DataRequired()])
    kind = StringField('kind', validators=[DataRequired()])
    check_yn = StringField('check_yn', validators=[DataRequired()])
    accum_check_yn = StringField('accum_check_yn', validators=[DataRequired()])

    send_yn = StringField('send_yn', validators=[DataRequired()])
    receive_person_list = StringField('receive_person_list', validators=[DataRequired()])
