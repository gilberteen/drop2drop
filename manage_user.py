# manage_user.py 
import os 
from flask_script import Manager 
from app import create_user_app 

app = create_user_app(os.getenv("TEST_APP") or "prod") 
##app = create_app()
manager = Manager(app) 

## user web port : 443
@manager.command 
def run(): 
    #app.run(host="0.0.0.0", port=80, debug=True)
    app.run(host="0.0.0.0", port=443, debug=True,ssl_context=('./app/certi/user/www_goeunai.com_cert.crt', './app/certi/user/www_goeunai.com.key'))

    #app.run(host="0.0.0.0", port=443, debug=True,ssl_context=('./app/certi/user/www_goeunai.com_chain_cert.crt', './app/certi/user/www_goeunai.com.key'))
    #app.run(host="0.0.0.0", port=443, debug=True,ssl_context=('./app/certi/user/www_goeunai.com_root_cert.crt', './app/certi/user/www_goeunai.com.key'))


if __name__ == "__main__": 
    manager.run()

