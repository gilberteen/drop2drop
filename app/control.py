
# app/control.py
# 
import flask
from flask.helpers import make_response
import app
import os
from re import search
from typing import Awaitable

from flask import Flask, Blueprint, url_for
from flask import request, redirect, render_template, session
from flask import jsonify
from flask.wrappers import Response
from flask_wtf import form
from sqlalchemy.sql import text,func
from sqlalchemy.types import String
from sqlalchemy import or_ , and_  ,not_
from sqlalchemy import Date, cast
from sqlalchemy.sql.sqltypes import DateTime , Integer
from sqlalchemy.sql.expression import asc, desc, false, insert, true
import sys
import os
from sqlalchemy.sql.expression import false, insert, true

from sqlalchemy.sql.functions import count, random
sys.path.append('.')

from modelpool.models import TB_COMPANY, TB_FOOD, TB_ODERS, TB_OFFERS, db , TB_DROP_USER ,TB_DROP_ADMIN
from .formspool.forms import RegisterForm, LoginForm 

from api_v1 import api as api_v1
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import func,Column

from datetime import datetime , timedelta,date
import json
import random
import string
import ast
import pickle
import pandas as pd

## return the list to oder
from datetime import date


main_control = Blueprint('main_control',__name__)
per_page_count = 10
home_url=""


## SSL검증용
@main_control.route('/.well-known/pki-validation/gsdv.txt', methods=['GET'])
def pki_validation():
    print("pki_validation !!!")  
    
    file_basedir = os.path.dirname(__file__)
    directory = "./static/pki-validation/gsdv.txt"
    directory_full_path= os.path.join(file_basedir, directory) 

    response = make_response(open(directory_full_path).read())
    response.headers["Content-type"] = "text/plain"
    return response

###################################################################
## login
###################################################################

@main_control.route('/privaeinfo', methods=['GET','POST'])
def privaeinfo():
    print("do privaeinfo()")

    return render_template('/useragreement/priviateinfo.html')


@main_control.route('/serviceagreement', methods=['GET','POST'])
def serviceagreement():
    print("do serviceagreement()")

    return render_template('/useragreement/serviceagreement.html')


###################################################################
### main
###################################################################

@main_control.route('/main', methods=['GET'])
def main():
    print("do main()")
    url_from = request.url 
    print("request.url  : " + str(request.url))

   
    sesstion_userid = session.get('userid', None)
    print(sesstion_userid)

    temp_now = datetime.now()
    today_date_time = temp_now.strftime("%Y-%m-%d %H:%M:%S")
    print("today_date_time : " + str(today_date_time))
    
    today_date_time_h_m= temp_now.strftime("%Y-%m-%d %H:%M")
    print("today_date_time_h_m : " + str(today_date_time_h_m))
    
    today_date= temp_now.strftime("%Y-%m-%d")
    print("today_date : " + str(today_date))

    three_days_ago = datetime.today() - timedelta(days = 3)
    three_days_ago = three_days_ago.strftime("%Y-%m-%d %H:%M:%S")
    print("three_days_ago : " + str(three_days_ago))

    try:
        user_id = ""
        company_code=""
        name = ""
        email =""
        temp_user = TB_DROP_USER.query.filter(TB_DROP_USER.user_id ==sesstion_userid).first()
        print(temp_user)
        if not ( temp_user is None) :
            user_id = temp_user.user_id
            company_code = temp_user.company_code
            name = temp_user.name
            email = temp_user.email
            print("in main , user_id :" +  user_id)
            print("in main , company_code :" +  company_code)
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

    return render_template('/main/main.html', user_id=user_id,company_code=company_code,name=name ,email=email  )


@main_control.route('/view_orderlist', methods=['GET'])
def view_orderlist():
    print("view_orderlist()")
    url_from = request.url 
    print("request.url  : " + str(request.url))

    admin_company_code= request.args.get('admin_company_code', default = '', type = str) 

    try:
        user_id = ""
        company_code=""
        name = ""
        email =""
        temp_user = TB_DROP_USER.query.filter(TB_DROP_USER.user_id ==session.get('userid', None)).first()
        print(temp_user)
        if not ( temp_user is None) :
            user_id = temp_user.user_id
            ##company_code = temp_user.company_code
            name = temp_user.name
            email = temp_user.email
            print("in main , user_id :" +  user_id)
            print("in main , company_code :" +  company_code)
            
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"system error !"})

   
    return render_template('/main/view_orderlist.html',user_id=user_id,company_code=admin_company_code,name=name ,email=email )

    

@main_control.route('/loginDo',  methods=['GET','POST'])
def loginDo():
    print("do loginDo()")
  
    form = LoginForm()
    if form.validate_on_submit():
        session['userid'] = form.data.get('userid')
        return redirect('/main')

    return render_template('/login/authentication-login1.html', form=form)

@main_control.route('/login', methods=['GET'])
def login():
    print("do login()")
    form = LoginForm()
    return render_template('/login/authentication-login1.html', form=form)
        
          
@main_control.route('/logout', methods=['GET','POST'])
def logout():
    print("do logout()")

    session.pop('userid', None)
    return redirect('/main')

@main_control.route('/regist', methods=['GET'])
def regist():
    print("do regist()")
    form = RegisterForm()
    return render_template('/login/authentication-register1.html', form=form)


@main_control.route('/regist_do', methods=['GET', 'POST'])
def regist_do():
    print("regist_do()")

    form = RegisterForm()

    if form.validate_on_submit():

        print("form.validate_on_submit()")

        company = TB_COMPANY.query.filter(TB_COMPANY.company_code ==form.data.get('company_code')).first()
        print(company)
        if not ( company is None) :
           
            fcuser = TB_DROP_USER()
            fcuser.user_id = form.data.get('userid')
            fcuser.name = form.data.get('username')
            fcuser.password = form.data.get('password')
            fcuser.company_code = form.data.get('company_code')
            fcuser.email = form.data.get('email')
            fcuser.phone_number = form.data.get('phone_number')
            fcuser.role = str("0")

            db.session.add(fcuser)
            db.session.commit()

            print("sucess insert new user!")
            return redirect('/login')
        else :
            print("company code is not vaild!)")
            return render_template('/login/authentication-register1.html', form=form)

    print(form.errors)
    print("not form.validate_on_submit()")
    return render_template('/login/authentication-register1.html', form=form)



@main_control.route('/find_id', methods=['GET','POST'])
def find_id():
    print("do find_id()")

    return render_template('/login/find_id.html')


@main_control.route('/get_user_id',methods=['GET','POST'])
def get_user_id():
    print("get_user_id")


    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')

    if request.method == 'POST' :

        username = request.form.get('username')
        print("user_name : "  + str(username))

        try:
            temp_user = TB_DROP_USER.query.filter(TB_DROP_USER.username ==username).first()
            print(temp_user);
            if not ( temp_user is None) :
                print("user is exgisit !")
                print("temp_user.userid : " + str(temp_user.userid))
                return jsonify({"result":temp_user.userid})
            else:
                print("user is not exgisit !")
                return jsonify({"result":"no"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204



##@main_control.route('/check_user/<userid>',methods=['GET','PUT','DELETE'])
##def check_user(userid):
##위 사용처럼 사용할 경우엔 보내는 ajax에서 반드시  url 스트링으로 파라매터를 구성해야함. "/check_user/실제유저아이디"
@main_control.route('/check_user',methods=['GET','PUT','DELETE'])
def check_user():
    print("check_user")

    print("reqeust querystring:" + str(request.query_string))

    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')

    if request.method == 'GET' :
        print("reqeust userid:" + request.args.get('userid'))

        userid = request.args.get('userid')

        try:
            temp_user = TB_DROP_USER.query.filter(TB_DROP_USER.user_id ==userid).first()
            
            print(temp_user);
            if not ( temp_user is None) :
                if(temp_user.user_id == userid):
                    print("user is exgisit !")
                    return jsonify({"result":"yes"})
                    ##return jsonify(user.serialize)
                else:
                    print("user is not exgisit1 !")
                    return jsonify({"result":"no"})
            else:
                print("user is not exgisit2 !")
                return jsonify({"result":"no"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"result":"no"})
    else:
        return jsonify(),204

#############################################################################################
## admin menu
#############################################################################################

@main_control.route('/admin_view_order_list',methods=['GET','PUT','DELETE'])
def admin_view_order_list():
  print("admin_view_order_list")
  return render_template('/admin/admin_view_order_list.html')


@main_control.route('/admin_view_offer_list',methods=['GET','PUT','DELETE'])
def admin_view_offer_list():
  print("admin_view_offer_list")
  return render_template('/admin/admin_view_offer_list.html')



## loadingCompanyCode
@main_control.route('/loadingCompanyCode', methods=['GET'])
def loadingCompanyCode():
    print("loadingCompanyCode()")
    print("request.url  : " + str(request.url))

    ##offer_index = request.form.get('offer_index')
    ##print("offer_index  : " + str(offer_index))

    try:
        # check exigst data
        companys= db.session.query(TB_COMPANY ).all()
        db.session.close()
        print("companys : " + str(companys))
       
        company_list = []
        if( companys ):
            for company in companys :
                company_json = {}
                company_json.update({"company_code" : company.company_code})
                company_json.update({"company_name" : company.company_name})
                
                company_list.append(company_json)
        
        return jsonify({"result":"ok","companys":company_list})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


## loadingOrdersForAdmin
@main_control.route('/loadingOrdersForAdmin', methods=['GET'])
def loadingOrdersForAdmin():
    print("loadingOrdersForAdmin()")
    print("request.url  : " + str(request.url))

    company_code= request.args.get('company_code', default = '', type = str) 
    print("company_code  : " + str(company_code))

    order_date= request.args.get('order_date', default = '', type = str) 
    print("order_date  : " + str(order_date))


    offer_type= request.args.get('offer_type', default = '', type = str) 
    print("offer_type  : " + str(offer_type))

    try:
        # check exigst data
    
        order_list = get_order_menu_list_to_download(company_code,order_date,offer_type,"All")
        print("order_list  : " + str(order_list))
        return jsonify({"result":"ok","order_list":order_list})
    

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


## loadingOffersForAdmin
@main_control.route('/loadingOffersForAdmin', methods=['GET'])
def loadingOffersForAdmin():
    print("loadingOffersForAdmin()")
    print("request.url  : " + str(request.url))

    company_code= request.args.get('company_code', default = '', type = str) 
    print("company_code  : " + str(company_code))


    company_name= request.args.get('company_name', default = '', type = str) 
    print("company_name  : " + str(company_name))

    order_date= request.args.get('order_date', default = '', type = str) 
    print("order_date  : " + str(order_date))


    offer_type= request.args.get('offer_type', default = '', type = str) 
    print("offer_type  : " + str(offer_type))

    try:
        # check exigst data
    
        offer_list = get_offer_list(company_code,company_name,order_date,offer_type,"All")
        print("offer_list  : " + str(offer_list))
        return jsonify({"result":"ok","offer_list":offer_list})
    

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


@main_control.route('/makeOffer', methods=['GET'])
def makeOffer():
    print("makeOffer()")
    print("request.url  : " + str(request.url))

    menu_id= request.args.get('menu_id', default = '', type = str) 
    print("menu_id  : " + str(menu_id))

    company_code= request.args.get('company_code', default = '', type = str) 
    print("company_code  : " + str(company_code))


    offer_date= request.args.get('offer_date', default = '', type = str) 
    print("offer_date  : " + str(offer_date))

    offer_type= request.args.get('offer_type', default = '', type = str) 
    print("offer_type  : " + str(offer_type))

    try:
        # check exigst data
        offer_list = db.session.query(TB_OFFERS )\
                                            .filter(and_(TB_OFFERS.menu_id ==menu_id , TB_OFFERS.company_code ==company_code ,TB_OFFERS.offer_date ==offer_date ,TB_OFFERS.offer_type ==offer_type))\
                                            .first()
        db.session.close()

        if offer_list :
            return jsonify({"result":"fail", "error":"offer is already in offer list"})
        else :

            db.session.add(TB_OFFERS(
                            ##id = str(notice_id),
                            menu_id = str(menu_id),
                            company_code = str(company_code),
                            offer_date = str(offer_date),
                            offer_type = str(offer_type)
                        ))
            db.session.commit()
            return jsonify({"result":"ok"})
    

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


@main_control.route('/cancelOffer', methods=['GET'])
def cancelOffer():
    print("cancelOffer()")
    print("request.url  : " + str(request.url))

    offer_index= request.args.get('offer_index', default = '', type = str) 
    print("offer_index  : " + str(offer_index))

    try:
        deteteQuery=db.session.query(TB_OFFERS).filter(TB_OFFERS.offer_index==offer_index)
        deteteQuery.delete(synchronize_session=False)
        db.session.commit()
        return jsonify({"result":"ok"})
    

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})



#############################################################################################
@main_control.route('/loadingOffers', methods=['GET'])
def loadingOffers():
    print("loadingOffers()")
    userid = session.get('userid', None)
    print(userid)

    url_from = request.url 
    print("request.url  : " + str(request.url))

    user_id= request.args.get('user_id', default = '', type = str) 
    print("user_id  : " + str(user_id))
    company_code= request.args.get('company_code', default = '', type = str) 
    print("company_code  : " + str(company_code))
    offer_date= request.args.get('offer_date', default = '', type = str) 
    print("offer_date  : " + str(offer_date))
    offer_type= request.args.get('offer_type', default = '', type = str) 
    print("offer_type  : " + str(offer_type))
    menu_kind= request.args.get('menu_kind', default = '', type = str) 
    print("menu_kind  : " + str(menu_kind))

    

    try:
        if user_id == "" :
            menu_list  , restorant_name_list= get_menu_list_all(userid,offer_date,offer_type,menu_kind)
        else :
            menu_list , restorant_name_list = get_menu_list(userid,offer_date,offer_type,menu_kind)

        print("menu_list  : " + str(menu_list))
        print("offer_type  : " + str(offer_type))
        print("menu_kind  : " + str(menu_kind))
        return jsonify({"result":"ok","menu_list":menu_list,"offer_type":offer_type,"menu_kind":menu_kind , "restorant_name_list" : restorant_name_list})
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"fail"})


@main_control.route('/loadingOrders', methods=['GET'])
def loadingOrders():
    print("loadingOrders()")
    userid = session.get('userid', None)
    print(userid)

    url_from = request.url 
    print("request.url  : " + str(request.url))

    user_id= request.args.get('user_id', default = '', type = str) 
    print("user_id  : " + str(user_id))
    company_code= request.args.get('company_code', default = '', type = str) 
    print("company_code  : " + str(company_code))
    order_date= request.args.get('order_date', default = '', type = str) 
    print("order_date  : " + str(order_date))
    offer_type= request.args.get('offer_type', default = '', type = str) 
    print("offer_type  : " + str(offer_type))
    menu_kind= request.args.get('menu_kind', default = '', type = str) 
    print("menu_kind  : " + str(menu_kind))


    try:
         
        menu_list = get_order_menu_list(company_code,order_date,offer_type,menu_kind)
        print("menu_list  : " + str(menu_list))
        print("offer_type  : " + str(offer_type))
        print("menu_kind  : " + str(menu_kind))
        return jsonify({"result":"ok","menu_list":menu_list,"offer_type":offer_type,"menu_kind":menu_kind})
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"fail"})


@main_control.route('/loadingOrdersToDownload', methods=['GET'])
def loadingOrdersToDownload():
    print("loadingOrdersToDownload()")
    userid = session.get('userid', None)
    print(userid)

    url_from = request.url 
    print("request.url  : " + str(request.url))


    company_code= request.args.get('company_code', default = '', type = str) 
    print("company_code  : " + str(company_code))
    order_date= request.args.get('order_date', default = '', type = str) 
    print("order_date  : " + str(order_date))
    offer_type= request.args.get('offer_type', default = '', type = str) 
    print("offer_type  : " + str(offer_type))
    menu_kind= request.args.get('menu_kind', default = '', type = str) 
    print("menu_kind  : " + str(menu_kind))


    try:
         
        menu_list = get_order_menu_list_to_download(company_code,order_date,offer_type,menu_kind)
        print("menu_list  : " + str(menu_list))
        print("offer_type  : " + str(offer_type))
        print("menu_kind  : " + str(menu_kind))
        return jsonify({"result":"ok","menu_list":menu_list,"offer_type":offer_type,"menu_kind":menu_kind})
        
    except Exception as ex:     
        print(ex)
        return jsonify({"result":"fail"})




@main_control.route('/loadingUsers', methods=['GET'])
def loadingUsers():
    print("loadingUsers()")
    url_from = request.url 
    print("request.url  : " + str(request.url))

    offer_index= request.args.get('offer_index', default = '', type = str) 

    user_list = get_user_list_off_offer(offer_index)
    
   
    return jsonify({"result":"ok","offer_index":offer_index,"user_list":user_list})



from datetime import datetime
## input order
@main_control.route('/input_order', methods=['POST'])
def input_order():
    print("input_order()")
    print("request.url  : " + str(request.url))


    offer_index = request.form.get('offer_index')
    print("offer_index  : " + str(offer_index))


    user_id = request.form.get('user_id')
    print("user_id  : " + str(user_id))

    offer_date = request.form.get('offer_date')
    print("offer_date  : " + str(offer_date))

    offer_type = request.form.get('offer_type')
    print("offer_type  : " + str(offer_type))



    # 1. if today is after 2days ?
    today = datetime.today()
    today = today.strftime("%Y-%m-%d")
    print("Current today =", today)

    after2day = datetime.today()+timedelta(days=1)
    print("after2day =", str(after2day))
    after2day = after2day.strftime("%Y-%m-%d")
    print("after2day =", str(after2day))

    if after2day == offer_date :
        current_time = datetime.strptime(datetime.now().strftime('%H:%M:%S'), '%H:%M:%S')
        print("current_time =", str(current_time))

        limit_time = datetime.strptime("17:00:00", '%H:%M:%S')
        print("limit_time =", str(limit_time))

        # 2. if time is over ?
        if current_time > limit_time :
            print("current_time > limit_time !!")
            return jsonify({"result":"fail", "error":"You can not order because the time is over!"})
        else :
            print("current_time <= limit_time !!")


    try:
        # check exigst data
       
        orders= db.session.query(TB_ODERS )\
                                        .filter(and_(TB_ODERS.user_id ==user_id ,TB_ODERS.order_date == offer_date))\
                                        .all()
        db.session.close()
        print("orders : " + str(orders))
       
        if( len(orders) > 0 ):


            for order in orders :
                offers = db.session.query(TB_OFFERS )\
                                            .filter(and_(TB_OFFERS.offer_index ==order.offer_index))\
                                            .first()
                db.session.close()

                print("offers : " + str(offers))
                if ( not (offers is None) ):
                    print("offers.offer_type : " + offers.offer_type)
                    if  offers.offer_type == offer_type :
                        print("Order is duplicated !!!")
                        return jsonify({"result":"fail" , "error":"Order is duplicated!"})


            print("order is requested  1!!!")
            db.session.add(TB_ODERS(
                order_date = str(offer_date),
                user_id = str(user_id),
                offer_index = str(offer_index)
            ))
            db.session.commit()
            return jsonify({"result":"ok"})

        else :
            print("order is requested 2 !!!")
            db.session.add(TB_ODERS(
                    order_date = str(offer_date),
                    user_id = str(user_id),
                    offer_index = str(offer_index)
                ))
            db.session.commit()

       
        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})

## input cancel_order
@main_control.route('/cancel_order', methods=['POST'])
def cancel_order():
    print("cancel_order()")
    print("request.url  : " + str(request.url))

    order_index = request.form.get('order_index')
    print("order_index  : " + str(order_index))

    try:
        # check exigst data
        check_data= db.session.query(func.count(TB_ODERS.order_index)).\
                        filter(and_(TB_ODERS.order_index==order_index )).scalar()      
        db.session.close()
        if( not(check_data > 0) ) : 
            return jsonify({"result":"fail" , "error":"data is not exist"})
            
        else :
            u = db.session.get(TB_ODERS, order_index)
            db.session.delete(u)
            db.session.commit()
            db.session.close()
            return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


## update_order_nut_free
@main_control.route('/update_order_nut_free', methods=['POST'])
def update_order_nut_free():
    print("update_order_nut_free()")
    print("request.url  : " + str(request.url))

    order_index = request.form.get('order_index')
    print("order_index  : " + str(order_index))

    nut_free = request.form.get('nut_free')
    print("nut_free  : " + str(nut_free))

    try:
        # check exigst data
        check_data= db.session.query(func.count(TB_ODERS.order_index)).\
                        filter(and_(TB_ODERS.order_index==order_index )).scalar()      
        db.session.close()
        if( not(check_data > 0) ) : 
            return jsonify({"result":"fail" , "error":"data is not exist"})
            
        else :
            upateQery = db.session.query(TB_ODERS)
            upateQery = upateQery.filter(TB_ODERS.order_index == order_index)
            upateRecord = upateQery.one()
            upateRecord.nut_free = str(nut_free)

            return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


## update glueten_free
@main_control.route('/update_order_glueten_free', methods=['POST'])
def update_order_glueten_free():
    print("update_order_glueten_free()")
    print("request.url  : " + str(request.url))

    order_index = request.form.get('order_index')
    print("order_index  : " + str(order_index))

    glueten_free = request.form.get('glueten_free')
    print("glueten_free  : " + str(glueten_free))

    try:
        # check exigst data
        check_data= db.session.query(func.count(TB_ODERS.order_index)).\
                        filter(and_(TB_ODERS.order_index==order_index )).scalar()      
        db.session.close()
        if( not(check_data > 0) ) : 
            return jsonify({"result":"fail" , "error":"data is not exist"})
            
        else :
            upateQery = db.session.query(TB_ODERS)
            upateQery = upateQery.filter(TB_ODERS.order_index == order_index)
            upateRecord = upateQery.one()
            upateRecord.glueten_free = str(glueten_free)
            db.session.commit()
            db.session.close()

            return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


## update glueten_free
@main_control.route('/update_popular', methods=['POST'])
def update_popular():
    print("update_popular()")
    print("request.url  : " + str(request.url))

    offer_index = request.form.get('offer_index')
    print("offer_index  : " + str(offer_index))

    user_id = request.form.get('user_id')
    print("user_id  : " + str(user_id))

    try:
        # check exigst data
        offers= db.session.query(TB_OFFERS).filter(and_(TB_OFFERS.offer_index==offer_index )).first()   
        print("offers  : " + str(offers))
        db.session.close()
        if offers :   
            menu= db.session.query(TB_FOOD).\
                        filter(and_(TB_FOOD.menu_id==offers.menu_id ,TB_FOOD.papular_click_history.contains(user_id) )).all()      
            db.session.close()

            print("menu  : " + str(menu))

            if menu :
                return jsonify({"result":"fail" , "error":"You clicked already !"})
            else :
                #papular_cnt = menu.papular_cnt

                menu2= db.session.query(TB_FOOD).\
                                        filter(and_(TB_FOOD.menu_id==offers.menu_id)).first()      
                db.session.close()

                print("menu2  : " + str(menu2))

                if menu2 :
                    history = str(menu2.papular_click_history) + "," + str(user_id);\
                    print("history  : " + str(history))
                    upateQery = db.session.query(TB_FOOD)
                    upateQery = upateQery.filter(TB_FOOD.menu_id == menu2.menu_id)
                    upateRecord = upateQery.one()
                    upateRecord.papular_cnt = str(int(str(menu2.papular_cnt)) + 1)
                    upateRecord.papular_click_history = history
                    db.session.commit()
                    db.session.close()
                    return jsonify({"result":"ok"})
                else :
                    return jsonify({"result":"fail", "error":"system error"})
            
        else :
            return jsonify({"result":"fail" , "error":"data is not exist"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


@main_control.route('/loginAdmin', methods=['POST'])
def loginAdmin():
    print("loginAdmin()")
    print("request.url  : " + str(request.url))

    admin_company_code = request.form.get('admin_company_code')
    print("admin_company_code  : " + str(admin_company_code))

    admin_password = request.form.get('admin_password')
    print("admin_password  : " + str(admin_password))

    try:
        # check exigst data
        check_data= db.session.query(func.count(TB_COMPANY.company_code)).\
                        filter(and_(TB_COMPANY.company_code==admin_company_code  ,TB_COMPANY.company_password==admin_password  )).scalar()      
        print("check_data : " + str(check_data))
        db.session.close()
        if( not(check_data > 0) ) : 
            return jsonify({"result":"fail" , "error":"data is not exist"})
            
        else :
            return jsonify({"result":"ok" , "company_code":admin_company_code})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"fail", "error":"system error"})


####################################################################################
## Fuction
####################################################################################

def get_company_code(user_id):
    print("get_company_code  user_id : " + user_id)
    company_code = ""
    try:
        if user_id :
            temp_company_code = db.session.query(TB_DROP_USER).filter(TB_DROP_USER.user_id==user_id).first()
            db.session.close()
            print(temp_company_code)
            if not ( temp_company_code is None ) :
                company_code = str(temp_company_code.company_code)

                print("get_company_code  return company_code : " + company_code)
                return company_code
      
        return company_code
        
    except Exception as ex:     
        print(ex)
        return company_code

def get_user_list_off_offer(offer_index) :
    print("get_user_list_off_offer offer_index : " + offer_index)
    user_list = []
    try:
        temp_user_list= db.session.query(TB_OFFERS,TB_ODERS)\
                                            .outerjoin( TB_ODERS , TB_ODERS.offer_index==TB_OFFERS.offer_index) \
                                            .outerjoin( TB_FOOD , TB_FOOD.menu_id==TB_OFFERS.menu_id) \
                                            .order_by(TB_OFFERS.offer_index.asc())\
                                            .filter(  and_(TB_OFFERS.offer_index == offer_index))\
                                            .all()
        db.session.close()

        print(str(temp_user_list))

        if( len(temp_user_list) > 0 ):
            user_list = []
            for order in temp_user_list :

                temp_user= {} 

                temp_user.update({"offer_index" : int(order.TB_ODERS.offer_index)})
                temp_user.update({"user_id" : order.TB_ODERS.user_id})
                temp_user.update({"nut_free" : order.TB_ODERS.nut_free})
                temp_user.update({"glueten_free" : int(order.TB_ODERS.glueten_free)})

                if order.TB_ODERS :
                    temp_user_detail= db.session.query(TB_DROP_USER)\
                                            .filter(  and_(TB_DROP_USER.user_id == order.TB_ODERS.user_id))\
                                            .first()

                    if temp_user_detail :
                        temp_user.update({"name" : temp_user_detail.name})
                        temp_user.update({"email" : temp_user_detail.email})

                    


                user_list.append(temp_user)
               

        print("user_list : " + str(user_list))
        return user_list

    except Exception as ex:
        print(ex)
        return user_list

def get_menu_list(user_id,date_to_order,offer_type,menu_kind) :
    print("get_menu_list !!!")
    print("get_menu_list user_id : " + user_id)
    print("get_menu_list date_to_order : " + date_to_order)
    menu_list = []
    restorant_name_list = []
    try:
        company_code = get_company_code(user_id)
        if menu_kind ==str("All") :
            temp_menu_list= db.session.query(TB_OFFERS,TB_ODERS,TB_FOOD)\
                                                .outerjoin( TB_ODERS , TB_ODERS.offer_index==TB_OFFERS.offer_index) \
                                                .outerjoin( TB_FOOD , TB_FOOD.menu_id==TB_OFFERS.menu_id) \
                                                .filter(  and_(TB_OFFERS.company_code == company_code , TB_OFFERS.offer_date == date_to_order , TB_OFFERS.offer_type==offer_type))\
                                                .order_by(TB_OFFERS.offer_index.asc())\
                                                .order_by(TB_FOOD.restorant_name.asc())\
                                                .all()
            db.session.close()
        else :
            temp_menu_list= db.session.query(TB_OFFERS,TB_ODERS,TB_FOOD)\
                                                .outerjoin( TB_ODERS , TB_ODERS.offer_index==TB_OFFERS.offer_index) \
                                                .outerjoin( TB_FOOD , TB_FOOD.menu_id==TB_OFFERS.menu_id) \
                                                .filter(  and_(TB_OFFERS.company_code == company_code , TB_OFFERS.offer_date == date_to_order , TB_OFFERS.offer_type==offer_type , TB_FOOD.menu_kind==menu_kind))\
                                                .order_by(TB_OFFERS.offer_index.asc())\
                                                .order_by(TB_FOOD.restorant_name.asc())\
                                                .all()
            db.session.close()

        print(str(temp_menu_list))
        pre_offer_index = ""
        pre_restrant_name = ""

        if( len(temp_menu_list) > 0 ):
            menu_list = []
            for menu in temp_menu_list :

                temp_menu= {} 

                ## 아우터 조인으로 인한 중복 열 제거
                if pre_offer_index == menu.TB_OFFERS.offer_index :
                    continue
                pre_offer_index = menu.TB_OFFERS.offer_index

                temp_menu.update({"user_id" : user_id})
                temp_menu.update({"company_code" : company_code})

                temp_menu.update({"offer_index" : int(menu.TB_OFFERS.offer_index)})
                temp_menu.update({"offer_date" : menu.TB_OFFERS.offer_date})
                temp_menu.update({"company_code" : menu.TB_OFFERS.company_code})
                temp_menu.update({"offer_type" : menu.TB_OFFERS.offer_type})
                temp_menu.update({"offer_cnt" : menu.TB_OFFERS.offer_cnt})

                if menu.TB_ODERS and user_id == str(menu.TB_ODERS.user_id) :
                    temp_menu.update({"order_index" : int(menu.TB_ODERS.order_index)})
                    temp_menu.update({"order_date" : menu.TB_ODERS.order_date})
                    temp_menu.update({"user_id" : menu.TB_ODERS.user_id})
                    temp_menu.update({"order_option" : menu.TB_ODERS.order_option})
                    temp_menu.update({"nut_free" : menu.TB_ODERS.nut_free})
                    temp_menu.update({"glueten_free" : menu.TB_ODERS.glueten_free})

                if menu.TB_FOOD :
                    temp_menu.update({"menu_id" : menu.TB_FOOD.menu_id})
                    temp_menu.update({"menu_name" : menu.TB_FOOD.menu_name})
                    temp_menu.update({"menu_image_file" : menu.TB_FOOD.menu_image_file_path})
                    temp_menu.update({"content" : menu.TB_FOOD.content})
                    temp_menu.update({"restorant_name" : menu.TB_FOOD.restorant_name})
                    temp_menu.update({"restorant_id" : menu.TB_FOOD.restorant_id})
                    temp_menu.update({"papular_cnt" : menu.TB_FOOD.papular_cnt})
                    temp_menu.update({"remark" : menu.TB_FOOD.remark})

                    if pre_restrant_name != menu.TB_FOOD.restorant_name :
                        restorant_name_list.append(menu.TB_FOOD.restorant_name)
                    pre_restrant_name = menu.TB_FOOD.restorant_name


                menu_list.append(temp_menu)
               

        #rprint("menu_list : " + str(menu_list))
        return menu_list , restorant_name_list
        #return json.dumps(menu_list, ensure_ascii=False)

    except Exception as ex:
        print(ex)
        return menu_list, restorant_name_list


def get_menu_list_all(user_id,date_to_order,offer_type,menu_kind) :
    print("get_menu_list_all !!!")
    print("get_menu_list_all user_id : " + str(user_id))
    print("get_menu_list_all date_to_order : " + str(date_to_order))
    menu_list = []
    restorant_name_list = []
    try:
        company_code = ""
        if menu_kind ==str("All") :
            temp_menu_list= db.session.query(TB_OFFERS,TB_ODERS,TB_FOOD)\
                                                .outerjoin( TB_ODERS , TB_ODERS.offer_index==TB_OFFERS.offer_index) \
                                                .outerjoin( TB_FOOD , TB_FOOD.menu_id==TB_OFFERS.menu_id) \
                                                .order_by(TB_OFFERS.offer_index.asc())\
                                                .filter(  and_( TB_OFFERS.offer_date == date_to_order , TB_OFFERS.offer_type==offer_type))\
                                                .order_by(TB_OFFERS.offer_index.asc())\
                                                .order_by(TB_FOOD.restorant_name.asc())\
                                                .all()
            db.session.close()
        else :
            temp_menu_list= db.session.query(TB_OFFERS,TB_ODERS,TB_FOOD)\
                                                .outerjoin( TB_ODERS , TB_ODERS.offer_index==TB_OFFERS.offer_index) \
                                                .outerjoin( TB_FOOD , TB_FOOD.menu_id==TB_OFFERS.menu_id) \
                                                .order_by(TB_OFFERS.offer_index.asc())\
                                                .filter(  and_(TB_OFFERS.offer_date == date_to_order , TB_OFFERS.offer_type==offer_type , TB_FOOD.menu_kind==menu_kind))\
                                                .order_by(TB_OFFERS.offer_index.asc())\
                                                .order_by(TB_FOOD.restorant_name.asc())\
                                                .all()
            db.session.close()

        print(str(temp_menu_list))
        pre_offer_index = ""
        pre_restrant_name = ""
        if( len(temp_menu_list) > 0 ):
            menu_list = []
            for menu in temp_menu_list :

                temp_menu= {} 
                ## 아우터 조인으로 인한 중복 열 제거
                if pre_offer_index == menu.TB_OFFERS.offer_index :
                    continue
                pre_offer_index = menu.TB_OFFERS.offer_index

                temp_menu.update({"user_id" : user_id})
                temp_menu.update({"company_code" : company_code})

                temp_menu.update({"offer_index" : int(menu.TB_OFFERS.offer_index)})
                temp_menu.update({"offer_date" : menu.TB_OFFERS.offer_date})
                temp_menu.update({"company_code" : menu.TB_OFFERS.company_code})
                temp_menu.update({"offer_type" : menu.TB_OFFERS.offer_type})
                temp_menu.update({"offer_cnt" : menu.TB_OFFERS.offer_cnt})
                '''
                if menu.TB_ODERS :
                    temp_menu.update({"order_index" : int(menu.TB_ODERS.order_index)})
                    temp_menu.update({"order_date" : menu.TB_ODERS.order_date})
                    temp_menu.update({"user_id" : menu.TB_ODERS.user_id})
                    temp_menu.update({"order_option" : menu.TB_ODERS.order_option})
                    temp_menu.update({"nut_free" : menu.TB_ODERS.nut_free})
                    temp_menu.update({"glueten_free" : menu.TB_ODERS.glueten_free})
                '''

                if menu.TB_FOOD :
                    temp_menu.update({"menu_id" : menu.TB_FOOD.menu_id})
                    temp_menu.update({"menu_name" : menu.TB_FOOD.menu_name})
                    temp_menu.update({"menu_image_file" : menu.TB_FOOD.menu_image_file_path})
                    temp_menu.update({"content" : menu.TB_FOOD.content})
                    temp_menu.update({"restorant_name" : menu.TB_FOOD.restorant_name})
                    temp_menu.update({"restorant_id" : menu.TB_FOOD.restorant_id})
                    temp_menu.update({"papular_cnt" : menu.TB_FOOD.papular_cnt})
                    temp_menu.update({"remark" : menu.TB_FOOD.remark})

                    if pre_restrant_name != menu.TB_FOOD.restorant_name :
                        restorant_name_list.append(menu.TB_FOOD.restorant_name)

                    pre_restrant_name = menu.TB_FOOD.restorant_name


                menu_list.append(temp_menu)
               

        #rprint("menu_list : " + str(menu_list))
        return menu_list , restorant_name_list
        #return json.dumps(menu_list, ensure_ascii=False)

    except Exception as ex:
        print(ex)
        return menu_list, restorant_name_list




def get_order_menu_list(company_code,date_to_order,offer_type,menu_kind) :
    print("get_order_menu_list !!!")
    print("get_order_menu_list company_code : " + company_code)
    print("get_order_menu_list date_to_order : " + date_to_order)
    print("get_order_menu_list offer_type : " + offer_type)
    print("get_order_menu_list menu_kind : " + menu_kind)
    menu_list = []
    try:
      
    
        temp_menu_list= db.session.query(TB_OFFERS ,TB_ODERS, TB_DROP_USER)\
                                            .outerjoin( TB_OFFERS , TB_OFFERS.offer_index==TB_ODERS.offer_index) \
                                            .outerjoin( TB_DROP_USER , TB_DROP_USER.user_id==TB_ODERS.user_id) \
                                            .order_by(TB_ODERS.order_index.asc())\
                                            .filter(  and_(TB_DROP_USER.company_code == company_code , TB_ODERS.order_date == date_to_order, TB_OFFERS.offer_type==offer_type ))\
                                            .all()
        db.session.close()
       
        print(str(temp_menu_list))

        if( len(temp_menu_list) > 0 ):
            menu_list = []
            for menu in temp_menu_list :

                temp_menu= {} 
                temp_menu.update({"company_code" : company_code})

                if menu.TB_OFFERS :
                    temp_menu.update({"offer_type" : menu.TB_OFFERS.offer_type})
                    temp_menu.update({"offer_index" : int(menu.TB_ODERS.offer_index)})

                    print("offer_index : " + str(menu.TB_ODERS.offer_index))
                    temp_offer_list = db.session.query(TB_OFFERS, TB_ODERS)\
                                                .join( TB_OFFERS , TB_OFFERS.offer_index == TB_ODERS.offer_index)\
                                                    .filter( and_(TB_OFFERS.offer_index == menu.TB_ODERS.offer_index, TB_OFFERS.offer_date == date_to_order \
                                                                                ,TB_OFFERS.offer_type==offer_type \
                                                                                    , TB_OFFERS.company_code == company_code
                                                                    ))\
                                                                    .all()

                    db.session.close()

                    temp_menu.update({"total_order_cnt" : int(len(temp_offer_list))})
                

                    temp_food= db.session.query(TB_FOOD)\
                                                .filter(  and_(TB_FOOD.menu_id == menu.TB_OFFERS.menu_id))\
                                                .first()
                    db.session.close()

                    if menu_kind != str("All") :
                        if menu_kind !=  temp_food.menu_kind :
                            continue

                    temp_menu.update({"menu_id" : temp_food.menu_id})
                    temp_menu.update({"menu_name" : temp_food.menu_name})
                    temp_menu.update({"menu_image_file" : temp_food.menu_image_file_path})
                    temp_menu.update({"content" : temp_food.content})
                    temp_menu.update({"restorant_name" : temp_food.restorant_name})
                    temp_menu.update({"restorant_id" : temp_food.restorant_id})
                    temp_menu.update({"papular_cnt" : temp_food.papular_cnt})
                    temp_menu.update({"remark" :temp_food.remark})


                if menu.TB_ODERS :
                    temp_menu.update({"order_date" : menu.TB_ODERS.order_date})
                    temp_menu.update({"user_id" : menu.TB_ODERS.user_id})
               

                menu_list.append(temp_menu)
               

        #rprint("menu_list : " + str(menu_list))
        return menu_list
        #return json.dumps(menu_list, ensure_ascii=False)

    except Exception as ex:
        print(ex)
        return menu_list
     
def get_order_menu_list_to_download(company_code,date_to_order,offer_type,menu_kind) :
    print("get_order_menu_list_to_download !!!")
    print("get_order_menu_list_to_download company_code : " + company_code)
    print("get_order_menu_list_to_download date_to_order : " + date_to_order)
    print("get_order_menu_list_to_download offer_type : " + offer_type)
    print("get_order_menu_list_to_download menu_kind : " + menu_kind)
    menu_list = []
    try:
      
        temp_menu_list= db.session.query(TB_ODERS ,TB_OFFERS, TB_DROP_USER)\
                                            .outerjoin( TB_OFFERS , TB_OFFERS.offer_index==TB_ODERS.offer_index) \
                                            .outerjoin( TB_DROP_USER , TB_DROP_USER.user_id==TB_ODERS.user_id) \
                                            .order_by(TB_ODERS.order_index.asc())\
                                            .filter(  and_(TB_DROP_USER.company_code == company_code , TB_ODERS.order_date == date_to_order, TB_OFFERS.offer_type==offer_type ))\
                                            .all()
        db.session.close()
        print("here !")
        print(str(temp_menu_list))

        if( len(temp_menu_list) > 0 ):
            menu_list = []
            for menu in temp_menu_list :

                temp_menu= {} 
                temp_menu.update({"user_id" : menu.TB_ODERS.user_id})
                temp_menu.update({"offer_index" : int(menu.TB_ODERS.offer_index)})
                temp_menu.update({"order_date" : menu.TB_ODERS.order_date})
                temp_menu.update({"nut_free" : menu.TB_ODERS.nut_free})
                temp_menu.update({"glueten_free" : menu.TB_ODERS.glueten_free})

                temp_menu.update({"offer_type" : menu.TB_OFFERS.offer_type})

                if menu.TB_DROP_USER :
                    temp_menu.update({"company_code" : menu.TB_DROP_USER.company_code})
                    temp_menu.update({"name" : menu.TB_DROP_USER.name})

                    temp_company= db.session.query(TB_COMPANY)\
                                                .filter(  and_(TB_COMPANY.company_code == menu.TB_DROP_USER.company_code))\
                                                .first()
                    db.session.close()
                    if temp_company :
                        temp_menu.update({"company_name" : temp_company.company_name})


                    

                if menu.TB_OFFERS :

                    temp_food= db.session.query(TB_FOOD)\
                                                .filter(  and_(TB_FOOD.menu_id == menu.TB_OFFERS.menu_id))\
                                                .first()
                    db.session.close()

                    if menu_kind != str("All") :
                        if menu_kind !=  temp_food.menu_kind :
                            continue

                    temp_menu.update({"menu_id" : temp_food.menu_id})
                    temp_menu.update({"menu_name" : temp_food.menu_name})
                    temp_menu.update({"menu_image_file" : temp_food.menu_image_file_path})
                    temp_menu.update({"content" : temp_food.content})
                    temp_menu.update({"restorant_name" : temp_food.restorant_name})
                    temp_menu.update({"restorant_id" : temp_food.restorant_id})
                    temp_menu.update({"papular_cnt" : temp_food.papular_cnt})
                    temp_menu.update({"remark" :temp_food.remark})


                menu_list.append(temp_menu)
               

        print("menu_list : " + str(menu_list))
        return menu_list
        #return json.dumps(menu_list, ensure_ascii=False)

    except Exception as ex:
        print(ex)
        return menu_list
     
def get_offer_list(company_code,company_name,date_to_order,offer_type,menu_kind):
    print("get_offer_list !!!")
    print("get_offer_list company_code : " + company_code)
    print("get_offer_list company_name : " + company_name)
    print("get_offer_list date_to_order : " + date_to_order)
    print("get_offer_list offer_type : " + offer_type)
    print("get_offer_list menu_kind : " + menu_kind)
    offer_list = []
    try:
      
        temp_menu_list= db.session.query(TB_FOOD).all()
        db.session.close()
        print("here !")
        print(str(temp_menu_list))

        if( len(temp_menu_list) > 0 ):
            offer_list = []
            for menu in temp_menu_list :

                temp_menu= {} 
                menu_id_ = ""
                menu_name_ = ""

                company_code_ = company_code
                company_name_ = company_name
                offer_index_ = ""
                offer_date_ = date_to_order
                offer_type_ = offer_type
                offer_yn_ = "false"

                menu_id_ = menu.menu_id
                menu_name_ = menu.menu_name

                
                temp_offers= db.session.query(TB_OFFERS)\
                                            .filter(  and_(TB_OFFERS.company_code == company_code ,TB_OFFERS.menu_id == menu.menu_id ,TB_OFFERS.offer_date == date_to_order ,TB_OFFERS.offer_type == offer_type ))\
                                            .first()
                db.session.close()

                if temp_offers  :
                    offer_yn_ = "true"
                    offer_index_ = temp_offers.offer_index
                    offer_date_ = temp_offers.offer_date
                    offer_type_ = temp_offers.offer_type
                else :
                    offer_yn_ = "false"


                temp_menu.update({"menu_id" :menu_id_})
                temp_menu.update({"menu_name" : menu_name_})
                
                temp_menu.update({"offer_yn" : offer_yn_})
                temp_menu.update({"offer_index" : offer_index_})
                temp_menu.update({"offer_date" : offer_date_})
                temp_menu.update({"offer_type" : offer_type_})

                temp_menu.update({"company_code" : company_code_})
                temp_menu.update({"company_name" : company_name_})



                offer_list.append(temp_menu)
               

        print("offer_list : " + str(offer_list))
        return offer_list
        #return json.dumps(menu_list, ensure_ascii=False)

    except Exception as ex:
        print(ex)
        return offer_list

