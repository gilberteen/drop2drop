from modelpool.sensors import sensor
from typing import Sequence
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.types import TypeDecorator, CHAR
from datetime import datetime , timedelta
import uuid

db = SQLAlchemy()



#############################################################
## drop2drop call center admin
#############################################################
class TB_DROP_USER(db.Model):
    __tablename__ = 'TB_DROP_USER'
    user_id	=db.Column(db.String(128), primary_key=True)
    create_date	=db.Column(db.DateTime, server_default=db.func.now())
    name	=db.Column(db.String(128))
    email	=db.Column(db.String(128))
    phone_number	=db.Column(db.String(128))
    password	=db.Column(db.String(128))
    company_code	=db.Column(db.String(128))
    role	=db.Column(db.String(128))

class TB_DROP_ADMIN(db.Model):
    __tablename__ = 'TB_DROP_ADMIN'
    company_code	=db.Column(db.String(128), primary_key=True)
    create_date	=db.Column(db.DateTime, server_default=db.func.now())
    company_password	=db.Column(db.String(128))
    charger_name	=db.Column(db.String(128))
    charger_email	=db.Column(db.String(128))
    charger_phone_number	=db.Column(db.String(128))

class TB_COMPANY(db.Model):
    __tablename__ = 'TB_COMPANY'
    company_code	=db.Column(db.String(128), primary_key=True)
    create_date	=db.Column(db.DateTime, server_default=db.func.now())
    company_name	=db.Column(db.String(128))
    company_password	=db.Column(db.String(128))
    person_in_charge_name	=db.Column(db.String(128))
    person_in_charge_phone_number	=db.Column(db.String(128))
    person_in_charge_email =db.Column(db.String(128))

class TB_FOOD(db.Model):
    __tablename__ = 'TB_FOOD'
    menu_index	 =db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_date	=db.Column(db.DateTime, server_default=db.func.now())
    menu_id	=db.Column(db.String(128))
    menu_name	=db.Column(db.String(128))
    menu_image_file_path	=db.Column(db.String(128))
    content	=db.Column(db.String(128))
    restorant_name	=db.Column(db.String(128))
    restorant_id	=db.Column(db.String(128))
    papular_cnt	=db.Column(db.String(128) , default='0')
    papular_click_history	=db.Column(db.Text)
    remark	=db.Column(db.String(128))
    menu_kind	=db.Column(db.String(128))


class TB_ODERS(db.Model):
    __tablename__ = 'TB_ODERS'
    order_index	 =db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_date	=db.Column(db.DateTime, server_default=db.func.now())
    order_date	=db.Column(db.String(128))

    user_id	=db.Column(db.String(128))
    
    order_option	=db.Column(db.String(128))

    nut_free = db.Column(db.String(64) , default='0')
    glueten_free = db.Column(db.String(64) , default='0')
    offer_index	= db.Column(db.Integer)

class TB_OFFERS(db.Model):
    __tablename__ = 'TB_OFFERS'
    offer_index	 =db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_date	=db.Column(db.DateTime, server_default=db.func.now())
    offer_date	=db.Column(db.String(128))
    company_code	=db.Column(db.String(128))
    offer_type	=db.Column(db.String(128))
    menu_id	=db.Column(db.String(128))
    offer_cnt	=db.Column(db.Integer)




#############################################################
## health call center admin
#############################################################

## 대상자정보
class TB_SILVER(db.Model):
    __tablename__ = 'TB_SILVER'

    silver_id	=db.Column(db.String(128), primary_key=True)
    silver_password	=db.Column(db.String(128))
    silver_created_date	=db.Column(db.DateTime, server_default=db.func.now())
    silver_name	=db.Column(db.String(128))
    silver_bod	=db.Column(db.String(64))
    silver_gender	=db.Column(db.String(64))
    silver_recent_connection_time	=db.Column(db.String(128))
    silver_email	=db.Column(db.String(128))
    silver_address_post_num	=db.Column(db.String(128))
    silver_address_top	=db.Column(db.String(128))
    silver_address_detail	=db.Column(db.String(128))
    silver_address_gps	=db.Column(db.String(128))
    
    silver_tel	=db.Column(db.String(64))
    silver_today_care_id	=db.Column(db.String(128))
    silver_public_care_admin_name	=db.Column(db.String(128))
    silver_public_care_area	=db.Column(db.String(128))
    silver_public_care_name	=db.Column(db.String(128))
    silver_ai_speaker_id	=db.Column(db.String(128))
    silver_cpe_id	=db.Column(db.String(128))
    silver_status	=db.Column(db.String(64))
    silver_deleted_reason	=db.Column(db.String(64))
    silver_deleted_reason_detail	= db.Column(db.Text)
    silver_deleted_date	=db.Column(db.String(64))
    silver_admin_id_delete_action	=db.Column(db.String(64))
    
    silver_service_end_time=db.Column(db.String(64))


##  CPE
class TB_CPE(db.Model):
    __tablename__ = 'TB_CPE'
    cpe_id	= db.Column(db.String(128), primary_key=True)
    cpe_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    cpe_tel	= db.Column(db.String(128))
    cpe_reg_date	= db.Column(db.String(128))
    cpe_status	= db.Column(db.String(128),default='실패')
    cpe_reset_time	= db.Column(db.String(64))
    cpe_install_status	= db.Column(db.String(64),default='미설치')
    cpe_installer_id	= db.Column(db.String(128),default='')

##  AI스피커
class TB_AI_SPEAKER(db.Model):
    __tablename__ = 'TB_AI_SPEAKER'
    ai_speaker_id	= db.Column(db.String(128), primary_key=True)
    ai_speaker_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    ai_speaker_password	= db.Column(db.String(128))
    ai_speaker_sn	= db.Column(db.String(128))
    ai_speaker_model_name	= db.Column(db.String(64))
    ai_speaker_make_date	= db.Column(db.String(64))
    ai_speaker_macaddress	= db.Column(db.String(128))
    ai_speaker_status	= db.Column(db.String(128),default='입고')
    ai_speaker_network_status = db.Column(db.String(64),default='연결안됨')
    ai_speaker_certificate_yn	= db.Column(db.String(128))
    ai_speaker_history	= db.Column(db.Text)
    ai_speaker_remark	= db.Column(db.Text)
    ai_speaker_user_aggrement_yn	= db.Column(db.String(128))
    ai_speaker_user_aggrement_date	= db.Column(db.String(128))
    
    ai_speaker_regist_writer_id	= db.Column(db.String(128))
    ai_speaker_regist_writer_name	= db.Column(db.String(128))
    
    
    ai_speaker_installer_id	= db.Column(db.String(128),default='')
    ai_speaker_install_state = db.Column(db.String(128),default='미설치')
    ai_speaker_cam_use_yn	= db.Column(db.String(64),default='사용')
    
    
    ai_speaker_alive_check_time	= db.Column(db.String(128))
    ai_speaker_service_end_time=db.Column(db.String(64))


##  응급요청이력
class TB_EMERGENCY_HISTORY(db.Model):
    __tablename__ = 'TB_EMERGENCY_HISTORY'
    emergency_call_id	= db.Column(db.String(128), primary_key=True )
    emergency_call_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    emergency_call_silver_id = db.Column(db.String(128))
    emergency_call_source	= db.Column(db.String(128))
    emergency_call_source_detail	= db.Column(db.String(128))

    emergency_call_sms_receive_date	= db.Column(db.String(128))
    emergency_call_proc_status	= db.Column(db.String(128),default='접수대기')
    emergency_call_call_status	= db.Column(db.String(128))
    emergency_call_result	= db.Column(db.String(128))
    emergency_call_memo	= db.Column(db.Text)
    emergency_call_confirmed_id = db.Column(db.String(128))
    emergency_call_confirmed_date = db.Column(db.String(128))

##  문의이력
class TB_QUESTION_HISTORY(db.Model):
    __tablename__ = 'TB_QUESTION_HISTORY'
    ack_request_id	= db.Column(db.String(128), primary_key=True)
    ack_request_created_date	= db.Column(db.DateTime, server_default=db.func.now())

    ack_request_silver_id	= db.Column(db.String(128))
    ack_request_title	= db.Column(db.String(128))
    ack_request_reg_date	= db.Column(db.String(128))
    ack_request_user_id_save	= db.Column(db.String(128))
    ack_request_status	= db.Column(db.String(128))


##  공지사항
class TB_NOTIFY(db.Model):
    __tablename__ = 'TB_NOTIFY'
    notify_id	= db.Column(db.Integer, primary_key=True,autoincrement=True)
    notify_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    
    notify_title	= db.Column(db.String(128))
    notify_content	= db.Column(db.Text)
    notify_writer_id	= db.Column(db.String(128))
    
    notify_top_open_yn	= db.Column(db.String(64))
    notify_type	= db.Column(db.String(64))
    notify_writer_name	= db.Column(db.String(128))

    notify_file_name	= db.Column(db.String(256))
    notify_regist_date_time	= db.Column(db.String(128))
    notify_update_date_time	= db.Column(db.String(128))
    notify_update_id	= db.Column(db.String(128))
    notify_update_name	= db.Column(db.String(128))
    notify_open_yn	= db.Column(db.String(64))
    notify_read_count	= db.Column(db.String(64))

class TB_FAQ(db.Model):
    __tablename__ = 'TB_FAQ'
    faq_id	= db.Column(db.Integer, primary_key=True,autoincrement=True)
    faq_created_date	=db.Column(db.DateTime, server_default=db.func.now())
    faq_category_name	=db.Column(db.String(64))
    faq_title	=db.Column(db.String(128))
    faq_content	=db.Column(db.Text)
        
    faq_regist_date_time	=db.Column(db.String(128))
    faq_regist_id	=db.Column(db.String(128))
    faq_regist_name	=db.Column(db.String(128))
    faq_update_date_time	=db.Column(db.String(128))
    faq_update_id	=db.Column(db.String(128))
    faq_update_name	=db.Column(db.String(128))
    faq_open_yn	=db.Column(db.String(64))
    faq_read_count	=db.Column(db.String(64))
 
class TB_SILVER_QUESTION(db.Model):
    __tablename__ = 'TB_SILVER_QUESTION'
    silver_question_id	 =db.Column(db.Integer, primary_key=True,autoincrement=True)
    silver_question_created_date	=db.Column(db.DateTime, server_default=db.func.now())
    silver_question_category	=db.Column(db.String(64))
    silver_question_silver_id	=db.Column(db.String(64))
    silver_question_silver_name	=db.Column(db.String(64))
    silver_question_tile	=db.Column(db.String(64))
    silver_question_contents	=db.Column(db.Text)
    silver_question_attach_file_name	=db.Column(db.String(256))
    silver_question_silver_phone_number =	db.Column(db.String(64))
    silver_question_regist_date	=db.Column(db.String(64))
    silver_question_regist_id	=db.Column(db.String(64))
    silver_question_regist_name	=db.Column(db.String(64))
    silver_question_update_date	=db.Column(db.String(64))
    silver_question_update_id	=db.Column(db.String(64))
    silver_question_update_name	=db.Column(db.String(64))
    silver_question_read_question	=db.Column(db.String(64))
    silver_question_update_content	=db.Column(db.Text)
    
    silver_question_answer_status	=db.Column(db.String(64))
    
    
class TB_PUBLIC_CARE_QUESTION(db.Model):
    __tablename__ = 'TB_PUBLIC_CARE_QUESTION'
    public_care_question_id	 =db.Column(db.Integer, primary_key=True,autoincrement=True)
    public_care_question_created_date	=db.Column(db.DateTime, server_default=db.func.now())
    public_care_question_type	=db.Column(db.String(64))
    public_care_question_category	=db.Column(db.String(64))
    public_care_question_silver_id	=db.Column(db.String(64))
    public_care_question_silver_name	=db.Column(db.String(64))
    public_care_question_tile	=db.Column(db.String(64))
    public_care_question_contents	=db.Column(db.Text)
    public_care_question_attach_file_name	=db.Column(db.String(64))
    public_care_question_email	=db.Column(db.String(64))
    public_care_question_answer_contents	=db.Column(db.Text)
    public_care_question_regist_date	=db.Column(db.String(64))
    public_care_question_regist_id	=db.Column(db.String(64))
    public_care_question_regist_name	=db.Column(db.String(64))
    public_care_question_answer_regist_date	=db.Column(db.String(64))
    public_care_question_answer_regist_id	=db.Column(db.String(64))
    public_care_question_answer_regist_name	=db.Column(db.String(64))
    public_care_question_answer_update_date =	db.Column(db.String(64))
    public_care_question_answer_update_id	=db.Column(db.String(64))
    public_care_question_answer_update_name	=db.Column(db.String(64))
    public_care_question_answer_status=	db.Column(db.String(64))
    public_care_question_read_count	=db.Column(db.String(64))
    public_care_question_user_agreement	=db.Column(db.String(64))

## 보건소관리자
class TB_PUBLICK_CARE_ADMIN_USER(db.Model):
    __tablename__ = 'TB_PUBLICK_CARE_ADMIN_USER'
    public_care_admin_id	 = db.Column(db.String(128), primary_key=True)
    public_care_created_date	 = db.Column(db.DateTime, server_default=db.func.now())
    public_care_password	 = db.Column(db.String(128))
    public_care_username	 = db.Column(db.String(128))
    public_care_bod	 = db.Column(db.String(128))
    public_care_role	 = db.Column(db.String(128))
    public_care_gender	 = db.Column(db.String(128))
    public_care_recent_connection_time	 = db.Column(db.String(128))
    public_care_email	 = db.Column(db.String(128))
    public_care_address	 = db.Column(db.String(128))
    public_care_tel	 = db.Column(db.String(128))
    public_care_del_reason	 = db.Column(db.String(128))
    public_care_del_reason_detail	 = db.Column(db.String(128))
    public_care_del_date	 = db.Column(db.String(128))
    public_care_date_user_agreement	 = db.Column(db.String(128))
    public_care_del_user	 = db.Column(db.String(128))
    public_care_company_name	 = db.Column(db.String(128))
    public_care_organization	 = db.Column(db.String(128))
    public_care_job	 = db.Column(db.String(128))
    public_care_date_user_agreement	 = db.Column(db.String(128))
    public_care_memo	 = db.Column(db.Text)
    public_care_status	 = db.Column(db.String(128))

## 운영관리자정보

class TB_ADMIN_USER(db.Model):
    __tablename__ = 'TB_ADMIN_USER'
    admin_user_id	= db.Column(db.String(128), primary_key=True)
    admin_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    admin_password	= db.Column(db.String(128))
    admin_username	= db.Column(db.String(128))
    admin_bod	= db.Column(db.String(128))
    admin_role	= db.Column(db.String(128))
    admin_gender	= db.Column(db.String(128))
    admin_recent_connection_time	= db.Column(db.String(128))
    admin_email	= db.Column(db.String(128))
    admin_address	= db.Column(db.String(128))
    admin_tel	= db.Column(db.String(128))
    admin_del_reason	= db.Column(db.String(128))
    admin_del_reason_detail	= db.Column(db.String(128))
    admin_del_date	= db.Column(db.String(128))
    admin_date_user_agreement	= db.Column(db.String(128))
    admin_del_user	= db.Column(db.String(128))
    admin_company_name	= db.Column(db.String(128))
    admin_organization	= db.Column(db.String(128))
    admin_job	= db.Column(db.String(128))
    admin_date_user_agreement	= db.Column(db.String(128))
    admin_memo	= db.Column(db.Text)
    admin_status	= db.Column(db.String(128))

## 보건소정보
class TB_PUBLIC_CARE(db.Model):
    __tablename__ = 'TB_PUBLIC_CARE'
    public_care_id	= db.Column(db.String(64), primary_key=True)
    public_care_area	= db.Column(db.String(64))
    public_care_name	= db.Column(db.String(64))
    

class TB_APP_MGR(db.Model):
    __tablename__ = 'TB_APP_MGR'
    app_id	 = db.Column(db.Integer, primary_key=True,autoincrement=True)
    app_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    app_model_name	= db.Column(db.String(256))
    app_name	= db.Column(db.String(64))
    app_package_id	= db.Column(db.String(256))
    app_voice_name	= db.Column(db.String(256))
    app_activity_name	= db.Column(db.String(256))
    app_remark	= db.Column(db.String(256))
    app_status	= db.Column(db.String(64))
    app_free_yn	= db.Column(db.String(256))
    app_icon_file_path	= db.Column(db.String(256))
    app_developer	= db.Column(db.String(256))
    app_content	= db.Column(db.String(128))
    app_period	= db.Column(db.String(128))
    app_start_date	= db.Column(db.String(128))
    app_end_date	= db.Column(db.String(128))
    
    app_version	= db.Column(db.String(128))
    app_validation	= db.Column(db.String(128))
    

class TB_FIRMWARE_MGR(db.Model):
    __tablename__ = 'TB_FIRMWARE_MGR'
    firmware_id	 = db.Column(db.Integer, primary_key=True,autoincrement=True)
    firmware_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    firmware_model_name	= db.Column(db.String(256))
    firmware_type	= db.Column(db.String(128))
    firmware_version	= db.Column(db.String(256))
    
    
    firmware_factory_file_path	= db.Column(db.String(256))
    firmware_factory_file_validate	= db.Column(db.String(256))
    firmware_full_file_path	= db.Column(db.String(256))
    firmware_full_file_validate	= db.Column(db.String(256))
    firmware_diff_file_path	= db.Column(db.String(256))
    firmware_diff_file_validate	= db.Column(db.String(256))

    firmware_diff_from_version	= db.Column(db.String(256))
    firmware_diff_to_version	= db.Column(db.String(256))
    firmware_status	= db.Column(db.String(256))
    firmware_history	= db.Column(db.String(256))
    
    firmware_release_date	= db.Column(db.String(128))
    firmware_writer_id	= db.Column(db.String(128))
    firmware_writer_name	= db.Column(db.String(128))
    
    firmware_diff_list= db.Column(db.Text)

class TB_USER_AGREEMENT(db.Model):
    __tablename__ = 'TB_USER_AGREEMENT'
    agreement_id	 = db.Column(db.Integer, primary_key=True,autoincrement=True)
    agreement_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    agreement_channel	= db.Column(db.String(128))
    agreement_type	= db.Column(db.String(128))
    agreement_use_yn	= db.Column(db.String(128))
    agreement_use_date	= db.Column(db.String(256))
    agreement_regist_witer	= db.Column(db.String(256))
    agreement_update_date	= db.Column(db.String(128))
    agreement_update_witer	= db.Column(db.String(256))
    agreement_file_path	= db.Column(db.String(256))
    
class TB_CHATBOT(db.Model):
    __tablename__ = 'TB_CHATBOT'
    chatbot_id	 = db.Column(db.Integer, primary_key=True,autoincrement=True)
    chatbot_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    chatbot_type	= db.Column(db.String(128))
    chatbot_pattern_id	= db.Column(db.String(128))
    chatbot_pattern_depth_1	= db.Column(db.String(256))
    chatbot_pattern_depth_2	= db.Column(db.String(256))
    chatbot_voice_command	= db.Column(db.String(128))
    chatbot_time_enable	= db.Column(db.String(128))
    chatbot_time_start	= db.Column(db.String(128))
    chatbot_time_end	= db.Column(db.String(128))
    chatbot_start_day	= db.Column(db.String(128))
    chatbot_use_yn	= db.Column(db.String(128))
    chatbot_positive_voice	= db.Column(db.String(128))
    chatbot_positive_operation_id	= db.Column(db.String(128))
    chatbot_positive_operation_depth_1	= db.Column(db.String(256))
    chatbot_positive_operation_depth_2	= db.Column(db.String(256))
    chatbot_positive_operation_depth_3	= db.Column(db.String(256))
    chatbot_negative_voice	= db.Column(db.String(128))
    chatbot_negative_operation_id	= db.Column(db.String(128))
    chatbot_negative_operation_depth_1	= db.Column(db.String(256))
    chatbot_negative_operation_depth_2	= db.Column(db.String(256))
    chatbot_negative_operation_depth_3	= db.Column(db.String(256))
    

class TB_CHATBOT_PATTERN(db.Model):
    __tablename__ = 'TB_CHATBOT_PATTERN'
    pattern_id	 = db.Column(db.Integer, primary_key=True,autoincrement=True)
    pattern_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    pattern_depth_1	= db.Column(db.String(128))
    pattern_depth_2	= db.Column(db.String(128))
    pattern_start_time	= db.Column(db.String(128))
    pattern_end_time	= db.Column(db.String(128))
    

class TB_CHATBOT_OPERATION(db.Model):
    __tablename__ = 'TB_CHATBOT_OPERATION'
    operation_id	 = db.Column(db.Integer, primary_key=True,autoincrement=True)
    operation_created_date	= db.Column(db.DateTime, server_default=db.func.now())
    operation_depth_1	= db.Column(db.String(128))
    operation_depth_2	= db.Column(db.String(128))
    operation_depth_3	= db.Column(db.String(128))
    operation_start_time	= db.Column(db.String(128))
    operation_end_time	= db.Column(db.String(128))

#############################################################
## end / health call center admin
#############################################################

#############################################################
## smarthealth only
#############################################################
## 사용자 정보 , 센서로데이터 저장, 분석결과저장
#############################################################
class Fcuser(db.Model):
    __tablename__ = 'fcuser'
    ##ALTER TABLE fcuser MODIFY userid int NOT NULL AUTO_INCREMENT;   
    userid = db.Column(db.String(64), primary_key=True)
    ##created_at = db.Column(db.String(32))
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    username = db.Column(db.String(32))
    password = db.Column(db.String(128))
    height = db.Column(db.String(32))
    weight = db.Column(db.String(32))
    gender = db.Column(db.String(32))
    bod = db.Column(db.String(32))
    address = db.Column(db.String(128))
    email = db.Column(db.String(32))
    phone_number = db.Column(db.String(32))

    role = db.Column(db.String(32))

    score_total = db.Column(db.String(32))
    score_today = db.Column(db.String(32))
    level = db.Column(db.String(32))
    ranking = db.Column(db.String(32))

    recent_date = db.Column(db.String(128))

    walking_count_total  = db.Column(db.String(128))
    walking_count_hour = db.Column(db.String(128))
    walking_count_monthly = db.Column(db.String(128))
    walking_count_monthly_accum = db.Column(db.String(128))
    walking_count_step_size = db.Column(db.String(128))


    walking_count_today = db.Column(db.String(128))
    walking_count_step_size_today = db.Column(db.String(128))
    
    ##todos = db.relationship('Todo', backref='fcuser', lazy=True)

class UserDiseaseDb(db.Model):
    __tablename__ = 'UserDiseaseDb'

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    user_id = db.Column(db.String(128))
    disease_none= db.Column(db.String(32))
    disease_gohyulap=db.Column(db.String(32))
    disease_dangyo=db.Column(db.String(32))
    disease_gogiyung=db.Column(db.String(32))
    disease_simjang=db.Column(db.String(32))
    disease_gabsang=db.Column(db.String(32))
    disease_gan=db.Column(db.String(32))
    disease_pae=db.Column(db.String(32))
    disease_sin=db.Column(db.String(32))
        
    smoke=db.Column(db.String(32))
    chuckchu=db.Column(db.String(32))
    muroup=db.Column(db.String(32)) 
    family_disease_none= db.Column(db.String(32)) 
    family_disease_gohyulap=db.Column(db.String(32)) 
    family_disease_dangyo=db.Column(db.String(32)) 
    family_disease_gogiyung=db.Column(db.String(32)) 
    family_disease_simjang=db.Column(db.String(32)) 
    family_disease_gabsang=db.Column(db.String(32)) 
    family_disease_gan=db.Column(db.String(32)) 
    family_disease_pae=db.Column(db.String(32)) 
    family_disease_sin=db.Column(db.String(32)) 


class HealthBandDataDb(db.Model):
    __tablename__ = 'healthbanddata'

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id=db.Column(db.String(256))
    keti_number=db.Column(db.String(256))
    type = db.Column(db.String(32))
    
    userid = db.Column(db.String(32))
    survey_level = db.Column(db.String(32))
    health_data = db.Column(db.Text)
    health_result_data = db.Column(db.Text)
    type = db.Column(db.String(32))
    send_yn = db.Column(db.String(32))
    

class Scale(db.Model):
    __tablename__ = 'scale'

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id=db.Column(db.String(256))
    type = db.Column(db.String(32))
    keti_number=db.Column(db.String(256))
    
    userid = db.Column(db.String(256))
    
    scale_data = db.Column(db.Text)
    sope_result_data = db.Column(db.Text)
    send_yn = db.Column(db.String(32))

class SensorDb(db.Model):
    __tablename__ = 'sensor'
    index=db.Column(db.Integer, primary_key=True,autoincrement=True)
    device_id=db.Column(db.String(256))
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    type=db.Column(db.String(128))
    practice_type=db.Column(db.String(128))
    device_name = db.Column(db.String(128))
    
    userid=db.Column(db.String(128))
    keti_number=db.Column(db.String(256))


class SensorLogDb(db.Model):
    __tablename__ = 'sensorlog'

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    sensor_id = db.Column(db.String(128))
    sensor_data = db.Column(db.String(256))

    userid = db.Column(db.String(32))
    

#############################################################
## end / smart health
#############################################################
class GUID(TypeDecorator):
    """Platform-independent GUID type.
    Uses PostgreSQL's UUID type, otherwise uses
    CHAR(32), storing as stringified hex values.
    """
    impl = CHAR

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(UUID())
        else:
            return dialect.type_descriptor(CHAR(32))

    def process_bind_param(self, value, dialect):
        if value is None:
            return value
        elif dialect.name == 'postgresql':
            return str(value)
        else:
            if not isinstance(value, uuid.UUID):
                return "%.32x" % uuid.UUID(value).int
            else:
                # hexstring
                return "%.32x" % value.int

    def process_result_value(self, value, dialect):
        if value is None:
            return value
        else:
            if not isinstance(value, uuid.UUID):
                value = uuid.UUID(value)
            return value

class MessageToken(db.Model):
    __tablename__ = 'messagetoken'

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    user_id = db.Column(db.String(32))
    token_key = db.Column(db.String(256))



class Todo(db.Model):
    __tablename__ = 'todo'

    id = db.Column(db.Integer, primary_key=True)
    ##fcuser_id = db.Column(db.String(32), db.ForeignKey('fcuser.userid'), nullable=False)
    fcuser_id = db.Column(db.String(32))
    title = db.Column(db.String(256))
    status = db.Column(db.Integer)
    due = db.Column(db.String(64))
    tstamp = db.Column(db.DateTime, server_default=db.func.now())

    @property
    def serialize(self):
        return {
            'id': self.id,
            'title': self.title,
            'fcuser': self.fcuser.userid,
            'tstamp': self.tstamp
        }



class FarmDb(db.Model):
    __tablename__ = 'farm'

    ##farm_id  = db.Column(db.String(32), primary_key=True)
    ##farm_id=db.Column(db.Integer, primary_key=True,autoincrement=True)

    farm_id = db.Column(db.String(128), primary_key=True, default=lambda: str(uuid.uuid4()))

    farm_name  = db.Column(db.String(32))
    ##created_at = db.Column(db.String(32))
    created_at = db.Column(db.DateTime, server_default=db.func.now())


    farm_crop  = db.Column(db.String(32))

    farm_kind = db.Column(db.String(32))
    farm_address  = db.Column(db.String(32),)

    user_id  = db.Column(db.String(32))
    user_bod  = db.Column(db.String(32))
    user_name  = db.Column(db.String(32))
    user_email = db.Column(db.String(32))

    user_mobile  = db.Column(db.String(32))
    user_fax  = db.Column(db.String(32))
    user_tel  = db.Column(db.String(32))

    house_cnt=db.Column(db.Integer)
    cctv_cnt=db.Column(db.Integer)
    sensor_cnt=db.Column(db.Integer)
    ana_cnt=db.Column(db.Integer)
    control_cnt=db.Column(db.Integer)



class HouseDb(db.Model):
    __tablename__="house"

    ##house_id=db.Column(db.String(32), primary_key=True)
    ##house_id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    ##house_id = db.Column(GUID(), primary_key=True, default=lambda: str(uuid.uuid4()))
    house_id = db.Column(db.String(128), primary_key=True, default=lambda: str(uuid.uuid4()))

    created_at = db.Column(db.DateTime, server_default=db.func.now())
    ##created_at = db.Column(db.String(32)) 
    
    house_name=db.Column(db.String(64))
    farm_id=db.Column(db.String(128))


class DeviceDb(db.Model):
    __tablename__ ="device"

    device_id=db.Column(db.String(32), primary_key=True)
    ##device_id = db.Column(GUID(), primary_key=True, default=lambda: str(uuid.uuid4()))

    created_at = db.Column(db.String(32))
    type=db.Column(db.String(32))
    sale_date=db.Column(db.String(32))
    contents=db.Column(db.String(32))
    status=db.Column(db.String(32))
    remark=db.Column(db.String(128))
    farm_id=db.Column(db.String(128))
    farm_name=db.Column(db.String(32))
    house_id=db.Column(db.String(128))
    house_name=db.Column(db.String(32))
    mac_address=db.Column(db.String(32))
    set_location=db.Column(db.String(32))
    usage_history_list=db.Column(db.String(512))
    sensor_data=db.Column(db.Text)
    data_file_path_prefix=db.Column(db.String(128))

    operation=db.Column(db.Text)

class SensorDataDb(db.Model):
    __tablename__ ="sensor_data"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())


    send_alarm=db.Column(db.String(32),default='0')

    device_id=db.Column(db.String(32))
    field1=db.Column(db.String(32))
    field2=db.Column(db.String(32))
    field3=db.Column(db.String(32))
    field4=db.Column(db.String(32))
    field5=db.Column(db.String(32))
    field6=db.Column(db.String(32))
    field7=db.Column(db.String(32))
    field8=db.Column(db.String(32))
    field9=db.Column(db.String(32))
    field10=db.Column(db.String(32))
    field11=db.Column(db.String(32))
    field12=db.Column(db.String(32))
    field13=db.Column(db.String(32))
    field14=db.Column(db.String(32))
    field15=db.Column(db.String(32))
    field16=db.Column(db.String(32))
    field17=db.Column(db.String(32))
    field18=db.Column(db.String(32))
    field19=db.Column(db.String(32))
    field20=db.Column(db.String(32))
    field19=db.Column(db.String(32))



class GrowthDataDb(db.Model):
    __tablename__ ="growthdata"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    send_alarm=db.Column(db.String(32),default='0')
    device_id=db.Column(db.String(32))


    farmID= db.Column(db.String(128))

    greenHouseID= db.Column(db.String(128))
    date= db.Column(db.String(64))
    sequenceID= db.Column(db.String(64))
    roundID= db.Column(db.String(128))
    cameraname= db.Column(db.String(64))
    timestamp= db.Column(db.String(64))

    flowercount= db.Column(db.String(64))
    fruitcount= db.Column(db.String(64))
    fruitmaturity= db.Column(db.String(64))
    fruitSize= db.Column(db.String(64))
    avgleafarea= db.Column(db.String(64))
    totalleafcount= db.Column(db.String(64))
    leafcolor= db.Column(db.String(64))
    aplicalleafcount= db.Column(db.String(64))
    avgaplicalleaflength= db.Column(db.String(64))
    plantheadcount= db.Column(db.String(64))
    avgplantheadsize= db.Column(db.String(64))
    guttation= db.Column(db.String(64))
    avgguttationsize= db.Column(db.String(64))
    mite= db.Column(db.String(64))
    tick= db.Column(db.String(64))
    graymoldrot= db.Column(db.String(64))
    sphaerothecahumuli= db.Column(db.String(64))
    larva= db.Column(db.String(64))
    doubleblossom= db.Column(db.String(64))
    duster= db.Column(db.String(64))
    trifoliilarvar= db.Column(db.String(64))
    phytophtoranicotianae= db.Column(db.String(64))
    fusariumoxysporum= db.Column(db.String(64))

class PestDataDb(db.Model):
    __tablename__ ="pestdata"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    send_alarm=db.Column(db.String(32),default='0')
    device_id=db.Column(db.String(32))


    farmID= db.Column(db.String(128))

    greenHouseID= db.Column(db.String(128))
    date= db.Column(db.String(64))
    sequenceID= db.Column(db.String(64))
    roundID= db.Column(db.String(128))
    cameraname= db.Column(db.String(64))
    timestamp= db.Column(db.String(64))

    mite= db.Column(db.String(64))
    tick= db.Column(db.String(64))
    graymoldrot= db.Column(db.String(64))
    sphaerothecahumuli= db.Column(db.String(64))
    fusariumoxysporum= db.Column(db.String(64))
    phytophtoranicotianae= db.Column(db.String(64))

class DisorderDataDb(db.Model):
    __tablename__ ="disorderdata"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    send_alarm=db.Column(db.String(32),default='0')
    device_id=db.Column(db.String(32))

    nitrogen= db.Column(db.String(128))
    phosphorus= db.Column(db.String(128))
    potassium= db.Column(db.String(64))
    magnesium= db.Column(db.String(64))
    calcium= db.Column(db.String(64))
    etc= db.Column(db.String(64))


class DeviceDefectDb(db.Model):
    __tablename__ ="device_defect"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    category = db.Column(db.String(32))
    device_id = db.Column(db.String(32))
    error_code = db.Column(db.String(32))
    detail_content = db.Column(db.String(32))
    user_id = db.Column(db.String(32))
    user_name = db.Column(db.String(32))
    house_id = db.Column(db.String(128))
    house_name = db.Column(db.String(32))
    user_tel = db.Column(db.String(32))
    proc_yn = db.Column(db.String(32))
    proc_date = db.Column(db.String(32))
    fix_yn = db.Column(db.String(32))
    fix_accept_date = db.Column(db.String(32))
    fix_fish_date = db.Column(db.String(32))
    fix_content = db.Column(db.Text)


class DeviceOperationDb(db.Model):
    __tablename__ ="device_operation"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    type = db.Column(db.String(32))
    time = db.Column(db.String(32))
    checked = db.Column(db.String(32))


class AiStatusDb(db.Model):

    __tablename__ = 'ai_status'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id=db.Column(db.String(32))

    phase = db.Column(db.String(64))
    status = db.Column(db.String(64))
    image_path = db.Column(db.String(64))

    
class TestDb(db.Model):

    __tablename__ = 'test'

    index = db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.String(32))

    farm_id  = db.Column(db.String(128))
    farm_name  = db.Column(db.String(32))
    owner_id  = db.Column(db.String(32))
    crop  = db.Column(db.String(32))
    kind = db.Column(db.String(32))

    address_provin  = db.Column(db.String(32))
    address_city  = db.Column(db.String(32))
    address_ect  = db.Column(db.String(32))

    business_create_date  = db.Column(db.String(32))
    tel_num  = db.Column(db.String(32),)
    mobile_num  = db.Column(db.String(32))
    fax_num  = db.Column(db.String(32))
    email  = db.Column(db.String(32))
    homepage  = db.Column(db.String(32))
    house_list  = db.Column(db.String(32))


class QuestionDb(db.Model):

    __tablename__ = 'question'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    from_type = db.Column(db.String(64))
    type = db.Column(db.String(64))
    title = db.Column(db.Text)
    content = db.Column(db.Text)

    user_id = db.Column(db.String(64))

    user_phone = db.Column(db.String(64))
    user_address = db.Column(db.String(64))
    response_yn = db.Column(db.String(64))
    response_date = db.Column(db.String(64))
    response_content = db.Column(db.Text)



class NoticeHistoryDb(db.Model):

    __tablename__ = 'notice_history'

    ##id = db.Column(db.String(64), primary_key=True)

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    urgent_yn = db.Column(db.String(64))
    title = db.Column(db.String(64))
    content = db.Column(db.Text)

    date_to_send = db.Column(db.String(64))

    receive_person_list_display = db.Column(db.String(64))
    receive_person_list = db.Column(db.String(64))



class NoticeUserHistoryDb(db.Model):

    __tablename__ = 'notice_user_history'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())
 
    notice_id = db.Column(db.Integer)
    user_id = db.Column(db.String(64))   
    check_yn = db.Column(db.String(64))



class NoticeDb(db.Model):

    __tablename__ = 'notice'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.String(64))
    title = db.Column(db.String(64))
    urgent_yn = db.Column(db.String(64))
    catagory = db.Column(db.String(64))
    detail_item = db.Column(db.String(64))
    content = db.Column(db.String(64))
    farm_owner = db.Column(db.String(64))
    house_name = db.Column(db.String(64))
    crop = db.Column(db.String(64))
    kind = db.Column(db.String(64))
    check_yn = db.Column(db.String(64))
    accum_check_yn = db.Column(db.String(64))
    send_yn = db.Column(db.String(64))
    receive_person_list = db.Column(db.String(64))


class AlarmSettingSensor(db.Model):

    __tablename__ = 'alarm_setting_sensor'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    alarm_type = db.Column(db.String(64))

    indoor_temp_min=db.Column(db.String(64))
    indoor_temp_max=db.Column(db.String(64))
    indoor_temp_alarm_issu_type=db.Column(db.String(64))

    indoor_humidity_min=db.Column(db.String(64))
    indoor_humidity_max=db.Column(db.String(64))
    indoor_humidity_alarm_issu_type=db.Column(db.String(64))

    indoor_co2_min=db.Column(db.String(64))
    indoor_co2_max=db.Column(db.String(64))
    indoor_co2_alarm_issu_type=db.Column(db.String(64))

    indoor_sunny_min=db.Column(db.String(64))
    indoor_sunny_max=db.Column(db.String(64))
    indoor_sunny_alarm_issu_type=db.Column(db.String(64))

    basic_temp_min=db.Column(db.String(64))
    basic_temp_max=db.Column(db.String(64))
    basic_temp_alarm_issu_type=db.Column(db.String(64))

    basic_humidity_min=db.Column(db.String(64))
    basic_humidity_max=db.Column(db.String(64))
    basic_humidity_alarm_issu_type=db.Column(db.String(64))

    basic_ec_min=db.Column(db.String(64))
    basic_ec_max=db.Column(db.String(64))
    basic_ec_alarm_issu_type=db.Column(db.String(64))



class AlarmSettingSensorIndividual(db.Model):

    __tablename__ = 'alarm_setting_sensor_individaul'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id = db.Column(db.String(64))
    alarm_type = db.Column(db.String(64))

    indoor_temp_min=db.Column(db.String(64))
    indoor_temp_max=db.Column(db.String(64))
    indoor_temp_alarm_issu_type=db.Column(db.String(64))

    indoor_humidity_min=db.Column(db.String(64))
    indoor_humidity_max=db.Column(db.String(64))
    indoor_humidity_alarm_issu_type=db.Column(db.String(64))

    indoor_co2_min=db.Column(db.String(64))
    indoor_co2_max=db.Column(db.String(64))
    indoor_co2_alarm_issu_type=db.Column(db.String(64))

    indoor_sunny_min=db.Column(db.String(64))
    indoor_sunny_max=db.Column(db.String(64))
    indoor_sunny_alarm_issu_type=db.Column(db.String(64))

    basic_temp_min=db.Column(db.String(64))
    basic_temp_max=db.Column(db.String(64))
    basic_temp_alarm_issu_type=db.Column(db.String(64))

    basic_humidity_min=db.Column(db.String(64))
    basic_humidity_max=db.Column(db.String(64))
    basic_humidity_alarm_issu_type=db.Column(db.String(64))

    basic_ec_min=db.Column(db.String(64))
    basic_ec_max=db.Column(db.String(64))
    basic_ec_alarm_issu_type=db.Column(db.String(64))



class AlarmSettingGrowth(db.Model):

    __tablename__ = 'alarm_setting_growth'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())
    alarm_type = db.Column(db.String(64))

    leaf_cnt_min=db.Column(db.String(64)) ## indoor_temp_min
    leaf_cnt_max=db.Column(db.String(64)) ## indoor_temp_max
    leaf_cnt_alarm_issu_type=db.Column(db.String(64)) ## indoor_temp_alarm_issu_type

    in_leaf_cnt_min=db.Column(db.String(64)) ## indoor_humidity_min
    in_leaf_cnt_max=db.Column(db.String(64)) ## indoor_humidity_max
    in_leaf_cnt_alarm_issu_type=db.Column(db.String(64)) ## indoor_humidity_alarm_issu_type

    head_cnt_min=db.Column(db.String(64)) ## indoor_co2_min
    head_cnt_max=db.Column(db.String(64)) ## indoor_co2_max
    head_cnt_alarm_issu_type=db.Column(db.String(64)) ##indoor_co2_alarm_issu_type

    ilack_cnt_min=db.Column(db.String(64)) ## indoor_sunny_min
    ilack_cnt_max=db.Column(db.String(64)) ## indoor_sunny_max
    ilack_cnt_alarm_issu_type=db.Column(db.String(64)) ## indoor_sunny_alarm_issu_type


    flower_cnt_min=db.Column(db.String(64)) ## basic_temp_min
    flower_cnt_max=db.Column(db.String(64)) ## basic_temp_max
    flower_cnt_alarm_issu_type=db.Column(db.String(64)) ## basic_temp_alarm_issu_type

    fruit_cnt_min=db.Column(db.String(64)) ## basic_humidity_min
    fruit_cnt_max=db.Column(db.String(64)) ## basic_humidity_max
    fruit_cnt_alarm_issu_type=db.Column(db.String(64)) ## basic_humidity_alarm_issu_type

    fruit_size_min=db.Column(db.String(64)) ## basic_ec_min
    fruit_size_max=db.Column(db.String(64)) ## basic_ec_max
    fruit_size_alarm_issu_type=db.Column(db.String(64)) ## basic_ec_alarm_issu_type


    in_leaf_length_min=db.Column(db.String(64)) 
    in_leaf_length_max=db.Column(db.String(64)) 
    in_leaf_length_alarm_issu_type=db.Column(db.String(64)) 

    in_leaf_size_min=db.Column(db.String(64)) 
    in_leaf_size_max=db.Column(db.String(64)) 
    in_leaf_size_alarm_issu_type=db.Column(db.String(64)) 



class AlarmSettingGrowthIndividual(db.Model):

    __tablename__ = 'alarm_setting_growth_individual'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id = db.Column(db.String(64))
    alarm_type = db.Column(db.String(64))

    leaf_cnt_min=db.Column(db.String(64)) ## indoor_temp_min
    leaf_cnt_max=db.Column(db.String(64)) ## indoor_temp_max
    leaf_cnt_alarm_issu_type=db.Column(db.String(64)) ## indoor_temp_alarm_issu_type

    in_leaf_cnt_min=db.Column(db.String(64)) ## indoor_humidity_min
    in_leaf_cnt_max=db.Column(db.String(64)) ## indoor_humidity_max
    in_leaf_cnt_alarm_issu_type=db.Column(db.String(64)) ## indoor_humidity_alarm_issu_type

    head_cnt_min=db.Column(db.String(64)) ## indoor_co2_min
    head_cnt_max=db.Column(db.String(64)) ## indoor_co2_max
    head_cnt_alarm_issu_type=db.Column(db.String(64)) ##indoor_co2_alarm_issu_type

    ilack_cnt_min=db.Column(db.String(64)) ## indoor_sunny_min
    ilack_cnt_max=db.Column(db.String(64)) ## indoor_sunny_max
    ilack_cnt_alarm_issu_type=db.Column(db.String(64)) ## indoor_sunny_alarm_issu_type


    flower_cnt_min=db.Column(db.String(64)) ## basic_temp_min
    flower_cnt_max=db.Column(db.String(64)) ## basic_temp_max
    flower_cnt_alarm_issu_type=db.Column(db.String(64)) ## basic_temp_alarm_issu_type

    fruit_cnt_min=db.Column(db.String(64)) ## basic_humidity_min
    fruit_cnt_max=db.Column(db.String(64)) ## basic_humidity_max
    fruit_cnt_alarm_issu_type=db.Column(db.String(64)) ## basic_humidity_alarm_issu_type

    fruit_size_min=db.Column(db.String(64)) ## basic_ec_min
    fruit_size_max=db.Column(db.String(64)) ## basic_ec_max
    fruit_size_alarm_issu_type=db.Column(db.String(64)) ## basic_ec_alarm_issu_type


    in_leaf_length_min=db.Column(db.String(64)) 
    in_leaf_length_max=db.Column(db.String(64)) 
    in_leaf_length_alarm_issu_type=db.Column(db.String(64)) 

    in_leaf_size_min=db.Column(db.String(64)) 
    in_leaf_size_max=db.Column(db.String(64)) 
    in_leaf_size_alarm_issu_type=db.Column(db.String(64)) 




class AlarmSettingDisorder(db.Model):

    __tablename__ = 'alarm_setting_disorder'


    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())
    alarm_type = db.Column(db.String(64))

    nitrogen=db.Column(db.String(64)) 
    nitrogen_min=db.Column(db.String(64)) 
    nitrogen_alarm_issu_type=db.Column(db.String(64)) 

    phosphorus=db.Column(db.String(64)) 
    phosphorus_min=db.Column(db.String(64)) 
    phosphorus_alarm_issu_type=db.Column(db.String(64)) 

    patassium=db.Column(db.String(64))
    patassium_min=db.Column(db.String(64)) 
    patassium_alarm_issu_type=db.Column(db.String(64)) 

    magnetsium=db.Column(db.String(64)) 
    magnetsium_min=db.Column(db.String(64)) 
    magnetsium_alarm_issu_type=db.Column(db.String(64)) 

    calcium=db.Column(db.String(64)) 
    calcium_min=db.Column(db.String(64)) 
    calcium_alarm_issu_type=db.Column(db.String(64)) 

    etc=db.Column(db.String(64))
    etc_min=db.Column(db.String(64)) 
    etc_alarm_issu_type=db.Column(db.String(64)) 



class AlarmSettingDisorderIndividual(db.Model):

    __tablename__ = 'alarm_setting_disorder_individual'


    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id = db.Column(db.String(64))
    alarm_type = db.Column(db.String(64))

    nitrogen=db.Column(db.String(64)) 
    nitrogen_min=db.Column(db.String(64)) 
    nitrogen_alarm_issu_type=db.Column(db.String(64)) 

    phosphorus=db.Column(db.String(64)) 
    phosphorus_min=db.Column(db.String(64)) 
    phosphorus_alarm_issu_type=db.Column(db.String(64)) 

    patassium=db.Column(db.String(64))
    patassium_min=db.Column(db.String(64)) 
    patassium_alarm_issu_type=db.Column(db.String(64)) 

    magnetsium=db.Column(db.String(64)) 
    magnetsium_min=db.Column(db.String(64)) 
    magnetsium_alarm_issu_type=db.Column(db.String(64)) 

    calcium=db.Column(db.String(64)) 
    calcium_min=db.Column(db.String(64)) 
    calcium_alarm_issu_type=db.Column(db.String(64)) 

    etc=db.Column(db.String(64))
    etc_min=db.Column(db.String(64)) 
    etc_alarm_issu_type=db.Column(db.String(64)) 
    
    

class AlarmSettingPest(db.Model):

    __tablename__ = 'alarm_setting_pest'


    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())
    alarm_type = db.Column(db.String(64))

    ice_min=db.Column(db.String(64)) ## indoor_temp_min
    ice_max=db.Column(db.String(64)) ## indoor_temp_max
    ice_alarm_issu_type=db.Column(db.String(64)) ## indoor_temp_alarm_issu_type

    aphid_min=db.Column(db.String(64)) ## indoor_humidity_min
    aphid_max=db.Column(db.String(64)) ## indoor_humidity_max
    aphid_alarm_issu_type=db.Column(db.String(64)) ## indoor_humidity_alarm_issu_type

    ash_mold_min=db.Column(db.String(64)) ## indoor_co2_min
    ash_mold_max=db.Column(db.String(64)) ## indoor_co2_max
    ash_mold_alarm_issu_type=db.Column(db.String(64)) ##indoor_co2_alarm_issu_type


    white_mold_min=db.Column(db.String(64)) ## indoor_sunny_min
    white_mold_max=db.Column(db.String(64)) ## indoor_sunny_min
    white_mold_alarm_issu_type=db.Column(db.String(64)) ## indoor_sunny_alarm_issu_type


    withered_disease_min=db.Column(db.String(64)) ## indoor_sunny_min
    withered_disease_alarm_issu_type=db.Column(db.String(64)) ## indoor_sunny_alarm_issu_type

    plague_min=db.Column(db.String(64)) ## basic_temp_min
    plague_alarm_issu_type=db.Column(db.String(64)) ## basic_temp_alarm_issu_type

    anthrax_min=db.Column(db.String(64)) ## basic_humidity_min
    anthrax_alarm_issu_type=db.Column(db.String(64)) ## basic_humidity_alarm_issu_type

    etc_min=db.Column(db.String(64)) ## basic_ec_min
    etc_alarm_issu_type=db.Column(db.String(64)) ## basic_ec_alarm_issu_type



class AlarmSettingPestIndividual(db.Model):

    __tablename__ = 'alarm_setting_pest_individual'


    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id = db.Column(db.String(64))
    alarm_type = db.Column(db.String(64))

    ice_min=db.Column(db.String(64)) ## indoor_temp_min
    ice_max=db.Column(db.String(64)) ## indoor_temp_max
    ice_alarm_issu_type=db.Column(db.String(64)) ## indoor_temp_alarm_issu_type

    aphid_min=db.Column(db.String(64)) ## indoor_humidity_min
    aphid_max=db.Column(db.String(64)) ## indoor_humidity_max
    aphid_alarm_issu_type=db.Column(db.String(64)) ## indoor_humidity_alarm_issu_type

    ash_mold_min=db.Column(db.String(64)) ## indoor_co2_min
    ash_mold_max=db.Column(db.String(64)) ## indoor_co2_max
    ash_mold_alarm_issu_type=db.Column(db.String(64)) ##indoor_co2_alarm_issu_type


    white_mold_min=db.Column(db.String(64)) ## indoor_sunny_min
    white_mold_max=db.Column(db.String(64)) ## indoor_sunny_min
    white_mold_alarm_issu_type=db.Column(db.String(64)) ## indoor_sunny_alarm_issu_type


    withered_disease_min=db.Column(db.String(64)) ## indoor_sunny_min
    withered_disease_alarm_issu_type=db.Column(db.String(64)) ## indoor_sunny_alarm_issu_type

    plague_min=db.Column(db.String(64)) ## basic_temp_min
    plague_alarm_issu_type=db.Column(db.String(64)) ## basic_temp_alarm_issu_type

    anthrax_min=db.Column(db.String(64)) ## basic_humidity_min
    anthrax_alarm_issu_type=db.Column(db.String(64)) ## basic_humidity_alarm_issu_type

    etc_min=db.Column(db.String(64)) ## basic_ec_min
    etc_alarm_issu_type=db.Column(db.String(64)) ## basic_ec_alarm_issu_type



class AlarmHistoryDb(db.Model):

    __tablename__ = 'alarm_history'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id = db.Column(db.String(64))
    
    type = db.Column(db.String(64))
    category = db.Column(db.String(64))
    sensor_name = db.Column(db.String(64))
    sensor_item = db.Column(db.String(64))
    alarm_contents = db.Column(db.Text)

    farm_owner_name = db.Column(db.String(64))
    farm_owner_id = db.Column(db.String(64))
    house_id = db.Column(db.String(64))

    house_name = db.Column(db.String(64))
    crop = db.Column(db.String(64))
    kind = db.Column(db.String(64))
    check_yn = db.Column(db.String(64) , default='미확인')
    alarm_none_check_cnt = db.Column(db.String(64))
    send_yn = db.Column(db.String(64))


class LaonFarmDb(db.Model):
    __tablename__ ="laon_farm_db"

    id=db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    upate_date = db.Column(db.String(64))

    farm_id=db.Column(db.String(128))
    house_id=db.Column(db.String(128))
    sequence_id=db.Column(db.String(128))
    round_id=db.Column(db.String(128))



class ImageBasicDataDb(db.Model):

    __tablename__ = 'image_basic_data'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    device_id = db.Column(db.String(64))


    farmID = db.Column(db.String(128))
    greenHouseID = db.Column(db.String(128))
    targetDate = db.Column(db.String(128))
    sequenceID = db.Column(db.String(128))

    roundID = db.Column(db.String(64))


    image_path = db.Column(db.String(64))
    
    check_defect_manul = db.Column(db.String(64))
    check_defect_auto = db.Column(db.String(64))
    plague_type = db.Column(db.String(256))
    plague_level = db.Column(db.String(256))


class FarmDialyDb(db.Model):

    __tablename__ = 'farmdialry'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    created_at = db.Column(db.DateTime, server_default=db.func.now())

    user_id = db.Column(db.String(64))
    input_date = db.Column(db.String(64))
    image_path = db.Column(db.String(128))

    checkbox_gwanju = db.Column(db.String(64))
    title_gwanju = db.Column(db.String(256))

    checkbox_banjae = db.Column(db.String(64))
    title_banjae = db.Column(db.String(256))

    checkbox_goome = db.Column(db.String(64))
    title_goome = db.Column(db.String(256))
    
    checkbox_panme = db.Column(db.String(64))
    title_panme = db.Column(db.String(256))

class EmployeesDb(db.Model):

    __tablename__ = 'employee'

    id = db.Column(db.Integer, primary_key=True)
    fullname = db.Column(db.String(150))
    position = db.Column(db.String(150))
    office = db.Column(db.String(150))
    age = db.Column(db.Integer)
    startdate = db.Column(db.String(150))
    salary = db.Column(db.String(150))



class TechDb(db.Model):

    __tablename__ = 'tech'

    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    create_at = db.Column(db.String(64))
    kind = db.Column(db.String(64))

    title = db.Column(db.String(64))
    content = db.Column(db.Text)