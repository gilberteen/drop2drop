# app/__init__.py 
# 
import os
from re import T
from flask import Flask,Blueprint
from flask import request, redirect, render_template, session
from flask_wtf.csrf import CSRFProtect

from modelpool.models import db , TB_DROP_USER
from .formspool.forms import RegisterForm, LoginForm
from api_v1 import api as api_v1
from api_cc_v1 import api_cc as api_cc_v1

from .threadPool import checkDeiceConnectionThread
from .config import config_by_name ,key
from .control import main_control
from .control_mobile import mobile_control
import pymysql  
pymysql.install_as_MySQLdb()

#old server
#db_url = "mysql://dba:informarkDBA1!@ec2-52-78-23-186.ap-northeast-2.compute.amazonaws.com:3306/smarthealth"

#new sever 
#db_url = "mysql://dba:drop2drop1!@34.69.241.46:3306/drop2drop"
db_url = "mysql://dba:drop2drop1!@34.171.223.237:3306/drop2drop"


home_url = ""

#ADMIN_URL = 'https://www.drop2droporder.com'
#USER_URL = 'https://www.drop2droporder.com'

ADMIN_URL = 'http://34.171.223.237'
USER_URL = 'http://34.171.223.237'

def create_admin_app_80(config_name="prod"):
    app = Flask(__name__) 
    

    ###################################################################
    ## before
    ###################################################################
    @app.before_request
    def redirect_path():
        return redirect(ADMIN_URL);
    
    return app


def create_admin_app(config_name="prod"):


    app = Flask(__name__) 
    ##app = Flask(__name__,host_matching=True,static_host=ADMIN_URL   ) 
    
    app.config.from_object(config_by_name[config_name]) 
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

    ## db setup for sqlite
    ##basedir = os.path.abspath(os.path.dirname(__file__))
    ##dbfile = os.path.join(basedir, 'db.sqlite')
    ##app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + dbfile

    
    ## db setup for mysql
    app.config["SQLALCHEMY_DATABASE_URI"] = db_url
    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    csrf = CSRFProtect()

    db.init_app(app)
    db.app = app
    db.create_all()


    ## control app 등록
    app.register_blueprint(api_v1, url_prefix='/api/v1')
    app.register_blueprint(api_cc_v1,url_prefix='/api_cc/v1')
    app.register_blueprint(main_control)
    app.register_blueprint(mobile_control)


    ## start thread  ( only admin)
    ####################################################################
    #threadPool.checkDeiceConnectionThread()

    ###################################################################
    ## root
    ###################################################################
    @app.route('/', methods=['GET'])
    def home():
        print("do main()")
        home_url = "/main"
        return redirect(home_url)


    '''
    def home():
        print("do main()")
        userid = session.get('userid', None)

        print("session.get('userid', None) : " + str(userid))

        if userid:
            fcuser = TB_DROP_USER.query.filter_by(user_id=userid).first()

            home_url = "/main"
            return redirect(home_url)
            ##return redirect('/main')
            ##return render_template('/main/main.html', userid=fcuser.userid)
            
        else:
        
            form = LoginForm()
            if form.validate_on_submit():
                session['userid'] = form.data.get('userid')
                return redirect('/')

            return render_template('/login/authentication-login1.html', form=form)
    '''
    ## sample router
    @app.route("/test") 
    def test(): 
        return "test" 

    return app



def create_user_app_80(config_name="prod"):
    app = Flask(__name__) 
    

    ###################################################################
    ## before
    ###################################################################
    @app.before_request
    def redirect_path():
        return redirect(USER_URL);
    
    return app


def create_user_app(config_name="prod"):
    app = Flask(__name__) 
    
    app.config.from_object(config_by_name[config_name]) 
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0


    ## db setup for sqlite
    ##basedir = os.path.abspath(os.path.dirname(__file__))
    ##dbfile = os.path.join(basedir, 'db.sqlite')
    ##app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + dbfile

    ## db setup for mysql
    app.config["SQLALCHEMY_DATABASE_URI"] = db_url

    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    csrf = CSRFProtect()

    db.init_app(app)
    db.app = app
    db.create_all()

   
    ## control app 등록
    app.register_blueprint(api_v1, url_prefix='/api/v1')
    app.register_blueprint(api_cc_v1, url_prefix='/api_cc/v1')
    app.register_blueprint(main_control)
    app.register_blueprint(mobile_control)


    ###################################################################
    ## before
    ###################################################################
    '''
    @app.before_request
    def redirect_path():
        https_url = "https://www.goeunai.com"
        return redirect(https_url);
    '''

    
    return app


