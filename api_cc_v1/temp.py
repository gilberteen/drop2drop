
@api_cc.route('/get_public_care_admin_list_search',methods=['GET','POST','PUT','DELETE'])
def get_public_care_admin_list_search():
     if request.method == 'POST':
        print("get_public_care_admin_list_search() !!!")
        search_item ="" 
        search_content=""
        from_date=""
        end_date=""
        area=""

        from_date_ = datetime.today().strftime('%Y.%m.%d %H:%M:%S')
        end_date_ = datetime.today().strftime('%Y.%m.%d %H:%M:%S')

        request_data = request.get_json(force=True)
        if(is_json_key_present(request_data ,'search_item')):
            search_item = request_data['search_item']
            print(search_item)
        if(is_json_key_present(request_data ,'search_content')):   
            search_content = request_data['search_content']
            print(str(search_content))
        if(is_json_key_present(request_data ,'from_date')):   
            from_date = request_data['from_date']
            if(from_date!=""):
                if(isDateFormat(1,from_date) == true):
                    from_date_ = datetime.strptime(str(from_date), '%Y-%m-%d %H:%M:%S')
                    print("from_date_:" + str(from_date_))
                else:
                    return jsonify({"status":"fail","error":"dataformat is not validate"})
        
        if(is_json_key_present(request_data ,'end_date')):  
            end_date = request_data['end_date'] 
            if(end_date!=""):
                if(isDateFormat(1,end_date) == true):
                    end_date_ = datetime.strptime(str(end_date), '%Y-%m-%d %H:%M:%S')
                    print("end_date_:" + str(end_date_))
                else:
                    return jsonify({"status":"fail","error":"dataformat is not validate"})
        if(is_json_key_present(request_data ,'area')):  
            area = request_data['area']
      
        public_care_list_db= db.session.query(TB_PUBLICK_CARE_ADMIN_USER).all()
        print(str(public_care_list_db))

        if( len(public_care_list_db) > 0 ):
            public_care_json = {};
            public_care_json.update({"state" : "ok"})
            public_care_list = [];
            for public_care in public_care_list_db:
                temp_public_care = {}
                temp_public_care.update({"public_care_admin_id" : public_care.public_care_admin_id})
                temp_public_care.update({"public_care_created_date" : str(public_care.public_care_created_date)})
                temp_public_care.update({"public_care_password" : public_care.public_care_password})
                temp_public_care.update({"public_care_username" : public_care.public_care_username})
                temp_public_care.update({"public_care_bod" : public_care.public_care_bod})
                temp_public_care.update({"public_care_role" : public_care.public_care_role})
                temp_public_care.update({"public_care_gender" : public_care.public_care_gender})
                temp_public_care.update({"public_care_recent_connection_time" : str(public_care.public_care_recent_connection_time)})
                temp_public_care.update({"public_care_email" : public_care.public_care_email})
                temp_public_care.update({"public_care_address" : public_care.public_care_address})
                temp_public_care.update({"public_care_tel" : public_care.public_care_tel})
                temp_public_care.update({"public_care_del_reason" : public_care.public_care_del_reason})
                temp_public_care.update({"public_care_del_reason_detail" : public_care.public_care_del_reason_detail})
                temp_public_care.update({"public_care_del_date" : str(public_care.public_care_del_date)})
                temp_public_care.update({"public_care_date_user_agreement" : str(public_care.public_care_date_user_agreement)})
                temp_public_care.update({"public_care_del_user" : public_care.public_care_del_user})
                temp_public_care.update({"public_care_company_name" : public_care.public_care_company_name})
                temp_public_care.update({"public_care_organization" : public_care.public_care_organization})
                temp_public_care.update({"public_care_job" : public_care.public_care_job})
                print("id : "  + str(public_care.public_care_admin_id))
                temp_string = str(public_care.public_care_memo)
                temp_string = json.loads(temp_string)  
                temp_public_care.update({"public_care_memo" : temp_string})
                temp_public_care.update({"public_care_status" : public_care.public_care_status})
               

                bFind = false
                print("search itme : " + str(search_item))
                if(search_item == "public_care_username"):
                    print("public_care.public_care_username : " + public_care.public_care_username)
                    print("search_content : " + search_content)
                    if(public_care.public_care_username==search_content):
                        bFind=true
                    else:
                        bFind=false
                elif(search_item=="public_care_username"):
                    if(public_care.silver_public_care_name==search_content):
                        bFind=true
                    else:
                        bFind=false
                elif(search_item=="public_care_admin_id"):
                    if(public_care.public_care_admin_id==search_content):
                        bFind=true
                    else:
                        bFind=false

                if(not(from_date_ is None)):
                    temp_date = ""
                    temp_date = public_care.public_care_recent_connection_time
                    if(not (temp_date is None) and temp_date !="") :
                        if(isDateFormat(1,temp_date)==true):
                            
                            temp_date_ =  datetime.strptime(str(temp_date), '%Y-%m-%d %H:%M:%S')
                            if(from_date_ >= temp_date_):
                                bFind=true
                            else:
                                bFind=false
                        else:
                            bFind=false
                    else:
                        bFind=false
                
                if(not(end_date_ is None)):
                    temp_date = ""
                    temp_date = public_care.public_care_recent_connection_time
                    if(not (temp_date is None) and temp_date !="") :
                        if(isDateFormat(1,temp_date)==true):
                            temp_date_ =  datetime.strptime(str(temp_date), '%Y-%m-%d %H:%M:%S')
                            if(end_date_ >= temp_date_):
                                bFind=true
                            else:
                                bFind=false
                        else:
                            bFind=false
                    else:
                        bFind=false
                if (not(area is None) and area != "" ):
                    temp_address = str(public_care.public_care_address);
                    if(area  in temp_address):
                        bFind=true
                    else:
                        bFind=false
                        
                if bFind == true :
                    public_care_list.append(temp_public_care)

            public_care_json.update({"silvers" : public_care_list})
            print("public_care_json : " + str(public_care_json))
            ##return str(farms_json)
            ##return jsonify(json.dumps(farms_json, ensure_ascii=False))
            ##return jsonify(farms_json)
            return json.dumps(public_care_json, ensure_ascii=False)
        
        else:
            return jsonify({"status":"fail","error":"no data"})
            ##return jsonify(), 201
