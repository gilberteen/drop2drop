from flask import Blueprint

api = Blueprint('api', __name__)


from . import main_api
from . import todo
from . import usermanage
