from flask import jsonify
from flask import request
from flask import Blueprint
from flask import session
import datetime
import requests
from . import api

import sys
import os
sys.path.append('..')
from modelpool.models import db, Fcuser, Todo


def send_slack(msg):
    res = requests.post('https://hooks.slack.com/services/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', json={
        'text': msg
    }, headers={'Content-Type': 'application/json'})


@api.route('/get_user_info/<uid>',methods=['GET','PUT','DELETE'])
def get_user_info(uid):
    if request.method == 'GET' :
        user = Fcuser.query.filter(Fcuser.id == uid).first()
        return jsonify(user.serialize)
    elif request.method == 'DELETE':
        Fcuser.query.delete(Fcuser.id == uid)
        return jsonify(),204


