
# app/control_mobile.py
# 
import os
from re import A, search
from typing import Awaitable

from flask import Flask, Blueprint, url_for
from flask import request, redirect, render_template, session
from flask import jsonify
from flask.wrappers import Response
from flask_wtf import form
from sqlalchemy.sql import text,func
from sqlalchemy import or_ , and_
from sqlalchemy import Date, cast
import sys
import os
from sqlalchemy.sql.elements import not_
from sqlalchemy.sql.expression import false, insert, true

from sqlalchemy.sql.functions import count, random
sys.path.append('.')


from modelpool.models import AlarmSettingSensorIndividual,AlarmSettingGrowthIndividual,AlarmSettingDisorderIndividual,AlarmSettingPestIndividual
from modelpool.models import DeviceDb, DeviceDefectDb, FarmDialyDb,HouseDb, NoticeHistoryDb, NoticeUserHistoryDb, QuestionDb ,TechDb, db, Fcuser, Todo, EmployeesDb, NoticeDb, TestDb, FarmDb
from modelpool.models import AlarmHistoryDb,AlarmSettingSensor ,AlarmSettingGrowth ,AlarmSettingPest,AlarmSettingDisorder,DeviceOperationDb
from modelpool.models import SensorDataDb
from .formspool.forms import FarmBasicForm, RegisterForm, LoginForm , MainFrame
from .formspool.forms import NoticeForm

from api_v1 import api as api_v1
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import func,Column

from datetime import datetime , timedelta
import json
import random
import string
import ast
import pickle

mobile_control = Blueprint('mobile_control', __name__)
per_page_count = 5
per_page_count_4 = 4

###################################################################
## mobile
###################################################################
@mobile_control.route('/mobile_home', methods=['GET'])
def mobile_home():
    print("mobile_home")
    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

    temp_now = datetime.now()
    today_date_time = temp_now.strftime("%Y-%m-%d %H:%M:%S")
    print("today_date_time : " + str(today_date_time))
    today_date= temp_now.strftime("%Y-%m-%d")
    print("today_date : " + str(today_date))

    three_days_ago = datetime.today() - timedelta(days = 3)
    three_days_ago = three_days_ago.strftime("%Y-%m-%d %H:%M:%S")
    print("three_days_ago : " + str(three_days_ago))
   

    sensor_display=""
    sensor_alarm_cnt = 0
    sensor__alarm_list = []
    #############################################################################################
    ## 알람
    #############################################################################################
    urgent_sensor__alarm_list = db.session.query(AlarmHistoryDb).\
                                        filter(and_( AlarmHistoryDb.farm_owner_id == userid ,\
                                                    AlarmHistoryDb.type == "긴급", AlarmHistoryDb.check_yn == "미확인" )).all()

    print("str(len(urgent_sensor__alarm_list) : " + str(len(urgent_sensor__alarm_list)))
    print("urgent_sensor__alarm_list : " + str(urgent_sensor__alarm_list))


    normal_sensor__alarm_list = db.session.query(AlarmHistoryDb).\
                                        filter(and_( AlarmHistoryDb.farm_owner_id == userid ,\
                                                    AlarmHistoryDb.type == "일반", AlarmHistoryDb.check_yn == "미확인" )).all()

    print("str(len(normal_sensor__alarm_list) : " + str(len(normal_sensor__alarm_list)))
    print("normal_sensor__alarm_list : " + str(normal_sensor__alarm_list))

     
    if( len(urgent_sensor__alarm_list) >  0 ) :
            sensor_display ="주의"
            sensor__alarm_list = urgent_sensor__alarm_list
            sensor_alarm_cnt =  len(urgent_sensor__alarm_list)
    elif (len(normal_sensor__alarm_list) > 0) :
            sensor_display ="정상"
            sensor__alarm_list = normal_sensor__alarm_list
            sensor_alarm_cnt =  len(normal_sensor__alarm_list)
    else:
            sensor_display ="정상"
    

    print("normal_sensor__alarm_list : " + str(sensor__alarm_list))
    #############################################################################################
       
    

    current_date = datetime.today().strftime('%Y.%m.%d')
    return render_template('/mobile/main/mobile_main.html', userid=userid , current_date=current_date
                            ,sensor_display=sensor_display, sensor_alarm_cnt = sensor_alarm_cnt,sensor__alarm_list=sensor__alarm_list\
                            )


@mobile_control.route('/get_mobile_notice_history',methods=['GET','POST'])
def get_mobile_notice_history():
    print("do get_mobile_notice_history()!!!")

    notice_id= request.args.get('notice_id', default = '', type = str) 
    print("notice_id : " + str(notice_id))

    userid = session.get('userid', None)
    print("userid : " + str(userid))

    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

    try :
        

        notices= db.session.query(NoticeHistoryDb).filter( NoticeHistoryDb.id.contains(notice_id) ).first();

        print("notices : " + str(notices))
        noticeitem= {}
        noticeitem.update({"id" : notices.id})
        noticeitem.update({"urgent_yn" : notices.urgent_yn})
        noticeitem.update({"title" : notices.title})
        noticeitem.update({"content" : notices.content})
        

        print("noticelist : " + str(noticeitem))
        

        return jsonify({"result":noticeitem})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})

## 모바일홈, 공지사항 리스트
@mobile_control.route('/get_mobile_notice',methods=['GET','POST'])
def get_mobile_notice():
    print("do get_mobile_notice()!!!")

    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
        
    print("userid : " + str(userid))

    try :
        

        notices= db.session.query(NoticeUserHistoryDb , NoticeHistoryDb ).\
            filter(and_( NoticeUserHistoryDb.check_yn == "미확인" , NoticeUserHistoryDb.user_id.contains(userid) )).\
            join(NoticeUserHistoryDb,NoticeUserHistoryDb.notice_id == NoticeHistoryDb.id ).all()

        
        ##userinfos=  db.session.query(Fcuser, FarmDb).join(Fcuser, Fcuser.userid == FarmDb.user_id).filter(Fcuser.userid==userid).first();


        print("notices : " + str(notices))

        noticelist=[]
        for notice in notices:
            noticeitem= {}
            noticeitem.update({"id" : notice.NoticeUserHistoryDb.id})
            noticeitem.update({"notice_id" : notice.NoticeHistoryDb.id})
            noticeitem.update({"urgent_yn" : notice.NoticeHistoryDb.urgent_yn})
            noticeitem.update({"title" : notice.NoticeHistoryDb.title})
            noticeitem.update({"content" : notice.NoticeHistoryDb.content})
            noticelist.append(noticeitem)
        

        print("noticelist : " + str(noticelist))
        

        return jsonify({"result":noticelist})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@mobile_control.route('/save_checked_notice',methods=['GET','POST'])
def save_checked_notice():
    print("do save_checked_notice()!!!")

    notice_id= request.args.get('notice_id', default = '', type = str) 
    print("notice_id : " + str(notice_id))

    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
    print("userid : " + str(userid))

    try :
        

        upateQery = db.session.query(NoticeUserHistoryDb)
        upateQery = upateQery.filter(and_(NoticeUserHistoryDb.id==notice_id , NoticeUserHistoryDb.user_id==userid))
        upateItem= upateQery.one()
        upateItem.check_yn = "확인"
        #upateDeviceRecord.sensor_data = sensor_data_list
        #upateDeviceRecord.sensor_data = "[{'device_id':'000000001',field1':'2021.09.20','field2':'12:13','field3:'10' }]"
        #upateDeviceRecord.sensor_data = json.dumps(sensor_data_list, ensure_ascii=False) ## python object --> json string with unicode
        db.session.commit()
        

        return jsonify({"result":"ok"})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})



###################################################################
## 센서
###################################################################
@mobile_control.route('/mobile_sensor', methods=['GET'])
def mobile_sensor():
    print("mobile_sensor")
    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

       

    return render_template('/mobile/sensor/mobile_sensor.html', userid=userid )


###################################################################
## 모니터링
###################################################################
@mobile_control.route('/mobile_monitoring', methods=['GET'])
def mobile_monitoring():
    print("mobile_monitoring")
    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

    return render_template('/mobile/crop/mobile_monitoring.html', userid=userid )

@mobile_control.route('/mobile_monitoring_growth', methods=['GET'])
def mobile_monitoring_growth():
    print("mobile_monitoring_growth")
    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

    return render_template('/mobile/crop/mobile_monitoring_growth.html', userid=userid )


@mobile_control.route('/mobile_monitoring_pest', methods=['GET'])
def mobile_monitoring_pest():
    print("mobile_monitoring_pest")
    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

    return render_template('/mobile/crop/mobile_monitoring_pest.html', userid=userid )



@mobile_control.route('/mobile_monitoring_disorder', methods=['GET'])
def mobile_monitoring_disorder():
    print("mobile_monitoring_disorder")
    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"

    return render_template('/mobile/crop/mobile_monitoring_disorder.html', userid=userid )




###################################################################
## 생육 모니터링
###################################################################
@mobile_control.route('/get_mobile_alarm',methods=['GET','POST'])
def get_mobile_alarm():
    print("do get_mobile_alarm()!!!")

    userid = session.get('userid', None)
    print("userid : " + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
        
    print("userid : " + str(userid))

    try :
        

        alarms= db.session.query(AlarmHistoryDb).\
            filter(and_( AlarmHistoryDb.farm_owner_id.contains(userid) )).all()

        
        ##userinfos=  db.session.query(Fcuser, FarmDb).join(Fcuser, Fcuser.userid == FarmDb.user_id).filter(Fcuser.userid==userid).first();


        print("alarms : " + str(alarms))

        alarmslist=[]
        for alarm in alarms:
            alarmitem= {}
            alarmitem.update({"id" : alarm.id})
            
            temp_date_time = alarm.create_at.strftime("%Y-%m-%d %H:%M:%S")
            alarmitem.update({"create_at" : temp_date_time})

            alarmitem.update({"sensor_name" : alarm.sensor_name})
            alarmitem.update({"house_name" : alarm.house_name})
            temp_contents = alarm.alarm_contents[0:5] + "...";
            alarmitem.update({"alarm_contents" : temp_contents})
            alarmslist.append(alarmitem)
        

        print("alarmscelist : " + str(alarmslist))
        

        return jsonify({"result":alarmslist})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})



###################################################################
## 설정
###################################################################
@mobile_control.route('/mobile_setting', methods=['GET'])
def mobile_setting():
    print("mobile_setting")

    return render_template('/mobile/setting/mobile_setting.html')

@mobile_control.route('/mobile_basic_info', methods=['GET'])
def mobile_basic_info():
    print("mobile_basic_info")


    userid = session.get('userid',None) 
    print("userid :" + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
    

    try :
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        sessiondb = Session()

        userinfos=  db.session.query(Fcuser, FarmDb).join(Fcuser, Fcuser.userid == FarmDb.user_id)\
                                                    .filter(Fcuser.userid==userid).first();

        print("userinfos : " + str(userinfos))

        userid = userinfos.Fcuser.userid
        username = userinfos.Fcuser.username

        farm_name = userinfos.FarmDb.farm_name
        farm_crop = userinfos.FarmDb.farm_crop

        user_email = userinfos.FarmDb.user_email
        user_mobile = userinfos.FarmDb.user_mobile
        user_tel = userinfos.FarmDb.user_tel
        user_fax = userinfos.FarmDb.user_fax
        farm_address = userinfos.FarmDb.farm_address
        

        print("ere ")


        return render_template('/mobile/setting/mobile_basic_info.html' , userinfos = userinfos)

    except Exception as ex:
        print(ex)
        return jsonify({"data" : "no"})


@mobile_control.route('/mobile_password_change', methods=['GET'])
def mobile_password_change():
    print("mobile_password_change")


    userid = session.get('userid',None) 
    print("userid :" + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
    

    try :
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        sessiondb = Session()

        userinfos=  db.session.query(Fcuser).filter(Fcuser.userid==userid).first();

        print("userinfos : " + str(userinfos))

        userid = userinfos.userid
        username = userinfos.username
        password = userinfos.password
        
        return render_template('/mobile/setting/mobile_password_change.html' , userinfos = userinfos)

    except Exception as ex:
        print(ex)
        return jsonify({"data" : "no"})


@mobile_control.route('/change_password',methods=['GET','POST'])
def change_password():
    print("do change_password()")

    if request.method == 'POST': 
        try:

            print("====================================================================")
            ##{"userid":"None","input_date":"","checkbox_diary":true,"title_gwanju":""}
           
            ##{"userid":"admin","newpassword":"1"}

            userid =request.form['userid']
            print("userid : " + str(userid) )

            newpassword =request.form['newpassword']
            print("newpassword : " + str(newpassword) )

            print("====================================================================")

            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            upateQery = db.session.query(Fcuser)
            upateQery = upateQery.filter( Fcuser.userid==userid )
            upateItem = upateQery.one()

            upateItem.password = str(newpassword)
            db.session.commit()

            print("update sucess !")
            print("====================================================================")
       
            return jsonify({"result":"ok"})
            

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204


@mobile_control.route('/mobile_question', methods=['GET'])
def mobile_question():
    print("mobile_question")


    userid = session.get('userid',None) 
    print("userid :" + str(userid))
    if(  userid == "None" or  userid is None ):
        #return jsonify({"data" : "no"})
        userid= "admin"
    

    return render_template('/mobile/setting/mobile_question.html' , userid = userid)



@mobile_control.route('/save_question',methods=['GET','POST'])
def save_question():
    print("do save_question()")

    if request.method == 'POST': 
        try:

            print("====================================================================")
            ##{"userid":"None","input_date":"","checkbox_diary":true,"title_gwanju":""}
           
            ##{"userid":"admin","newpassword":"1"}

            userid =request.form['userid']
            print("userid : " + str(userid) )

            question_type =request.form['question_type']
            print("question_type : " + str(question_type) )
            question_title =request.form['question_title']
            print("question_title : " + str(question_title) )
            question_contents =request.form['question_contents']
            print("question_contents : " + str(question_type) )

            print("====================================================================")

            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            sessiondb = Session()

            print("====================================================================")            
            print("select user !")
            userinfos=  db.session.query(Fcuser, FarmDb).join(Fcuser, Fcuser.userid == FarmDb.user_id)\
                                                    .filter(Fcuser.userid==userid).first();
            print("userinfos : " + str(userinfos))

            userid = userinfos.Fcuser.userid
            phone_number = userinfos.Fcuser.phone_number
            farm_address = userinfos.FarmDb.farm_address

            print("====================================================================")
            print("try to update !")

            db.session.add(QuestionDb(      
                            from_type = "online",
                            type = str(question_type),
                            title = str(question_title),
                            content = str(question_contents),

                            user_id = str(userid),
                            user_phone = str(phone_number),
                            user_address = str(farm_address)
                        ))
            db.session.commit()

            print("update sucess !")
            print("====================================================================")
       
            return jsonify({"result":"ok"})
            

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204

###################################################################
## 생육정보
###################################################################
@mobile_control.route('/mobile_crop_monitoring', methods=['GET'])
def mobile_crop_monitoring():
    print("mobile_crop_monitoring")

    return render_template('/mobile/mobile_crop_monitoring.html')

###################################################################
## 영농일지
###################################################################
@mobile_control.route('/mobile_farm_diary/<int:page_num>', methods=['GET'])
def mobile_farm_diary(page_num):
    print("mobile_farm_diary")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    if(  userid == "None" or  userid is None ):
        ##return jsonify({"result":"user is not logined"})
        userid = "admin"
     
    print("userid :" + str(userid))
    diary_type= request.args.get("diary_type","")
    print("diary_type :" + str(diary_type))

    #########################################################################
    try:
      
        if ( not (diary_type == "" or diary_type is None or diary_type=="None" )):
            
            print("diary_type is not : " + str(diary_type))
            if(diary_type == "관주"):
                farm_diarys= FarmDialyDb.query.\
                                    add_columns(FarmDialyDb.id,\
                                                        FarmDialyDb.created_at ,\
                                                        func.ifnull(FarmDialyDb.user_id,'').label("user_id"),\
                                                        func.ifnull(FarmDialyDb.input_date,'').label("input_date"),\
                                                        func.ifnull(FarmDialyDb.image_path,'').label("image_path"),\
                                                        func.ifnull(FarmDialyDb.checkbox_gwanju,'').label("checkbox_gwanju"),\
                                                        func.ifnull(FarmDialyDb.title_gwanju,'').label("title_gwanju"),\
                                                        func.ifnull(FarmDialyDb.checkbox_banjae,'').label("checkbox_banjae"),\
                                                        func.ifnull(FarmDialyDb.title_banjae,'').label("title_banjae"),\
                                                        func.ifnull(FarmDialyDb.checkbox_goome,'').label("checkbox_goome"),\
                                                        func.ifnull(FarmDialyDb.title_goome,'').label("title_goome"),\
                                                        func.ifnull(FarmDialyDb.checkbox_panme,'').label("checkbox_panme"),\
                                                        func.ifnull(FarmDialyDb.title_panme,'').label("title_panme")).\
                                                        filter(  FarmDialyDb.checkbox_gwanju.contains("true") ).\
                                                        paginate(per_page=per_page_count_4, page=page_num, error_out=True)
            
            if(diary_type == "방제"):
                farm_diarys= FarmDialyDb.query.\
                                    add_columns(FarmDialyDb.id,\
                                                        FarmDialyDb.created_at ,\
                                                        func.ifnull(FarmDialyDb.user_id,'').label("user_id"),\
                                                        func.ifnull(FarmDialyDb.input_date,'').label("input_date"),\
                                                        func.ifnull(FarmDialyDb.image_path,'').label("image_path"),\
                                                        func.ifnull(FarmDialyDb.checkbox_gwanju,'').label("checkbox_gwanju"),\
                                                        func.ifnull(FarmDialyDb.title_gwanju,'').label("title_gwanju"),\
                                                        func.ifnull(FarmDialyDb.checkbox_banjae,'').label("checkbox_banjae"),\
                                                        func.ifnull(FarmDialyDb.title_banjae,'').label("title_banjae"),\
                                                        func.ifnull(FarmDialyDb.checkbox_goome,'').label("checkbox_goome"),\
                                                        func.ifnull(FarmDialyDb.title_goome,'').label("title_goome"),\
                                                        func.ifnull(FarmDialyDb.checkbox_panme,'').label("checkbox_panme"),\
                                                        func.ifnull(FarmDialyDb.title_panme,'').label("title_panme")).\
                                                        filter(  FarmDialyDb.checkbox_banjae.contains("true") ).\
                                                        paginate(per_page=per_page_count_4, page=page_num, error_out=True)
            
            if(diary_type == "구매"):
                farm_diarys= FarmDialyDb.query.\
                                    add_columns(FarmDialyDb.id,\
                                                        FarmDialyDb.created_at ,\
                                                        func.ifnull(FarmDialyDb.user_id,'').label("user_id"),\
                                                        func.ifnull(FarmDialyDb.input_date,'').label("input_date"),\
                                                        func.ifnull(FarmDialyDb.image_path,'').label("image_path"),\
                                                        func.ifnull(FarmDialyDb.checkbox_gwanju,'').label("checkbox_gwanju"),\
                                                        func.ifnull(FarmDialyDb.title_gwanju,'').label("title_gwanju"),\
                                                        func.ifnull(FarmDialyDb.checkbox_banjae,'').label("checkbox_banjae"),\
                                                        func.ifnull(FarmDialyDb.title_banjae,'').label("title_banjae"),\
                                                        func.ifnull(FarmDialyDb.checkbox_goome,'').label("checkbox_goome"),\
                                                        func.ifnull(FarmDialyDb.title_goome,'').label("title_goome"),\
                                                        func.ifnull(FarmDialyDb.checkbox_panme,'').label("checkbox_panme"),\
                                                        func.ifnull(FarmDialyDb.title_panme,'').label("title_panme")).\
                                                        filter(  FarmDialyDb.checkbox_goome.contains("true") ).\
                                                        paginate(per_page=per_page_count_4, page=page_num, error_out=True)
            
            if(diary_type == "판매"):
                farm_diarys= FarmDialyDb.query.\
                                    add_columns(FarmDialyDb.id,\
                                                        FarmDialyDb.created_at ,\
                                                        func.ifnull(FarmDialyDb.user_id,'').label("user_id"),\
                                                        func.ifnull(FarmDialyDb.input_date,'').label("input_date"),\
                                                        func.ifnull(FarmDialyDb.image_path,'').label("image_path"),\
                                                        func.ifnull(FarmDialyDb.checkbox_gwanju,'').label("checkbox_gwanju"),\
                                                        func.ifnull(FarmDialyDb.title_gwanju,'').label("title_gwanju"),\
                                                        func.ifnull(FarmDialyDb.checkbox_banjae,'').label("checkbox_banjae"),\
                                                        func.ifnull(FarmDialyDb.title_banjae,'').label("title_banjae"),\
                                                        func.ifnull(FarmDialyDb.checkbox_goome,'').label("checkbox_goome"),\
                                                        func.ifnull(FarmDialyDb.title_goome,'').label("title_goome"),\
                                                        func.ifnull(FarmDialyDb.checkbox_panme,'').label("checkbox_panme"),\
                                                        func.ifnull(FarmDialyDb.title_panme,'').label("title_panme")).\
                                                        filter(  FarmDialyDb.checkbox_panme.contains("true") ).\
                                                        paginate(per_page=per_page_count_4, page=page_num, error_out=True)
        else:
            farm_diarys= FarmDialyDb.query.\
                                add_columns(FarmDialyDb.id,\
                                                    FarmDialyDb.created_at ,\
                                                    func.ifnull(FarmDialyDb.user_id,'').label("user_id"),\
                                                    func.ifnull(FarmDialyDb.input_date,'').label("input_date"),\
                                                    func.ifnull(FarmDialyDb.image_path,'').label("image_path"),\
                                                    func.ifnull(FarmDialyDb.checkbox_gwanju,'').label("checkbox_gwanju"),\
                                                    func.ifnull(FarmDialyDb.title_gwanju,'').label("title_gwanju"),\
                                                    func.ifnull(FarmDialyDb.checkbox_banjae,'').label("checkbox_banjae"),\
                                                    func.ifnull(FarmDialyDb.title_banjae,'').label("title_banjae"),\
                                                    func.ifnull(FarmDialyDb.checkbox_goome,'').label("checkbox_goome"),\
                                                    func.ifnull(FarmDialyDb.title_goome,'').label("title_goome"),\
                                                    func.ifnull(FarmDialyDb.checkbox_panme,'').label("checkbox_panme"),\
                                                    func.ifnull(FarmDialyDb.title_panme,'').label("title_panme")).\
                                                    paginate(per_page=per_page_count_4, page=page_num, error_out=True)

        print(str(farm_diarys))
                    
        url ="mobile_control.mobile_farm_diary";
        return render_template('/mobile/diary/mobile_farm_diary.html', userid = userid, url=url,farm_diarys = farm_diarys)


    except Exception as ex:
        print(ex)
        return jsonify({"data" : "no"})
        

@mobile_control.route('/farm_diary')
def farm_diary():
    print("do farm_diary() !!!")

    user_id= request.args.get('userid', default = '', type = str) 
    print("user_id :"  + str(user_id))
    user_id = "admin"

    diary_type= request.args.get('diary_type', default = '', type = str) 
    print("diary_type :"  + str(diary_type))


    if(user_id == "None" or user_id =='' or user_id is None or user_id.isspace()):
        return jsonify({"data" : [{"image_path":"","input_date":"","type":""}]})


    try:
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        db_session = Session()


        if(diary_type == "gwanju"):
            farm_diarys = db_session.query(FarmDialyDb).filter(and_(FarmDialyDb.user_id == user_id,FarmDialyDb.checkbox_gwanju=="true")).all()
            db_session.commit()
        elif(diary_type == "banjae"):
            farm_diarys = db_session.query(FarmDialyDb).filter(and_(FarmDialyDb.user_id == user_id,FarmDialyDb.checkbox_banjae=="true")).all()
            db_session.commit()
        elif(diary_type == "goome"):
            farm_diarys = db_session.query(FarmDialyDb).filter(and_(FarmDialyDb.user_id == user_id,FarmDialyDb.checkbox_goome=="true")).all()
            db_session.commit()
        elif(diary_type == "panme"):
            farm_diarys = db_session.query(FarmDialyDb).filter(and_(FarmDialyDb.user_id == user_id,FarmDialyDb.checkbox_panme=="true")).all()
            db_session.commit()
        else:
            farm_diarys = db_session.query(FarmDialyDb).filter(and_(FarmDialyDb.user_id == user_id)).all()
            db_session.commit()
        

        print(farm_diarys)
        farm_diary_list=[]

        for farm_diary in farm_diarys :  
            farm_diary_json= {}
            farm_diary_json.update({"diary_id" : farm_diary.id})
            farm_diary_json.update({"created_at" : farm_diary.created_at})
            farm_diary_json.update({"user_id" : farm_diary.user_id})

            ##date_temp = farm_diary.input_date.strftime("%Y-%m-%d %H:%M:%S")
            farm_diary_json.update({"input_date" : farm_diary.input_date})
            farm_diary_json.update({"image_path" : "sample_crop.png"})


            farm_diary_json.update({"checkbox_gwanju" : farm_diary.checkbox_gwanju})
            farm_diary_json.update({"title_gwanju" : farm_diary.title_gwanju})

            farm_diary_json.update({"checkbox_banjae" : farm_diary.checkbox_banjae})
            farm_diary_json.update({"title_banjae" : farm_diary.title_banjae})


            farm_diary_json.update({"checkbox_goome" : farm_diary.checkbox_goome})
            farm_diary_json.update({"title_goome" : farm_diary.title_goome})

            farm_diary_json.update({"checkbox_panme" : farm_diary.checkbox_panme})
            farm_diary_json.update({"title_panme" : farm_diary.title_panme})

        
            type_val = ""
            if(farm_diary.checkbox_gwanju == "true"):
                type_val = "[관주]"
            if(farm_diary.checkbox_banjae == "true"):
                type_val = type_val + "[방제]"
            if(farm_diary.checkbox_goome == "true"):
                type_val =  type_val + "[구매]"
            if(farm_diary.checkbox_panme == "true"):
                type_val =  type_val + "[판매]"

            farm_diary_json.update({"type" : type_val})


            farm_diary_list.append(farm_diary_json)

        print(str(farm_diary_list))
                    
        return jsonify({"data" : farm_diary_list})



    except Exception as ex:
        print(ex)
        return jsonify({"data" : "no"})

    '''
    try:
        farm_diary_json= {}
        farm_diary_list=[]
        temp_cnt = 0
        while ( temp_cnt < 11 ):
            temp_cnt = temp_cnt + 1
            farm_diary_json= {}
            farm_diary_json.update({"image" : "sample_crop.png"})
            farm_diary_json.update({"date" : "2021.04.21"})
            farm_diary_json.update({"activity" : temp_cnt})
            farm_diary_list.append(farm_diary_json)
            print("farm_diary_json : "  + str(farm_diary_json))       

        print("Final farm_diary_json : "  + str(farm_diary_json))    

        return jsonify({"data" : farm_diary_list})


    except Exception as ex:
        print(ex)
        return jsonify({"no"}) 
    '''

@mobile_control.route('/farm_diary_save',methods=['GET','POST'])
def farm_diary_save():
    print("do farm_diary_save()")

    if request.method == 'POST': 
        try:

            print("====================================================================")

            ##{"userid":"None","input_date":"","checkbox_diary":true,"title_gwanju":""}

            userid = session.get('userid', None)
            print("userid : " + str(userid))
            if(  userid == "None" or  userid is None ):
                #return jsonify({"data" : "no"})
                userid= "admin"
            print("userid : " + str(userid) )
        

            diary_id =request.form['diary_id']
            print("diary_id : " + str(diary_id) )
            if(diary_id is None):
                print("diary_id is None")
            if(diary_id == ""):
                print("diary_id  == ''")


            image_path =request.form['image_path']
            print("image_path : " + str(image_path) )


            input_date =request.form['input_date']
            print("input_date : " + str(input_date) )
            checkbox_gwanju =request.form['checkbox_gwanju']
            print("checkbox_gwanju : " + str(checkbox_gwanju) )
            title_gwanju =request.form['title_gwanju']
            print("title_gwanju : " + str(title_gwanju) )
            
            checkbox_banjae =request.form['checkbox_banjae']
            print("checkbox_banjae : " + str(checkbox_banjae) )
            title_banjae =request.form['title_banjae']
            print("title_banjae : " + str(title_banjae) )

            checkbox_goome =request.form['checkbox_goome']
            print("checkbox_goome : " + str(checkbox_goome) )
            title_goome =request.form['title_goome']
            print("title_goome : " + str(title_goome) )

            checkbox_panme =request.form['checkbox_panme']
            print("checkbox_panme : " + str(checkbox_panme) )
            title_panme =request.form['title_panme']
            print("title_panme : " + str(title_panme) )

            print("====================================================================")



            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            sessionDb = Session()

            temp_count= sessionDb.query(func.count(FarmDialyDb.id)).\
                                    filter(FarmDialyDb.id==str(diary_id)).scalar()
            print("temp_count : " + str(temp_count))
            sessionDb.commit()
           

            if ((diary_id == "" or diary_id is None or diary_id=="None")  \
                 or not (temp_count > 0) ) :

                sessionDb.add(FarmDialyDb(
                            user_id = str(userid),
                            
                            input_date =  str(input_date) ,
                            
                            
                            image_path = str(image_path),

                            checkbox_gwanju = str(checkbox_gwanju),
                            title_gwanju = str(title_gwanju),

                            checkbox_banjae = str(checkbox_banjae),
                            title_banjae = str(title_banjae),

                            checkbox_goome = str(checkbox_goome),
                            title_goome = str(title_goome),

                            checkbox_panme = str(checkbox_panme),
                            title_panme = str(title_panme)

    
                        ))
                sessionDb.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("udapte try ")

                upateQery = sessionDb.query(FarmDialyDb)
                upateQery = upateQery.filter( FarmDialyDb.id==diary_id )
                upateItem = upateQery.one()

                upateItem.input_date = str(input_date)
                upateItem.image_path = str(image_path)

                upateItem.checkbox_gwanju = str(checkbox_gwanju)
                upateItem.title_gwanju = str(title_gwanju)

                upateItem.checkbox_banjae = str(checkbox_banjae)
                upateItem.title_banjae = str(title_banjae)

                upateItem.checkbox_goome = str(checkbox_goome)
                upateItem.title_goome = str(title_goome)

                upateItem.checkbox_panme = str(checkbox_panme)
                upateItem.title_panme = str(title_panme)

                sessionDb.commit()
    
                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})
            

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204


###################################################################
## 농장정보 공지사항
###################################################################
@mobile_control.route('/mobile_notice_list/<int:page_num>')
def mobile_notice_list(page_num):
    print("mobile_notice_list")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
        search_keyword= request.args.get("search_keyword","")
        search_cloumn= request.args.get("search_cloumn","")
        print("reqeust search_keyword:" + search_keyword)
        print("reqeust search_cloumn:" + search_cloumn)

        try :
            if( search_cloumn and not not(search_cloumn.isspace()) ):
                if search_cloumn == "urgent_yn" :
                    print("here2")
                    notices= NoticeHistoryDb.query.\
                                        add_columns(NoticeHistoryDb.id,\
                                                            func.ifnull(NoticeHistoryDb.date_to_send,'').label("date_to_send") ,\
                                                            func.ifnull(NoticeHistoryDb.urgent_yn,'').label("urgent_yn"),\
                                                            func.ifnull(NoticeHistoryDb.title,'').label("title"),\
                                                            func.ifnull(NoticeHistoryDb.content,'').label("content"),\
                                                            func.ifnull(NoticeHistoryDb.receive_person_list_display,'').label("receive_person_list_display")).\
                                                            filter(  NoticeHistoryDb.urgent_yn.contains(search_keyword) ).\
                                                            paginate(per_page=per_page_count, page=page_num, error_out=True)
            else :
                print("here2")
                if(search_keyword is None) :         
                    notices= NoticeHistoryDb.query.\
                                        add_columns(NoticeHistoryDb.id,\
                                                            func.ifnull(NoticeHistoryDb.date_to_send,'').label("date_to_send") ,\
                                                            func.ifnull(NoticeHistoryDb.urgent_yn,'').label("urgent_yn"),\
                                                            func.ifnull(NoticeHistoryDb.title,'').label("title"),\
                                                            func.ifnull(NoticeHistoryDb.content,'').label("content"),\
                                                            func.ifnull(NoticeHistoryDb.receive_person_list_display,'').label("receive_person_list_display"),\
                                                        
                                                            ).paginate(per_page=per_page_count, page=page_num, error_out=True)
                else:
                    print("here3")
                    notices=  NoticeHistoryDb.query.\
                                        add_columns(NoticeHistoryDb.id,\
                                                            func.ifnull(NoticeHistoryDb.date_to_send,'').label("date_to_send") ,\
                                                            func.ifnull(NoticeHistoryDb.urgent_yn,'').label("urgent_yn"),\
                                                            func.ifnull(NoticeHistoryDb.title,'').label("title"),\
                                                            func.ifnull(NoticeHistoryDb.content,'').label("content"),\
                                                            func.ifnull(NoticeHistoryDb.receive_person_list_display,'').label("receive_person_list_display"),\
                                                            ).\
                                        filter( or_(\
                                                    NoticeHistoryDb.date_to_send.contains(search_keyword) , NoticeHistoryDb.urgent_yn.contains(search_keyword) ,
                                                    NoticeHistoryDb.title.contains(search_keyword) , NoticeHistoryDb.content.contains(search_keyword) ,
                                                    NoticeHistoryDb.receive_person_list.contains(search_keyword)  
                                                    )).paginate(per_page=per_page_count, page=page_num, error_out=True)

            print(notices)

        
        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

        url ="mobile_control.mobile_notice_list";
        return render_template('/mobile/farm_info/mobile_notice_list.html', notices=notices, url=url,search_keyword=search_keyword ,search_cloumn=search_cloumn )


    else:

        return jsonify({"result":"user is not logined"})

@mobile_control.route('/mobile_notice_detail', methods=['GET'])
def mobile_notice_detail():
    print("mobile_notice_detail")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))
    
    id= request.args.get("id","")

    ##if( not ( userid == "None" or  userid is None ) ):
    if( true ):

        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        dbsession = Session()

        notices= db.session.query(NoticeHistoryDb).filter(NoticeHistoryDb.id == id).all()
        dbsession.commit()

        for notice in notices:
            date_to_send = notice.date_to_send
            urgent_yn = notice.urgent_yn
            title = notice.title
            content = notice.content
            print(content)

        return render_template('/mobile/farm_info/mobile_notice_detail.html',userid = userid, \
            date_to_send=date_to_send,\
            urgent_yn=urgent_yn,\
            title=title,\
            content=content)
    else:
        return jsonify({"result":"user is not logined"})
    



## 농장정보 알람
###################################################################
@mobile_control.route('/mobile_alarm_list/<int:page_num>')
def mobile_alarm_list(page_num):
    print("mobile_alarm_list")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
        search_keyword= request.args.get("search_keyword","")
        search_cloumn= request.args.get("search_cloumn","")
        print("reqeust search_keyword:" + search_keyword)
        print("reqeust search_cloumn:" + search_cloumn)

        try :
         
            if (search_cloumn=='all'):
                alarms = AlarmHistoryDb.query.\
                                                add_columns(
                                                            func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                                            func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                                            func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                                            func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                                            func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                                            func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                                            func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                                            func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                                            func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                                            func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                                            func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                                            func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                                            func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                                            func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                                            func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                                            filter( \
                                                                or_(AlarmHistoryDb.device_id.contains(search_keyword), 
                                                                    AlarmHistoryDb.type.contains(search_keyword),
                                                                    AlarmHistoryDb.category.contains(search_keyword),
                                                                    AlarmHistoryDb.sensor_item.contains(search_keyword),
                                                                    AlarmHistoryDb.alarm_contents.contains(search_keyword),
                                                                    AlarmHistoryDb.farm_owner_id.contains(search_keyword),
                                                                    AlarmHistoryDb.farm_owner_name.contains(search_keyword),
                                                                    AlarmHistoryDb.house_name.contains(search_keyword),
                                                                    AlarmHistoryDb.crop.contains(search_keyword),
                                                                    AlarmHistoryDb.kind.contains(search_keyword),
                                                                    AlarmHistoryDb.house_name.contains(search_keyword),
                                                                    AlarmHistoryDb.check_yn.contains(search_keyword)
                                                            
                                                            )).\
                                                            paginate(per_page=per_page_count, page=page_num, error_out=True)
                                                            ##'%Y-%m-%d %H:%M:%S'

            elif (search_cloumn=='3'):
                alarms = AlarmHistoryDb.query.\
                                                add_columns(
                                                            func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                                            func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                                            func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                                            func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                                            func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                                            func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                                            func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                                            func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                                            func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                                            func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                                            func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                                            func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                                            func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                                            func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                                            func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                                            filter( and_(AlarmHistoryDb.create_at < three_days_ago ,AlarmHistoryDb.check_yn =='미확인' )).\
                                                            paginate(per_page=per_page_count, page=page_num, error_out=True)
                                                            ##'%Y-%m-%d %H:%M:%S'
            
            elif (search_cloumn=='today'):
                alarms = AlarmHistoryDb.query.\
                                                add_columns(
                                                            func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                                            func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                                            func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                                            func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                                            func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                                            func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                                            func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                                            func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                                            func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                                            func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                                            func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                                            func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                                            func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                                            func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                                            func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                                            filter( AlarmHistoryDb.create_at >= todays_datetime ).\
                                                            paginate(per_page=per_page_count, page=page_num, error_out=True)
                                                            ##'%Y-%m-%d %H:%M:%S'

            elif (search_cloumn=='type'):
                     
                if len(search_keyword) != 0 : 
                    ##긴급/일반
                    print("search_cloumn=='type'")
                    alarms = AlarmHistoryDb.query.\
                                add_columns(AlarmHistoryDb.id,\
                                            func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                            func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                            func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                            func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                            func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                            func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                            func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                            func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                            func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                            func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                            func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                            func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                            func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                            func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                            func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter(AlarmHistoryDb.type.contains(search_keyword)).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)

            elif (search_cloumn=='check_yn'):
                if len(search_keyword) != 0 : 

                    print("search_cloumn=='check_yn'")
                    ##확인 / 미확인
                    alarms = AlarmHistoryDb.query.\
                                add_columns(AlarmHistoryDb.id,\
                                            func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                            func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                            func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                            func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                            func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                            func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                            func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                            func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                            func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                            func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                            func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                            func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                            func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                            func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                            func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                        filter(AlarmHistoryDb.check_yn.contains(search_keyword)).\
                                        paginate(per_page=per_page_count, page=page_num, error_out=True)
            else: # all
                    
                print("all")
                alarms = AlarmHistoryDb.query.\
                            add_columns(AlarmHistoryDb.id,\
                                        func.ifnull(AlarmHistoryDb.create_at,'').label("create_at") ,\
                                        func.ifnull(AlarmHistoryDb.device_id,'').label("user_id"),\
                                        func.ifnull(AlarmHistoryDb.type,'').label("type"),\
                                        func.ifnull(AlarmHistoryDb.category,'').label("category"),\
                                        func.ifnull(AlarmHistoryDb.sensor_name,'').label("sensor_name"),\
                                        func.ifnull(AlarmHistoryDb.sensor_item,'').label("sensor_item"),\
                                        func.ifnull(AlarmHistoryDb.alarm_contents,'').label("alarm_contents"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_id,'').label("farm_owner_id"),\
                                        func.ifnull(AlarmHistoryDb.farm_owner_name,'').label("farm_owner_name"),\
                                        func.ifnull(AlarmHistoryDb.house_id,'').label("house_id"),\
                                        func.ifnull(AlarmHistoryDb.house_name,'').label("house_name"),\
                                        func.ifnull(AlarmHistoryDb.crop,'').label("crop"),\
                                        func.ifnull(AlarmHistoryDb.kind,'').label("kind"),\
                                        func.ifnull(AlarmHistoryDb.check_yn,'').label("check_yn"),\
                                        func.ifnull(AlarmHistoryDb.alarm_none_check_cnt,'').label("alarm_none_check_cnt")).\
                                    paginate(per_page=per_page_count, page=page_num, error_out=True)
                
            
            print(alarms.items)

            url ="mobile_control.mobile_alarm_list";

            return render_template('/mobile/farm_info/mobile_alarm_list.html',userid = userid, \
                alarms=alarms,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword,page_num=page_num)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})



@mobile_control.route('/mobile_alarm_detail', methods=['GET'])
def mobile_alarm_detail():
    print("mobile_alarm_detail")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))
    
    id= request.args.get("id","")

    ##if( not ( userid == "None" or  userid is None ) ):
    if( true ):

        alarms= db.session.query(AlarmHistoryDb).filter(AlarmHistoryDb.id == id).all()

        for alarm in alarms:
            alarm_contents = alarm.alarm_contents
            create_at = alarm.create_at
            sensor_name = alarm.sensor_name
            sensor_item = alarm.sensor_item
            device_id = alarm.device_id
            house_name = alarm.house_name
            type = alarm.type
            category = alarm.category

        upateQery = db.session.query(AlarmHistoryDb)
        upateQery = upateQery.filter(AlarmHistoryDb.id==id)
        upateDeviceRecord = upateQery.one()
        upateDeviceRecord.check_yn = "확인"
        db.session.commit()
        db.session.close()
            

        print(str(type))
        return render_template('/mobile/farm_info/mobile_alarm_detail.html',userid = userid, \
            alarm_contents=alarm_contents,\
            create_at=create_at,\
            sensor_name=sensor_name,\
            sensor_item=sensor_item,\
            device_id=device_id,\
            house_name=house_name,\
            type=type, category = category)
    else:
        return jsonify({"result":"user is not logined"})


###################################################################
## 농장정보 디바이스 리스트
###################################################################
@mobile_control.route('/mobile_device_list')
def mobile_device_list():
    print("mobile_device_list")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
        search_keyword= request.args.get("search_keyword","")
        search_cloumn= request.args.get("search_cloumn","")
        print("reqeust search_keyword:" + search_keyword)
        print("reqeust search_cloumn:" + search_cloumn)

        try :
            '''
            devices = DeviceDb.query.outerjoin(DeviceDb,DeviceDb.device_id==DeviceDefectDb.device_id).\
                        add_columns(func.ifnull(DeviceDb.device_id,'').label("device_id"),\
                                    func.ifnull(DeviceDb.type,'').label("type") ,\
                                    func.ifnull(DeviceDb.house_name,'').label("house_name"),\
                                    func.ifnull(DeviceDb.house_id,'').label("house_id"),\
                                    func.ifnull(DeviceDefectDb.id,'').label("defect_id"),\
                                    func.ifnull(DeviceDefectDb.fix_yn,'').label("fix_yn")).\
                                paginate(per_page=per_page_count, page=page_num, error_out=True)
            '''
                
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            print(dburl)
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            sessiondb = Session()

            devices= db.session.query(DeviceDb, HouseDb).\
                            join(DeviceDb , DeviceDb.house_id == HouseDb.house_id).all()

            
            print(str(devices))



            url ="mobile_control.mobile_device_list";

            return render_template('/mobile/farm_info/mobile_device_list.html',userid = userid, \
                devices=devices,url=url,search_cloumn=search_cloumn,search_keyword=search_keyword)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})

@mobile_control.route('/mobile_device_detail')
def mobile_device_detail():
    print("do mobile_device_detail()")

    device_id= request.args.get("device_id","")
    print("reqeust device_id:" + device_id)

    basedir = os.path.abspath(os.path.dirname(__file__))
    dbfile = os.path.join(basedir, 'db.sqlite')
    dburl= 'sqlite:///' + dbfile
    print(dburl)
    engine = create_engine(dburl)
    Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    sessiondb = Session()

    device= db.session.query(DeviceDb).\
                    filter(DeviceDb.device_id == device_id).first()

    print(str(device))
    print(type(device))

    created_at = device.created_at
    house_name = device.house_name
    type_ = device.type


    try:
        return render_template('/mobile/farm_info/mobile_device_detail.html', device_id=device_id\
            , created_at=created_at, type=type_, house_name=house_name
            )

    except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})



@mobile_control.route('/device_individual_operation_loading')
def device_individual_operation_loading():
    print("do device_individual_operation_loading()")

    device_id= request.args.get("device_id","")
    print("reqeust device_id:" + device_id)

    try:
       
        basedir = os.path.abspath(os.path.dirname(__file__))
        dbfile = os.path.join(basedir, 'db.sqlite')
        dburl= 'sqlite:///' + dbfile
        print(dburl)
        engine = create_engine(dburl)
        Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        sessiondb = Session()

        devices= db.session.query(DeviceDb).\
                        filter(DeviceDb.device_id == device_id).all()

        ##print(str(devices))
        ##print(type(devices))

        for device in devices:
            operation = device.operation
            ##print("operation : " + str(operation))
            
        print("str(operation) : " + str(operation))
        if( operation is None):
            return jsonify({"result":"no"})
        
        operation_json = json.loads(str(operation))
        ##print("operation_json : " + str(operation_json))

        return jsonify({"result":operation_json})


    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@mobile_control.route('/device_save_individual_operation',methods=['GET','POST'])
def device_save_individual_operation():
    print("do device_save_individual_operation()")

    if request.method == 'POST': 
        try:
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()
            
            if(request.data):
                jdata = request.get_json()
                device_id = jdata['device_id']
                print( "device_id : "  + device_id )
                print( "operation_setting : "  + str(jdata['operation_setting']) )
 

                upateQery = db.session.query(DeviceDb)
                upateQery = upateQery.filter(and_(DeviceDb.device_id == device_id ))
                upateDeviceRecord = upateQery.one()
                upateDeviceRecord.operation = json.dumps(jdata['operation_setting'])
                print("========================Update===============================")

                '''
                for j in jdata['operation_setting']:
    
                    print( "udpate : "  + str(j['time']) ) 
                    
                    upateQery = db.session.query(DeviceDb)
                    upateQery = upateQery.filter(and_(DeviceDb.type == device_id ))
                    upateDeviceRecord = upateQery.one()
                    upateDeviceRecord.checked = j['checked']

                    print( "udpate : "  + str(j['time']) ) 
                '''
            else:
                print( "nothig !") 
                return jsonify({"result":"no"})

            db.session.commit()
            print("Updae sucess !")
            print("====================================================================")
            return jsonify({"result":"ok"})

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204


###################################################################
## 농장정보 알람설정 센서
###################################################################
@mobile_control.route('/mobile_alarm_setting_sensor')
def mobile_alarm_setting_sensor():
    print("mobile_alarm_setting_sensor")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    userid = "admin"

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
            url ="mobile_control.mobile_alarm_setting_sensor";

            return render_template('/mobile/farm_info/mobile_alarm_setting_sensor.html',userid = userid)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})

@mobile_control.route('/alarm_loading_sensor_data_individual')
def alarm_loading_sensor_data_individual():
    print("do alarm_loading_sensor_data_individual()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id : " + device_id)
    
    try:
        
        alarm_setting_info= db.session.query(AlarmSettingSensorIndividual).filter(\
            and_( AlarmSettingSensorIndividual.device_id==device_id)).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:


            alarm_json.update({"alarm_id":alarm_setting.id})
            alarm_json.update({"alarm_type":alarm_setting.alarm_type})

            alarm_json.update({"indoor_temp_min":alarm_setting.indoor_temp_min})

            alarm_json.update({"indoor_temp_max":alarm_setting.indoor_temp_max})
            alarm_json.update({"indoor_temp_alarm_issu_type":alarm_setting.indoor_temp_alarm_issu_type})

            alarm_json.update({"indoor_humidity_min":alarm_setting.indoor_humidity_min})
            alarm_json.update({"indoor_humidity_max":alarm_setting.indoor_humidity_max}) 
            alarm_json.update({"indoor_humidity_alarm_issu_type":alarm_setting.indoor_humidity_alarm_issu_type}) 

            alarm_json.update({"indoor_co2_min":alarm_setting.indoor_co2_min}) 
            alarm_json.update({"indoor_co2_max":alarm_setting.indoor_co2_max})
            alarm_json.update({"indoor_co2_alarm_issu_type":alarm_setting.indoor_co2_alarm_issu_type}) 

            alarm_json.update({"indoor_sunny_min":alarm_setting.indoor_sunny_min}) 
            alarm_json.update({"indoor_sunny_max":alarm_setting.indoor_sunny_max}) 
            alarm_json.update({"indoor_sunny_alarm_issu_type":alarm_setting.indoor_sunny_alarm_issu_type}) 

            alarm_json.update({"basic_temp_min":alarm_setting.basic_temp_min})
            alarm_json.update({"basic_temp_max":alarm_setting.basic_temp_max})
            alarm_json.update({"basic_temp_alarm_issu_type":alarm_setting.basic_temp_alarm_issu_type}) 

            print("basic_temp_alarm_issu_type : " + str(alarm_setting.basic_temp_alarm_issu_type))

            alarm_json.update({"basic_humidity_min":alarm_setting.basic_humidity_min}) 
            alarm_json.update({"basic_humidity_max":alarm_setting.basic_humidity_max}) 
            alarm_json.update({"basic_humidity_alarm_issu_type":alarm_setting.basic_humidity_alarm_issu_type}) 

            alarm_json.update({"basic_ec_min":alarm_setting.basic_ec_min}) 
            alarm_json.update({"basic_ec_max":alarm_setting.basic_ec_max}) 
            alarm_json.update({"basic_ec_alarm_issu_type":alarm_setting.basic_ec_alarm_issu_type}) 
        


        print("alarm_json : " + str(alarm_json))
        print("alarm_json : " + str(len(alarm_json)))

        if(len(alarm_json) > 0):
             root_jon.update({"result":alarm_json})
             return jsonify(root_jon)
        else :
            return jsonify({"result":"no"})



    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@mobile_control.route('/alarm_save_setting_sensor_individual',methods=['GET','POST'])
def alarm_save_setting_sensor():
    print("do alarm_save_setting_sensor()!!!")

    if request.method == 'POST': 
        try:
            device_id = request.form['device_id']
            print("device_id  : "  + str(device_id))

            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))


            indoor_temp_min = request.form['indoor_temp_min']
            #indoor_temp_min = request.form.get('indoor_temp_min')
            print(indoor_temp_min)
            indoor_temp_max = request.form['indoor_temp_max']
            print(indoor_temp_max)
            indoor_temp_alarm_issu_type = request.form['indoor_temp_alarm_issu_type']
            print(indoor_temp_alarm_issu_type)

            indoor_humidity_min = request.form['indoor_humidity_min']
            print(indoor_humidity_min)
            indoor_humidity_max = request.form['indoor_humidity_max']
            print(indoor_humidity_max)
            print("111")
            indoor_humidity_alarm_issu_type = request.form['indoor_humidity_alarm_issu_type']
            print(indoor_humidity_alarm_issu_type)
            print("33")

            indoor_co2_min = request.form['indoor_co2_min']
            indoor_co2_max = request.form['indoor_co2_max']
            indoor_co2_alarm_issu_type = request.form['indoor_co2_alarm_issu_type']

            print("34")
            indoor_sunny_min = request.form['indoor_sunny_min']
            print("4")
            indoor_sunny_max = request.form['indoor_sunny_max']
            print("4")
            indoor_sunny_alarm_issu_type = request.form['indoor_sunny_alarm_issu_type']

            print("4")
            basic_temp_min = request.form['basic_temp_min']
            print("4")
            basic_temp_max = request.form['basic_temp_max']
            basic_temp_alarm_issu_type = request.form['basic_temp_alarm_issu_type']
            print("basic_temp_alarm_issu_type : " + str(basic_temp_alarm_issu_type))

            print("4")
            basic_humidity_min = request.form['basic_humidity_min']
            basic_humidity_max = request.form['basic_humidity_max']
            basic_humidity_alarm_issu_type = request.form['basic_humidity_alarm_issu_type']

            print("3")
            basic_ec_min = request.form['basic_ec_min']
            basic_ec_max = request.form['basic_ec_max']
            basic_ec_alarm_issu_type = request.form['basic_ec_alarm_issu_type']
           
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()


            if ( bool(db.session.query(AlarmSettingSensorIndividual).\
                    filter(and_(AlarmSettingSensorIndividual.device_id==str(device_id))).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingSensorIndividual(


                            device_id = str(device_id),

                            indoor_temp_min = str(indoor_temp_min),
                            indoor_temp_max = str(indoor_temp_max),
                            indoor_temp_alarm_issu_type = str(indoor_temp_alarm_issu_type),

                            indoor_humidity_min = str(indoor_humidity_min),
                            indoor_humidity_max = str(indoor_humidity_max),
                            indoor_humidity_alarm_issu_type = str(indoor_humidity_alarm_issu_type),

                            indoor_co2_min = str(indoor_co2_min),
                            indoor_co2_max = str(indoor_co2_max),
                            indoor_co2_alarm_issu_type = str(indoor_co2_alarm_issu_type),

                            indoor_sunny_min = str(indoor_sunny_min),
                            indoor_sunny_max = str(indoor_sunny_max),
                            indoor_sunny_alarm_issu_type = str(indoor_sunny_alarm_issu_type),

                            basic_temp_min = str(basic_temp_min),
                            basic_temp_max = str(basic_temp_max),
                            basic_temp_alarm_issu_type = str(basic_temp_alarm_issu_type),

                            basic_humidity_min = str(basic_humidity_min),
                            basic_humidity_max = str(basic_humidity_max),
                            basic_humidity_alarm_issu_type = str(basic_humidity_alarm_issu_type),

                            basic_ec_min = str(basic_ec_min),
                            basic_ec_max = str(basic_ec_max),
                            basic_ec_alarm_issu_type = str(basic_ec_alarm_issu_type)
                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingSensorIndividual)
                upateQery = upateQery.filter( and_(AlarmSettingSensorIndividual.device_id==str(device_id)))
                upateAlarm = upateQery.one()


                upateAlarm.indoor_temp_min = str(indoor_temp_min)
                upateAlarm.indoor_temp_max = str(indoor_temp_max)
                upateAlarm.indoor_temp_alarm_issu_type = str(indoor_temp_alarm_issu_type)
                print("indoor_temp_alarm_issu_type : " + indoor_temp_alarm_issu_type)

                upateAlarm.indoor_humidity_min = str(indoor_humidity_min)
                upateAlarm.indoor_humidity_max = str(indoor_humidity_max)
                upateAlarm.indoor_humidity_alarm_issu_type = str(indoor_humidity_alarm_issu_type)

                upateAlarm.indoor_co2_min = str(indoor_co2_min)
                upateAlarm.indoor_co2_max = str(indoor_co2_max)
                upateAlarm.indoor_co2_alarm_issu_type = str(indoor_co2_alarm_issu_type)

                upateAlarm.indoor_sunny_min = str(indoor_sunny_min)
                upateAlarm.indoor_sunny_max = str(indoor_sunny_max)
                upateAlarm.indoor_sunny_alarm_issu_type = str(indoor_sunny_alarm_issu_type)

                upateAlarm.basic_temp_min = str(basic_temp_min)
                upateAlarm.basic_temp_max = str(basic_temp_max)
                upateAlarm.basic_temp_alarm_issu_type = str(basic_temp_alarm_issu_type)
                upateAlarm.basic_humidity_min = str(basic_humidity_min)
                upateAlarm.basic_humidity_max = str(basic_humidity_max)
                upateAlarm.basic_humidity_alarm_issu_type = str(basic_humidity_alarm_issu_type)

                upateAlarm.basic_ec_min = str(basic_ec_min)
                upateAlarm.basic_ec_max = str(basic_ec_max)
                upateAlarm.basic_ec_alarm_issu_type = str(basic_ec_alarm_issu_type)

                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204



###################################################################
## 농장정보 알람설정 생육
###################################################################
@mobile_control.route('/mobile_alarm_setting_growth')
def mobile_alarm_setting_growth():
    print("mobile_alarm_setting_growth")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    userid = "admin"

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
            url ="mobile_control.mobile_alarm_setting_growth";

            return render_template('/mobile/farm_info/mobile_alarm_setting_growth.html',userid = userid)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})

@mobile_control.route('/alarm_loading_growth_data_individual')
def alarm_loading_growth_data_individual():
    print("do alarm_loading_growth_data_individual()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id : " + device_id)
    
    try:
        
        alarm_setting_info= db.session.query(AlarmSettingGrowthIndividual).filter(\
            and_( AlarmSettingGrowthIndividual.device_id==device_id)).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:


            alarm_json.update({"alarm_id":alarm_setting.id})
            alarm_json.update({"alarm_type":alarm_setting.alarm_type})

            alarm_json.update({"leaf_cnt_min":alarm_setting.leaf_cnt_min})
            alarm_json.update({"leaf_cnt_max":alarm_setting.leaf_cnt_max})
            alarm_json.update({"leaf_cnt_alarm_issu_type":alarm_setting.leaf_cnt_alarm_issu_type})

            alarm_json.update({"in_leaf_cnt_min":alarm_setting.in_leaf_cnt_min})
            alarm_json.update({"in_leaf_cnt_max":alarm_setting.in_leaf_cnt_max}) 
            alarm_json.update({"in_leaf_cnt_alarm_issu_type":alarm_setting.in_leaf_cnt_alarm_issu_type}) 

            alarm_json.update({"head_cnt_min":alarm_setting.head_cnt_min}) 
            alarm_json.update({"head_cnt_max":alarm_setting.head_cnt_max})
            alarm_json.update({"head_cnt_alarm_issu_type":alarm_setting.head_cnt_alarm_issu_type}) 

            alarm_json.update({"ilack_cnt_min":alarm_setting.ilack_cnt_min}) 
            alarm_json.update({"ilack_cnt_max":alarm_setting.ilack_cnt_max}) 
            alarm_json.update({"ilack_cnt_alarm_issu_type":alarm_setting.ilack_cnt_alarm_issu_type}) 

            alarm_json.update({"flower_cnt_min":alarm_setting.flower_cnt_min})
            alarm_json.update({"flower_cnt_max":alarm_setting.flower_cnt_max})
            alarm_json.update({"flower_cnt_alarm_issu_type":alarm_setting.flower_cnt_alarm_issu_type}) 

            alarm_json.update({"fruit_cnt_min":alarm_setting.fruit_cnt_min}) 
            alarm_json.update({"fruit_cnt_max":alarm_setting.fruit_cnt_max}) 
            alarm_json.update({"fruit_cnt_alarm_issu_type":alarm_setting.fruit_cnt_alarm_issu_type}) 

            alarm_json.update({"fruit_size_min":alarm_setting.fruit_size_min}) 
            alarm_json.update({"fruit_size_max":alarm_setting.fruit_size_max}) 
            alarm_json.update({"fruit_size_alarm_issu_type":alarm_setting.fruit_size_alarm_issu_type}) 

            alarm_json.update({"in_leaf_length_min":alarm_setting.in_leaf_length_min}) 
            alarm_json.update({"in_leaf_length_max":alarm_setting.in_leaf_length_max}) 
            alarm_json.update({"in_leaf_length_alarm_issu_type":alarm_setting.in_leaf_length_alarm_issu_type}) 

            alarm_json.update({"in_leaf_size_min":alarm_setting.in_leaf_size_min}) 
            alarm_json.update({"in_leaf_size_max":alarm_setting.in_leaf_size_max}) 
            alarm_json.update({"in_leaf_size_alarm_issu_type":alarm_setting.in_leaf_size_alarm_issu_type}) 



        print("alarm_json : " + str(alarm_json))
        print("alarm_json : " + str(len(alarm_json)))

        if(len(alarm_json) > 0):
             root_jon.update({"result":alarm_json})
             return jsonify(root_jon)
        else :
            return jsonify({"result":"no"})



    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@mobile_control.route('/alarm_save_setting_growth_individual',methods=['GET','POST'])
def alarm_save_setting_growth_individual():
    print("do alarm_save_setting_growth_individual()!!!")

    if request.method == 'POST': 
        try:

            device_id = request.form['device_id']
            
            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))

            leaf_cnt_min = request.form['leaf_cnt_min']
            #leaf_cnt_min = request.form.get('leaf_cnt_min')
            print(leaf_cnt_min)
            leaf_cnt_max = request.form['leaf_cnt_max']
            print(leaf_cnt_max)
            leaf_cnt_alarm_issu_type = request.form['leaf_cnt_alarm_issu_type']
            print(leaf_cnt_alarm_issu_type)

            in_leaf_cnt_min = request.form['in_leaf_cnt_min']
            print(in_leaf_cnt_min)
            in_leaf_cnt_max = request.form['in_leaf_cnt_max']
            print(in_leaf_cnt_max)
            in_leaf_cnt_alarm_issu_type = request.form['in_leaf_cnt_alarm_issu_type']
            print(in_leaf_cnt_alarm_issu_type)

            head_cnt_min = request.form['head_cnt_min']
            head_cnt_max = request.form['head_cnt_max']
            head_cnt_alarm_issu_type = request.form['head_cnt_alarm_issu_type']

            ilack_cnt_min = request.form['ilack_cnt_min']
            ilack_cnt_max = request.form['ilack_cnt_max']
            ilack_cnt_alarm_issu_type = request.form['ilack_cnt_alarm_issu_type']
            print("ilack_cnt_alarm_issu_type:"  + str(ilack_cnt_alarm_issu_type))

            flower_cnt_min = request.form['flower_cnt_min']
            flower_cnt_max = request.form['flower_cnt_max']
            flower_cnt_alarm_issu_type = request.form['flower_cnt_alarm_issu_type']

            fruit_cnt_min = request.form['fruit_cnt_min']
            fruit_cnt_max = request.form['fruit_cnt_max']
            fruit_cnt_alarm_issu_type = request.form['fruit_cnt_alarm_issu_type']

            fruit_size_min = request.form['fruit_size_min']
            fruit_size_max = request.form['fruit_size_max']
            fruit_size_alarm_issu_type = request.form['fruit_size_alarm_issu_type']

            in_leaf_length_min = request.form['in_leaf_length_min']
            in_leaf_length_max = request.form['in_leaf_length_max']
            in_leaf_length_alarm_issu_type = request.form['in_leaf_length_alarm_issu_type']


            in_leaf_size_min = request.form['in_leaf_size_min']
            in_leaf_size_max = request.form['in_leaf_size_max']
            in_leaf_size_alarm_issu_type = request.form['in_leaf_size_alarm_issu_type']
           
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            if ( bool(db.session.query(AlarmSettingGrowthIndividual).\
                    filter(AlarmSettingGrowthIndividual.device_id==str(device_id)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingGrowthIndividual(

                            device_id = str(device_id),

                            leaf_cnt_min = str(leaf_cnt_min),
                            leaf_cnt_max = str(leaf_cnt_max),
                            leaf_cnt_alarm_issu_type = str(leaf_cnt_alarm_issu_type),

                            in_leaf_cnt_min = str(in_leaf_cnt_min),
                            in_leaf_cnt_max = str(in_leaf_cnt_max),
                            in_leaf_cnt_alarm_issu_type = str(in_leaf_cnt_alarm_issu_type),

                            head_cnt_min = str(head_cnt_min),
                            head_cnt_max = str(head_cnt_max),
                            head_cnt_alarm_issu_type = str(head_cnt_alarm_issu_type),

                            ilack_cnt_min = str(ilack_cnt_min),
                            ilack_cnt_max = str(ilack_cnt_max),
                            ilack_cnt_alarm_issu_type = str(ilack_cnt_alarm_issu_type),

                            flower_cnt_min = str(flower_cnt_min),
                            flower_cnt_max = str(flower_cnt_max),
                            flower_cnt_alarm_issu_type = str(flower_cnt_alarm_issu_type),

                            fruit_cnt_min = str(fruit_cnt_min),
                            fruit_cnt_max = str(fruit_cnt_max),
                            fruit_cnt_alarm_issu_type = str(fruit_cnt_alarm_issu_type),

                            fruit_size_min = str(fruit_size_min),
                            fruit_size_max = str(fruit_size_max),
                            fruit_size_alarm_issu_type = str(fruit_size_alarm_issu_type),


                            in_leaf_length_min = str(in_leaf_length_min),
                            in_leaf_length_max = str(in_leaf_length_max),
                            in_leaf_length_alarm_issu_type = str(in_leaf_length_alarm_issu_type),


                            in_leaf_size_min = str(in_leaf_size_min),
                            in_leaf_size_max = str(in_leaf_size_max),
                            in_leaf_size_alarm_issu_type = str(in_leaf_size_alarm_issu_type)
                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingGrowthIndividual)
                upateQery = upateQery.filter( AlarmSettingGrowthIndividual.device_id==str(device_id) )
                upateAlarm = upateQery.one()

                upateAlarm.device_id = str(device_id)

                upateAlarm.leaf_cnt_min = str(leaf_cnt_min)
                upateAlarm.leaf_cnt_max = str(leaf_cnt_max)
                upateAlarm.leaf_cnt_alarm_issu_type = str(leaf_cnt_alarm_issu_type)
                print("leaf_cnt_alarm_issu_type : " + leaf_cnt_alarm_issu_type)

                upateAlarm.in_leaf_cnt_min = str(in_leaf_cnt_min)
                upateAlarm.in_leaf_cnt_max = str(in_leaf_cnt_max)
                upateAlarm.in_leaf_cnt_alarm_issu_type = str(in_leaf_cnt_alarm_issu_type)

                upateAlarm.head_cnt_min = str(head_cnt_min)
                upateAlarm.head_cnt_max = str(head_cnt_max)
                upateAlarm.head_cnt_alarm_issu_type = str(head_cnt_alarm_issu_type)

                upateAlarm.ilack_cnt_min = str(ilack_cnt_min)
                upateAlarm.ilack_cnt_max = str(ilack_cnt_max)
                upateAlarm.ilack_cnt_alarm_issu_type = str(ilack_cnt_alarm_issu_type)

                upateAlarm.flower_cnt_min = str(flower_cnt_min)
                upateAlarm.flower_cnt_max = str(flower_cnt_max)
                upateAlarm.flower_cnt_alarm_issu_type = str(flower_cnt_alarm_issu_type)

                upateAlarm.fruit_cnt_min = str(fruit_cnt_min)
                upateAlarm.fruit_cnt_max = str(fruit_cnt_max)
                upateAlarm.fruit_cnt_alarm_issu_type = str(fruit_cnt_alarm_issu_type)

                upateAlarm.fruit_size_min = str(fruit_size_min)
                upateAlarm.fruit_size_max = str(fruit_size_max)
                upateAlarm.fruit_size_alarm_issu_type = str(fruit_size_alarm_issu_type)


                
                upateAlarm.in_leaf_length_min = str(in_leaf_length_min)
                upateAlarm.in_leaf_length_max = str(in_leaf_length_max)
                upateAlarm.in_leaf_length_alarm_issu_type = str(in_leaf_length_alarm_issu_type)


                
                upateAlarm.in_leaf_size_min = str(in_leaf_size_min)
                upateAlarm.in_leaf_size_max = str(in_leaf_size_max)
                upateAlarm.in_leaf_size_alarm_issu_type = str(in_leaf_size_alarm_issu_type)


                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204


###################################################################
## 농장정보 알람설정 병혜충
###################################################################
@mobile_control.route('/mobile_alarm_setting_pest')
def mobile_alarm_setting_pest():
    print("mobile_alarm_setting_pest")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    userid = "admin"

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
            url ="mobile_control.mobile_alarm_setting_pest";

            return render_template('/mobile/farm_info/mobile_alarm_setting_pest.html',userid = userid)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})

@mobile_control.route('/alarm_loading_pest_data_individual')
def alarm_loading_pest_data_individual():
    print("do alarm_loading_pest_data_individual()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id : " + device_id)
    
    try:
        
        alarm_setting_info= db.session.query(AlarmSettingPestIndividual).filter(\
            and_( AlarmSettingPestIndividual.device_id==device_id)).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:
    

            alarm_json.update({"alarm_id":alarm_setting.id})

            alarm_json.update({"ice_min":alarm_setting.ice_min})
            alarm_json.update({"ice_max":alarm_setting.ice_max})
            alarm_json.update({"ice_alarm_issu_type":alarm_setting.ice_alarm_issu_type})

            alarm_json.update({"aphid_min":alarm_setting.aphid_min})
            alarm_json.update({"aphid_max":alarm_setting.aphid_max}) 
            alarm_json.update({"aphid_alarm_issu_type":alarm_setting.aphid_alarm_issu_type}) 

            alarm_json.update({"ash_mold_min":alarm_setting.ash_mold_min}) 
            alarm_json.update({"ash_mold_max":alarm_setting.ash_mold_max})
            alarm_json.update({"ash_mold_alarm_issu_type":alarm_setting.ash_mold_alarm_issu_type}) 



            alarm_json.update({"white_mold_min":alarm_setting.white_mold_min}) 
            alarm_json.update({"white_mold_max":alarm_setting.white_mold_max})
            alarm_json.update({"white_mold_alarm_issu_type":alarm_setting.white_mold_alarm_issu_type}) 

            alarm_json.update({"withered_disease_min":alarm_setting.withered_disease_min}) 
            alarm_json.update({"withered_disease_alarm_issu_type":alarm_setting.withered_disease_alarm_issu_type}) 

            alarm_json.update({"plague_min":alarm_setting.plague_min})
            alarm_json.update({"plague_alarm_issu_type":alarm_setting.plague_alarm_issu_type}) 

            alarm_json.update({"anthrax_min":alarm_setting.anthrax_min}) 
            alarm_json.update({"anthrax_alarm_issu_type":alarm_setting.anthrax_alarm_issu_type}) 

            alarm_json.update({"etc_min":alarm_setting.etc_min}) 
            alarm_json.update({"etc_alarm_issu_type":alarm_setting.etc_alarm_issu_type}) 



        print("alarm_json : " + str(alarm_json))
        print("alarm_json : " + str(len(alarm_json)))

        if(len(alarm_json) > 0):
             root_jon.update({"result":alarm_json})
             return jsonify(root_jon)
        else :
            return jsonify({"result":"no"})



    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@mobile_control.route('/alarm_save_setting_pest_individual',methods=['GET','POST'])
def alarm_save_setting_pest_individual():
    print("do alarm_save_setting_pest_individual()!!!")

    if request.method == 'POST': 
        try:
            
            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))

            device_id = request.form['device_id']
            print("device_id  : "  + str(device_id))

            ice_min = request.form['ice_min']
            #ice_min = request.form.get('ice_min')
            print("ice_min : "  + str(ice_min))
            ice_max = request.form['ice_max']
            print(ice_max)
            ice_alarm_issu_type = request.form['ice_alarm_issu_type']
            print(ice_alarm_issu_type)

            aphid_min = request.form['aphid_min']
            print(aphid_min)
            aphid_max = request.form['aphid_max']
            print(aphid_max)
            aphid_alarm_issu_type = request.form['aphid_alarm_issu_type']
            print(aphid_alarm_issu_type)

            ash_mold_min = request.form['ash_mold_min']
            ash_mold_max = request.form['ash_mold_max']
            ash_mold_alarm_issu_type = request.form['ash_mold_alarm_issu_type']



            white_mold_min = request.form['white_mold_min']
            white_mold_max = request.form['white_mold_max']
            white_mold_alarm_issu_type = request.form['white_mold_alarm_issu_type']



            withered_disease_min = request.form['withered_disease_min']
            withered_disease_alarm_issu_type = request.form['withered_disease_alarm_issu_type']
            print("withered_disease_alarm_issu_type:"  + str(withered_disease_alarm_issu_type))

            plague_min = request.form['plague_min']
            plague_alarm_issu_type = request.form['plague_alarm_issu_type']
            print("plague_min:"  + str(plague_min))

            anthrax_min = request.form['anthrax_min']
            anthrax_alarm_issu_type = request.form['anthrax_alarm_issu_type']

            print("anthrax_alarm_issu_type:"  + str(anthrax_alarm_issu_type))

            etc_min = request.form['etc_min']
            print("etc_min:"  + str(etc_min))
            etc_alarm_issu_type = request.form['etc_alarm_issu_type']
            print("etc_alarm_issu_type:"  + str(etc_alarm_issu_type))
           
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            if ( bool(db.session.query(AlarmSettingPestIndividual).\
                    filter(AlarmSettingPestIndividual.device_id==str(device_id)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingPestIndividual(

                            
                            device_id = str(device_id),

                            ice_min = str(ice_min),
                            ice_max = str(ice_max),
                            ice_alarm_issu_type = str(ice_alarm_issu_type),

                            aphid_min = str(aphid_min),
                            aphid_max = str(aphid_max),
                            aphid_alarm_issu_type = str(aphid_alarm_issu_type),

                            ash_mold_min = str(ash_mold_min),
                            ash_mold_max = str(ash_mold_max),
                            ash_mold_alarm_issu_type = str(ash_mold_alarm_issu_type),
                            
                            white_mold_min = str(white_mold_min),
                            white_mold_max = str(white_mold_max),
                            white_mold_alarm_issu_type = str(white_mold_alarm_issu_type),

                            withered_disease_min = str(withered_disease_min),
                            withered_disease_alarm_issu_type = str(withered_disease_alarm_issu_type),

                            plague_min = str(plague_min),
                            plague_alarm_issu_type = str(plague_alarm_issu_type),

                            anthrax_min = str(anthrax_min),
                            anthrax_alarm_issu_type = str(anthrax_alarm_issu_type),

                            etc_min = str(etc_min),
                            etc_alarm_issu_type = str(etc_alarm_issu_type),

                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingPestIndividual)
                upateQery = upateQery.filter( AlarmSettingPestIndividual.device_id==str(device_id) )
                upateAlarm = upateQery.one()


                upateAlarm.ice_min = str(ice_min)
                upateAlarm.ice_max = str(ice_max)
                upateAlarm.ice_alarm_issu_type = str(ice_alarm_issu_type)
                print("ice_alarm_issu_type : " + ice_alarm_issu_type)

                upateAlarm.aphid_min = str(aphid_min)
                upateAlarm.aphid_max = str(aphid_max)
                upateAlarm.aphid_alarm_issu_type = str(aphid_alarm_issu_type)

                upateAlarm.ash_mold_min = str(ash_mold_min)
                upateAlarm.ash_mold_max = str(ash_mold_max)
                upateAlarm.ash_mold_alarm_issu_type = str(ash_mold_alarm_issu_type)

                upateAlarm.white_mold_min = str(white_mold_min)
                upateAlarm.white_mold_max = str(white_mold_max)
                upateAlarm.white_mold_alarm_issu_type = str(white_mold_alarm_issu_type)

                upateAlarm.withered_disease_min = str(withered_disease_min)
                upateAlarm.withered_disease_alarm_issu_type = str(withered_disease_alarm_issu_type)

                upateAlarm.plague_min = str(plague_min)
                upateAlarm.plague_alarm_issu_type = str(plague_alarm_issu_type)

                upateAlarm.anthrax_min = str(anthrax_min)
                upateAlarm.anthrax_alarm_issu_type = str(anthrax_alarm_issu_type)

                upateAlarm.etc_min = str(etc_min)
                upateAlarm.etc_alarm_issu_type = str(etc_alarm_issu_type)


                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204



###################################################################
## 농장정보 알람설정 생리장애
###################################################################
@mobile_control.route('/mobile_alarm_setting_disorder')
def mobile_alarm_setting_disorder():
    print("mobile_alarm_setting_disorder")

    userid = session.get('userid',None) 
    print("userid :" + str(userid))

    userid = "admin"

    ##if( not ( userid == "None" or  userid is None  ) ):
    if( true ):
       
        try:
            url ="mobile_control.mobile_alarm_setting_disorder";

            return render_template('/mobile/farm_info/mobile_alarm_setting_disorder.html',userid = userid)

        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})

    else:

        return jsonify({"result":"user is not logined"})

@mobile_control.route('/alarm_loading_disorder_data_individual')
def alarm_loading_disorder_data_individual():
    print("do alarm_loading_disorder_data_individual()!!!")

    device_id = str(request.args.get('device_id', default = '', type = str) )
    print("device_id : " + device_id)
    
    try:
        
        alarm_setting_info= db.session.query(AlarmSettingDisorderIndividual).filter(\
            and_( AlarmSettingDisorderIndividual.device_id==device_id)).all()

        db.session.commit()

        print(alarm_setting_info);

        root_jon ={}
        alarm_json = {}
        

        for alarm_setting in alarm_setting_info:

            alarm_json.update({"alarm_id":alarm_setting.id})

            alarm_json.update({"nitrogen_min":alarm_setting.nitrogen_min})
            alarm_json.update({"nitrogen_alarm_issu_type":alarm_setting.nitrogen_alarm_issu_type})

            alarm_json.update({"phosphorus_min":alarm_setting.phosphorus_min})
            alarm_json.update({"phosphorus_alarm_issu_type":alarm_setting.phosphorus_alarm_issu_type}) 

            alarm_json.update({"patassium_min":alarm_setting.patassium_min}) 
            alarm_json.update({"patassium_alarm_issu_type":alarm_setting.patassium_alarm_issu_type}) 



            alarm_json.update({"magnetsium_min":alarm_setting.magnetsium_min}) 
            alarm_json.update({"magnetsium_alarm_issu_type":alarm_setting.magnetsium_alarm_issu_type}) 

            alarm_json.update({"calcium_min":alarm_setting.calcium_min}) 
            alarm_json.update({"calcium_alarm_issu_type":alarm_setting.calcium_alarm_issu_type}) 

            alarm_json.update({"etc_min":alarm_setting.etc_min})
            alarm_json.update({"etc_alarm_issu_type":alarm_setting.etc_alarm_issu_type}) 


        print("alarm_json : " + str(alarm_json))
        print("alarm_json : " + str(len(alarm_json)))

        if(len(alarm_json) > 0):
             root_jon.update({"result":alarm_json})
             return jsonify(root_jon)
        else :
            return jsonify({"result":"no"})



    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})


@mobile_control.route('/alarm_save_setting_disorder_individual',methods=['GET','POST'])
def alarm_save_setting_disorder_individual():
    print("do alarm_save_setting_disorder_individual()!!!")
    
    if request.method == 'POST': 
        try:
            
            device_id = request.form['device_id']
            print("device_id  : "  + str(device_id))

            alarm_id = request.form['alarm_id']
            print("alarm_id  : "  + str(alarm_id))

            nitrogen_min = request.form['nitrogen_min']
            #nitrogen_min = request.form.get('nitrogen_min')
            print("nitrogen_min : "  + str(nitrogen_min))
            nitrogen_alarm_issu_type = request.form['nitrogen_alarm_issu_type']
            print(nitrogen_alarm_issu_type)

            phosphorus_min = request.form['phosphorus_min']
            print(phosphorus_min)
            phosphorus_alarm_issu_type = request.form['phosphorus_alarm_issu_type']
            print(phosphorus_alarm_issu_type)

            patassium_min = request.form['patassium_min']
            patassium_alarm_issu_type = request.form['patassium_alarm_issu_type']

            magnetsium_min = request.form['magnetsium_min']
            magnetsium_alarm_issu_type = request.form['magnetsium_alarm_issu_type']


            calcium_min = request.form['calcium_min']
            calcium_alarm_issu_type = request.form['calcium_alarm_issu_type']
            print("calcium_alarm_issu_type:"  + str(calcium_alarm_issu_type))

            etc_min = request.form['etc_min']
            etc_alarm_issu_type = request.form['etc_alarm_issu_type']
            print("etc_min:"  + str(etc_min))
      
            basedir = os.path.abspath(os.path.dirname(__file__))
            dbfile = os.path.join(basedir, 'db.sqlite')
            dburl= 'sqlite:///' + dbfile
            engine = create_engine(dburl)
            Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
            session = Session()

            if ( bool(db.session.query(AlarmSettingDisorderIndividual).\
                    filter(AlarmSettingDisorderIndividual.device_id==str(device_id)).\
                    first()) == False ) :
    
                print("================ insert ==============================")
                db.session.add(AlarmSettingDisorderIndividual(

                            
                            device_id = str(device_id),

                            nitrogen_min = str(nitrogen_min),
                            nitrogen_alarm_issu_type = str(nitrogen_alarm_issu_type),

                            phosphorus_min = str(phosphorus_min),
                            phosphorus_alarm_issu_type = str(phosphorus_alarm_issu_type),

                            patassium_min = str(patassium_min),
                            patassium_alarm_issu_type = str(patassium_alarm_issu_type),
                            
                            magnetsium_min = str(magnetsium_min),
                            magnetsium_alarm_issu_type = str(magnetsium_alarm_issu_type),

                            calcium_min = str(calcium_min),
                            calcium_alarm_issu_type = str(calcium_alarm_issu_type),

                            etc_min = str(etc_min),
                            etc_alarm_issu_type = str(etc_alarm_issu_type)

                        ))
                db.session.commit()
                print("insert sucess !")
                print("====================================================================")
            
            else :

                print("========================update===============================")
                upateQery = db.session.query(AlarmSettingDisorderIndividual)
                upateQery = upateQery.filter( AlarmSettingDisorderIndividual.device_id==str(device_id) )
                upateAlarm = upateQery.one()


                upateAlarm.nitrogen_min = str(nitrogen_min)
                upateAlarm.nitrogen_alarm_issu_type = str(nitrogen_alarm_issu_type)
                print("nitrogen_alarm_issu_type : " + nitrogen_alarm_issu_type)

                upateAlarm.phosphorus_min = str(phosphorus_min)
                upateAlarm.phosphorus_alarm_issu_type = str(phosphorus_alarm_issu_type)

                upateAlarm.patassium_min = str(patassium_min)
                upateAlarm.patassium_alarm_issu_type = str(patassium_alarm_issu_type)

                upateAlarm.magnetsium_min = str(magnetsium_min)
                upateAlarm.magnetsium_alarm_issu_type = str(magnetsium_alarm_issu_type)

                upateAlarm.calcium_min = str(calcium_min)
                upateAlarm.calcium_alarm_issu_type = str(calcium_alarm_issu_type)

                upateAlarm.etc_min = str(etc_min)
                upateAlarm.etc_alarm_issu_type = str(etc_alarm_issu_type)

                db.session.commit()
    

                print("update sucess !")
                print("====================================================================")
       
            return jsonify({"result":"ok"})


        except Exception as ex:
            print(ex)
            return jsonify({"result":"no"})


    return jsonify(),204

###################################################################
## Utill
###################################################################
@mobile_control.route('/get_device_by_userid',methods=['GET','POST'])
def get_device():
    print("do get_device_by_userid()!!!")


    user_id = str(request.args.get('userid', default = '', type = str) )
    print("user_id : " + user_id)

    try :
        

        devices= db.session.query(DeviceDb,FarmDb).filter(FarmDb.user_id == user_id )\
                                        .join(DeviceDb,FarmDb.farm_id == DeviceDb.farm_id).all()

        print("devices : " + str(devices))

        devices_list = []
        for device in devices:
            devices_list.append(device.DeviceDb.device_id)
        
        print("devices_list : " + str(devices_list))

        return jsonify({"result":devices_list})

    except Exception as ex:
        print(ex)
        return jsonify({"result":"no"})
