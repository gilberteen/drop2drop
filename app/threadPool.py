import os
from re import search
from typing import Awaitable
import requests

from flask import Flask, Blueprint, url_for
from flask import request ,redirect, render_template, session
from flask import jsonify
from flask.wrappers import Response
from flask_wtf import form
from sqlalchemy.sql import text,func
from sqlalchemy import or_ , and_
from sqlalchemy import Date, cast
import sys
import os
from sqlalchemy.sql.expression import false, insert, true

from sqlalchemy.sql.functions import count, random

from .formspool.forms import FarmBasicForm, RegisterForm, LoginForm , MainFrame
from .formspool.forms import NoticeForm

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import func,Column

from datetime import datetime , timedelta
import json
import random
import string
import ast
import pickle
import threading, time
import atexit
import asyncio
from pyfcm import FCMNotification
from modelpool.models import  db,TB_AI_SPEAKER

from api_cc_v1 import api_cc as api_cc_v1

POOL_TIME_CHEK_THREAD = 30*1
threadCheckServer=threading.Thread()
threadSelftCheck=threading.Thread()


## check checkDeiceConnectionThread
def checkDeiceConnectionThread(): 
    ## tread
    def interrupt():
        global threadSelftCheck
        threadSelftCheck.cancel()
    def doStuff():
        
        while true:
            time.sleep(POOL_TIME_CHEK_THREAD)
            checkDeviceConnectionState() 
        
    def doStuffStart():
        global threadSelftCheck
        threadSelftCheck = threading.Thread(target=doStuff)
        threadSelftCheck.daemon = True
        threadSelftCheck.setDaemon(True)
        threadSelftCheck.start()
    doStuffStart()
    atexit.register(interrupt)


def checkDeviceConnectionState():

    try:
     
        spearker_list= db.session.query(TB_AI_SPEAKER).all()
        db.session.close()
        if(not(spearker_list is None) ):
            
  
            for speaker in spearker_list :
                #print("ai_speaker_id : " + speaker.ai_speaker_id)
                #print("ai_speaker_status : " + speaker.ai_speaker_status)
                #print("ai_speaker_network_status : " + speaker.ai_speaker_network_status)
                #print("ai_speaker_alive_check_time : " + str(speaker.ai_speaker_alive_check_time))
            
                temp_ai_speaker_alive_check_time = ""
                temp_ai_speaker_alive_check_time = speaker.ai_speaker_alive_check_time
                if(temp_ai_speaker_alive_check_time != "" and not temp_ai_speaker_alive_check_time is None) :
                        
                    current_ai_speaker_alive_check_time = datetime.strptime(str(temp_ai_speaker_alive_check_time), '%Y-%m-%d %H:%M:%S')
                    #print("current_ai_speaker_alive_check_time :" + str(current_ai_speaker_alive_check_time));
                    
                    today_now_date_time = datetime.now()
                   # print("today_now_date_time :" + str(today_now_date_time));
                    
                    diff =  today_now_date_time - current_ai_speaker_alive_check_time
                    #print("diff :" + str(diff));
                    #print("Second difference : ", str(diff.seconds))

                    if(int(diff.seconds) > 300):
                        if (speaker.ai_speaker_status == "출고"):
                            upateSilverQery = db.session.query(TB_AI_SPEAKER)
                            upateSilverQery = upateSilverQery.filter(TB_AI_SPEAKER.ai_speaker_id == speaker.ai_speaker_id)
                            upateSilverRecord = upateSilverQery.one()
                            upateSilverRecord.ai_speaker_network_status = "미사용"
                            db.session.commit()
                            db.session.close()
                            
                            print("speaker status is upated to 연결안됨 : ", speaker.ai_speaker_id)

    except Exception as ex:     
        print("checkDeviceConnectionState() : " + str(ex))
