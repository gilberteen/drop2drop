# main_api.py
# 
from flask.signals import message_flashed
from sqlalchemy.sql.sqltypes import DateTime
from sqlalchemy.sql.expression import asc, desc, false, insert, true
from . import api
import sys
import os

sys.path.append('..')

from flask import jsonify
from flask import request
from flask import Blueprint
from flask import session
from flask import Flask, Blueprint, url_for
from flask import request, redirect, render_template, session
from flask.wrappers import Response
from flask_wtf import form

from sqlalchemy.sql import text,func
from sqlalchemy import or_ , and_
from sqlalchemy import Date, cast
from sqlalchemy.sql.expression import false, insert, true
from sqlalchemy.sql.functions import count, random
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import func,Column

from datetime import datetime , timedelta
import json
import random
import string
import ast
import pickle
import requests
from re import search
from typing import Awaitable

from modelpool.models import DeviceDb, DeviceDefectDb,HouseDb, NoticeHistoryDb, NoticeUserHistoryDb, QuestionDb, Scale, SensorDb ,TechDb, db, Fcuser, Todo, EmployeesDb, NoticeDb, TestDb, FarmDb
from modelpool.models import AlarmHistoryDb,AlarmSettingSensor ,AlarmSettingGrowth ,AlarmSettingPest,AlarmSettingDisorder,DeviceOperationDb
from modelpool.models import SensorDataDb , ImageBasicDataDb , AiStatusDb , MessageToken
from modelpool.models import HealthBandDataDb , UserDiseaseDb 


def return_msg(url_,code,msg):
    res = requests.post(url_, json={
        'statusCode': code ,
        'message': msg
    }, headers={'Content-Type': 'application/json'})


@api.route('/set_message_token', methods=['POST'])
def set_message_token():
    if request.method == 'POST':
        print("set_message_token() !!!")

        ## 1. 내부에서 호출할때는  request.form['user_id'] 형태로
        '''
        user_id = request.form['user_id']
        print("user_id : "  + user_id)
        token_key = request.form['token_key']
        print("token_key : "  + token_key)
        '''
        ## 2. 외부에서 호출할때는  rrequest.get_json(force=True) 
        posted_data = request.get_json(force=True)
        user_id = posted_data['user_id']
        print("user_id : "  + user_id)
        token_key = posted_data['token_key']
        print("token_key : "  + token_key)

        
        if( not token_key is None ):

                tokens= db.session.query(func.count(MessageToken.user_id)).\
                    filter(and_(MessageToken.user_id==user_id ,MessageToken.token_key==token_key )).scalar()
                
                print ("tokens : " + str(tokens))                 
                db.session.close()

                if( tokens > 0 ) :
                    print ("User's tocken is already in databae")
                else:
                    db.session.add(MessageToken( user_id=user_id,token_key=token_key))
                    db.session.commit()
                    db.session.close()

                print("token is added successfully !! ")

        return jsonify({"statusCode":"200"})

    else:
        return jsonify({"statusCode":"500"})
        ##return jsonify(), 201

@api.route('/logindo',methods=['GET','PUT','DELETE'])
def logindo():
    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')
    print("logindo() !!!")
    if request.method == 'GET' :
        id= request.args.get("id","")
        password= request.args.get("password","")
        print("reqeust id:" + id)
        print("reqeust password:" + password)

        try:
            temp_user = Fcuser.query.filter(and_(Fcuser.userid ==id)).first()
            print(temp_user);
            if not ( temp_user is None) :
                if(temp_user.password == password):
                    print("user is exigst !")
                    return jsonify({"status":"ok"})
                    ##return jsonify(user.serialize)
                else:
                    print("password is not correct !")
                    return jsonify({"status":"fail" , "error":"password is not correct"})
            else:
                print("user is not exigst !")
                return jsonify({"status":"fail" , "error":"user is not exigst"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"status":"204"})
    else:
        return jsonify(),204

#사용자 상세 정보 로딩
@api.route('/logindoAll',methods=['GET'])
def get_user_info_detail():
     if request.method == 'GET':
        print("logindoAll() !!!")
        id= request.args.get("id","")
        password= request.args.get("password","")
        print("reqeust id:" + id)
        print("reqeust password:" + password)
        if(id !=""):
            
            user= db.session.query(Fcuser).filter(Fcuser.userid ==id).first()
            print(str(user))
            if(user is not None):

                if(user.password == password):
                    print("user is exigst !")
                else:
                    print("password is not correct !")
                    return jsonify({"status":"fail" , "error":"password is not correct"})

                user_json = {};
                user_json.update({"status" : "ok"})
                if(user.userid) :
                    user_json.update({"userid" : user.userid})
                else :
                    user_json.update({"userid" : ""})

                if(user.username) :
                    user_json.update({"username" : user.username})
                else :
                    user_json.update({"username" : ""})

                if(user.password) :
                    user_json.update({"password" : user.password})
                else :
                    user_json.update({"password" : ""})

                if(user.height) :
                    user_json.update({"height" : user.height})
                else :
                    user_json.update({"height" : ""})

                if(user.weight) :
                    user_json.update({"weight" : user.weight})
                else :
                    user_json.update({"weight" : ""})

                if(user.gender) :
                    user_json.update({"gender" : user.gender})
                else :
                    user_json.update({"gender" : ""})

                if(user.bod) :
                    user_json.update({"bod" : user.bod})
                else :
                    user_json.update({"bod" : ""})

                if(user.address) :
                    user_json.update({"address" : user.address})
                else :
                    user_json.update({"address" : ""})

                if(user.email) :
                    user_json.update({"email" : user.email})
                else :
                    user_json.update({"email" : ""})

                if(user.phone_number) :
                    user_json.update({"phone_number" : user.phone_number})
                else :
                    user_json.update({"phone_number" : ""})
                    
                if(user.role) :
                    user_json.update({"role" : user.role})
                else :
                    user_json.update({"role" : ""})

                if(user.score_total) :
                    user_json.update({"score_total" : user.score_total})
                else :
                    user_json.update({"score_total" : ""})
                    
                if(user.score_today) :
                    user_json.update({"score_today" : user.score_today})
                else :
                    user_json.update({"score_today" : ""})

                if(user.level) :
                    user_json.update({"level" : user.level})
                else :
                    user_json.update({"level" : ""})

                if(user.ranking) :
                    user_json.update({"ranking" : user.ranking})
                else :
                    user_json.update({"ranking" : ""})

                if(user.recent_date) :
                    user_json.update({"recent_date" : user.recent_date})
                else :
                    user_json.update({"recent_date" : ""})

                if(user.walking_count_total) :
                    user_json.update({"walking_count_total" : user.walking_count_total})
                else :
                    user_json.update({"walking_count_total" : ""})

                if(user.walking_count_hour) :
                    user_json.update({"walking_count_hour" : user.walking_count_hour})
                else :
                    user_json.update({"walking_count_hour" : ""})

                if(user.walking_count_monthly) :
                    user_json.update({"walking_count_monthly" : user.walking_count_monthly})
                else :
                    user_json.update({"walking_count_monthly" : ""})

                if(user.walking_count_monthly_accum) :
                    user_json.update({"walking_count_monthly_accum" : user.walking_count_monthly_accum})
                else :
                    user_json.update({"walking_count_monthly_accum" : ""})

                    
                if(user.walking_count_step_size) :
                    user_json.update({"walking_count_step_size" : user.walking_count_step_size})
                else :
                    user_json.update({"walking_count_step_size" : ""})
                    
                if(user.walking_count_today) :
                    user_json.update({"walking_count_today" : user.walking_count_today})
                else :
                    user_json.update({"walking_count_today" : ""})

                user_json.update({"walking_count_step_size_today" : user.walking_count_step_size_today}) if user.walking_count_step_size_today else user_json.update({"walking_count_step_size_today" : ""})
              

                disease= db.session.query(UserDiseaseDb).filter(UserDiseaseDb.user_id ==user.userid).first()
                if(disease is not None):
                    print(str(disease))

                       
                    if(disease.disease_none) :
                        user_json.update({"disease_none" : disease.disease_none})
                    else :
                        user_json.update({"disease_none" : ""})
                    if(disease.disease_gohyulap) :
                        user_json.update({"disease_gohyulap" : disease.disease_gohyulap})
                    else :
                        user_json.update({"disease_gohyulap" : ""})
                    if(disease.disease_dangyo) :
                        user_json.update({"disease_dangyo" : disease.disease_dangyo})
                    else :
                        user_json.update({"disease_dangyo" : ""})
                    if(disease.disease_gogiyung) :
                        user_json.update({"disease_gogiyung" : disease.disease_gogiyung})
                    else :
                        user_json.update({"disease_gogiyung" : ""})
                    if(disease.disease_simjang) :
                        user_json.update({"disease_simjang" : disease.disease_simjang})
                    else :
                        user_json.update({"disease_simjang" : ""})
                    if(disease.disease_gabsang) :
                        user_json.update({"disease_gabsang" : disease.disease_gabsang})
                    else :
                        user_json.update({"disease_gabsang" : ""})
                    if(disease.disease_gan) :
                        user_json.update({"disease_gan" : disease.disease_gan})
                    else :
                        user_json.update({"disease_gan" : ""})
                    if(disease.disease_pae) :
                        user_json.update({"disease_pae" : disease.disease_pae})
                    else :
                        user_json.update({"disease_pae" : ""})
                    if(disease.disease_sin) :
                        user_json.update({"disease_sin" : disease.disease_sin})
                    else :
                        user_json.update({"disease_sin" : ""})
                    if(disease.smoke) :
                        user_json.update({"smoke" : disease.smoke})
                    else :
                        user_json.update({"smoke" : ""})
                    if(disease.chuckchu) :
                        user_json.update({"chuckchu" : disease.chuckchu})
                    else :
                        user_json.update({"chuckchu" : ""})
                    if(disease.muroup) :
                        user_json.update({"muroup" : disease.muroup})
                    else :
                        user_json.update({"muroup" : ""})


                    if(disease.family_disease_none) :
                        user_json.update({"family_disease_none" : disease.family_disease_none})
                    else :
                        user_json.update({"family_disease_none" : ""})
                    if(disease.family_disease_gohyulap) :
                        user_json.update({"family_disease_gohyulap" : disease.family_disease_gohyulap})
                    else :
                        user_json.update({"family_disease_gohyulap" : ""})
                    if(disease.family_disease_dangyo) :
                        user_json.update({"family_disease_dangyo" : disease.family_disease_dangyo})
                    else :
                        user_json.update({"family_disease_dangyo" : ""})
                    if(disease.family_disease_gogiyung) :
                        user_json.update({"family_disease_gogiyung" : disease.family_disease_gogiyung})
                    else :
                        user_json.update({"family_disease_gogiyung" : ""})
                    if(disease.family_disease_simjang) :
                        user_json.update({"family_disease_simjang" : disease.family_disease_simjang})
                    else :
                        user_json.update({"family_disease_simjang" : ""})
                    if(disease.family_disease_gabsang) :
                        user_json.update({"family_disease_gabsang" : disease.family_disease_gabsang})
                    else :
                        user_json.update({"family_disease_gabsang" : ""})
                    if(disease.family_disease_gan) :
                        user_json.update({"family_disease_gan" : disease.family_disease_gan})
                    else :
                        user_json.update({"family_disease_gan" : ""})
                    if(disease.family_disease_pae) :
                        user_json.update({"family_disease_pae" : disease.family_disease_pae})
                    else :
                        user_json.update({"family_disease_pae" : ""})
                    if(disease.family_disease_sin) :
                        user_json.update({"family_disease_sin" : disease.family_disease_sin})
                    else :
                        user_json.update({"family_disease_sin" : ""})
                else :
                        user_json.update({"disease_none" : ""})
                        user_json.update({"disease_gohyulap" : ""})
                        user_json.update({"disease_dangyo" : ""})
                        user_json.update({"disease_gogiyung" : ""})
                        user_json.update({"disease_simjang" : ""})
                        user_json.update({"disease_gabsang" : ""})
                        user_json.update({"disease_gan" : ""})
                        user_json.update({"disease_pae" : ""})
                        user_json.update({"disease_sin" : ""})
                        user_json.update({"smoke" : ""})
                        user_json.update({"chuckchu" : ""})
                        user_json.update({"muroup" : ""})
                        user_json.update({"family_disease_none" : ""})
                        user_json.update({"family_disease_gohyulap" : ""})
                        user_json.update({"family_disease_dangyo" : ""})
                        user_json.update({"family_disease_gogiyung" : ""})
                        user_json.update({"family_disease_simjang" : ""})
                        user_json.update({"family_disease_gabsang" : ""})
                        user_json.update({"family_disease_gan" : ""})
                        user_json.update({"family_disease_pae" : ""})
                        user_json.update({"family_disease_sin" : ""})

                print("user_json : " + str(user_json))
                ##return str(farms_json)
                ##return jsonify(json.dumps(farms_json, ensure_ascii=False))
                ##return jsonify(farms_json)
                return json.dumps(user_json, ensure_ascii=False)
                
            else:
                return jsonify({"status":"fail","error":"no data"})
                ##return jsonify(), 201


@api.route('/user_info',methods=['GET','PUT','DELETE'])
def user_info():
    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')
    print("user_info() !!!")
    if request.method == 'GET' :
        id= request.args.get("id","")
        password= request.args.get("password","")
        print("reqeust id:" + id)
        print("reqeust password:" + password)

        try:
            temp_user = Fcuser.query.filter(and_(Fcuser.userid ==id)).first()
            print(temp_user);
            if not ( temp_user is None) :
                if(temp_user.password == password):
                    print("user is exigst !")
                    userinfo = {}
                    userinfo.update({"status" : "ok"})
                    userinfo.update({"id" : temp_user.userid})
                    userinfo.update({"password" : temp_user.password})
                    userinfo.update({"name" : temp_user.username})
                    userinfo.update({"height" : temp_user.height})
                    userinfo.update({"weight" : temp_user.weight})
                    userinfo.update({"gender" : temp_user.gender})
                    userinfo.update({"bod" : temp_user.bod})
                    userinfo.update({"address" : temp_user.address})
                    userinfo.update({"role" : temp_user.role})
                    userinfo.update({"email" : temp_user.email})
                    userinfo.update({"phone_number" : temp_user.phone_number})
                
                    return json.dumps(userinfo, ensure_ascii=False)
                else:
                    print("password is not correct !")
                    return jsonify({"status":"fail" , "error":"password is not correct"})
            else:
                print("user is not exigst !")
                return jsonify({"status":"fail" , "error":"user is not exigst"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"status":"204"})
    else:
        return jsonify(),204

@api.route('/get_users',methods=['GET','PUT','DELETE'])
def get_users():
    print("get_users() !!!")
    if request.method == 'GET' :
        try:
            temp_users = db.session.query(Fcuser).order_by(desc(Fcuser.created_at)).all()
            print(temp_users);
            userlist=[]
            resonsejson={}
            if temp_users :
                print("user is exigst !")
                for user in temp_users:
                    userinfo = {}
                    userinfo.update({"id" : user.userid})
                    userinfo.update({"password" : user.password})
                    userinfo.update({"username" : user.username})

                    if(user.score_total != "" and not (user.score_total is None)):
                            try:
                                userinfo.update({"score_total" : user.score_total})
                            except Exception as ex:     
                                print(ex)
                    else:
                        userinfo.update({"score_total" : ""})

                    if(user.score_today != "" and not (user.score_today is None)):
                            try:
                                userinfo.update({"score_today" : user.score_today})
                            except Exception as ex:     
                                print(ex)
                    else:
                        userinfo.update({"score_today" : ""})

                    if(user.level != "" and not (user.level is None)):
                            try:
                                userinfo.update({"level" : user.level})
                            except Exception as ex:     
                                print(ex)
                    else:
                        userinfo.update({"level" : ""})

                    if(user.ranking != "" and not (user.ranking is None)):
                            try:
                                userinfo.update({"ranking" : user.ranking})
                            except Exception as ex:     
                                print(ex)
                    else:
                        userinfo.update({"ranking" : ""})

                    if(user.recent_date != "" and not (user.recent_date is None)):
                            try:
                                userinfo.update({"recent_date" : user.recent_date})
                            except Exception as ex:     
                                print(ex)
                    else:
                        userinfo.update({"recent_date" : ""})

                    userlist.append(userinfo)
                    
                resonsejson.update({"status" : "ok"})
                resonsejson.update({"users" : userlist})
                print("resonsejson : " + str(resonsejson))
                return json.dumps(resonsejson, ensure_ascii=False)
            else:
                print("users are not exigst !")
                return jsonify({"status":"fail" , "error":"users are not exigst"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"status":"204"})
    else:
        return jsonify(),204


@api.route('/is_id',methods=['GET','PUT','DELETE'])
def is_id():
    ## post
    ##request_data = request.get_json()
    ##user_id = request_data.get('userid')
    print("is_id() !!!")
    if request.method == 'GET' :
        id= request.args.get("id","")
        print("reqeust id:" + id)
        try:
            temp_user = Fcuser.query.filter(and_(Fcuser.userid ==id)).first()
            print(temp_user);
            if not ( temp_user is None) :
                print("user is exigst !")
                return jsonify({"status":"ok"})
            else:
                print("user is not exigst !")
                return jsonify({"status":"fail" , "error":"user is not exigst"})
                ##return jsonify(),204
        except Exception as ex:     
            print(ex)
            return jsonify({"status":"204"})
    else:
        return jsonify(),204

@api.route('/registUser', methods=['POST'])
def registUser():
    if request.method == 'POST':
        print("registUser() !!!")

        posted_data_json = request.get_json(force=True)
        print("posted_data_json : "  + str(posted_data_json))

        ##posted_data_json = json.loads(posted_data)
        userid = posted_data_json["userid"]
        print("reqeust userid : "  + str(userid))


        user_check_count= db.session.query(func.count(Fcuser.userid)).filter(and_( Fcuser.userid == userid)).scalar()
        print("user_check_count : " + str(user_check_count))

        if(user_check_count > 0 ):
            return jsonify({"status":"500"})
        else:
            userid="";
            username="";
            password="";
            height="";
            weight="";
            gender="";
            bod="";
            address="";
            role="";
            email="";
            phone_number="";
            if(is_json_key_present(posted_data_json ,'userid')):
                userid = posted_data_json['userid']
            if(is_json_key_present(posted_data_json ,'name')):
                username = posted_data_json['name']
            if(is_json_key_present(posted_data_json ,'password')):  
                password = posted_data_json['password']
            if(is_json_key_present(posted_data_json ,'height')):   
                height = posted_data_json['height']
            if(is_json_key_present(posted_data_json ,'weight')):  
                weight = posted_data_json['weight']
            if(is_json_key_present(posted_data_json ,'gender')):  
                gender = posted_data_json['gender']
            if(is_json_key_present(posted_data_json ,'bod')):  
                bod = posted_data_json['bod']
            if(is_json_key_present(posted_data_json ,'address')): 
                address = posted_data_json['address']
            if(is_json_key_present(posted_data_json ,'role')): 
                role = posted_data_json['role']
            if(is_json_key_present(posted_data_json ,'email')): 
                email = posted_data_json['email']
            if(is_json_key_present(posted_data_json ,'phone_number')): 
                phone_number = posted_data_json['phone_number']
            
            db.session.add(Fcuser(
                        userid = userid,
                        password = str(password),
                        username = str(username),
                        height = str(height),
                        weight = str(weight),
                        gender = str(gender),
                        bod = str(bod),
                        address = str(address),
                        role = str(role),
                        email = str(email),
                        phone_number = str(phone_number)
                    ))
        
            db.session.commit()

        return jsonify({"status":"ok"})

    else:
        return jsonify({"status":"500"})
        ##return jsonify(), 201

@api.route('/updateUser', methods=['POST'])
def updateUser():
    if request.method == 'POST':
        print("updateUser() !!!")

        posted_data_json = request.get_json(force=True)
        print("posted_data_json : "  + str(posted_data_json))
        userid = posted_data_json["userid"]
        print("reqeust userid : "  + str(userid))


        user_check_count= db.session.query(func.count(Fcuser.userid)).filter(and_( Fcuser.userid == userid)).scalar()
        print("user_check_count : " + str(user_check_count))

        if(user_check_count > 0 ):
            upateQery = db.session.query(Fcuser)
            upateQery = upateQery.filter(Fcuser.userid==userid)
            upateRecord = upateQery.one()

            if(is_json_key_present(posted_data_json ,'password')):
                password = posted_data_json['password']
                upateRecord.password = password
            if(is_json_key_present(posted_data_json ,'name')):   
                name = posted_data_json['name']
                upateRecord.username = name
            if(is_json_key_present(posted_data_json ,'height')):   
                height = posted_data_json['height']
                upateRecord.height = height
            if(is_json_key_present(posted_data_json ,'weight')):  
                weight = posted_data_json['weight']
                upateRecord.weight = weight
            if(is_json_key_present(posted_data_json ,'gender')):  
                gender = posted_data_json['gender']
                upateRecord.gender = gender
            if(is_json_key_present(posted_data_json ,'bod')):  
                bod = posted_data_json['bod']
                upateRecord.bod = bod
            if(is_json_key_present(posted_data_json ,'address')):  
                address = posted_data_json['address']
                upateRecord.address = address
            if(is_json_key_present(posted_data_json ,'role')): 
                role = posted_data_json['role']
                upateRecord.role = role
            if(is_json_key_present(posted_data_json ,'email')): 
                email = posted_data_json['email']
                upateRecord.email = email
            if(is_json_key_present(posted_data_json ,'phone_number')): 
                phone_number = posted_data_json['phone_number']
                upateRecord.phone_number = phone_number

            ############################################################
            if(is_json_key_present(posted_data_json ,'phone_number')): 
                phone_number = posted_data_json['phone_number']
                upateRecord.phone_number = phone_number

            ############################################################

            db.session.commit()
            return jsonify({"status":"ok"})
        else:
            return jsonify({"status":"500"})

@api.route('/updateDisease', methods=['POST'])
def updateDisease():
    if request.method == 'POST':
        print("updateDisease() !!!")

        posted_data_json = request.get_json(force=True)
        print("posted_data_json : "  + str(posted_data_json))
        userid = posted_data_json["userid"]
        print("reqeust userid : "  + str(userid))


        user_check_count= db.session.query(func.count(UserDiseaseDb.user_id)).filter(and_( UserDiseaseDb.user_id == userid)).scalar()
        print("user_check_count : " + str(user_check_count))

        if(user_check_count > 0 ):
            upateQery = db.session.query(UserDiseaseDb)
            upateQery = upateQery.filter(UserDiseaseDb.user_id==userid)
            upateRecord = upateQery.one()

            if(is_json_key_present(posted_data_json ,'disease_none')):
                disease_none = posted_data_json['disease_none']
                upateRecord.disease_none = disease_none
            if(is_json_key_present(posted_data_json ,'disease_gohyulap')):   
                disease_gohyulap = posted_data_json['disease_gohyulap']
                upateRecord.disease_gohyulap = disease_gohyulap
            if(is_json_key_present(posted_data_json ,'disease_dangyo')):   
                disease_dangyo = posted_data_json['disease_dangyo']
                upateRecord.disease_dangyo = disease_dangyo
            if(is_json_key_present(posted_data_json ,'disease_gogiyung')):  
                disease_gogiyung = posted_data_json['disease_gogiyung']
                upateRecord.disease_gogiyung = disease_gogiyung
            if(is_json_key_present(posted_data_json ,'disease_simjang')):  
                disease_simjang = posted_data_json['disease_simjang']
                upateRecord.disease_simjang = disease_simjang
            if(is_json_key_present(posted_data_json ,'disease_gabsang')):  
                disease_gabsang = posted_data_json['disease_gabsang']
                upateRecord.disease_gabsang = disease_gabsang
            if(is_json_key_present(posted_data_json ,'disease_gan')):  
                disease_gan = posted_data_json['disease_gan']
                upateRecord.disease_gan = disease_gan
            if(is_json_key_present(posted_data_json ,'disease_pae')): 
                disease_pae = posted_data_json['disease_pae']
                upateRecord.disease_pae = disease_pae
            if(is_json_key_present(posted_data_json ,'disease_sin')): 
                disease_sin = posted_data_json['disease_sin']
                upateRecord.disease_sin = disease_sin
            if(is_json_key_present(posted_data_json ,'smoke')): 
                smoke = posted_data_json['smoke']
                upateRecord.smoke = smoke
            if(is_json_key_present(posted_data_json ,'chuckchu')): 
                chuckchu = posted_data_json['chuckchu']
                upateRecord.chuckchu = chuckchu
            if(is_json_key_present(posted_data_json ,'muroup')): 
                muroup = posted_data_json['muroup']
                upateRecord.muroup = muroup
            if(is_json_key_present(posted_data_json ,'family_disease_none')): 
                family_disease_none = posted_data_json['family_disease_none']
                upateRecord.family_disease_none = family_disease_none
            if(is_json_key_present(posted_data_json ,'family_disease_gohyulap')): 
                family_disease_gohyulap = posted_data_json['family_disease_gohyulap']
                upateRecord.family_disease_gohyulap = family_disease_gohyulap
            if(is_json_key_present(posted_data_json ,'family_disease_dangyo')): 
                family_disease_dangyo = posted_data_json['family_disease_dangyo']
                upateRecord.family_disease_dangyo = family_disease_dangyo
            if(is_json_key_present(posted_data_json ,'family_disease_gogiyung')): 
                family_disease_gogiyung = posted_data_json['family_disease_gogiyung']
                upateRecord.family_disease_gogiyung = family_disease_gogiyung
            if(is_json_key_present(posted_data_json ,'family_disease_simjang')): 
                family_disease_simjang = posted_data_json['family_disease_simjang']
                upateRecord.family_disease_simjang = family_disease_simjang
            if(is_json_key_present(posted_data_json ,'family_disease_gabsang')): 
                family_disease_gabsang = posted_data_json['family_disease_gabsang']
                upateRecord.smoke = family_disease_gabsang
            if(is_json_key_present(posted_data_json ,'family_disease_gan')): 
                family_disease_gan = posted_data_json['family_disease_gan']
                upateRecord.family_disease_gan = family_disease_gan
            if(is_json_key_present(posted_data_json ,'family_disease_pae')): 
                family_disease_pae = posted_data_json['family_disease_pae']
                upateRecord.family_disease_pae = family_disease_pae
            if(is_json_key_present(posted_data_json ,'family_disease_sin')): 
                family_disease_sin = posted_data_json['family_disease_sin']
                upateRecord.family_disease_sin = family_disease_sin

            db.session.commit()
            return jsonify({"status":"ok"})
        else:
            db.session.add(UserDiseaseDb(
                user_id = posted_data_json["userid"],
                disease_none = posted_data_json['disease_none'],
                disease_gohyulap = posted_data_json['disease_gohyulap'],
                disease_dangyo = posted_data_json['disease_dangyo'],
                disease_gogiyung = posted_data_json['disease_gogiyung'],
                disease_simjang = posted_data_json['disease_simjang'],
                disease_gabsang = posted_data_json['disease_gabsang'],
                disease_gan = posted_data_json['disease_gan'],
                disease_pae = posted_data_json['disease_pae'],
                disease_sin = posted_data_json['disease_sin'],
                smoke = posted_data_json['smoke'],
                chuckchu = posted_data_json['chuckchu'],
                muroup = posted_data_json['muroup'],
                family_disease_none = posted_data_json['family_disease_none'],
                family_disease_gohyulap = posted_data_json['family_disease_gohyulap'],
                family_disease_dangyo = posted_data_json['family_disease_dangyo'],
                family_disease_gogiyung = posted_data_json['family_disease_gogiyung'],
                family_disease_simjang = posted_data_json['family_disease_simjang'],
                family_disease_gabsang = posted_data_json['family_disease_gabsang'],
                family_disease_gan = posted_data_json['family_disease_gan'],
           
                family_disease_pae = posted_data_json['family_disease_pae'],
                family_disease_sin = posted_data_json['family_disease_sin']
                   
                ))
            db.session.commit()

        return jsonify({"status":"ok"})

@api.route('/set_user_info', methods=['POST'])
def set_user_info():
    if request.method == 'POST':
        print("set_user_info() !!!")

        posted_data = request.get_json(force=True)
        userinfo = posted_data['userinfo']
        print("userinfo : "  + str(userinfo))

        id = userinfo['id']
        print("id : "  + str(id))

        user_check_count= db.session.query(func.count(Fcuser.userid)).filter(and_( Fcuser.userid == id)).scalar()
        print("user_check_count : " + str(user_check_count))

        if(user_check_count > 0 ):
            upateQery = db.session.query(Fcuser)
            upateQery = upateQery.filter(Fcuser.userid==id)
            upateRecord = upateQery.one()

            if(is_json_key_present(userinfo ,'password')):
                password = userinfo['password']
                upateRecord.password = password
            if(is_json_key_present(userinfo ,'name')):   
                name = userinfo['name']
                upateRecord.username = name
            if(is_json_key_present(userinfo ,'height')):   
                height = userinfo['height']
                upateRecord.height = height
            if(is_json_key_present(userinfo ,'weight')):  
                weight = userinfo['weight']
                upateRecord.weight = weight
            if(is_json_key_present(userinfo ,'gender')):  
                gender = userinfo['gender']
                upateRecord.gender = gender
            if(is_json_key_present(userinfo ,'bod')):  
                bod = userinfo['bod']
                upateRecord.bod = bod
            if(is_json_key_present(userinfo ,'address')):  
                address = userinfo['address']
                upateRecord.address = address
            if(is_json_key_present(userinfo ,'role')): 
                role = userinfo['role']
                upateRecord.role = role
            if(is_json_key_present(userinfo ,'email')): 
                email = userinfo['email']
                upateRecord.email = email
            if(is_json_key_present(userinfo ,'phone_number')): 
                phone_number = userinfo['phone_number']
                upateRecord.phone_number = phone_number
                
            if(is_json_key_present(userinfo ,'ROM')): 
                ROM = userinfo['ROM']
                upateRecord.ROM = ROM
            if(is_json_key_present(userinfo ,'BMI')): 
                BMI = userinfo['BMI']
                upateRecord.BMI = BMI
            if(is_json_key_present(userinfo ,'BFR')): 
                BFR = userinfo['BFR']
                upateRecord.BFR = BFR
            if(is_json_key_present(userinfo ,'SFR')): 
                SFR = userinfo['SFR']
                upateRecord.ROM = SFR
            if(is_json_key_present(userinfo ,'UVI')): 
                UVI = userinfo['UVI']
                upateRecord.UVI = UVI
            if(is_json_key_present(userinfo ,'BMR')): 
                BMR = userinfo['BMR']
                upateRecord.BMR = BMR
            if(is_json_key_present(userinfo ,'VWC')): 
                VWC = userinfo['VWC']
                upateRecord.VWC = VWC
            if(is_json_key_present(userinfo ,'BODYAGE')): 
                BODYAGE = userinfo['BODYAGE']
                upateRecord.BODYAGE = BODYAGE
            if(is_json_key_present(userinfo ,'PP')): 
                PP = userinfo['PP']
                upateRecord.PP = PP
            if(is_json_key_present(userinfo ,'ADC')): 
                ADC = userinfo['ADC']
                upateRecord.ADC = ADC
                
            db.session.commit()
        else:
            id="";
            username="";
            password="";
            height="";
            weight="";
            gender="";
            bod="";
            address="";
            role="";
            email="";
            phone_number="";
            ROM="";  #근육량
            BMI="";  #체질량 지수
            BFR="";  #골형성률
            SFR="";  #피하지방률
            UVI="";  #내장 지방 지수
            BMR="";  #기초 대사율
            VWC="";  #수분량
            BODYAGE=""; #신체 나이
            PP=""; #단백질량
            ADC=""; #
                        
            if(is_json_key_present(userinfo ,'id')):
                id = userinfo['id']
            if(is_json_key_present(userinfo ,'name')):
                username = userinfo['name']
            if(is_json_key_present(userinfo ,'password')):  
                password = userinfo['password']
            if(is_json_key_present(userinfo ,'height')):   
                height = userinfo['height']
            if(is_json_key_present(userinfo ,'weight')):  
                weight = userinfo['weight']
            if(is_json_key_present(userinfo ,'gender')):  
                gender = userinfo['gender']
            if(is_json_key_present(userinfo ,'bod')):  
                bod = userinfo['bod']
            if(is_json_key_present(userinfo ,'address')): 
                address = userinfo['address']
            if(is_json_key_present(userinfo ,'role')): 
                role = userinfo['id']
            if(is_json_key_present(userinfo ,'email')): 
                email = userinfo['email']
            if(is_json_key_present(userinfo ,'phone_number')): 
                phone_number = userinfo['phone_number']
                
                
            if(is_json_key_present(userinfo ,'ROM')): 
                ROM = userinfo['ROM']
            if(is_json_key_present(userinfo ,'BMI')): 
                BMI = userinfo['BMI']
            if(is_json_key_present(userinfo ,'BFR')): 
                BFR = userinfo['BFR']
            if(is_json_key_present(userinfo ,'SFR')): 
                SFR = userinfo['SFR']
            if(is_json_key_present(userinfo ,'UVI')): 
                UVI = userinfo['UVI']
            if(is_json_key_present(userinfo ,'BMR')): 
                BMR = userinfo['BMR']
            if(is_json_key_present(userinfo ,'VWC')): 
                VWC = userinfo['VWC']
            if(is_json_key_present(userinfo ,'BODYAGE')): 
                BODYAGE = userinfo['BODYAGE']
            if(is_json_key_present(userinfo ,'PP')): 
                PP = userinfo['PP']
            if(is_json_key_present(userinfo ,'ADC')): 
                ADC = userinfo['ADC']
            
            db.session.add(Fcuser(
                        userid = id,
                        password = str(password),
                        username = str(username),
                        height = str(height),
                        weight = str(weight),
                        gender = str(gender),
                        bod = str(bod),
                        address = str(address),
                        role = str(role),
                        email = str(email),
                        phone_number = str(phone_number) ,
                        
                        ROM=str(ROM),  #근육량
                        BMI=str(BMI),  #체질량 지수
                        BFR=str(BFR),  #골형성률
                        SFR=str(SFR),  #피하지방률
                        UVI=str(UVI),  #내장 지방 지수
                        BMR=str(BMR),  #기초 대사율
                        VWC=str(VWC),  #수분량
                        BODYAGE=str(BODYAGE), #신체 나이
                        PP=str(PP), #단백질량
                        ADC=str(ADC) #
                        
                    ))
        
            db.session.commit()

        return jsonify({"status":"ok"})

    else:
        return jsonify({"status":"500"})
        ##return jsonify(), 201

def is_json_key_present(json, key):
    try:
        buf = json[key]
    except KeyError:
        return False

    return True

##디바스를 추가한다.
@api.route('/addDevice', methods=['POST'])
def addDevice():

    if request.method == 'POST':
        print("addDevice() !!!")

        posted_data = request.get_json(force=True)
        print("posted_data : "  + str(posted_data))

        device_id="";
        userid="";
        type="";
        device_name="";

        if(is_json_key_present(posted_data ,'device_id')):
            device_id = posted_data['device_id']
        else:
            return jsonify({"status":"fail","error":"no id "})

        if(is_json_key_present(posted_data ,'userid')):
            userid = posted_data['userid']
        else:
            return jsonify({"status":"fail","error":"no userid "})

        if(is_json_key_present(posted_data ,'type')):
            type = posted_data['type']
        else:
            return jsonify({"status":"fail","error":"no type "})

        if(is_json_key_present(posted_data ,'device_name')):
            device_name = posted_data['device_name']
        else:
            return jsonify({"status":"fail","error":"no device_name "})

        user_check_count= db.session.query(func.count(SensorDb.device_id)).filter(and_( SensorDb.device_id == device_id)).scalar()
        print("user_check_count : " + str(user_check_count))

        if(user_check_count > 0 ):
            return jsonify({"status":"500"})
        else:
            db.session.add(SensorDb(
                        device_id = str(device_id),
                        type = str(type),
                        device_name = str(device_name),
                        userid = str(userid)
                    ))
            db.session.commit()
        return jsonify({"status":"ok"})
    else:
        return jsonify({"status":"500"})
        ##return jsonify(), 201

##설문정보 및 운동데이터 저장
@api.route('/smarthealthband_practice', methods=['POST'])
def finish_practice():
    
    posted_data_json = request.get_json(force=True)
    print("reqeust posted_data str(posted_data_json) : "  + str(posted_data_json))
   
    #andori 에서 요청할경우
    ##posted_data_json = json.loads(str(posted_data_json))
    ##posted_data_json = json.loads(str(json.dumps(posted_data_json)))

    device_id = posted_data_json["device_id"]
    print("reqeust device_id : "  + str(device_id))

    device_name = posted_data_json["device_name"]
    print("reqeust device_name : "  + str(device_name))

    userid = posted_data_json["userid"]
    print("reqeust userid : "  + str(userid))

    type = posted_data_json['type']
    print("reqeust type : "  + str(type))

    level = posted_data_json['level']
    print("reqeust level : "  + str(level))
    
    keti_number = posted_data_json['keti_number']
    print("reqeust keti_number : "  + str(keti_number))
    
    
    if is_json_key_present(posted_data_json ,'contents'):
        contents = posted_data_json['contents']
        print("reqeust contents : "  + str(contents))
        #print("reqeust contents json.loads(contents) : "  + str(json.dumps(str(contents))))
    else:
        print("contents data is not valid !")
        return jsonify({"status":"fail" , "error":"contents data is not valid"})
    '''
    for content in posted_data_json['contents']:
        datatimes =  content['datatimes']
        eu_x =  content['eu_x']
        eu_y =  content['eu_y']
        eu_z =  content['eu_z']
        acc_x =  content['acc_x']
        acc_y =  content['acc_y']
        acc_z =  content['acc_z']
    '''
    '''
    for content_ in posted_data_json['contents']:
        sub_content_ =  content_['data']
        for data in sub_content_ :
            eu_x =  data['datatimes']
            practiceType =  data['practiceType']
            eu_y =  data['eu_y']
            eu_z =  data['eu_z']
            acc_x =  data['acc_x']
            acc_y =  data['acc_y']
            acc_z =  data['acc_z']
    '''
    try:
        device_count= db.session.query(func.count(SensorDb.device_id)).filter(and_( SensorDb.device_id == device_id)).scalar()
        print("device_count : " + str(device_count))

        if(device_count <= 0 ):
            db.session.add(SensorDb(\
                        device_id=str(device_id),\
                        device_name=str(device_name),\
                        userid=str(userid),
                        type=str(type),
                        keti_number=str(keti_number)
                         ))
            db.session.commit()
        

        temp_user = Fcuser.query.filter(and_(Fcuser.userid ==userid)).first()
        print(temp_user);
        if not ( temp_user is None) :
            print("user is exist !")
            ###############################################################
            ## insert
            db.session.add(HealthBandDataDb(\
                            userid=str(userid),\
                            survey_level=str(level),\
                            health_data=json.dumps(contents, ensure_ascii=False),
                            #health_data=json.loads(contents),
                            type=str(type),
                            send_yn="false",
                            device_id=str(device_id),
                            keti_number=str(keti_number)
                         ))
            db.session.commit()

            ## update recent datetime  
            upateQery = db.session.query(Fcuser)
            upateQery = upateQery.filter(Fcuser.userid==userid)
            upateRecord = upateQery.one()
            upateRecord.recent_date=""
            temp_now = datetime.now()
            today_date_time = temp_now.strftime("%Y-%m-%d")
            upateRecord.recent_date=str(today_date_time)
            db.session.commit()
            
            #temp_healthdata= db.session.query(HealthBandDataDb).order_by(desc(HealthBandDataDb.created_at)).first()
            #print("temp_healthdata : " + str(json.loads(temp_healthdata.health_data)))
            send_health_data()
        

            ###############################################################
            userinfo = {}
            userinfo.update({"status" : "ok"})
            return json.dumps(userinfo, ensure_ascii=False)
        
            
        else:
            print("user is not exist !")
            return jsonify({"status":"fail" , "error":"user is not exigst"})
            ##return jsonify(),204
    except Exception as ex:     
        print(ex)
        return jsonify({"status":"204"})
    

##체중계 데이터 저장
@api.route('/set_scale', methods=['POST'])
def set_scale():
    
    posted_data_json = request.get_json(force=True)
    print("reqeust posted_data str(posted_data_json) : "  + str(posted_data_json))
   
    #andori 에서 요청할경우
    ##posted_data_json = json.loads(str(posted_data_json))
    ##posted_data_json = json.loads(str(json.dumps(posted_data_json)))

    device_id = posted_data_json["device_id"]
    print("reqeust device_id : "  + str(device_id))

    device_name = posted_data_json["device_name"]
    print("reqeust device_name : "  + str(device_name))

    userid = posted_data_json["userid"]
    print("reqeust userid : "  + str(userid))

    type = posted_data_json['type']
    print("reqeust type : "  + str(type))
    
    keti_number = posted_data_json['keti_number']
    print("reqeust keti_number : "  + str(keti_number))
    
    
    if is_json_key_present(posted_data_json ,'contents'):
        contents = posted_data_json['contents']
        print("reqeust contents : "  + str(contents))
        #print("reqeust contents json.loads(contents) : "  + str(json.dumps(str(contents))))
    else:
        print("contents data is not valid !")
        return jsonify({"status":"fail" , "error":"contents data is not valid"})
  
    try:
        device_count= db.session.query(func.count(SensorDb.device_id)).filter(and_( SensorDb.device_id == device_id)).scalar()
        print("device_count : " + str(device_count))

        if(device_count <= 0 ):
            db.session.add(SensorDb(\
                        device_id=str(device_id),\
                        device_name=str(device_name),\
                        userid=str(userid),
                        type=str(type),
                        keti_number=str(keti_number)
                         ))
            db.session.commit()
        

        temp_user = Fcuser.query.filter(and_(Fcuser.userid ==userid)).first()
        print(temp_user);
        if not ( temp_user is None) :
            print("user is exist !")
            ###############################################################
            ## insert
            db.session.add(Scale(\
                            userid=str(userid),\
                            scale_data=json.dumps(contents, ensure_ascii=False),
                            type=str(type),
                            send_yn="false",
                            device_id=str(device_id),
                            keti_number=str(keti_number)
                         ))
            db.session.commit()

            ## update recent datetime  
            upateQery = db.session.query(Fcuser)
            upateQery = upateQery.filter(Fcuser.userid==userid)
            upateRecord = upateQery.one()
            upateRecord.recent_date=""
            temp_now = datetime.now()
            today_date_time = temp_now.strftime("%Y-%m-%d")
            upateRecord.recent_date=str(today_date_time)
            db.session.commit()
           
            send_scale_data()
        

            ###############################################################
            userinfo = {}
            userinfo.update({"status" : "ok"})
            return json.dumps(userinfo, ensure_ascii=False)
        
            
        else:
            print("user is not exist !")
            return jsonify({"status":"fail" , "error":"user is not exigst"})
            ##return jsonify(),204
    except Exception as ex:     
        print(ex)
        return jsonify({"status":"204"})
  
  
import time
def send_health_data():
    API_HOST = "http://www.keticloud.re.kr"
    url_dumbbell_raw = API_HOST + "/dumbbell_raw/push"
    headers = {'Content-Type':'application/json','charset':'UTF-8','Accept':'*/*'}
    
    data_= db.session.query(HealthBandDataDb).filter(and_(HealthBandDataDb.send_yn=="false")).all()
    db.session.close()
    if( (not data_ is None) ):
        for health_data in data_:
            temp_id = health_data.id
            #body_ = str(health_data.health_data).replace("\\", "")
            body_ = json.loads(health_data.health_data)
            print("body_ : " + str(body_))
            '''
            body_ = {
                    "farmID":farm_id,
                    "greenHouseID": house_id,
                    "targetDate": str(targetDate)
                    }
            '''
            try:

                response = requests.post(url_dumbbell_raw,headers=headers , data=json.dumps(body_,ensure_ascii=False,indent="\t"), timeout=3)
                time.sleep(6)
                
                ##print(type(response.text))
                ##print("response.text : " + str(response.text))
                if( not ( response.text is None ) ):

                    response_message= json.loads(response.text)
                    print("response_message : "  + str(response_message))
                    if ( "success" in response_message ) :
                        db.session.add(HealthBandDataDb(
                                    id = temp_id,
                                    send_yn = "true"
                                    ))
                        
                    db.session.commit()
                    db.session.close()

            except Exception as ex:
                print("error : " + str(ex))
                

def send_scale_data():
    API_HOST = "http://www.keticloud.re.kr"
    url_scale_raw = API_HOST + "/aifit/push"
    headers = {'Content-Type':'application/json','charset':'UTF-8','Accept':'*/*'}
    
    data_= db.session.query(Scale).filter(and_(Scale.send_yn=="false")).all()
    db.session.close()
    if( (not data_ is None) ):
        for health_data in data_:
            temp_id = health_data.id
            #body_ = str(health_data.health_data).replace("\\", "")
            body_ = json.loads(health_data.scale_data)
            print("body_ : " + str(body_))
            '''
            body_ = {
                    "farmID":farm_id,
                    "greenHouseID": house_id,
                    "targetDate": str(targetDate)
                    }
            '''
            try:

                response = requests.post(url_scale_raw,headers=headers , data=json.dumps(body_,ensure_ascii=False,indent="\t"), timeout=3)
                time.sleep(6)
                
                ##print(type(response.text))
                ##print("response.text : " + str(response.text))
                if( not ( response.text is None ) ):

                    response_message= json.loads(response.text)
                    print("response_message : "  + str(response_message))
                    if ( "success" in response_message ) :
                        db.session.add(Scale(
                                    id = temp_id,
                                    send_yn = "true"
                                    ))
                        
                    db.session.commit()
                    db.session.close()

            except Exception as ex:
                print("error : " + str(ex))